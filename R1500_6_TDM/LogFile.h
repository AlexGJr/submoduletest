#ifndef LogFileH
#define LogFileH

#define MaxLogData 20

#define lcClear              1
#define lcClearAll           2
#define lcGammaAlarmEvent    3
#define lcGammaAlarmEventAdd 4
#define lcPowerOn            5
#define lcFlashWriteFail     6
#define lcFlashReadFail      7
#define lcPhaseShiftSet1     8
#define lcPhaseShiftSet2     9
#define lcChannels          10
#define lcFreq              11
#define lcLowSignal         12
#define lcHiSignal          13
#define lcUnitOff           14
#define lcTrendAlarmEvent   15
#define lcTKAlarmEvent      16
#define lcSet1Off           17
#define lcSet2Off           18
#define lcBalance           19
#define lcGammaWarning      20
#define lcLogCleared        21
#define lcPhaseShiftSetSpot1  	25		// acm, v2.00, help isolate transgrid error, differentiate that erPhaseShiftSetx happened line 823 (not 827/1164/1187)
#define lcPhaseShiftSetSpot2  	26		// acm, v2.00, help isolate transgrid error, differentiate that erPhaseShiftSetx happened line 827 (not 823/1164/1187)
#define lcPhaseShiftSetSpot3  	27		// acm, v2.00, help isolate transgrid error, differentiate that erPhaseShiftSetx happened line 1164 (not 823/827/1187)
#define lcPhaseShiftSetSpot4  	28		// acm, v2.00, help isolate transgrid error, differentiate that erPhaseShiftSetx happened line 1187 (not 823/827/1164/1187)
#pragma pack(1)

// acm, R505 reads out log data in this order:
// LOG NUMBER Date Time LogCode Alarmstatus1 Gamma1 GammaPhase1 Temperature1 Ktemp1 KtempPhase1 Trend1
// Alarmstatus2 Gamma2 GammaPhase2 Temperature2 Ktemp2 KtempPhase2 Trend2;

struct stLogData {
       //Date of measurement
       char Day;
       char Month;
       char Year;  //-2000
       //Time of measurement
       char Hour;
       char Min;

       unsigned int LogCode;
//     Gamma - G (0 - 12.75% with step 0.05)
       unsigned short Gamma[MaxTransformerSide];
//Phase Angle
       unsigned short GammaPhase[MaxTransformerSide];
//Temperature - T (-70 - +185 0C with step 1 0C)
       char Temperature[MaxTransformerSide];
//Trend - TR (0 - 51 %/Year with step 0.2%/Year) //acm, 4-27 change from .2 to 1
       char Trend[MaxTransformerSide];
//Temp coeff. - TK (0 - 0.51%/0C with step 0.002)
       char KT[MaxTransformerSide];
//Temp coeff. Phase
       char KTPhase[MaxTransformerSide];

       //Defect Code
       char DefectCode[MaxTransformerSide];  // acm, v1.79   note, legacy method stores alarm status stored, not newly defined phaseB/C swap & hi balance encountered.

       unsigned short LogID;

       //Gamma Alarm level
       char GammaRedThreshold[MaxTransformerSide];

       unsigned long Error;

       char reserved[31];
};
#define szLogData sizeof(struct stLogData)
//60 bytes

#pragma pack()

extern struct stLogData LogData;
extern unsigned int LastLogID;

void LoadLog(char Num);
void InitLog(void);
void SaveLog(unsigned int LogCode);
char LogNumToIndex(char Num);
void SaveLogIfNewError(unsigned long OldError, unsigned long NewError);
void SaveLogIfNewErrorAndSkipLast(unsigned long OldError, unsigned long NewError);
void SaveCurentLog(char Num);
#endif
