#ifndef Utils_h
#define Utils_h

#include "Defs.h"

long Round(float x);
void zerointstr(char *s1);
unsigned int DivAndRound(float f1,float f2,unsigned int MaxValue);
unsigned int YesNoMessage(char ROM *Str,char Font);
void SetEEPROMData(unsigned int Addr,char Data);
char PLMVersion(void);
char BoardType(void);
void PhaseRound(float *Phase);

#endif

