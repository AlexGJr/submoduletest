#ifndef _scrlmngr_h
#define _scrlmngr_h

#ifndef __arm9
#define MaxScrlStrs 7
#else
#define MaxScrlStrs 55
#endif

#include "Keyboard.h"


struct ScrollerMngr{
           unsigned short X,Y;
           char Font;
           signed char CurStr,NumberStr;
           KEY LeftKey;
           KEY RightKey;
           KEY ActionKey1;
           KEY ActionKey2;
           char ROM *String[MaxScrlStrs];
           char ROM *LongString[MaxScrlStrs];
           char ShortDraw;
};

void InitScroller(struct ScrollerMngr *Scrl,unsigned int X,unsigned int Y,char Font);
char AddScrollerString(struct ScrollerMngr *Scrl,char ROM *ScrlString);
char AddScrollerStringX(struct ScrollerMngr *Scrl,char ROM *ScrlString,char ROM *LongScrlString);
void RedrawScrl(struct ScrollerMngr *Scrl);
unsigned int DoScrlNoWait(struct ScrollerMngr *Scrl, KEY ch);
unsigned int DoScrl(struct ScrollerMngr *Scrl);
char GetCurScrl(struct ScrollerMngr *Scrl);
char SetCurScrl(struct ScrollerMngr *Scrl,char N);

#endif
