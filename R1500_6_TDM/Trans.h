//---------------------------------------------------------------------------
#ifndef TransH
#define TransH
//---------------------------------------------------------------------------

#define BUFLEN unsigned int
#define BufLen (BUFLEN)180
#define RESULT int

//typedef char BUF[BufLen];

// Input and Output buffers
extern char *OutBuf;
extern char*  InBuf;

extern BUFLEN CurOutSymbol;

signed char Translate(void);

#endif

