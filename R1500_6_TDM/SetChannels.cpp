#include "SetChannels.h"
#include "Defs.h"
#include "BoardAdd.h"
#include "SysUtil.h"
#include "ADCDevice.h"

extern char swap_phaseBC[]; 	// acm, v1.79 FW, set in beginning of Main()
extern char setx_state;			// acm, swap_state defined Main(), drivenin SetInputChannel(), 0==don't swap, 1==set1 selected, 2=set 2 selected

#ifdef __emulator
#include "RAM.h"
#include "GammaMeasurement.h"
#include <math.h>
#include <stdlib.h>
#endif

#define DelayValue 160000
//������� ��������� Coeff
char Gain=0;

#ifdef __emulator
char RefAddr;
char MuxAddr;
char BalanceSwitchAddr;
char GainAddr;
char ExtBushing;
#endif

void InitInputChannels(void)
{
RefAddr(0);
MuxAddr(0);
BalanceSwitchAddr(0);
GainAddr(0);
ExtBushing(0);
}


#ifdef __emulator
long round(double f)
{
  long l;

  l=f;
  if (f>=0) {
     if ((f-l)>=0.5) l++;
  } else {
     if ((f-l)<=0.5) l--;
  }
  return l;
}
#endif

void SetInputChannel(char TrSide, char ChannelType)
{
#ifdef __emulator
 unsigned int n;
 double f,Phase;

#define Ampl0 14000.0
//#define Ampl1 14050.0
//#define Ampl2 14550.0
//#define NullLineDef 4096
#define NullLineDef 16384

AddrSig[0]=(short*)BaseAddrSig1[TrSide];
AddrSig[1]=(short*)BaseAddrSig2[TrSide];
AddrSig[2]=(short*)BaseAddrSig3[TrSide];
AddrSig[3]=(short*)BaseAddrSig4[TrSide];

#ifdef Read8kHz
  #define SinPoints 160.0
//  #define SinPoints 80.0
#else
  #define SinPoints 120.0

//  #define SinPoints 100.0
#endif

randomize();
Phase=((random(360)%360)%3*3)/360.0*PiValue2;
//Phase=270.0/360.0*PiValue2;
Phase=0;

#endif

//#define RefAddr            (*(volatile unsigned char *)0x2100)
//#define MuxAddr            (*(volatile unsigned char *)0x5000)
//#define BalanceSwitchAddr  (*(volatile unsigned char *)0x2200)

//acm, fwv1.79, autophase rotation.  For next A/D conversion, tells IRQ service routine set 1 or 2, to decide if phase A/B need to be swapped
//              do this here applies to any states.
setx_state=TrSide;  
if(swap_phaseBC[setx_state])	// to swap PhaseBC, in addtion to sampling ADC signal swap, need to swap BalanceSwitchAddr mux selects out...	
{								// most efficient coding is to do this at this point, rather than higher level.
	if(ChannelType==chPhaseB) ChannelType=chPhaseC;
	else
	if(ChannelType==chPhaseC) ChannelType=chPhaseB;	 
}

switch (ChannelType) {
       case chGamma:{

                    BalanceSwitchAddr(0);
                    if (TrSide) {MuxAddr(0x17);} else MuxAddr(0);
                    RefAddr(0);

#ifdef __emulator
                    AmplifIndex[TrSide][BushingCh]=0;
                    if (!TrSide) AmplifIndex[TrSide][GammaCh]=2;
                    else AmplifIndex[TrSide][GammaCh]=3;

                    for (n=0;n<ReadCount;n++) {
                         f=Ampl0*sin(PiValue2*n/SinPoints + Phase)+NullLineDef;
//                         f=2047.5*sin(PiValue2*n/480 )+2047.5;
                         *((s16 *)AddrSig[0]+n)=round(f);
                         f=Ampl0*sin(PiValue2*n/SinPoints + PiValue2/3 +Phase)+NullLineDef;
                         *((s16 *)AddrSig[1]+n)=round(f);
                         f=Ampl0*sin(PiValue2*n/SinPoints + PiValue2/3*2 +Phase)+NullLineDef;
                         *((s16 *)AddrSig[2]+n)=round(f);
                         if (!TrSide)
                            f=420*sin(PiValue2*n/SinPoints - PiValue2/3*2 +Phase)+NullLineDef;
                         else
                            f=420*sin(PiValue2*n/SinPoints - PiValue2/3*2 +Phase)+NullLineDef;
                         *((s16 *)AddrSig[3]+n)=round(f);
                    }


#endif
                    break;
                    }
       case chPhaseA:{
                    if (TrSide) {BalanceSwitchAddr(0x60);}
                    else  BalanceSwitchAddr(0x06);
                    if (TrSide) {MuxAddr(0x17);}  else {MuxAddr(0);}
                    RefAddr(0); // acm, select chA/B/C input
#ifdef __emulator
                    AmplifIndex[TrSide][BushingCh]=0;
                    AmplifIndex[TrSide][GammaCh]=0;

                    for (n=0;n<ReadCount;n++) {
                         f=Ampl0*sin(PiValue2*n/SinPoints + 0 +Phase)+NullLineDef;
                         *((s16 *)AddrSig[0]+n)=round(f);
//                         if (f-*((int *)AddrSig[0]+n)>=0.5) *((int *)AddrSig[0]+n)+=1;
                    }

                    for (n=0;n<ReadCount;n++) {
                         f=Ampl0*sin(PiValue2*n/SinPoints - (1.5*PiValue2/360.0) +Phase )+NullLineDef;
                         *((s16 *)AddrSig[3]+n)=round(f);
//                         if (f-*((int *)AddrSig[3]+n)>=0.5) *((int *)AddrSig[3]+n)+=1;
                    }

#endif
                    break;
                    }
       case chPhaseB:{
                    if (TrSide) {BalanceSwitchAddr(0x50);}
                    else BalanceSwitchAddr(0x05);
                    if (TrSide) {MuxAddr(0x17);}  else MuxAddr(0);
                    RefAddr(0);// acm, select chA/B/C input
#ifdef __emulator
                    AmplifIndex[TrSide][BushingCh]=0;
                    AmplifIndex[TrSide][GammaCh]=0;

                    for (n=0;n<ReadCount;n++) {
                         f=Ampl0*sin(PiValue2*n/SinPoints + 0 +Phase)+NullLineDef;
                         *((s16 *)AddrSig[0]+n)=round(f);
//                         if (f-*((int *)AddrSig[0]+n)>=0.5) *((int *)AddrSig[0]+n)+=1;
                    }

                    for (n=0;n<ReadCount;n++) {
                         f=Ampl0*sin(PiValue2*n/SinPoints - PiValue2/3 +Phase)
//                           +20*sin(PiValue2*n/8)
//                           +20*sin(PiValue2*n/2)
                           +NullLineDef;
                         *((s16 *)AddrSig[3]+n)=round(f);
//                         if (f-*((int *)AddrSig[3]+n)>=0.5) *((int *)AddrSig[3]+n)+=1;
                    }

#endif
                    break;
                    }

       case chPhaseC:{
                    if (TrSide) {BalanceSwitchAddr(0x30);}
                    else BalanceSwitchAddr(0x03);
                    if (TrSide) {MuxAddr(0x17);}  else MuxAddr(0);
                    RefAddr(0);// acm, select chA/B/C input
#ifdef __emulator
                    AmplifIndex[TrSide][BushingCh]=0;
                    AmplifIndex[TrSide][GammaCh]=0;

                    for (n=0;n<ReadCount;n++) {
                         f=Ampl0*sin(PiValue2*n/SinPoints + 0 +Phase)+NullLineDef;
                         *((s16 *)AddrSig[0]+n)=round(f);
//                         if (f-*((int *)AddrSig[0]+n)>=0.5) *((int *)AddrSig[0]+n)+=1;
                    }

                    for (n=0;n<ReadCount;n++) {
                         f=Ampl0*sin(PiValue2*n/SinPoints - PiValue2/3*2 +Phase)+NullLineDef;
                         *((s16 *)AddrSig[3]+n)=round(f);
//                         if (f-*((int *)AddrSig[3]+n)>=0.5) *((int *)AddrSig[3]+n)+=1;
                    }

#endif
                    break;
                    }

       case chSourcePhaseA:{
/*
                    BalanceSwitchAddr=0;
                    if (TrSide) MuxAddr(0x0F;  else MuxAddr=0;
                    if (TrSide) RefAddr(0x30;  else RefAddr(0x06;
*/
                    switch (TrSide) {
                      case 0: ;
                      case 1: ExtBushing(0); break;
                      case 2: ExtBushing(0x07); break;
                      case 3: ExtBushing(0x0B); break;
                    }
//                    if (TrSide) BalanceSwitchAddr(0x60; else BalanceSwitchAddr(0x06;
                    if (TrSide) {BalanceSwitchAddr(0x70);} else BalanceSwitchAddr(0x07);
                    if (TrSide) {MuxAddr(0x17);}  else MuxAddr(0);
                    if (TrSide) {RefAddr(0x60);}  else RefAddr(0x06);
//                    RefAddr=0;
#ifdef __emulator
                    AmplifIndex[TrSide][BushingCh]=0;
                    AmplifIndex[TrSide][GammaCh]=0;

                    for (n=0;n<ReadCount;n++) {
                         f=Ampl0*sin(PiValue2*n/SinPoints + 0 +Phase)+NullLineDef;
                         *((s16 *)AddrSig[0]+n)=round(f);
//                         if (f-*((int *)AddrSig[0]+n)>=0.5) *((int *)AddrSig[0]+n)+=1;
//                         f=Ampl0*sin(PiValue2*n/SinPoints - PiValue2/360 +Phase)+NullLineDef;
                         *((s16 *)AddrSig[1]+n)=round(f);
//                         f=Ampl0*sin(PiValue2*n/SinPoints - (PiValue2/360*2 +Phase))+NullLineDef;
                         *((s16 *)AddrSig[2]+n)=round(f);
//                         f=Ampl0*sin(PiValue2*n/SinPoints - (PiValue2/360*3 +Phase))+NullLineDef;
                         *((s16 *)AddrSig[3]+n)=round(f);
                    }

#endif
					break;
                    }
       case chSourcePhases: {

                    switch (TrSide) {
                      case 0: ;
                      case 1: ExtBushing(0); break;
                      case 2: ExtBushing(0x07); break;
                      case 3: ExtBushing(0x0B); break;
                    }

                    if (TrSide) {BalanceSwitchAddr(0x70);} else BalanceSwitchAddr(0x07);
                    if (TrSide) {MuxAddr(0x17);}  else MuxAddr(0);
                    RefAddr(0);
#ifdef __emulator
                    AmplifIndex[TrSide][BushingCh]=0;
                    AmplifIndex[TrSide][GammaCh]=0;

                    for (n=0;n<ReadCount;n++) {
                         f=Ampl0*sin(PiValue2*n/SinPoints + 0 +Phase)+
//                           5.0 * sin(PiValue2*n/12.0 + 1)+
                            NullLineDef;
                         *((s16 *)AddrSig[0]+n)=round(f);
//                         if (f-*((int *)AddrSig[0]+n)>=0.5) *((int *)AddrSig[0]+n)+=1;


                         f=Ampl0*sin(PiValue2*n/SinPoints +Phase - PiValue2/3 - 1.5*PiValue2/360)+
//                           5.0 * sin(PiValue2*n/12.0 + 2)+
                         NullLineDef;
                         *((s16 *)AddrSig[1]+n)=round(f);
//                         if (f-*((int *)AddrSig[1]+n)>=0.5) *((int *)AddrSig[1]+n)+=1;

                         f=Ampl0*sin(PiValue2*n/SinPoints +Phase - PiValue2/3*2 - 1*PiValue2/360)+
//                           5.0 * sin(PiValue2*n/12.0 + 3)+
                           NullLineDef;
                         *((s16 *)AddrSig[2]+n)=round(f);
//                         if (f-*((int *)AddrSig[2]+n)>=0.5) *((int *)AddrSig[2]+n)+=1;

                    }

#endif
					break;
                    }
       case chAaCrossPhases: {
                    BalanceSwitchAddr(0x77);
                    switch (TrSide) {
                      case 0: ;
                      case 1: ExtBushing(0); break;
                      case 2: ExtBushing(0x07); break;
                      case 3: ExtBushing(0x0B); break;
                    }
                    MuxAddr(0x40);
                    RefAddr(0);


#ifdef __emulator
                    AmplifIndex[TrSide][BushingCh]=0;
                    AmplifIndex[TrSide][GammaCh]=0;

                    for (n=0;n<ReadCount;n++) {
                         f=Ampl0*sin(PiValue2*n/SinPoints + 0 +Phase)+NullLineDef;
                         *((s16 *)AddrSig[0]+n)=round(f);
                         f=Ampl0*sin(PiValue2*n/SinPoints  - PiValue2/360*3 +Phase)+NullLineDef;
                         *((s16 *)AddrSig[3]+n)=round(f);
                         //f=Ampl0*sin(PiValue2*n/SinPoints - PiValue2/3 - PiValue2/72)+NullLineDef;
                         f=Ampl0*sin(PiValue2*n/SinPoints  - PiValue2/360.0*195.57 +Phase)+NullLineDef;
                         *((s16 *)AddrSig[1]+n)=round(f);
                         f=Ampl0*sin(PiValue2*n/SinPoints  - PiValue2/3*2 +Phase)+NullLineDef;
                         *((s16 *)AddrSig[2]+n)=round(f);
                    }

#endif
                    break;
                    }
       case chBbCrossPhases: {

                    BalanceSwitchAddr(0x77);
                    switch (TrSide) {
                      case 0: ;
                      case 1: ExtBushing(0); break;
                      case 2: ExtBushing(0x07); break;
                      case 3: ExtBushing(0x0B0); break;
                    }
                    MuxAddr(0x50);
                    RefAddr(0);


#ifdef __emulator
                    AmplifIndex[TrSide][BushingCh]=0;
                    AmplifIndex[TrSide][GammaCh]=0;

                    for (n=0;n<ReadCount;n++) {
                         f=Ampl0*sin(PiValue2*n/SinPoints  - PiValue2/3 - PiValue2/360.0*195.57 +Phase)+NullLineDef;
                         *((s16 *)AddrSig[0]+n)=round(f);
                         f=Ampl0*sin(PiValue2*n/SinPoints  - PiValue2/3 - PiValue2/360*3 +Phase)+NullLineDef;
                         *((s16 *)AddrSig[3]+n)=round(f);
                         f=Ampl0*sin(PiValue2*n/SinPoints  - PiValue2/3 +Phase)+NullLineDef;
                         *((s16 *)AddrSig[1]+n)=round(f);
                         f=Ampl0*sin(PiValue2*n/SinPoints  - PiValue2/3*2 +Phase)+NullLineDef;
                         *((s16 *)AddrSig[2]+n)=round(f);
                    }

#endif
                    break;
                    }
       case chCcCrossPhases: {

                    BalanceSwitchAddr(0x77);
                    switch (TrSide) {
                      case 0: ;
                      case 1: ExtBushing(0); break;
                      case 2: ExtBushing(0x07); break;
                      case 3: ExtBushing(0x0B0); break;
                    }
                    MuxAddr(0x60); //C2 �� 4 �����
                    RefAddr(0);


#ifdef __emulator
                    AmplifIndex[TrSide][BushingCh]=0;
                    AmplifIndex[TrSide][GammaCh]=0;

                    for (n=0;n<ReadCount;n++) {
                         f=Ampl0*sin(PiValue2*n/SinPoints  - PiValue2/3*2 - PiValue2/360.0*195.57 +Phase)+NullLineDef;
                         *((s16 *)AddrSig[0]+n)=round(f);
                         f=Ampl0*sin(PiValue2*n/SinPoints  - PiValue2/3*2 - PiValue2/360*3 +Phase)+NullLineDef;
                         *((s16 *)AddrSig[3]+n)=round(f);
                         f=Ampl0*sin(PiValue2*n/SinPoints  - PiValue2/3 +Phase)+NullLineDef;
                         *((s16 *)AddrSig[1]+n)=round(f);
                         f=Ampl0*sin(PiValue2*n/SinPoints  - PiValue2/3*2 +Phase)+NullLineDef;
                         *((s16 *)AddrSig[2]+n)=round(f);
                    }

#endif
                    break;
                    }
       case chSourcePhasesCalibr:
       case chSourcePhaseACalibr:{

                    switch (TrSide) {
                      case 0: ;
                      case 1: ExtBushing(0); break;
                      case 2: ExtBushing(0x07); break;
                      case 3: ExtBushing(0x0B); break;
                    }
                    //���� �1 ����� 4 �����
                    MuxAddr(0x40);
                    //����� - ��� �� �����
                    BalanceSwitchAddr(0x77);
                    //�� ��� ������ �������� ������ � ����
                    RefAddr(0x77);
//                    RefAddr(0x37);
                    
//                    RefAddr(0x17);
//                    RefAddr(0x16);

#ifdef __emulator
                    AmplifIndex[TrSide][BushingCh]=0;
                    AmplifIndex[TrSide][GammaCh]=0;

                    for (n=0;n<ReadCount;n++) {
                         f=Ampl0*sin(PiValue2*n/SinPoints+Phase)+NullLineDef;
                         *((s16 *)AddrSig[0]+n)=round(f);
                         f=Ampl0*sin(PiValue2*n/SinPoints - PiValue2/360.0*3 +Phase)+NullLineDef;
                         *((s16 *)AddrSig[1]+n)=round(f);
                         f=Ampl0*sin(PiValue2*n/SinPoints - PiValue2/360.0*6 +Phase)+NullLineDef;
                         *((s16 *)AddrSig[2]+n)=round(f);
                         f=Ampl0*sin(PiValue2*n/SinPoints + PiValue2/360.0*111.0+Phase)+NullLineDef;
//                         f=511.5*sin(PiValue2*n/100.0)+511.5;
                         *((s16 *)AddrSig[3]+n)=round(f);

//                         *((int *)AddrSig[3]+n)=(n%120)<<4+0x4000;

/*
                         if (f-NullLineDef>=0) {
                            *((int *)AddrSig[3]+n)=NullLineDef+1500;
                         } else {
                            *((int *)AddrSig[3]+n)=NullLineDef-1500;
                         }
*/
                    }

#endif
                    break;
                    }
      case chGammaCalibr:{

                    switch (TrSide) {
                      case 0: ;
                      case 1: ExtBushing(0); break;
                      case 2: ExtBushing(0x07); break;
                      case 3: ExtBushing(0x0B); break;
                    }
                    BalanceSwitchAddr(0x06);
                    RefAddr(0x77);

#ifdef __emulator
                    AmplifIndex[TrSide][BushingCh]=0;
                    AmplifIndex[TrSide][GammaCh]=0;

                    for (n=0;n<ReadCount;n++) {
                         f=Ampl0*sin(PiValue2*n/SinPoints+Phase)+NullLineDef;
                         *((s16 *)AddrSig[0]+n)=round(f);
                         f=Ampl0*sin(PiValue2*n/SinPoints - PiValue2/360.0*3 +Phase)+NullLineDef;
                         *((s16 *)AddrSig[1]+n)=round(f);
                         f=Ampl0*sin(PiValue2*n/SinPoints - PiValue2/360.0*6 +Phase)+NullLineDef;
                         *((s16 *)AddrSig[2]+n)=round(f);
                         f=Ampl0*sin(PiValue2*n/SinPoints + PiValue2/360.0*111.0+Phase)+NullLineDef;
//                         f=511.5*sin(PiValue2*n/100.0)+511.5;
                         *((s16 *)AddrSig[3]+n)=round(f);

//                         *((int *)AddrSig[3]+n)=(n%120)<<4+0x4000;

/*
                         if (f-NullLineDef>=0) {
                            *((int *)AddrSig[3]+n)=NullLineDef+1500;
                         } else {
                            *((int *)AddrSig[3]+n)=NullLineDef-1500;
                         }
*/
                    }

#endif
                    break;
                    }
}
#ifndef __emulator
//Delay(100);
Delay(150);
#endif
}

void SetGain(char Channel, char AmplifIndex)
{
if ((Channel==1)||(Channel==2)||(Channel==3)) {	// acm, amps 1 to 3, phA/B/C amps, 4 gains using bits 0, 1.  GainValue[]={1.0,2.0,4.0,8.0}
  if (AmplifIndex==3) {
     Gain|= (1<<0);
     Gain|= (1<<1);
  } else {
     if (AmplifIndex==2) {
        Gain&=~(1<<0);
        Gain|= (1<<1);
     } else
        if (AmplifIndex==1) {
           Gain|= (1<<0);
           Gain&=~(1<<1);
        } else {
           Gain&=~(1<<0);
           Gain&=~(1<<1);
        }
  }
}else {											// acm, (0 OR 4)==gamma amp, 6 gains using bits 5,6,7 GammaGainValue[]={1.0,4.0048,16.0741,64.45698,2.0024,8.0096}
  if (AmplifIndex==5) {
     Gain|= (1<<5);
     Gain|= (1<<6);
     Gain&=~(1<<7);
  } else {
     if (AmplifIndex==4) {
        Gain|= (1<<5);
        Gain&=~(1<<6);
        Gain&=~(1<<7);
     } else {
        if (AmplifIndex==3) {
           Gain&=~(1<<5);
           Gain|= (1<<6);
           Gain|= (1<<7);
        } else {
           if (AmplifIndex==2) {
              Gain&=~(1<<5);
              Gain&=~(1<<6);
              Gain|= (1<<7);
           } else {
              if (AmplifIndex==1) {
                 Gain&=~(1<<5);
                 Gain|= (1<<6);
                 Gain&=~(1<<7);
              } else {
                 Gain&=~(1<<5);
                 Gain&=~(1<<6);
                 Gain&=~(1<<7);
              }
           }
        }
     }
  }
}

/*
#ifndef OldR1500_6

if (Channel==2) {
  if (AmplifIndex==3) {
     Gain|= (1<<0);
     Gain|= (1<<1);
  } else {
     if (AmplifIndex==2) {
        Gain&=~(1<<0);
        Gain|= (1<<1);
     } else
        if (AmplifIndex==1) {
           Gain|= (1<<0);
           Gain&=~(1<<1);
        } else {
           Gain&=~(1<<0);
           Gain&=~(1<<1);
        }
  }
}else
if (Channel==3) {
  if (AmplifIndex==3) {
     Gain|= (1<<0);
     Gain|= (1<<1);
  } else {
     if (AmplifIndex==2) {
        Gain&=~(1<<0);
        Gain|= (1<<1);
     } else
        if (AmplifIndex==1) {
           Gain|= (1<<0);
           Gain&=~(1<<1);
        } else {
           Gain&=~(1<<0);
           Gain&=~(1<<1);
        }
  }
}else

#else

if (Channel==2) {
  if (AmplifIndex==3) {
     Gain|= (1<<2);
     Gain|= (1<<3);
  } else {
     if (AmplifIndex==2) {
        Gain&=~(1<<2);
        Gain|= (1<<3);
     } else
        if (AmplifIndex==1) {
           Gain|= (1<<2);
           Gain&=~(1<<3);
        } else {
           Gain&=~(1<<2);
           Gain&=~(1<<3);
        }
  }

}else
if (Channel==3) {
  if (AmplifIndex==3) {
     Gain|= (1<<4);
     Gain|= (1<<5);
  } else {
     if (AmplifIndex==2) {
        Gain&=~(1<<4);
        Gain|= (1<<5);
     } else
        if (AmplifIndex==1) {
           Gain|= (1<<4);
           Gain&=~(1<<5);
        } else {
           Gain&=~(1<<4);
           Gain&=~(1<<5);
        }
  }
}else

#endif

if (Channel==4) {
  if (AmplifIndex==3) {
     Gain|= (1<<6);
     Gain|= (1<<7);
  } else {
     if (AmplifIndex==2) {
        Gain&=~(1<<6);
        Gain|= (1<<7);
     } else
        if (AmplifIndex==1) {
           Gain|= (1<<6);
           Gain&=~(1<<7);
        } else {
           Gain&=~(1<<6);
           Gain&=~(1<<7);
        }
  }
}
}
*/
CurShift=AmplifIndex;
GainAddr(Gain);

#ifndef __emulator
Delay(100);
#endif

}
