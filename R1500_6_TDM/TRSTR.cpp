
#include "Defs.h"

//---------------------------------------------------------------------------
ROM char trVersionValue[]="2.11"; // 2.06 de bumped the version to fix BHM2.05
ROM char trVersionIntValue = 211;
ROM char trDeviceType[]="R1500/6";
// acm, 11-18-10 add FW build date:  {seconds, minutes, hour, day, month, year}
// not critical this be constantly updated, point now is to define reasonable date
// nobody should ever want to set RTC to.
//ROM char fw_build_date[] ={0,0,12,2,17,11};
ROM char fw_build_date[11] =__DATE__ ;  // acm, v2.01, mmm dd yyyy, add build date to eliminate confusion what build is in system, carry over of idea started with BHM (which used a fixed date)
ROM char trEmptyStr[]=" ";
ROM char  trError[]="Error: ";
ROM char  trInvalidCommand[]="Invalid command: ";
ROM char  trInvalidParameter[]="Invalid parameter: ";
ROM char  trInvalidIntegerValue[]="Invalid integer value: ";
ROM char  trInvalidFloatValue[]="Invalid float value: ";
ROM char  trInvalidDate[]="Invalid date: ";
ROM char  trInvalidTime[]="Invalid time: ";
ROM char  trCPExpected[]="';' expected: ";
ROM char  trCP[]=";";
ROM char  trQuestion[]="?";

ROM char  trCommandDEVICETYPE[]="DEVICETYPE";
ROM char  trCommandVER[]="VER";
ROM char  trCommandTIME[]="TIME";
ROM char  trCommandTREAD[]="TREAD";
ROM char  trCommandSHOW[]="SHOW";
ROM char  trCommandSTART[]="SINGLE";
ROM char  trCommandBUSY[]="BUSY";
ROM char  trCommandCLEAR[]="CLEAR";
ROM char  trCommandCLEARALL[]="CLEARALL";
ROM char  trCommandPAUSE[]="PAUSE";
ROM char  trCommandRESUME[]="RESUME";
ROM char  trCommandSTOP[]="STOP";
ROM char  trCommandMONDATE[]="MONDATE";
ROM char  trCommandTHRESH[]="THRESH";
ROM char  trCommandALARM[]="ALARM";
ROM char  trCommandMISC[]="MISC";
ROM char  trCommandLOG[]="LOG";
ROM char  trCommandMEM[]="MEM";
ROM char  trCommandDEFAULT[]="DEFAULT";
ROM char  trCommandCALIBRT[]="CALIBRT";
ROM char  trCommandCALIBRA[]="CALIBRA";
ROM char  trCommandCALIBRH[]="CALIBRH";
ROM char  trCommandREADINIT[]="READINIT";
ROM char  trCommandREADREC[]="READREC";
ROM char  trCommandERROR[]="ERROR";
ROM char  trCommandERRORBIT[]="ERRORBIT";
ROM char  trCommandDIAG[]="DIAG";
ROM char  trCommandSTABLE[]="BASELINE";
ROM char  trCommandHEATTEST[]="HEATTEST";
ROM char  trCommandOFFLINEPARAM[]="OFFLINEPARAM";
ROM char  trCommandCHDIV[]="CHDIV";
ROM char  trCommandNEGTG[]="NEGTG";
ROM char  trCommandBALANCE[]="BALANCE";
ROM char  trCommandSETPARAM[]="SETPARAM";
ROM char  trCommandRST[]="RST";
ROM char  trCommandREGDATA[]="REGDATA";
ROM char  trCommandPROCTYPE[]="PROCTYPE";
ROM char  trCommandAUTOBALANCE[]="AUTOBALANCE";
ROM char  trCommandCLEARLOG[]="CLEARLOG";
#ifdef MEGACAN
ROM char  strPROCTYPE []="MEGACAN";
#else
ROM char  strPROCTYPE []="MEGA128";
#endif
