#include "ADCDevice.h"
#include "Defs.h"
#include "Complex.h"
#include "SetChannels.h"
#include "CalcParam.h"
#include "Setup.h"
#include "RAM.h"
#include "Error.h"
#include <math.h>
#include "BoardAdd.h"
#include "LCD.h"
#include "Utils.h"
#include "Link.h"

#ifndef __emulator
#include "ADC.h"
#include "DACDevice.h"
#include "Timer.h"
#include "SPI.h"
#endif

#include "MeasurementDefs.h"  //2.10
#include "GammaMeasurement.h" //2.10

//extern float CalcFreq(s16 *a, unsigned int ToX, float ReadFr, float *Phase, float *Ampl);  // acm, to resolve CalcFreq()
// comment back out, turns out issue was that .h declaration mismatch to source, first param short vs s16.  Changed
// .h to match, shouldn't cause functional issue, as short == signed short == s16

//#define SkipFirst 6000
#define SkipFirst 601

u32 CalibrSinType=0;
u32 SaveCalibrSin=0;

//������� ������������� ������������ �������� �� ������� The current gain setting via
char AmplifIndex[MaxTransformerSide][3]={{0,0,0},{0,0,0}};

// acm, should add qualifier "volatile"???  As this array set in IRQ routine...
s16 *AddrSig[MainADCChannels]={(s16 *)0x00200000,(s16 *)0x00200000,(s16 *)0x00200000,(s16 *)0x00200000};

ROM float GainValue[MainADCChannels]={1.0,2.0,4.0,8.0};
ROM float GammaGainValue[MainADCChannels+2]={1.0,4.0048,16.0741,64.45698,2.0024,8.0096};

ROM char CurrentOnCh[MaxBushingCountOnSide]={1,0,0};
//ROM char CurrentOnCh[MaxBushingCountOnSide]={3,3,3};

float PhasesShift[MaxTransformerSide][MainADCChannels]={0,0,0,0};
float ADCPhasesShift[MaxTransformerSide][MainADCChannels]={0,0,0,0};

//������� ����� �� ������ ������ ���
short NullLine[MaxTransformerSide][MainADCChannels+1]={{0x4000,0x4000,0x4000,0x4000,0x4000},
                                                           {0x4000,0x4000,0x4000,0x4000,0x4000}};  // acm, 0x4000*ADCStep->1.5V
unsigned int ReadPoint=0;
unsigned int Skiped=0;
char InADCRead=0;

u32 SaveIntChannels[4]={0,1,2,3};

volatile u32 EndRead=0;

// acm, 2-19-09  Belief:  following defines ADS8344 control byte (ADC, component DD3), so defines operation mode:   single ended, no powerdown between conversions/ext. clock.
// IMHO, could be done less cryptically.
ROM unsigned char ChanFullNum[8]={0x87       , 0x87 | 4<<4, 0x87 | 1<<4, 0x87 | 5<<4,
                                  0x87 | 2<<4, 0x87 | 6<<4, 0x87 | 3<<4, 0x87 | 7<<4};

char Ch[MainADCChannels]={0,1,2,3};

char OutSin=0;
char CurShift=0;

int MaxValue1=0;
int MaxValue2=0;
int MaxValue3=0;
int MaxValue4=0;

//Load Current
float  LoadCurrentA,LoadCurrentB,LoadCurrentC;
u32 LoadCurrentP;

u32 MinReadData=0;

ROM unsigned short CalibrSin50[120]={
                          512, 538, 565, 592, 618, 644, 670, 695, 720, 744,
                          767, 790, 812, 833, 854, 873, 892, 909, 925, 940,
                          954, 967, 979, 989, 998, 1006, 1012, 1017, 1020, 1022,
                          1023, 1022, 1020, 1017, 1012, 1006, 998, 989, 979, 967,
                          954, 940, 925, 909, 892, 873, 854, 833, 812, 790,
                          767, 744, 720, 695, 670, 644, 618, 592, 565, 538,
                          512, 485, 458, 431, 405, 379, 353, 328, 303, 279,
                          256, 233, 211, 190, 169, 150, 131, 114, 98, 83,
                          69, 56, 44, 34, 25, 17, 11, 6, 3, 1,
                          0, 1, 3, 6, 11, 17, 25, 34, 44, 56,
                          69, 83, 98, 114, 131, 150, 169, 190, 211, 233,
                          256, 279, 303, 328, 353, 379, 405, 431, 458, 485
                       };

ROM unsigned short CalibrSin60[100]={
                          512, 544, 576, 607, 639, 670, 700, 729, 758, 786,
                          812, 838, 862, 884, 906, 925, 943, 960, 974, 987,
                          998, 1007, 1014, 1019, 1022, 1023, 1022, 1019, 1014, 1007,
                          998, 987, 974, 960, 943, 925, 906, 884, 862, 838,
                          812, 786, 758, 729, 700, 670, 639, 607, 576, 544,
                          512, 479, 447, 416, 384, 353, 323, 294, 265, 237,
                          211, 185, 161, 139, 117, 98, 80, 63, 49, 36,
                          25, 16, 9, 4, 1, 0, 1, 4, 9, 16,
                          25, 36, 49, 63, 80, 98, 117, 139, 161, 185,
                          211, 237, 265, 294, 323, 353, 384, 416, 447, 479
                       };


extern char swap_phaseBC[]; 	// acm, v1.79 FW, set in beginning of Main()
extern char setx_state;			// acm, swap_state defined Main(), drivenin SetInputChannel(), 0==don't swap, 1==set1 selected, 2=set 2 selected

extern float AmplCoeff[MaxTransformerSide][3];

#ifndef __emulator

static short ReadOneADC(char Channel)
{
short v;
v=SPI_WriteRead(Channel);
v=((SPI_WriteRead(0)&0x7F)<<8);
v|=(short)(SPI_WriteRead(0));
return v;
}


void timer0_c_irq_handler_foradc(void)
{
unsigned short v;

AT91PS_TC TC_pt = AT91C_BASE_TC0;
unsigned int dummy;//acm, i;
unsigned short DataP[8];
u32 Limit;

   // Acknowledge interrupt status
   dummy = TC_pt->TC_SR;
   // Suppress warning variable "dummy" was set but never used
   dummy = dummy;

   if (MinReadData) Limit=BalansingReadCount;
   else Limit=ReadCount;

   if (ReadPoint>=Limit) {
       EndRead=0;
       StopWaveTimer(0);
   } else {
     if (InADCRead) {
         //Internal ADC
/*
        ReadADC(0xFF,DataP);
        AddrSig[0][ReadPoint]=DataP[SaveIntChannels[0]];
        AddrSig[1][ReadPoint]=DataP[SaveIntChannels[1]];
        AddrSig[2][ReadPoint]=DataP[SaveIntChannels[2]];
        AddrSig[3][ReadPoint]=DataP[SaveIntChannels[3]];
*/
        ReadADC(0xF0,DataP);			// acm, reads 4 channels, one at a time...
        AddrSig[0][ReadPoint]=DataP[0];
		if(swap_phaseBC[setx_state])		// acm, v1.79, autophase rotation.  setx_state defined in SetInputChannel, 0==don't swap, 1==set1 select, 2=set 2 select
		{
			AddrSig[2][ReadPoint]=DataP[1];	// swap phase BC
			AddrSig[1][ReadPoint]=DataP[2];
		}
		else
		{
			AddrSig[1][ReadPoint]=DataP[1];	// don't swap
			AddrSig[2][ReadPoint]=DataP[2];
		}
        AddrSig[3][ReadPoint]=DataP[3];
     } else {
         //external ADC
        DisableLCD(0xFF);
        SPI_Prepare(SPIFREQ_2000,0);	// acm, 2mhz is max conversion freq
        SPI_SetCS(SPI_CS_MAINADC);
		// acm, 3-19, delay SPI read ~100ns to account for 570pf cap rework (noise hardening).  Trc=1/(570pf*470ohm).
		//DelayNs(100);
        __asm( "nop" );__asm( "nop" );__asm( "nop" );__asm( "nop" );__asm( "nop" );
		        __asm( "nop" );__asm( "nop" );__asm( "nop" );__asm( "nop" );__asm( "nop" );

// acm, v1.79, autophase rotation.  setx_state driven in SetInputChannel(), 1==set1 being sampled, 2=set 2 being sampled
				// CH3=Unn
				//
				// CH2=PHASEC <-
				//				|
				// CH1=PHASEB <-
				//
				// CH0=PHASEA
        AddrSig[0][ReadPoint]=ReadOneADC(ChanFullNum[Ch[0]]);
		if(swap_phaseBC[setx_state])		
		{
			AddrSig[1][ReadPoint]=ReadOneADC(ChanFullNum[Ch[2]]);  // swap phase BC
			AddrSig[2][ReadPoint]=ReadOneADC(ChanFullNum[Ch[1]]);
		}
		else
		{
			AddrSig[1][ReadPoint]=ReadOneADC(ChanFullNum[Ch[1]]);  // don't swap
			AddrSig[2][ReadPoint]=ReadOneADC(ChanFullNum[Ch[2]]);
		}
        AddrSig[3][ReadPoint]=ReadOneADC(ChanFullNum[Ch[3]]);
//        AddrSig[2][ReadPoint]=ReadOneADC(ChanFullNum[Ch[2]]);//SPI_ClearCS();  // acm, removing CS here fixed a rework bug that grounded chip select!
//        AddrSig[3][ReadPoint]=ReadOneADC(ChanFullNum[Ch[3]]);
        SPI_ClearCS();
//		        __asm( "nop" );__asm( "nop" );__asm( "nop" );__asm( "nop" );__asm( "nop" );
//        __asm( "nop" );__asm( "nop" );__asm( "nop" );__asm( "nop" );__asm( "nop" );

        EnableLCD();
     }
  }
  if (Skiped<SkipFirst)
    Skiped++;
  else
    ReadPoint++; //��������� ����� ��������� ���� 	calculate the number of bytes read

  if (SaveCalibrSin) {

     if (CalibrSinType) {
       v=CalibrSin60[OutSin];
     } else {
       v=CalibrSin50[OutSin];
     }

     ++OutSin;
     if (CalibrSinType) {
        if (OutSin>=100) OutSin=0;
     } else {
        if (OutSin>=120) OutSin=0;
     }

//     SetCalibrDAC((v>>CurShift)+(512-(512>>CurShift)));
     SetCalibrDAC(((v-512)>>CurShift)+512);
  }

}
#endif

static void Init_Timer0_and_ADC(float Freq)
{

  //init Timer0
#ifndef __emulator

  //init ADC
  AT91F_ADC_SoftReset (AT91C_BASE_ADC);
  AT91F_ADC_CfgModeReg (AT91C_BASE_ADC, (0<<8) | (9<<16) | (7<<24));

  PrepareWaveTimer(0,0,&Freq);
  //* Open Timer 0 interrupt
  AT91F_AIC_ConfigureIt ( AT91C_BASE_AIC, AT91C_ID_TC0, ADC_PRIORITY,AT91C_AIC_SRCTYPE_INT_LEVEL_SENSITIVE, (void(*)(void))timer0_c_irq_handler_foradc);
//  AT91F_TC_InterruptEnable(AT91C_BASE_TC0,AT91C_TC_CPAS|AT91C_TC_CPCS);  //  IRQ enable CPC
  AT91F_TC_InterruptEnable(AT91C_BASE_TC0,AT91C_TC_CPAS);  //  IRQ enable CPC
  AT91F_AIC_EnableIt (AT91C_BASE_AIC, AT91C_ID_TC0);

  ReadPoint=0;
  Skiped=0;
  EndRead=1;
  OutSin=0;

  StartWaveTimer(0);
#else
  EndRead=0;
#endif

}

void DoRead(char Mesurement, char SetCurrent)
{
//DE 3-30-16 ext ADC sample rate is 12KHz for all 4 channels
#ifndef __emulator
  ScanMessages();

  PrepareRead(Mesurement);
  if (SetCurrent) {						// acm, current input logic depopulated, so this should never be called (StartCrossMeasure()), i.e. changing Ch[] to read depop current inputs
        switch (SetCurrent) {
         case 1:Ch[CurrentOnCh[0]]=5; break;
         case 2:Ch[CurrentOnCh[1]]=6; break;
         case 3:Ch[CurrentOnCh[2]]=7; break;
/*
          case 1:Ch[1]=5;Ch[2]=Ch[3]; break;
          case 2:Ch[0]=Ch[1];Ch[1]=6;Ch[2]=Ch[3]; break;
          case 3:Ch[0]=Ch[2];Ch[1]=7;Ch[2]=Ch[3]; break;
*/
//          case 1:Ch[2]=5; break;
//          case 2:Ch[0]=Ch[1];Ch[2]=6; break;
//          case 3:Ch[0]=Ch[2];Ch[2]=7; break;
        }
  }
  StartRead();
  while (EndRead) ;

  SetCalibrDAC(512);

  ScanMessages();

#endif

}

void ReSetNullLines(int aReadCount, char Side)
{ int i;
  char j;
#ifndef __arm
  char OldPage=GetPage();
#endif
  for (j=0;j<MainADCChannels;j++) {
      SetPage(GammaPage0+j);
      for (i=0;i<aReadCount;i++) {
          AddrSig[j][i]=AddrSig[j][i]-NullLine[Side][j];
      }
  }
  SetPage(OldPage);
}

char GetGain(int MaxValue)
{
int Max;

#ifndef OldR1500_6
if (InADCRead) Max=ADCInMax; else Max=ADCMax;
#else
Max=ADCMax;
#endif

if (MaxValue<Max/GainValue[3]) return 3;
else
   if (MaxValue<Max/GainValue[2]) return 2;
   else
      if (MaxValue<Max/GainValue[1]) return 1;
      else return 0;
}

char GetGammaGain(int MaxValue)
{
int Max;

#ifndef OldR1500_6
if (InADCRead) Max=ADCInMax; else Max=ADCMax;
#else
Max=ADCMax;
#endif
if (MaxValue<Max/GammaGainValue[3]) return 3; 		// acm, GammaGainValue[MainADCChannels+2]={1.0,4.0048,16.0741,64.45698,2.0024,8.0096};
else
   if (MaxValue<Max/GammaGainValue[2]) return 2;
   else
      if (MaxValue<Max/GammaGainValue[1]) return 1;
      else return 0;
}

void ReadData(char ChangeK1,char ChangeK4,char ReadChannels, char TrSide, char ReCalcNull, char SetCurrent)
{
//int Max;
char MinIndex,i;
char tmpAmplifIndex[3];
u32 Limit;

if (ChangeK1||ChangeK4) {

#ifndef __emulator
   if (ChangeK1) AmplifIndex[TrSide][BushingCh]=0;
   if (ChangeK4) AmplifIndex[TrSide][BushingOnGammaCh]=0;

   SetGain(1,AmplifIndex[TrSide][BushingCh]);
   SetGain(4,AmplifIndex[TrSide][BushingOnGammaCh]);

   Delay(60);

   DoRead(rdTest,SetCurrent);	
   //DoRead(rdMeasure,SetCurrent); //acm, 9-14-12, Transgrid experiment DoRead(rdTest,SetCurrent);
									// so experiment field results indicate no help.  Theory for using internal vs external ADC without filter, avoid ~90 phase shift, algorithm measures filter phase shift
									// this way, incorporates into measurement.  Have yet to verify, but since this doesn't make measurement less prone to noisey input signal, back out.
   if (MinReadData) Limit=BalansingReadCount;
   else Limit=ReadCount;
   ReCalcNullLines(Limit,TrSide,0);

#endif
#ifndef __emulator
   if (ChangeK1) {
      tmpAmplifIndex[0]=GetGain(MaxValue1);
      tmpAmplifIndex[1]=GetGain(MaxValue2);
      tmpAmplifIndex[2]=GetGain(MaxValue3);
      MinIndex=tmpAmplifIndex[0];
      if ((tmpAmplifIndex[0]!=tmpAmplifIndex[1])||
          (tmpAmplifIndex[1]!=tmpAmplifIndex[2])||
          (tmpAmplifIndex[0]!=tmpAmplifIndex[2]))
      {
          for (i=1;i<3;i++) if (tmpAmplifIndex[i]<MinIndex) MinIndex=tmpAmplifIndex[i];
     }
     AmplifIndex[TrSide][BushingCh]=MinIndex;

   }

   if (ChangeK4) AmplifIndex[TrSide][BushingOnGammaCh]=GetGammaGain(MaxValue4);

   SetGain(1,AmplifIndex[TrSide][BushingCh]);
   SetGain(4,AmplifIndex[TrSide][BushingOnGammaCh]);
   Delay(100);

   DoRead(rdMeasure,SetCurrent);
#endif

} else {
#ifndef __emulator
   DoRead(rdMeasure,SetCurrent);
#endif
}
if (ReCalcNull) {
   if (MinReadData) Limit=BalansingReadCount;
   else Limit=ReadCount;
   ReSetNullLines(ReadCount,TrSide);
}
//if (ReCalcNull) ReCalcNullLines(ReadCount,TrSide,0);

/*
#else
if (ReCalcNull) {
   if (ChangeK1||ChangeK4) {
      ReCalcNullLines(ReadCount,TrSide,0);
   } else {
      ReSetNullLines(ReadCount,TrSide);
   }
}
#endif
*/


}


/*
static unsigned int ReadIntADCAndAvg(char Channel, int NullLine)
{
#ifndef __emulator

 #define ReadADCValues 1024
 unsigned int i;
 unsigned short Value[8];
 unsigned long Sum=0;
 float f;

 for (i=0;i<ReadADCValues;i++) {
     ReadADC((1<<Channel),&Value[0]);
     Value[0]-=NullLine;
     Sum+=((unsigned long)Value[0]*Value[0]);
     DelayMks(200);
 }
 f=pow((float)Sum/ReadADCValues,0.5);
 i=f;
 if ((f-i)>=0.5) i++;
 return i;
#else
 return 0;
#endif

}
*/
float ReadTemperatureADCValue(char Channel)
{
/*
  unsigned long Value;
  float f;
  int n;
#ifndef __emulator

  //Set Channel
  OutToAltera(0x70,Channel);
  //Temp CS ON
  DisableLCD(0xFF);
  SPI_Prepare(SPIFREQ_2400,3);

  for (n=0;n<2;n++) {
      SPI_SetCS(SPI_CS_TEMP);
      Delay(1);
      //Wait ADC
      while (AT91F_PIO_IsInputSet(AT91C_BASE_PIOA, AT91C_PIO_PA12)) ;
      DelayMks(30);
      //Read
      Value=((unsigned long)SPI_WriteRead(0)<<16);
      Value|=((unsigned long)SPI_WriteRead(0)<<8);
      Value|=SPI_WriteRead(0);
      SPI_ClearCS();
  }
  EnableLCD();
  // ������� � ��
  f=(float)(Value-8388607)/8388607.0*1.024*2.0/16.0*1000.0;
  // ������� � �������
  f=(f-20.0)/0.077;

  return f;
#else
return 0;
#endif
*/
return 0;
}

int ReadTemperature(char Channel)
{
return 0;
}

void ReadActiveLoad(void)
{
extern unsigned int IsCurrentGain;
extern unsigned int CurrentGain;

 float f;
 float Sum;
 unsigned int i;
 unsigned int TrSide;

  PrepareRead(rdMeasure);
  Ch[0]=5;
  Ch[1]=6;
  Ch[2]=7;

  MinReadData=1;
  StartRead();
  while (EndRead);
  MinReadData=0;

  LoadCurrentA=LoadCurrentB=LoadCurrentC=0;
  ReCalcNullLines(BalansingUpLoadCount,0,0);
  if (Setup.CalibrationCoeff.CurrentChannelOnPhase[0]) {
     Sum=0;
     for (i=0;i<BalansingUpLoadCount;i++)
//            Sum=Sum+(long)(AddrSig[0][i]-16384)*(AddrSig[0][i]-16384);
         Sum=Sum+((long)AddrSig[0][i]*AddrSig[0][i]);
     f=pow((float)Sum/BalansingUpLoadCount,0.5);
     f=f*ADCStep/1000.0;
     if (IsCurrentGain) f/=CurrentGain;
     LoadCurrentA=f*Setup.CalibrationCoeff.CurrentK[0]+Setup.CalibrationCoeff.CurrentB[0];
     TrSide=0;
     if ((Setup.CalibrationCoeff.CurrentChannelOnPhase[0]>0)&&
         (Setup.CalibrationCoeff.CurrentChannelOnPhase[0]<4))
         TrSide=1;
     if ((Setup.CalibrationCoeff.CurrentChannelOnPhase[0]>3)&&
         (Setup.CalibrationCoeff.CurrentChannelOnPhase[0]<7))
         TrSide=2;
     if (!TrSide) {
        TrSide--;
        LoadCurrentA=0;
     }
  }

  if (Setup.CalibrationCoeff.CurrentChannelOnPhase[1]) {
     Sum=0;
     for (i=0;i<BalansingUpLoadCount;i++)
         Sum=Sum+((long)AddrSig[1][i]*AddrSig[1][i]);
     f=pow((float)Sum/BalansingUpLoadCount,0.5);
     f=f*ADCStep/1000.0;
     if (IsCurrentGain) f/=CurrentGain;
     LoadCurrentB=f*Setup.CalibrationCoeff.CurrentK[1]+Setup.CalibrationCoeff.CurrentB[1];
     TrSide=0;
     if ((Setup.CalibrationCoeff.CurrentChannelOnPhase[1]>0)&&
         (Setup.CalibrationCoeff.CurrentChannelOnPhase[1]<4))
         TrSide=1;
     if ((Setup.CalibrationCoeff.CurrentChannelOnPhase[1]>3)&&
         (Setup.CalibrationCoeff.CurrentChannelOnPhase[1]<7))
         TrSide=2;
     if (!TrSide) {
        TrSide--;
        LoadCurrentB=0;
     }
  }

  if (Setup.CalibrationCoeff.CurrentChannelOnPhase[2]) {
     Sum=0;
     for (i=0;i<BalansingUpLoadCount;i++)
         Sum=Sum+((long)AddrSig[2][i]*AddrSig[2][i]);
     f=pow((float)Sum/BalansingUpLoadCount,0.5);
     f=f*ADCStep/1000.0;
     if (IsCurrentGain) f/=CurrentGain;
     LoadCurrentC=f*Setup.CalibrationCoeff.CurrentK[2]+Setup.CalibrationCoeff.CurrentB[2];
     TrSide=0;
     if ((Setup.CalibrationCoeff.CurrentChannelOnPhase[2]>0)&&
         (Setup.CalibrationCoeff.CurrentChannelOnPhase[2]<4))
         TrSide=1;
     if ((Setup.CalibrationCoeff.CurrentChannelOnPhase[2]>3)&&
         (Setup.CalibrationCoeff.CurrentChannelOnPhase[2]<7))
         TrSide=2;
     if (!TrSide) {
        TrSide--;
        LoadCurrentC=0;
     }
  }
  LoadCurrentP=0;
  if ((Setup.GammaSetupInfo.LoadChannel)) {
     switch (Setup.GammaSetupInfo.LoadChannel) {
         case 1:LoadCurrentP=LoadCurrentA; if (LoadCurrentA-LoadCurrentP>=0.5) LoadCurrentP++; break;
         case 2:LoadCurrentP=LoadCurrentB; if (LoadCurrentB-LoadCurrentP>=0.5) LoadCurrentP++; break;
         case 3:LoadCurrentP=LoadCurrentC; if (LoadCurrentC-LoadCurrentP>=0.5) LoadCurrentP++; break;
         case 4:LoadCurrentP=(LoadCurrentA+LoadCurrentB+LoadCurrentC)/3;
                if (((LoadCurrentA+LoadCurrentB+LoadCurrentC)/3)-LoadCurrentP>=0.5) LoadCurrentP++;
                break;
     }
  }
  if (!Setup.SetupInfo.ReadAnalogFromRegister) {ExtLoadActive=LoadCurrentP;}
}

char ReadHumidity(void)
{
 return 0;
}

int ReadLTC(void)
{
 return 0;
}


float ReadIntTemperature(void)
{
 return 20;
}

void PrepareRead(char Mesurement)
{
    switch (Mesurement) {
      case rdTest:		// acm, specified by SetPhasesGainAndNullLine(), RdTest reads phA/phB/phC/Unn, bypassing filter.
           Ch[0]=4;		// acm, phA
           Ch[1]=5;		// acm, *depop current input 1
           Ch[2]=6;		// acm, *depop current input 2
           Ch[3]=7;		// acm, *depop current input 3
		   // acm,              *Ch[] unused when InADCRead==1, should stub this code out to eliminate confusion, lack time to test/verify now.
           InADCRead=1; // acm, use internal ADC, thus bypassing LPF, passing AC>60hz
           break;
      case rdMeasure:	// acm, this mode most commonly used
           Ch[0]=4;		// acm, phA
           Ch[1]=3;		// acm, phB
           Ch[2]=2;		// acm, phC
           Ch[3]=1;		// acm, Unn
           InADCRead=0;
           break;
      case rdADCTest:
/*???????????????
           Ch[0]=4;
           Ch[1]=4;
           Ch[2]=4;
           Ch[3]=4;
*/
           Ch[0]=4;
           Ch[1]=4;
           Ch[2]=4;
           Ch[3]=4;
           InADCRead=0;
           break;
  }

  EndRead    = 1;
  ReadPoint  = 0;
  Skiped     = 0;

  OutSin=0;

}

//#pragma optimize=s 2  //DE 12-9-15 turn off optimization so vars are resolved
static char ReadCalibrPhases(float *Ampl,float *PhaseShift, char TrSide, float *Freq) //Reads SourcePhase amplitudes and sets erVHTestFail
{
  char i,Result=1;
  int j;
  struct TComp tmpComplex1,tmpComplex2,tmp1,tmp,tmpComplex3,tmpComplex4,ResultComplex;
  static unsigned short LastDAC[2][3]={{5168,5168,5168},{5168,5168,5168}};
  unsigned short avAmpl, t; //2.10 avAmpl 5050 for 60Hz and 5250 for 50Hz, t - threshold for comparison and is defined by reg 120 setup %
  
#ifndef __arm
  char CurPage=GetPage();
#endif

//  struct StOneGraph Graph;

//Connect calibration channel
  SetInputChannel(TrSide,chSourcePhases);		// acm, superfluous or somehow subsequent cmd out Altera leaves chSourcePhases enabled??
  SetInputChannel(TrSide,chSourcePhasesCalibr);
  CurShift=AmplifIndex[TrSide][BushingCh];  //ag 2-26-16 To always keep calibration signal within range for selected gain
  SaveCalibrSin=1;

  DoRead(rdADCTest,0);
  ReSetNullLines(ReadCount,TrSide);
  for (i=1;i<4;i++) {
      ImpPhaseCalc((s16 *)(AddrSig[0]),
                   (s16 *)(AddrSig[i]),
                   UpLoadCount,
                   Freq,
                   &tmpComplex4.re,&tmpComplex4.im,&tmpComplex3.re,GammaPage0,GammaPage1);
      PhaseRound(&tmpComplex4.im);
      ADCPhasesShift[TrSide][i]=tmpComplex4.im;
      if (ADCPhasesShift[TrSide][i]>180) ADCPhasesShift[TrSide][i]-=360;
  }



//Set Gain

//  ReadData(0,0,0,0,chReadAllChannels);

  Ampl[0]=0;
  Ampl[1]=0;
  Ampl[2]=0;
  *PhaseShift=0;
  tmpComplex1.im=tmpComplex1.re=tmpComplex2.im=tmpComplex2.re=tmp.im=tmp.re=tmp1.im=tmp1.re=0;

//  tmpComplex3.im=0; //Freq

  for (i=0;i<CalibrRepeat;i++) {
//      while (1) {

#ifdef __emulator
      SetInputChannel(TrSide,chSourcePhasesCalibr);
#endif

      ReadData(0,0,chReadAllChannels,TrSide,SetNullLine,0);

      tmpComplex3.re=0;

      //Re Calc Freq
      *Freq=0;

      ImpPhaseCalc((s16 *)(AddrSig[0]),
                   (s16 *)(AddrSig[1]),
                   UpLoadCount,
                   Freq,
                   &tmpComplex4.re,&tmpComplex4.im,&tmpComplex3.re,GammaPage0,GammaPage1);
/*
ClearLCD();
SetFont(Font6x5);
OutFloat(0,0,tmpComplex4.re,7,4);
OutFloat(39,0,tmpComplex4.im,7,4);
*/
      Ampl[0]+=tmpComplex3.re;
      Ampl[1]+=tmpComplex4.re;

      vec_add(&tmpComplex2,&tmpComplex4,&ResultComplex);
      tmpComplex2.re=ResultComplex.re;
      tmpComplex2.im=ResultComplex.im;
      tmpComplex4.re=0;
      ImpPhaseCalc((s16 *)(AddrSig[0]),
                   (s16 *)(AddrSig[2]),
                   UpLoadCount,
                   Freq,
                   &tmpComplex4.re,&tmpComplex4.im,&tmpComplex3.re,GammaPage0,GammaPage2);
/*
OutFloat(0,8,tmpComplex4.re,7,4);
OutFloat(39,8,tmpComplex4.im,7,4);
*/
      Ampl[2]+=tmpComplex4.re;
      vec_add(&tmpComplex1,&tmpComplex4,&ResultComplex);
      tmpComplex1.re=ResultComplex.re;
      tmpComplex1.im=ResultComplex.im;

      tmpComplex4.re=0;
      ImpPhaseCalc((s16 *)(AddrSig[0]),
                   (s16 *)(AddrSig[3]),
                   UpLoadCount,
                   Freq,
                   &tmpComplex4.re,&tmpComplex4.im,&tmpComplex3.re,GammaPage3,GammaPage0);

      vec_add(&tmp1,&tmpComplex4,&ResultComplex);
      tmp1.re=ResultComplex.re;
      tmp1.im=ResultComplex.im;

      SetPage(GammaPage3);
      if (CalibrSinType) {
//         for (j=0;j<ReadCount;j++) AddrSig[3][j]=(((int)CalibrSin60[j%100]-(int)512)<<2);
         for (j=0;j<ReadCount;j++) AddrSig[3][j]=(short)(CalibrSin60[j%100]-512)<<4;
//         for (j=0;j<ReadCount;j++) AddrSig[3][j]=(((int)CalibrSin60[j%100]-(int)512));
      } else {
//         for (j=0;j<ReadCount;j++) AddrSig[3][j]=(((int)CalibrSin50[j%120]-(int)512)<<2);
         for (j=0;j<ReadCount;j++) AddrSig[3][j]=(short)(CalibrSin50[j%120]-512)<<4;
//         for (j=0;j<ReadCount;j++) AddrSig[3][j]=(((int)CalibrSin50[j%120]-(int)512));
      }

      tmpComplex4.re=0;
      ImpPhaseCalc((s16 *)(AddrSig[3]),
                   (s16 *)(AddrSig[0]),
                   UpLoadCount,
                   Freq,
                   &tmpComplex4.re,&tmpComplex4.im,&tmpComplex3.re,GammaPage3,GammaPage0);

/*
SetPage(GammaPage3);
OutFloat(77,0,CalcAmpl((int *)(AddrSig[3]),UpLoadCount),7,3);
OutFloat(77,8,tmpComplex4.im,7,4);
Redraw();
*/
      vec_add(&tmp,&tmpComplex4,&ResultComplex);
      tmp.re=ResultComplex.re;
      tmp.im=ResultComplex.im;


/*
   SetFont(Font6x5);
   ClearLCD();
   InitGraph(&Graph,GammaPage0,(int*)&AddrSig[0][0],240);
   DrawGraph(&Graph,16,0,111,15);
   OutFloat(75,0,tmp.im,7,3);
   Redraw();
   WaitReadKey();

   ClearLCD();
   InitGraph(&Graph,GammaPage3,(int*)&AddrSig[3][0],360);
   DrawGraph(&Graph,16,0,111,15);
   OutFloat(75,0,tmp.im,7,3);
   Redraw();
   WaitReadKey();
*/
  }
  SaveCalibrSin=0;

  Ampl[0]/=CalibrRepeat;  //ag 2-11-16 All=25mA, A=5028, B=5038, C=4996
  Ampl[1]/=CalibrRepeat;  //ag 2-11-16 A=0mA, A=5224, B=5231, C=5186, CurShift=1; C=0, A=1326, B=1300, C=1288, CurShift=3
  Ampl[2]/=CalibrRepeat;  //ag 2-11-16 A&C=0mA, A=1326, B=1299, C=1289, CurShift=3
  //DWE 10-8-15 measured 3 PCB average as 233.34 and changed limits to +/-2.5%
  //float g; //ag 2-26-16 gain and CurSihft. In reality always 1 for current gain values.
  //g=(1<<CurShift)/GainValue[AmplifIndex[TrSide][BushingCh]];
  
  // 2.10 assign to new variable DAC_CalAmplitude
  Parameters->TrSide[TrSide].DAC_CalAmplitude[0] = (unsigned short) Ampl[0];
  Parameters->TrSide[TrSide].DAC_CalAmplitude[1] = (unsigned short) Ampl[1];
  Parameters->TrSide[TrSide].DAC_CalAmplitude[2] = (unsigned short) Ampl[2];
    
  if (*(float*)Freq > 55) //2.10 switch threshold between 50/60Hz
  {
    avAmpl = 5050;
  }
  else {
    avAmpl = 5250;
  }
  t = (unsigned short) (avAmpl*Setup.SetupInfo.CalibTreshould/100); ///g; //2.10 threshold as % of average
  
  //if (((Ampl[0]*g)<4966)||((Ampl[1]*g)<4966)||((Ampl[2]*g)<4966)||((Ampl[0]*g)>5273)||((Ampl[1]*g)>5273)||((Ampl[2]*g)>5273))
  if ((Ampl[0]<avAmpl-t)||(Ampl[1]<avAmpl-t)||(Ampl[2]<avAmpl-t)||(Ampl[0]>avAmpl+t)||(Ampl[1]>avAmpl+t)||(Ampl[2]>avAmpl+t)) //2.05 widened to +/-15% from 5%
  {
    Ampl[0] = LastDAC[TrSide][0]; //use last Ampl when HVTestFail
    Ampl[1] = LastDAC[TrSide][1];
    Ampl[2] = LastDAC[TrSide][2];
    Error|=((unsigned long)1<<(erHVTestFail+TrSide)); // DE 2-2-16 removed to allow measure to continue
	Error|=((unsigned long)1<<(erSide1Critical+TrSide)); //ad 2.06
  }
  else
  {
    LastDAC[TrSide][0] = (unsigned short) Ampl[0]; //remember Ampl
    LastDAC[TrSide][1] = (unsigned short) Ampl[1];
    LastDAC[TrSide][2] = (unsigned short) Ampl[2];        
    Error&=~((unsigned long)1<<(erHVTestFail+TrSide)); //clear HVTestFail Error
//    Result = 0; // DE 2-2-16 removed to allow measure to continue
  }

  Ampl[0]=(Ampl[0]*ADCStep)/GainValue[AmplifIndex[TrSide][BushingCh]];  //ag 2-11-16 All=25mA, A=2.302, B=2.307, C=2.288
  Ampl[1]=(Ampl[1]*ADCStep)/GainValue[AmplifIndex[TrSide][BushingCh]];  //ag 2-11-16 A=0mA, A=2.391, B=2.395, C=2.374
  Ampl[2]=(Ampl[2]*ADCStep)/GainValue[AmplifIndex[TrSide][BushingCh]];  //ag 2-11-16 A&C=0mA, A=60.7, B=59.49, C=59.04
  
//  if(Ampl[0]<1) Ampl[0]=1; //DE 2-4-16
//  if(Ampl[1]<1) Ampl[1]=1; //DE 2-4-16
//  if(Ampl[2]<1) Ampl[2]=1; //DE 2-4-16
  


  PhasesShift[TrSide][0]=tmp1.im;
  if (PhasesShift[TrSide][0]>180) PhasesShift[TrSide][0]-=360;
  PhasesShift[TrSide][1]=tmpComplex2.im;
  if (PhasesShift[TrSide][1]>180) PhasesShift[TrSide][1]-=360;
  PhasesShift[TrSide][2]=tmpComplex1.im;
  if (PhasesShift[TrSide][2]>180) PhasesShift[TrSide][2]-=360;
  PhasesShift[TrSide][3]=tmp.im;
  if (PhasesShift[TrSide][3]>180) PhasesShift[TrSide][3]-=360;

/*
ClearLCD();
SetFont(Font8x6);
OutFloat(0,0,PhasesShift[TrSide][0],7,3);
OutFloat(60,0,PhasesShift[TrSide][1],7,3);
OutFloat(0,8,PhasesShift[TrSide][2],7,3);
OutFloat(60,8,PhasesShift[TrSide][3],7,3);
Redraw();
WaitReadKey();
ClearLCD();
Redraw();
*/
  *PhaseShift=tmp.im;

//EndSourceRead:
  return Result;
}


static float SetPhasesGainAndNullLine(char TrSide) //ag02102016 Selects gains using signals in all combinations. Calcs averaged frequency using SourcePhases
{
  int i,aNullLine;
  char n,MinIndex;
  float f,Freq;
  unsigned long LTemp;
  float a_,b_;

//while (1) {

  //��  ���� �.�. �� ������� ������ � �����   Ra calculate gain for the input channels and Gamma
//  SetInputChannel(TrSide,chSourcePhases);
  SetInputChannel(TrSide,chGamma);
  ReadData(1,1,chReadAllChannels,TrSide,SetNullLine,0);  //1-������ ���� �������� Finding gain AmplifIndex
  AmplifIndex[TrSide][GammaCh]=AmplifIndex[TrSide][BushingOnGammaCh];
  CurShift=AmplifIndex[TrSide][BushingCh];

  //Connect calibration channel
  SetInputChannel(TrSide,chSourcePhasesCalibr);				// acm, cross phase calib?
  ReadData(0,0,chReadAllChannels,TrSide,NoSetNullLine,0);

  //������ 0 ����� ��� ������ ����� Calculation of the zero line for channel 0 Gamma
  SetPage(GammaPage3);
  LTemp=0;
  for (i=0;i<ReadCount;i++) {
      LTemp+=AddrSig[3][i];
  }
  f=(float)LTemp/ReadCount;
  aNullLine=(int)f;
  if (f-aNullLine>=0.5) aNullLine++;
  NullLine[TrSide][4]=aNullLine;

  //������ �.�. �� ���� ����� �����  ����� Calculation of gain for Phase signals using Channel Gamma
  SetGain(4,0);
  SetInputChannel(TrSide,chPhaseA);
  ReadData(0,1,chReadAllChannels,TrSide,SetNullLine,0);  //1-������ ���� �������� 1 Coef Bet gain selection
  MinIndex=AmplifIndex[TrSide][BushingOnGammaCh];

  SetInputChannel(TrSide,chPhaseB);
  ReadData(0,1,chReadAllChannels,TrSide,SetNullLine,0);  //1-������ ���� �������� 1 Coef Bet gain selection
  if (AmplifIndex[TrSide][BushingOnGammaCh]<MinIndex) MinIndex=AmplifIndex[TrSide][BushingOnGammaCh];

  SetInputChannel(TrSide,chPhaseC);
  ReadData(0,1,chReadAllChannels,TrSide,SetNullLine,0);  //1-������ ���� �������� 1 Coef Bet gain selection
  if (AmplifIndex[TrSide][BushingOnGammaCh]<MinIndex) MinIndex=AmplifIndex[TrSide][BushingOnGammaCh];

  AmplifIndex[TrSide][BushingOnGammaCh]=MinIndex;

  //Connect calibration channel
  SetInputChannel(TrSide,chSourcePhasesCalibr);				// acm, cal DAC outputting a fixed value (need to set saveCalibsin==1 first?)?
  ReadData(0,0,chReadAllChannels,TrSide,NoSetNullLine,0);

  for (n=0; n<MainADCChannels; n++) {
      SetPage(GammaPage0+n);
      LTemp=0;
      for (i=0;i<ReadCount;i++) {
          LTemp+=AddrSig[n][i];
      }
      f=(float)LTemp/ReadCount;
     aNullLine=(int)f;
     if (f-aNullLine>=0.5) aNullLine++;
     NullLine[TrSide][n]=aNullLine;
  }

  SetInputChannel(TrSide,chSourcePhases);
  ReadData(0,0,chReadAllChannels,TrSide,SetNullLine,0);
  SetPage(GammaPage0);
  Freq=CalcFreq((s16 *)AddrSig[0],UpLoadCount,ReadFreq,&a_,&b_);
  SetPage(GammaPage1);
  Freq+=CalcFreq((s16 *)AddrSig[1],UpLoadCount,ReadFreq,&a_,&b_);
  SetPage(GammaPage2);
  Freq+=CalcFreq((s16 *)AddrSig[2],UpLoadCount,ReadFreq,&a_,&b_);
  Freq/=3.0;



/*
//SetInputChannel(TrSide,chSourcePhases);
ClearLCD();
SetFont(Font6x5);
OutInt(0, 0,NullLine[TrSide][0],5);
OutInt(30,0,NullLine[TrSide][1],5);
OutInt(60,0,NullLine[TrSide][2],5);
OutInt(92,0,AmplifIndex[TrSide][BushingCh],1);
OutInt(99,0,AmplifIndex[TrSide][BushingOnGammaCh],1);
OutInt(106,0,AmplifIndex[TrSide][GammaCh],1);

OutInt(0,8,NullLine[TrSide][3],5);
OutInt(30,8,NullLine[TrSide][4],5);
OutFloat(60,8,Freq,7,4);
Redraw();
}
*/
//WaitReadKey();

  return Freq;
}


static float TestSinOn(float Freq)
{ //unsigned int Div;


  OutSin=0;
  if (Freq>55) {
     CalibrSinType=1;
     return ReadFreq/100.0;
  } else {
     CalibrSinType=0;
     return ReadFreq/120.0;
  }
}

float RunCalibrAmpl(char Side, float *Garbage)
{
#define MaxADC (float)1500.0

float Freq=0;
float NeedFreq;
float K;
float PhaseShift;
NeedFreq=SetPhasesGainAndNullLine(Side); //ag Finds gains and null lines and average frequency for SourcePhases

//ag 2-11-16 Block commented
/*if (NeedFreq<20)  {
//    Error|=(1<<erFreq);
//    Error|=(1<<erSet1Off+Side); // DE 2-2-16 removed to allow measure to continue 
    Garbage[0]=Garbage[1]=Garbage[2]=1;
//    return 0; // DE 2-2-16 removed to allow measure to continue
}
else if (NeedFreq>500)  {
//    Error|=(1<<erFreq);
//    Error|=(1<<erSet1Off+Side); // DE 2-2-16 removed to allow measure to continue
    Garbage[0]=Garbage[1]=Garbage[2]=1;
//    return 0; // DE 2-2-16 removed to allow measure to continue
}*/
if ((NeedFreq<45)||(NeedFreq>65))  {
     NeedFreq=Setup.SetupInfo.InternalSyncFrequency;
    Error|=(1<<erFreq);
	Error|=((unsigned long)1<<(erSide1Critical+Side)); //ag 2.06
//    Error|=(1<<erSet1Off+Side); // DE 2-2-16 removed to allow measure to continue 
    Garbage[0]=Garbage[1]=Garbage[2]=1;
//    return 0; // DE 2-2-16 removed to allow measure to continue
}

Freq=TestSinOn(NeedFreq); // ag Selects 50 or 60Hz for calibration with DAC

//while (1) {
SaveCalibrSin=1;
ReadCalibrPhases(Garbage,&PhaseShift,Side,&Freq);
SaveCalibrSin=0;
//TestSinOff();

K=(MaxADC/(1<<CurShift));
Garbage[0]=K/Garbage[0];
Garbage[1]=K/Garbage[1];
Garbage[2]=K/Garbage[2];

#ifdef __emulator
AmplCoeff[0]=AmplCoeff[1]=AmplCoeff[2]=1;
#endif

return NeedFreq;
/*
SetFont(Font6x5);
ClearLCD();
OutFloat(0,0,AmplCoeff[0],7,4);
OutFloat(38,0,AmplCoeff[1],7,4);
OutFloat(76,0,AmplCoeff[2],7,4);
OutFloat(0,8,*PhaseShift,7,3);
OutFloat(38,8,Freq,7,3);
Redraw();
*/
//}

}

void ReCalcNullLines(unsigned int NReadCount, char Side, char ReSetCh1)
{ int aNullLine;
  int j;
  unsigned int i;
  int MinValue1=0x7FFF;
  int MinValue2=0x7FFF;
  int MinValue3=0x7FFF;
  int MinValue4=0x7FFF;
  unsigned long LTemp;
  float f;

  MaxValue1=0;
  MaxValue2=0;
  MaxValue3=0;
  MaxValue4=0;
  SetPage0;
  LTemp=0;
  for (i=0;i<NReadCount;i++) {
      j=AddrSig[0][i];
      LTemp+=j;
      if (j>MaxValue1) MaxValue1=j;
      if (j<MinValue1) MinValue1=j;
  }

  f=(float)LTemp/NReadCount;
  aNullLine=(int)f;
  if (f-aNullLine>=0.5) aNullLine++;

  if (ReSetCh1) NullLine[Side][0]=aNullLine;

  MaxValue1-=aNullLine;
  MinValue1=aNullLine-MinValue1;
  if (MinValue1>MaxValue1) MaxValue1=MinValue1;

  for (i=0;i<ReadCount;i++) {
      j=AddrSig[0][i]-aNullLine;
      AddrSig[0][i]=j;
  }

  SetPage1;
  LTemp=0;
  for (i=0;i<NReadCount;i++) {
      j=AddrSig[1][i];
      LTemp+=j;
      if (j>MaxValue2) MaxValue2=j;
      if (j<MinValue2) MinValue2=j;
  }

  f=(float)LTemp/NReadCount;
  aNullLine=(int)f;
  if (f-aNullLine>=0.5) aNullLine++;
  if (ReSetCh1) NullLine[Side][1]=aNullLine;

  MaxValue2-=aNullLine;
  MinValue2=aNullLine-MinValue2;
  if (MinValue2>MaxValue2) MaxValue2=MinValue1;

  for (i=0;i<ReadCount;i++) {
      j=AddrSig[1][i]-aNullLine;
      AddrSig[1][i]=j;
  }

  SetPage2;
  LTemp=0;
  for (i=0;i<NReadCount;i++) {
      j=AddrSig[2][i];
      LTemp+=j;
      if (j>MaxValue3) MaxValue3=j;
      if (j<MinValue3) MinValue3=j;
  }
  f=(float)LTemp/NReadCount;
  aNullLine=(int)f;
  if (f-aNullLine>=0.5) aNullLine++;
  if (ReSetCh1) NullLine[Side][2]=aNullLine;

  MaxValue3-=aNullLine;
  MinValue3=aNullLine-MinValue3;
  if (MinValue3>MaxValue3) MaxValue3=MinValue3;

  for (i=0;i<ReadCount;i++) {
      j=AddrSig[2][i]-aNullLine;
      AddrSig[2][i]=j;
  }

  SetPage3;
  LTemp=0;
  for (i=0;i<NReadCount;i++) {
      j=AddrSig[3][i];
      LTemp+=j;
      if (j>MaxValue4) MaxValue4=j;
      if (j<MinValue4) MinValue4=j;
  }
  f=(float)LTemp/NReadCount;
  aNullLine=(int)f;
  if (f-aNullLine>=0.5) aNullLine++;

  if (ReSetCh1) NullLine[Side][3]=aNullLine;

  MaxValue4-=aNullLine;
  MinValue4=aNullLine-MinValue4;
  if (MinValue4>MaxValue4) MaxValue4=MinValue4;

  for (i=0;i<ReadCount;i++) {
      j=AddrSig[3][i]-aNullLine;
      AddrSig[3][i]=j;
  }

//  return Result;
}


char WatchFunc(float *WAmpl)											// acm, stripped down StartMeasure() function
{
#ifndef __emulator

//  #define WatchReadCount 10000
//  #define WDelay 400
  char TrSide;
  unsigned int i;
  int MaxValue,MinValue;
  volatile float Sum;  // acm, change volatile to help persistance with debugger

 for (TrSide=0;TrSide<MaxTransformerSide;TrSide++) {

     if  (!(Setup.GammaSetupInfo.ReadOnSide&(1<<TrSide))) continue;
     if (*(WAmpl+TrSide)<0.1) continue;								// acm, avoid divide by 0 exception.  Wampl == phA current only, not AVG(A+B+C)

     SetInputChannel(TrSide,chGamma);
     SetGain(4,AmplifIndex[TrSide][GammaCh]=0);						// acm, first read gain == 1?

     StartRead();
     while (EndRead) ;

     MaxValue=0;
     MinValue=32767;

     //Calc Gain
     for (i=0;i<ReadCount;i++){
         if (AddrSig[3][i]<MinValue) MinValue=AddrSig[3][i];
         if (AddrSig[3][i]>MaxValue) MaxValue=AddrSig[3][i];
     }																	// acm, triggered SAM-ICE on test input, Max-Min very small, ~10, resulting gain was 3->64, made sense...

     AmplifIndex[TrSide][GammaCh]=GetGammaGain(((int)(MaxValue-MinValue)/2));	// acm, GetGammaGain() returns 1-3, corrsponds to real gain 4.0048,16.0741,64.45698?
     SetGain(4,AmplifIndex[TrSide][GammaCh]);
     Delay(200);

     StartRead(); 		// acm, verified with debugger that InADCRead=0 at this time, indicating data coming from external not internal ADC
     while (EndRead) ;	

     MaxValue=0;
     MinValue=32767;
     //Read Values
     for (i=0;i<ReadCount;i++){
         if (AddrSig[3][i]<MinValue) MinValue=AddrSig[3][i];
         if (AddrSig[3][i]>MaxValue) MaxValue=AddrSig[3][i];
     }																	// acm, note, sampled values NOT null line adjusted, e.g. return Unn varies 16275-16371, so 1.5V offset (~16300?)

     Sum=(((MaxValue-MinValue)/2*(float)ADCStep)/GammaGainValue[AmplifIndex[TrSide][GammaCh]]);

     if (Sum/(*(WAmpl+TrSide))*100.0>((float)Setup.GammaSetupInfo.GammaSideSetup[TrSide].GammaRedThresholld*0.05)) return 1;

// acm, 9-15-13, just reminded by Claude we also wanted to check for signal loss, finally recalled this already verified, remove input calculates large Unn, triggering full measurement, and "Set off" error.
// "" claude didn't see 5 min watch before sched. measurement.  Oops, forgot to make change in Main...  Keep watch enabled always!  Fix in rc3	 
// New feature request, display set # in error
 }

#endif

 return 0;
}

void StartRead(void)
{
#ifndef __emulator
float Fr;

  SetCalibrDAC(512);
//  Fr=3000.0;
  Fr=6000.0;
  Init_Timer0_and_ADC(Fr);

#else
char i;

while (ReadPoint<ReadCount) {
  for (i=0;i<4;i++) {
      *((int *)AddrSig[i]+ReadPoint)=i;
  }
  if (Skiped<SkipFirst) Skiped++;
  else ReadPoint++; //��������� ����� ��������� ����
}
EndRead = 0;

#endif
}

