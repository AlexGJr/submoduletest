#ifndef Timer_h
#define Timer_h

#include "Board.h"

#define TC_CLKS_MCK2             0x0
#define TC_CLKS_MCK8             0x1
#define TC_CLKS_MCK32            0x2
#define TC_CLKS_MCK128           0x3
#define TC_CLKS_MCK1024          0x4

void PrepareWaveTimer(int Timer, int LineOut, float *Freq);
void StartWaveTimer(int Timer);
void StopWaveTimer(int Timer);

void TestTimer(void);

#endif
