#include "BoardAdd.h"
#include "Timer.h"





AT91PS_TC AT91C_BASE_TCx[3]={AT91C_BASE_TC0,AT91C_BASE_TC1,AT91C_BASE_TC2};

// Timer=0..2, 5MHz-1Hz
// � LineOut ��������� !!!
void PrepareWaveTimer(int Timer, int LineOut, float *Freq)
{
unsigned int clk;
unsigned int f;

//* First, enable the clock of the TIMER
if (Timer==0) AT91F_PMC_EnablePeriphClock ( AT91C_BASE_PMC, 1<< AT91C_ID_TC0 ) ;
else if (Timer==1) AT91F_PMC_EnablePeriphClock ( AT91C_BASE_PMC, 1<< AT91C_ID_TC1 ) ;
else if (Timer==2) AT91F_PMC_EnablePeriphClock ( AT91C_BASE_PMC, 1<< AT91C_ID_TC2 ) ;

//* Disable the clock and the interrupts
AT91C_BASE_TCx[Timer]->TC_CCR = AT91C_TC_CLKDIS ;
AT91C_BASE_TCx[Timer]->TC_IDR = 0xFFFFFFFF ;

//* Clear status bit
clk = AT91C_BASE_TCx[Timer]->TC_SR;


if (*Freq>30.0) {
	if (*Freq>400.0) {
		// 400... ��
        f=((unsigned int)((float)MCK/2.0/(*Freq)/2.0))&0xFFFF;
                if (f<1) f=1;
		*Freq=(float)MCK/2.0/(((float)f)*2.0);
                clk=TC_CLKS_MCK2;
	} else {
        // 30..400 ��
		f=((unsigned int)((float)MCK/32.0/(*Freq)/2.0))&0xFFFF;
                if (f<1) f=1;
		*Freq=(float)MCK/32.0/(((float)f)*2.0);
                clk=TC_CLKS_MCK32;
	}
} else {
	// 1..30 ��
	f=((unsigned int)((float)MCK/1024.0/(*Freq)/2.0))&0xFFFF;
        if (f<1) f=1;
	*Freq=(float)MCK/1024.0/(((float)f)*2.0);
        clk=TC_CLKS_MCK1024;
}


//* Set the Mode of the Timer Counter
AT91C_BASE_TCx[Timer]->TC_CMR = clk | AT91C_TC_WAVE | AT91C_TC_WAVESEL_UP_AUTO | AT91C_TC_ACPA_SET | AT91C_TC_ACPC_CLEAR;

AT91C_BASE_TCx[Timer]->TC_RA = f;
AT91C_BASE_TCx[Timer]->TC_RC = f*2;

/*
Lines:
TIOA0 PA0
TIOB0 PA1
TIOA1 PA15
TIOB1 PA16
TIOA2 PA26
TIOB2 PA27
*/
if (LineOut) {
  if (Timer==0) AT91F_PIO_CfgPeriph(AT91C_BASE_PIOA,0,AT91C_PIO_PA0);
  else if (Timer==1) AT91F_PIO_CfgPeriph(AT91C_BASE_PIOA,0,AT91C_PIO_PA15);
  else if (Timer==2) AT91F_PIO_CfgPeriph(AT91C_BASE_PIOA,0,AT91C_PIO_PA26);
}

}


void StartWaveTimer(int Timer)
{
//* Enable and Trigger the clock
AT91C_BASE_TCx[Timer]->TC_CCR = AT91C_TC_SWTRG | AT91C_TC_CLKEN;
}


void StopWaveTimer(int Timer)
{
//* Disable the clock
AT91C_BASE_TCx[Timer]->TC_CCR = AT91C_TC_CLKDIS;

}


void SetWaveFreq(float aFreq) // �������, ��   7 500 000 - 28.6
{
float Freq;

Freq=aFreq;
PrepareWaveTimer(0,1,&Freq);
StartWaveTimer(0);
WaitReadButton();
StopWaveTimer(0);
}


void TestTimer(void)
{
float Freq;

Freq=10.0*100.0;
PrepareWaveTimer(1,1,&Freq);
StartWaveTimer(1);

Freq=1000.0*100.0;
PrepareWaveTimer(0,1,&Freq);
StartWaveTimer(0);

while (1);
}

