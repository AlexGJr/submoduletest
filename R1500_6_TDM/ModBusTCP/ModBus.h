#ifndef _modbus_h
#define _modbus_h

#define  MaxDataLen 1024

enum ModbusModes
{
  MODBUS_ERROR   = -1,
  MODBUS_SUCCESS = 0,
  MODBUS_RTU     = 1,
  MODBUS_ASCII   = 2,
  MODBUS_TCP     = 3
};

enum ModbusFunctionCodes
{
  MF_ReadCoilStatus         = 1,
  MF_ReadInputStatus        = 2,
  MF_ReadHoldingRegisters   = 3,
  MF_ReadInputRegisters     = 4,
  MF_ForceSingleCoil        = 5,
  MF_PresetSingleRegister   = 6,
  MF_ReadExceptionStatus    = 7,
  MF_FetchCommEventCtr      = 11,
  MF_FetchCommEventLog      = 12,
  MF_ForceMultipleCoils     = 15,
  MF_PresetMultipleRegs     = 16,
  MF_ReportSlaveID          = 17,
  MF_ReadGeneralReference   = 20,
  MF_WriteGeneralReference  = 21,
  MF_MaskWrite4XRegisters   = 22,
  MF_ReadWrite4XRegisters   = 23,
  MF_ReadFifoQueue          = 24,
  MF_UserStrFunctionCode    = 71,
  MF_UserBinFunctionCode    = 72  //secret binary transfer command
};


enum ModbusExceptoionCodes
{
ME_ILLEGAL_FUNCTION           = 1,
ME_ILLEGAL_DATA_ADDRESS       = 2,
ME_ILLEGAL_DATA_VALUE         = 3,
ME_SLAVE_DEVICE_FAILURE       = 4,
ME_ACKNOWLEDGE                = 5,
ME_SLAVE_DEVICE_BUSY          = 6,
ME_MEMORY_PARITY_ERROR        = 8,
ME_GATEWAY_PATH_UNAVAILABLE   = 0x0A,
ME_GATEWAY_TARGET_DEVICE_FAILED_TO_RESPOND= 0x0B
};


extern int ClientModbusMode;
extern int ServerModbusMode;

// ---------- ���������� ���������� - ������ ���� �������� ���-�� ��� ----------
// ������� ����� TCP-���� 502. 1-Ok 0-Error
int SendModbusTCP(char *tel, int len);
// ������ �����, �� �� ���������� ������. return Len
int ReadModbusTCP(char *tel, int len);
// �������� ������
void ShiftModbusTCP(int len);

// ������� ����� RS-485. 1-Ok 0-Error
int SendModbusCOM(char *tel, int len);
// ������ �����, �� �� ���������� ������. return Len
int ReadModbusCOM(char *tel, int len);
// �������� ������
void ShiftModbusCOM(int len);
// -----------------------------------------------------------------------------

#endif
