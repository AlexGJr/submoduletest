#ifndef _DACDevice_h
#define _DACDevice_h

#include "Defs.h"

//���� ��������� ���������
extern volatile char ChangeDACValues;

extern char DACValue[MaxTransformerSide][MaxBushingCountOnSide];
extern char ExtDACValue[MaxTransformerSide][MaxBushingCountOnSide];

void InitDAC(void);
void SetDACDevice(char Side, char Value,char DACNum);
void SetCalibrDAC(unsigned int Value);
void SetExtDAC(char Side, char Value,char DACNum);  // acm, 3-11-10, for testing purposes

#endif
