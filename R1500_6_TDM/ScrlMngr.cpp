#include "defkb.h"
#include "defs.h"
#include "ScrlMngr.h"
#include "Graph.h"
#include "LCD.h"
#include "Keyboard.h"
#include <string.h>

void InitScroller(struct ScrollerMngr *Scrl,unsigned int X,unsigned  int Y,char Font)
{
 (*Scrl).CurStr=1;
 (*Scrl).NumberStr=0;
 (*Scrl).Font=Font;
 (*Scrl).LeftKey=(KEY)KB_LEFT;
 (*Scrl).RightKey=(KEY)KB_RIGHT;
 (*Scrl).ActionKey1=(KEY)KB_UP;
 (*Scrl).ActionKey2=(KEY)KB_DOWN;
 (*Scrl).X=X;
 (*Scrl).Y=Y;
#ifndef __arm9
 (*Scrl).ShortDraw=1;
#else
#ifdef R2000
 (*Scrl).ShortDraw=1;
#else
 (*Scrl).ShortDraw=(Scrl->Font==Font8x8);
#endif
#endif
}

void ClearScroller(struct ScrollerMngr *Scrl)
{
 (*Scrl).NumberStr=0;
}


char AddScrollerString(struct ScrollerMngr *Scrl,char ROM *ScrlString)
{
return AddScrollerStringX(Scrl,ScrlString,NULL);
}

char AddScrollerStringX(struct ScrollerMngr *Scrl,char ROM *ScrlString,char ROM *LongScrlString)
{
if ((*Scrl).NumberStr<MaxScrlStrs-1) {
   (*Scrl).String[(*Scrl).NumberStr]=ScrlString;
   if (LongScrlString)	(*Scrl).LongString[(*Scrl).NumberStr]=LongScrlString;
   		else 	(*Scrl).LongString[(*Scrl).NumberStr]=ScrlString;
   (*Scrl).NumberStr++;
   return  (*Scrl).NumberStr;
} else return  0;
}

void RedrawScrl(struct ScrollerMngr *Scrl)
{
register int x,y;

SetFont((*Scrl).Font);
if (Scrl->ShortDraw)
  OutString((*Scrl).X,(*Scrl).Y,(*Scrl).String[(*Scrl).CurStr-1]);
else {
  x=Scrl->X;
  y=Scrl->Y;
  DrawLine(x+CurFontWidth,y,x+CurFontWidth,y+CurFontHeight);
  DrawLine(x+CurFontWidth*2,y,x+CurFontWidth*2,y+CurFontHeight);

  DrawLine(x+2,y+CurFontHeight/2,x+CurFontWidth-2,y+2);
  DrawLine(x+CurFontWidth-2,y+2,x+CurFontWidth-2,y+CurFontHeight-2);
  DrawLine(x+CurFontWidth-2,y+CurFontHeight-2,x+2,y+CurFontHeight/2);

  DrawLine(x+CurFontWidth*2-2,y+CurFontHeight/2,x+CurFontWidth+2,y+2);
  DrawLine(x+CurFontWidth+2,y+2,x+CurFontWidth+2,y+CurFontHeight-2);
  DrawLine(x+CurFontWidth+2,y+CurFontHeight-2,x+CurFontWidth*2-2,y+CurFontHeight/2);

  OutString((*Scrl).X+CurFontWidth*2+8,(*Scrl).Y,(*Scrl).String[(*Scrl).CurStr-1]);

  DrawRect(x,y,x+CurFontWidth*2+CurFontWidth*strlen((char*)Scrl->String[Scrl->CurStr-1])+10,y+CurFontHeight);
}
}

static void DoLeftKeySc(struct ScrollerMngr *Scrl)
{
 if ((*Scrl).NumberStr) {
    if ((*Scrl).CurStr>1) {
       (*Scrl).CurStr--;
   }else {
      (*Scrl).CurStr=(*Scrl).NumberStr;
   }
   RedrawScrl(Scrl);
 }
}

static void DoRightKeySc(struct ScrollerMngr *Scrl)
{
 if ((*Scrl).NumberStr) {
 if ((*Scrl).CurStr<(*Scrl).NumberStr) {
         (*Scrl).CurStr++;
 } else {
 (*Scrl).CurStr=1;
 }
 RedrawScrl(Scrl);
 }
}


#ifdef __emulator
extern char TryClose;
#endif

#ifdef __arm9
void DoModKeySc(struct ScrollerMngr *Scrl)
{
#define MaxPopupLen 30
char ClearStr[MaxPopupLen+3];
char i, MaxLen, MaxHeight, DrawScrlr, TopEl;
int X1,Y1,X2,Y2,key,y;
int Result;

DrawScrlr=0;
TopEl=0;
MaxHeight=Scrl->NumberStr;
if (MaxHeight>11) {
	DrawScrlr=1;
	MaxHeight=11;
}

MaxLen=0;
for (i=0;i<Scrl->NumberStr;i++)
	if (MaxLen<(int)strlen((char*)Scrl->LongString[i]))
		MaxLen=strlen((char*)Scrl->LongString[i]);

if (MaxLen>MaxPopupLen)
	MaxLen=MaxPopupLen;

for (i=0;i<MaxPopupLen+3;i++) ClearStr[i]=' ';
ClearStr[MaxLen+2]=0;

X1=Scrl->X;
Y1=Scrl->Y+14;
X2=X1+MaxLen*8+8+15;
Y2=Y1+MaxHeight*16+7;

if (Y2>230) {
	Y2=Scrl->Y;
	Y1=Y2-MaxHeight*16-7;
	if (Y1<10) {
		Y2=230;
		Y1=Y2-MaxHeight*16-7;
	}
}
if (X2>310) {
	X2=310;
	X1=X2-MaxLen*8-8-15;
}
if (DrawScrlr)
	MaxHeight--;

SwitchCurScreen();
SetOutScreen();

while (ReadKey()!=KB_NO) ;
key=KB_NO;
Result=Scrl->CurStr-1;
while (1) {
	if (key==KB_UP) {if (Result>0) Result--; else Result=Scrl->NumberStr-1; }
	else if (key==KB_DOWN) {if (Result<Scrl->NumberStr-1) Result++; else Result=0; }
	if (key==KB_LEFT) { Result-=MaxHeight-1; if (Result<0) Result=0; }
	else if (key==KB_RIGHT) { Result+=MaxHeight-1; if (Result>Scrl->NumberStr-1) Result=Scrl->NumberStr-1; }
	else if (key==KB_ESC) {
		SwitchCurScreen();
		SetOutScreen();
		return;
	} else if ((key==KB_ENTER)||(key==KB_MOD)) {
		SwitchCurScreen();
		SetOutScreen();
		Result++;
		if (Result!=Scrl->CurStr) {
			Scrl->CurStr=Result;
			RedrawScrl(Scrl);
			return;
		} else return;
	}
	if (TopEl>Result)
		TopEl=Result;
	if (TopEl+MaxHeight-1<Result)
		TopEl=Result-MaxHeight+1;

	CopyCurScreen();

	SetFont(Font8x16);
	ClearRect(X1,Y1,Y2-Y1+1,X2-X1+1);
	DrawRect(X1,Y1,X2,Y2);
	if (DrawScrlr) {
		DrawRect(X1+2,Y1+10,X2-2,Y2-10);
		if (TopEl>0) {
			DrawLine(X1+4,Y1+8,X1+(X2-X1)/2,Y1+2);
			DrawLine(X1+(X2-X1)/2,Y1+2,X2-4,Y1+8);
			DrawLine(X1+4,Y1+8,X2-4,Y1+8);
		}
		if (TopEl+MaxHeight<Scrl->NumberStr) {
			DrawLine(X1+4,Y2-8,X1+(X2-X1)/2,Y2-2);
			DrawLine(X1+(X2-X1)/2,Y2-2,X2-4,Y2-8);
			DrawLine(X1+4,Y2-8,X2-4,Y2-8);
		}
	} else {
		DrawRect(X1+2,Y1+2,X2-2,Y2-2);
	}
	for (i=0;i<MaxHeight;i++) {
		y=Y1+4+i*16;
		if (DrawScrlr) y+=8;
		if (TopEl+i==Result) {
			OutStringInv(X1+4,y,ClearStr);
			OutStringInvN(X1+12,y,Scrl->LongString[TopEl+i],MaxLen);
		} else {
			OutString(X1+4,y,ClearStr);
			OutStringN(X1+12,y,Scrl->LongString[TopEl+i],MaxLen);
		}
	}

	key=WaitReadKey();
#ifdef __emulator
    if (TryClose) key=KB_ESC;
#endif
}
}
#endif

unsigned int DoScrlNoWait(struct ScrollerMngr *Scrl, KEY ch)
{
RedrawScrl(Scrl);
#ifdef __arm9
if (ch==KB_MOD) { DoModKeySc(Scrl); ch=KB_NO; } else
#endif
if (ch==(*Scrl).LeftKey) { DoLeftKeySc(Scrl); ch=KB_NO; } else
if (ch==(*Scrl).RightKey) { DoRightKeySc(Scrl); ch=KB_NO; }
return ch;
}

unsigned int DoScrl(struct ScrollerMngr *Scrl)
{
KEY ch;
RedrawScrl(Scrl);
ch=WaitReadKey();
#ifdef __emulator
    if (TryClose) ch=KB_ESC;
#endif
return DoScrlNoWait(Scrl,ch);
}


char GetCurScrl(struct ScrollerMngr *Scrl)
{
return (*Scrl).CurStr;
}
char SetCurScrl(struct ScrollerMngr *Scrl,char N)
{
if ((N<=(*Scrl).NumberStr)&(N>0)) (*Scrl).CurStr=N;
return (*Scrl).CurStr;
}

