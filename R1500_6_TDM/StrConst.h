#ifndef StrConst_h
#define StrConst_h

extern ROM char DateSeparator[];
extern ROM char TimeSeparator[];
extern ROM char EmptyStr[];

extern ROM char DeviceStr[];
extern ROM char VersionStr[];
//extern ROM char VersionNStr[];
extern ROM char CalibrStr[];
extern ROM char UnnStr[];
extern ROM char PhaseAStr[];
extern ROM char PhaseBStr[];
extern ROM char PhaseCStr[];
extern ROM char ErrorStr[];
extern ROM char SendDataStr[];
extern ROM char AmplEdStr[];
extern ROM char AmplGainStr[];
extern ROM char DegStr[];
extern ROM char PhaseStr[];
extern ROM char AmplEdPrcStr[];

extern ROM char InpPwdStr[];
extern ROM char StBalansStr[];

extern ROM char SelSideStr1[];
extern ROM char SelSideStr2[];
extern ROM char SelSideStr3[];
extern ROM char SelSideStr4[];

extern ROM char DelArchStr1[];
extern ROM char DelArchStr2[];
extern ROM char DelArchStr3[];
extern ROM char DelArchStr4[];

extern ROM char SingleStr1[];
extern ROM char SingleStr2[];

extern ROM char DelArchiveConfirm[];
extern ROM char DelMeasConfirm[];
extern ROM char YesStr[];
extern ROM char NoStr[];


extern ROM char DateStr[];
extern ROM char TimeStr[];
extern ROM char ReadTimeStr1[];
extern ROM char ReadTimeStr2[];
extern ROM char BaudRateStr1[];
extern ROM char BaudRateStr2[];
extern ROM char BaudRateStr3[];
extern ROM char BaudRateStr4[];
extern ROM char BaudRateStr5[];
extern ROM char BaudRateStr6[];
extern ROM char BaudRateStr7[];
extern ROM char BaudRateStr8[];
extern ROM char BaudRateStr9[];
extern ROM char ReadFromSideStr1[];
extern ROM char ReadFromSideStr2[];

extern ROM char RelayStr1[];
extern ROM char RelayStr2[];
extern ROM char RelayStr3[];
extern ROM char RelayStr4[];
extern ROM char RelayStr5[];
extern ROM char RelayStr6[];
extern ROM char SecStr[];

extern ROM char MeasuringStr[];
extern ROM char LoadingStr[];

extern ROM char OnOffSideStr1[];
extern ROM char OnOffSideStr2[];
extern ROM char OnOffSideStr3[];
extern ROM char OnOffSideStr4[];

extern ROM char ReadTimeStr3[];
extern ROM char ReadTimeStr4[];
extern ROM char ReadTimeStr5[];
extern ROM char ReadTimeStr6[];

extern ROM char DeviceNumberStr1[];
extern ROM char DeviceNumberStr2[];
extern ROM char DeviceNumberStr3[];

extern ROM char StoppedStr1[];
extern ROM char StoppedStr2[];
extern ROM char StoppedStr3[];
extern ROM char StoppedStr4[];

extern ROM char FillPauseStr1[];
extern ROM char FillPauseStr2[];
extern ROM char FillPauseStr3[];
extern ROM char FillPauseStr4[];
extern ROM char FillPauseStr5[];
extern ROM char FillPauseStr6[];
extern ROM char FillPauseStr7[];
extern ROM char FillPauseStr8[];
extern ROM char FillPauseStr9[];
extern ROM char FillPauseStr10[];
extern ROM char FillPauseStr11[];
extern ROM char FillPauseStr12[];
extern ROM char FillPauseStr13[];
extern ROM char FillPauseStr14[];
extern ROM char FillPauseStr15[];

extern ROM char ShowPauseStr6[];
extern ROM char ShowPauseStr10[];
extern ROM char ShowPauseStr11[];

extern ROM char ShowTimeStr1[];
//extern ROM char ShowTimeStr2[];

extern ROM char AverageNumStr1[];
extern ROM char AverageNumStr2[];

extern ROM char ReReadAlarmStr1[];
extern ROM char ReReadAlarmStr2[];
extern ROM char MinStr[];

extern ROM char AlarmHysteresisStr1[];
extern ROM char AlarmHysteresisStr2[];
extern ROM char PercentStr[];

extern ROM char DaysTrendStr1[];
extern ROM char DaysTrendStr2[];
extern ROM char DaysStr[];

extern ROM char DaysTKStr1[];
extern ROM char DaysTKStr2[];

extern ROM char PhaseDispStr1[];
extern ROM char PhaseDispStr2[];
extern ROM char DegrStr[];
extern ROM char CStr[];

extern ROM char AlarmEnableStr1[];
extern ROM char AlarmEnableStr2[];
extern ROM char AlarmEnableStr3[];
extern ROM char AlarmEnableStr4[];
extern ROM char AlarmEnableStr5[];
extern ROM char AlarmEnableStr6[];
extern ROM char AlarmEnableStr7[];
extern ROM char AlarmEnableStr8[];
extern ROM char AlarmEnableStr9[];
extern ROM char AlarmEnableStr10[];
extern ROM char AlarmEnableStr11[];
extern ROM char AlarmEnableStr12[];
extern ROM char AlarmEnableStr13[];
extern ROM char AlarmEnableStr14[];
extern ROM char AlarmEnableStr15[];

extern ROM char AlarmThreshStr1[];
extern ROM char AlarmThreshStr2[];
extern ROM char AlarmThreshStr3[];
extern ROM char AlarmThreshStr4[];
extern ROM char AlarmThreshStr5[];
extern ROM char AlarmThreshStr6[];
extern ROM char AlarmThreshStr7[];
extern ROM char AlarmThreshStr8[];

extern ROM char TKAlarmStr1[];
extern ROM char TKAlarmStr2[];
extern ROM char TKAlarmStr3[];

extern ROM char TrendAlarmStr1[];
extern ROM char TrendAlarmStr2[];
extern ROM char TrendAlarmStr3[];

extern ROM char RVoltageStr1[];
extern ROM char RVoltageStr2[];
extern ROM char RVoltageStr3[];

extern ROM char RCurrentStr1[];
extern ROM char RCurrentStr2[];
extern ROM char RCurrentStr3[];

extern ROM char Tg0Str1[];
extern ROM char Tg0Str2[];
extern ROM char Tg0Str3[];
extern ROM char Tg0Str4[];
extern ROM char Tg0Str5[];

extern ROM char C0Str1[];
extern ROM char C0Str2[];
extern ROM char C0Str3[];
extern ROM char C0Str4[];
extern ROM char C0Str5[];

extern ROM char TgTempStr1[];
extern ROM char TgTempStr2[];
extern ROM char TgTempStr3[];

extern ROM char InputCStr1[];
extern ROM char InputCStr2[];
extern ROM char InputCStr3[];
extern ROM char InputCStr4[];
extern ROM char InputCStr5[];

extern ROM char ImpedanceStr1[];
extern ROM char ImpedanceStr2[];
extern ROM char ImpedanceStr3[];
extern ROM char ImpedanceStr4[];
extern ROM char ImpedanceStr5[];

extern ROM char CalcZkStr1[];
extern ROM char CalcZkStr2[];
extern ROM char CalcZkStr3[];

extern ROM char CurrentTypeStr1[];
extern ROM char CurrentTypeStr2[];
extern ROM char CurrentTypeStr03[];
extern ROM char CurrentTypeStr3[];
extern ROM char CurrentTypeStr4[];
extern ROM char CurrentTypeStr5[];
extern ROM char CurrentTypeStr6[];
extern ROM char CurrentTypeStr7[];
extern ROM char CurrentTypeStr8[];
extern ROM char CurrentTypeStr9[];
extern ROM char CurrentTypeStr10[];
extern ROM char CurrentTypeStr11[];

extern ROM char OutPhA[];
extern ROM char OutPhB[];
extern ROM char OutPhC[];

extern ROM char CalibrVStr1[];
extern ROM char CalibrVStr2[];
extern ROM char CalibrVStr3[];
extern ROM char CalibrVStr4[];

extern ROM char CalibrIStr1[];
extern ROM char CalibrIStr2[];
extern ROM char CalibrIStr3[];
extern ROM char CalibrIStr4[];

extern ROM char XXModeStr1[];
extern ROM char XXModeStr2[];
extern ROM char XXModeStr3[];
extern ROM char XXModeStr4[];

extern ROM char AlarmStatusStr0[];
extern ROM char AlarmStatusStr1[];
extern ROM char AlarmStatusStr2[];
extern ROM char AlarmStatusStr3[];

extern ROM char DelArchiveStr1[];
extern ROM char DelArchiveStr2[];
extern ROM char DelArchiveStr3[];
extern ROM char DelArchiveStr4[];
extern ROM char DelArchiveStr5[];
extern ROM char DelArchiveStr6[];

extern ROM char ArchLoading[];
extern ROM char NoArchiveError[];
extern ROM char ArchViewCaption[];

extern ROM char AlarmStatusStr0[];
extern ROM char AlarmStatusStr1[];
extern ROM char AlarmStatusStr2[];
extern ROM char AlarmStatusStr3[];

extern ROM char SaveInitStr[];

extern ROM char BaseLineStr1[];
extern ROM char BaseLineStr2[];
extern ROM char BaseLineStr3[];
extern ROM char BaseLineStr4[];
extern ROM char BaseLineStr5[];

extern ROM char ProtocolStr1[];
extern ROM char ProtocolStr2[];
extern ROM char ProtocolStr3[];
extern ROM char ProtocolStr4[];

extern ROM char ShortPhaseA[];
extern ROM char ShortPhaseB[];
extern ROM char ShortPhaseC[];

extern ROM char EmptyTime[];

extern ROM char StrTEMP[];
extern ROM char StrTEMP1[];
extern ROM char StrTEMP2[];
extern ROM char StrTEMP3[];

extern ROM char CalibrTStr1[];
extern ROM char CalibrTStr2[];
extern ROM char CalibrTStr3[];
extern ROM char CalibrTStr4[];
extern ROM char CalibrTStr5[];
extern ROM char CalibrTStr6[];
extern ROM char CalibrTStr7[];

extern ROM char CalibrHStr1[];
extern ROM char CalibrHStr2[];
extern ROM char CalibrHStr3[];
extern ROM char CalibrHStr4[];

extern ROM char TOnSetStr1[];
extern ROM char TOnSetStr2[];
extern ROM char TOnSetStr3[];
extern ROM char TOnSetStr4[];
extern ROM char TOnSetStr5[];
extern ROM char TOnSetStr6[];
extern ROM char TOnSetStr7[];

extern ROM char HeaterTStr1[];
extern ROM char HeaterTStr2[];
extern ROM char HeaterTStr3[];

extern ROM char XXTestStr1[];
extern ROM char XXTestStr2[];
extern ROM char XXTestStr3[];
extern ROM char XXTestStr4[];
extern ROM char XXTestStr5[];

extern ROM char StTimeBalansStr[];
extern ROM char StTimeBalansStr1[];
extern ROM char StTimeBalansStr2[];
extern ROM char StTimeBalansStr3[];
extern ROM char StTimeBalansStr4[];
extern ROM char StTimeBalansStr5[];
extern ROM char StTimeBalansStr6[];
extern ROM char StTimeBalansStr7[];

extern ROM char BalancingStr[];

#endif
