//---------------------------------------------------------------------------
#ifndef trstrH
#define trstrH
//---------------------------------------------------------------------------
#include "Defs.h"

extern ROM char trVersionValue[];
extern ROM char trVersionIntValue;
extern ROM char trDeviceType[];

extern ROM char fw_build_date[];

extern ROM char trEmptyStr[];
extern ROM char trError[];
extern ROM char trInvalidCommand[];
extern ROM char trInvalidParameter[];
extern ROM char trInvalidIntegerValue[];
extern ROM char trInvalidFloatValue[];
extern ROM char trInvalidDate[];
extern ROM char trInvalidTime[];
extern ROM char trCPExpected[];
extern ROM char trCP[];
extern ROM char trQuestion[];

extern ROM char trCommandDEVICETYPE[];
extern ROM char trCommandVER[];
extern ROM char trCommandTIME[];
extern ROM char trCommandTREAD[];
extern ROM char trCommandSHOW[];
extern ROM char trCommandSTART[];
extern ROM char  trCommandBUSY[];
extern ROM char  trCommandCLEAR[];
extern ROM char  trCommandCLEARALL[];
extern ROM char  trCommandPAUSE[];
extern ROM char  trCommandRESUME[];
extern ROM char  trCommandSTOP[];
extern ROM char  trCommandMONDATE[];
extern ROM char  trCommandTHRESH[];
extern ROM char  trCommandALARM[];
extern ROM char  trCommandMISC[];
extern ROM char  trCommandLOG[];
extern ROM char  trCommandMEM[];
extern ROM char  trCommandDEFAULT[];
extern ROM char  trCommandCALIBRT[];
extern ROM char  trCommandCALIBRA[];
extern ROM char  trCommandCALIBRH[];
extern ROM char  trCommandREADREC[];
extern ROM char  trCommandERROR[];
extern ROM char  trCommandERRORBIT[];
extern ROM char  trCommandDIAG[];
extern ROM char  trCommandSTABLE[];
extern ROM char  trCommandHEATTEST[];
extern ROM char  trCommandOFFLINEPARAM[];
extern ROM char  trCommandCHDIV[];
extern ROM char  trCommandNEGTG[];
extern ROM char  trCommandBALANCE[];
extern ROM char  trCommandSETPARAM[];
extern ROM char  trCommandRST[];
extern ROM char  trCommandREGDATA[];
extern ROM char  trCommandPROCTYPE[];
extern ROM char  trCommandAUTOBALANCE[];
extern ROM char  trCommandCLEARLOG[];

extern ROM char  trCommandREADINIT[];

extern ROM char  strPROCTYPE[];

#endif

