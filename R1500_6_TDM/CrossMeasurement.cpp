#ifndef __arm
   #include "Protocol.h"
   #include "ADC.h"
#else
   #include "ADCDevice.h"
#endif
#include "MeasurementDefs.h"
#include "SetChannels.h"
#include "Complex.h"
#include "CalcParam.h"
#include "RAM.h"
#include "Setup.h"
#include "Utils.h"
#include "Error.h"
#include "Graph.h"
#include "lcd.h"
#include "keyboard.h"
#include "Diagnosis.h"
#include "RTC.h"

#include "StrConst.h"
#include "Math.h"
#include "DefKB.h"
#include "DrawGraph.h"
#include "Link.h"
#include "TypesDef.h"
#include "GammaMeasurement.h"
#include "TRSTR.h"            //acm, 3-12-10 do this to define version string

#ifdef __arm
#define TickPerSecond 32
#endif

signed char FillFlag=0;
char XXMeasure=0;
int swSide=-1,swPhase=-1;
extern _TDateTime TimeNextMeas;      //����� ���������� ���������� ������
unsigned long TickCount=0;      //������� �������

extern unsigned int RunXX;

extern float AmplCoeff[MaxTransformerSide][3];

extern struct stDiagnosis Diag;  //acm, make known to Understand

extern void TestSetup(); // acm, do this so Understand resolves

void StepUp()
{
  if ((FillFlag==0)||(FillFlag==1)||(FillFlag==7)||(FillFlag==8)||(FillFlag==9)) {FillFlag++; swSide=0; swPhase=0; return; }
  if (FillFlag==10) {
     if (swPhase < MaxBushingCountOnSide-1) swPhase++;
     else {
        FillFlag++; swSide=0; swPhase=0; }
     return;
  }
  if ((FillFlag==2)||(FillFlag==3)||(FillFlag==4)||(FillFlag==5)) {
      if (swSide < MaxTransformerSide-1)
        {
        swSide++; swPhase=0;
        }
      else {FillFlag++; swSide=0; swPhase=0;}
      return;
  }
/*
  if ((swSide>=0)&&(swPhase<0))
      {
      if (swSide < MaxTransformerSide-1)
        {
        swSide++; swPhase=-1;
        }
      else {FillFlag++; swSide=0; swPhase=0;}
      }
  else
*/
  if ((swSide>=0)&&(swPhase>=0)) {//tg,C,Zk
     if (swPhase < MaxBushingCountOnSide-1)
        swPhase++;
      else  {
           if (swSide < MaxTransformerSide-1) {
              swSide++; swPhase=0;
           } else {FillFlag++; swSide=0; swPhase=0;}
      }
    }
  else
    {FillFlag++; swSide=0; swPhase=0;}
}

void StepDown()
{
  if ((FillFlag==0)||(FillFlag==1)||(FillFlag==7)||(FillFlag==8)||(FillFlag==9)) {FillFlag--; swSide=MaxTransformerSide-1; swPhase=MaxBushingCountOnSide-1; return; }
  if (FillFlag==10) {
     if (swPhase) swPhase--;
     else {
        FillFlag--; swSide=MaxTransformerSide-1; swPhase=MaxBushingCountOnSide-1;     }
     return;
  }
//  if (FillFlag!=7) swPhase=0;
  if ((FillFlag==2)||(FillFlag==3)||(FillFlag==4)||(FillFlag==5)) {
      if (swSide>0)
        {
        swSide--; swPhase=0;
        }
      else {FillFlag--; swSide=MaxTransformerSide-1; swPhase=MaxBushingCountOnSide-1;}
      return;
  }
/*
  if ((swSide>=0)&&(swPhase<0))
      {
      if (swSide > 0)
        {
        swSide--; swPhase=-1;
        }
      else {FillFlag--; swSide=MaxTransformerSide-1; swPhase=MaxBushingCountOnSide-1;}
      }
  else
*/
  if ((swSide>=0)&&(swPhase>=0)) //tg,C,Zk
    {
    if (swPhase >0)
      swPhase--;
    else
      {
      if (swSide >0)
        {
        swSide--; swPhase=MaxBushingCountOnSide-1;
        }
      else {FillFlag--; swSide=MaxTransformerSide-1; swPhase=MaxBushingCountOnSide-1;}
      }
    }
  else
    {FillFlag--; swSide=MaxTransformerSide-1; swPhase=MaxBushingCountOnSide-1;}
}

char ShowParameters(struct stInsulationParameters *Param, struct stDiagnosis *Diag,unsigned int ShowMask, KEY ch, int ParamPage, int DiagPage,int ShowErrors)
{
char NeedRedraw
#ifndef __arm
,Page=GetPage()
#endif
;
//unsigned int Phase;
//float f;
unsigned int Hour,Min,Sec;

//ROM char FillPauseStr4[]   = LANG("Date,Time","Date,Time");
//ROM char FillPauseStr5[]   = LANG("Read time","Read time");
//ROM char FillPauseStr6[]   = LANG("Gamma value","Gamma value");
//ROM char FillPauseStr7[]   = LANG("Temperature","Temperature");
//ROM char FillPauseStr8[]   = LANG("Trend","Trend");
//ROM char FillPauseStr9[]   = LANG("Temp.coeff","Temp.coeff");
//ROM char FillPauseStr10[]  = LANG("Alarm status","Alarm status");
//ROM char FillPauseStr11[]  = LANG("Tg,Capacity","Tg,Capacity");
//ROM char FillPauseStr12[]  = LANG("Zk","Zk");
//FillFlag=-1;
if (!ShowMask) ShowMask=1;

SetPage(ParamPage);
NeedRedraw=0;
   if ((ch==KB_UP)||(ch==KB_RIGHT)) {
      while (1)
        {
        StepUp();
        if (FillFlag > MaxShowValues)
          {
          FillFlag = 0; swSide=0;
          //break;
          }
        if (ShowMask&((unsigned int)1<<FillFlag))
        //&&
//            ((FillFlag<2)||(FillFlag==3)||((*Param).TrSideCount&(1<<swSide))))
          break;
        }
//     ClearLCD();
     NeedRedraw=1;
     TickCount =
#ifdef __arm
         GetTic32();
#else
         TimerCount;
#endif
     ch=KB_NO;
     }

   if ((ch==KB_LEFT)||(ch==KB_DOWN)) {
      while (1)
        {
        if (FillFlag == 0)
          {
          FillFlag = MaxShowValues; //break;
          } else
        StepDown();
        if (ShowMask&((unsigned int)1<<FillFlag))
           //&&((FillFlag<2)||(FillFlag==3)||((*Param).TrSideCount&(1<<swSide))))
          break;
        }
//     ClearLCD();
     NeedRedraw=1;
     TickCount =
#ifdef __arm
         GetTic32();
#else
         TimerCount;
#endif
     ch=KB_NO;
     }
#ifdef __arm
  if ((float)(GetTic32() - TickCount)
#else
  if ((float)(TimerCount - TickCount)
#endif
      > TickPerSecond*Setup.SetupInfo.OutTime) {

//      while (1)  // acm, 4-30, comment out a series of lines, which in effect mask certain parameters from getting displayed.
// my analysis indicates display output code was incomplete, as it doesn't read in from IHM which parameters to display, rather this 
// kludge code outputs certain parameters at certain times.  Currently, everything is displayed, which is more useful at moment.  Intent
// is to readin from Setup structure which parameters IHM desires to be displayed.
        {
        StepUp();
        if (FillFlag > MaxShowValues)
          {
          FillFlag = 0; //break;
          }
//        if ((ShowMask&(1<<FillFlag)))
////            &&((FillFlag<2)||((FillFlag==3))/*||((*Param).TrSideCount&(1<<swSide))*/))
//          break;
        }
    NeedRedraw=1;
    if (ShowErrors)
        ShowError(Setup.SetupInfo.OutTime,Error,0);
//    ClearLCD();
     TickCount =
#ifdef __arm
         GetTic32();
#else
         TimerCount;
#endif
    }

 //     while (1)
        {
//        if (ShowMask & (1<<FillFlag))
//          break;
//        FillFlag++;
//        if (FillFlag > MaxShowValues)
          {
//          FillFlag = 0; break;
          }
        }

//      if (ShowMask & (1<<FillFlag))
        {
        ClearLCD();
        SetFont(Font8x8);
        SetPage(ParamPage);
        switch (FillFlag)
        {
          case 0:
          {
          OutStringCenter(0,0,112,FillPauseStr4);
#ifdef __arm
          GET_TIME_INBUF();
#else
          GET_TIME();
#endif
          SetFont(Font6x8);
          OutDate(6,8);
          OutTime(60,8);
          NeedRedraw=1;
          break;
          }
          case 1:
          {
          OutStringCenter(0,0,112,FillPauseStr5);
          if (TimeNextMeas) {
             DecodeTime(DateTimeToTime(TimeNextMeas),&Hour,&Min,&Sec);
             IntToStrFormat(Hour,2);
             zerointstr(OutStr);
             OutString1(36,8,OutStr);
             OutString(36+CurFontWidth*2,8,TimeSeparator);
             IntToStrFormat(Min,2);
             zerointstr(OutStr);
             OutString1(36+CurFontWidth*3,8,OutStr);
          } else {
             OutString(36,8,EmptyTime);
          }
          break;
          }
          case 2:
          {
          SetFont(Font6x8);
          if (swSide<0) swSide=0;
          OutString(0,8,ShowPauseStr6);
          switch (swSide)
            {
            case 0: OutString(34,0,AlarmEnableStr3);break;
            case 1: OutString(34,0,AlarmEnableStr4);break;
            case 2: OutString(34,0,AlarmEnableStr5);break;
            case 3: OutString(34,0,AlarmEnableStr6);break;
            }
          OutFloat(24,8,(float)((*Param).TrSide[swSide].Gamma)/100.0,5,2);
          OutInt(70,8,Round((float)(*Param).TrSide[swSide].GammaPhase/100.0),3);
          OutString(54,8,PercentStr);
          OutString(90,8,DegrStr);
         break;
          }
          case 3:
          {
          if (swSide<0) swSide=0;
          SetFont(Font6x8);
          OutStringCenter(0,0,112,FillPauseStr8);
          switch (swSide)
            {
            case 0: OutString(16,8,AlarmEnableStr3);break;
            case 1: OutString(16,8,AlarmEnableStr4);break;
            case 2: OutString(16,8,AlarmEnableStr5);break;
            case 3: OutString(16,8,AlarmEnableStr6);break;
            }
          OutFloat(54+16,8,(float)((*Param).TrSide[swSide].Trend)*1*1.0001,5,1);  //acm, 4-27 change from .2 to 1
          break;
          }
          case 4:
          {
          SetFont(Font6x8);
          if (swSide<0) swSide=0;
          OutStringCenter(0,0,112,FillPauseStr9);
          switch (swSide)
            {
            case 0:OutString(16,8,AlarmEnableStr3);break;
            case 1:OutString(16,8,AlarmEnableStr4);break;
            case 2:OutString(16,8,AlarmEnableStr5);break;
            case 3:OutString(16,8,AlarmEnableStr6);break;
            }
          OutFloat(54+16,8,(float)((*Param).TrSide[swSide].KT)*0.002,6,3);
          break;
          }
          case 5:
          {
          SetFont(Font6x8);
          if (swSide<0) swSide=0;

          switch (swSide)
            {
            case 0: OutString(16,0,AlarmEnableStr3);break;
            case 1: OutString(16,0,AlarmEnableStr4);break;
            case 2: OutString(16,0,AlarmEnableStr5);break;
            case 3: OutString(16,0,AlarmEnableStr6);break;
            }
          if (!((*Param).TrSide[swSide].AlarmStatus&(~(1<<4)))){
            OutString(29,8,AlarmStatusStr0); break;
          }

          OutString(64,0,ShowPauseStr10);

//          (*Param).TrSide[swSide].AlarmStatus=0xFF;

//          if (((*Param).TrSide[swSide].AlarmStatus & (1<<0))||((*Param).TrSide[swSide].AlarmStatus & (1<<4)))
          if ((*Param).TrSide[swSide].AlarmStatus & (1<<0))
            OutString(0,8,AlarmStatusStr1);
          if ((*Param).TrSide[swSide].AlarmStatus & (1<<1))
            OutString(20,8,AlarmStatusStr2);
          if ((*Param).TrSide[swSide].AlarmStatus & (1<<2))
            OutString(56,8,AlarmStatusStr3);
          break;
          }
          case 6:
          {
          SetFont(Font6x8);
          if (swSide<0) swSide=0;
          if (swPhase<0) swPhase=0;
          switch (swSide)
            {
            case 0: OutString(6,0,AlarmEnableStr3);break;
            case 1: OutString(6,0,AlarmEnableStr4);break;
            case 2: OutString(6,0,AlarmEnableStr5);break;
            case 3: OutString(6,0,AlarmEnableStr6);break;
            }
          switch (swPhase)
            {
            case 0: OutString(60,0,OutPhA); break;
            case 1: OutString(60,0,OutPhB); break;
            case 2: OutString(60,0,OutPhC); break;
            }
          OutString(0,8,ShowPauseStr11);
          //OutFloat(0,8,(*Param).SideToSideData[swSide].Amplitude1[swPhase],5,1);
          //OutFloat(36,8,(*Param).SideToSideData[swSide].Amplitude2[swPhase],5,1);
//          OutFloat(30,8,(*Param).SideToSideData[swSide].CurrentPhase[swPhase],6,2);
//          OutFloat(72,8,(*Param).SideToSideData[swSide].PhaseShift[swPhase],6,2);
          SetPage(DiagPage);
          OutFloat(12,8,Diag->TgParam[swSide].tg[swPhase]*0.01*1.0001,5,2);// acm, 3-31-10, bad values AG noticed on display, cleared up after next measurment->implies uninitialized after powerup (see def. in GammaMeasurement...)
          OutFloat(82,8,Diag->TgParam[swSide].C[swPhase]*0.1*1.0001,5,1);
          //OutFloat(0,54,(*Param).TrSide[swSide].,5,1);
          break;
          }

          case 7:
          {
          SetFont(Font6x8);
          if (swSide<0) swSide=0;
          OutStringCenter(0,0,112,FillPauseStr7);
#ifndef OldR1500_6
         if (Setup.SetupInfo.ReadAnalogFromRegister) {
            OutInt(44,8,(int)ExtTemperature[0]-70,3); OutInt(76,8,(int)ExtTemperature[1]-70,3);
         } else {
#ifdef TDM
            OutInt(44,8,(int)ExtTemperature[0]-70,3); OutInt(76,8,(int)ExtTemperature[1]-70,3);
#else
            if (BoardType()&(1<<7)) {
               OutInt(0,8,ReadTemperature(0),3);
               OutInt(28,8,ReadTemperature(1),3);
               OutInt(56,8,ReadTemperature(2),3);
               OutInt(84,8,ReadTemperature(3),3);
            } else {
               OutInt(20,8,ReadTemperature(0),3);
               OutInt(48,8,ReadTemperature(1),3);
               OutInt(76,8,ReadTemperature(2),3);
            }
#endif
          }
#else
          switch (swSide)
            {
            case 0: OutString(16,8,AlarmEnableStr3);break;
            case 1: OutString(16,8,AlarmEnableStr4);break;
            case 2: OutString(16,8,AlarmEnableStr5);break;
            case 3: OutString(16,8,AlarmEnableStr6);break;
            }
          OutFloat(54+16,8,ReadTemperature(swSide),3,0);
          OutString(78+16,8,CStr);
#endif
         break;
          }
          case 8: {
          SetFont(Font6x8);
          OutString(35,0,FillPauseStr14);
          ReadActiveLoad();
          SetPage(ParametersPage);
          if (Setup.CalibrationCoeff.CurrentChannelOnPhase[0]||
              Setup.CalibrationCoeff.CurrentChannelOnPhase[1]||
              Setup.CalibrationCoeff.CurrentChannelOnPhase[2]) {
             if (Setup.CalibrationCoeff.CurrentChannelOnPhase[0]) {
                Sec=LoadCurrentA; if (LoadCurrentA-Sec>0.5) Sec++;
                if (LoadCurrentA>9999)
                   OutFloat(0,8,LoadCurrentA/1000.0,5,3);
                else
                   OutInt(2,8,Sec,5);
             }
             if (Setup.CalibrationCoeff.CurrentChannelOnPhase[1]) {
                Sec=LoadCurrentB; if (LoadCurrentB-Sec>0.5) Sec++;
                if (LoadCurrentB>9999)
                   OutFloat(39,8,LoadCurrentB/1000.0,5,3);
                else
                   OutInt(39,8,Sec,5);
             }
             if (Setup.CalibrationCoeff.CurrentChannelOnPhase[2]) {
                Sec=LoadCurrentC; if (LoadCurrentC-Sec>0.5) Sec++;
                if (LoadCurrentC>9999)
                   OutFloat(76,8,LoadCurrentC/1000.0,5,3);
                else
                   OutInt(76,8,Sec,5);
             }
          } else {
             OutInt(39,8,ExtLoadActive,5);
          }
          break;
          }

          case 9: {
          SetFont(Font6x8);
          OutString(16,4,FillPauseStr13);
#ifndef TDM
          OutInt(70,4,ReadHumidity(),3);
#else
          OutInt(70,4,ExtHumidity,3);
#endif
          break;
          }

          case 10:
          {
/*
          if (swSide<0) swSide=0;
          if (swPhase<0) swPhase=0;
          while (1)
            {
            if (Setup.ZkSetupInfo.CalcZkOnSide & (1<<swSide)) break;
            swSide++;
            if (swSide >= MaxCorrelationSide) break;
            }
          if (swSide >= MaxCorrelationSide)
             {
             swPhase=MaxBushingCountOnSide;
             TickCount =
#ifdef __arm
                     GetTic32()
#else
                     TimerCount
#endif
                     + Setup.SetupInfo.OutTime*TickPerSecond+1;
             break;
             }
*/
          SetFont(Font6x8);
          OutString(0,0,FillPauseStr12);
          switch (swSide)
            {
            case 0: OutString(18,0,AlarmEnableStr15);break;
            case 1: OutString(18,0,AlarmEnableStr15);break;
            case 2: OutString(18,0,AlarmEnableStr15);break;
            case 3: OutString(18,0,AlarmEnableStr15);break;
            }
          switch (swPhase)
            {
            case 0: OutString(72,0,CurrentTypeStr8); break;
            case 1: OutString(72,0,CurrentTypeStr9); break;
            case 2: OutString(72,0,CurrentTypeStr10); break;
            }
//          f=(*Param).SideToSideData[swSide].ZkPhase[swPhase];
          OutFloat(0,8,(*Param).SideToSideData[swSide].ZkAmpl[swPhase],6,3);
//          OutFloat(54,8,f,5,1);
          break;
          }
          default:
          {
          OutString(28,0,DeviceStr);
          SetFont(Font6x8);
          OutString(20,8,VersionStr);
          OutString(68,8,trVersionValue);  // acm, change 3-12 to make consistent with other modules scheme
          }
        }
        NeedRedraw=1;
      }

SetPage(Page);
return NeedRedraw;
}

/*void ReadCrossAmlPhase(unsigned int *Ampl,float *Phase,char NoCalibr,float PshHV,float Freq)
{

  char i;
  float f;
  struct TComp tmpComplex1,tmpComplex2,ResultComplex;

  ResultComplex.re=0;
  ResultComplex.im=0;
  if (NoCalibr==0) ReadData(1,1,0,0,chReadAllChannels);
  else ReadData(0,0,0,0,chReadAllChannels);
#ifndef __emulator
  __delay_cycles(8000);
#endif
  for (i=0;i<CalibrRepeat;i++) {
//      if ((i==0)&&(NoCalibr==0)) ReadData(1,0,0,1,chReadAllChannels);
//      else
      ReadData(0,0,0,0,chReadAllChannels);

      tmpComplex1.re=0;
//      tmpComplex1.im=0;
      ImpPhaseCalc((int *)(AddrSig[0]),
                   (int *)(AddrSig[3]),
                   UpLoadCount,
//                   &tmpComplex1.im,
                   &Freq,
                   &tmpComplex2.re,&tmpComplex2.im,&tmpComplex1.re,GammaPage0,GammaPage3);

//ClearLCD();
//SetFont(Font6x5);
//OutFloat(56,0,tmpComplex1.im,8,4);

      tmpComplex2.re=tmpComplex2.re*(float)ADCStep/GammaGainValue[AmplifIndex4];
      tmpComplex1.re=ResultComplex.re;
      tmpComplex1.im=ResultComplex.im;
      vec_add(&tmpComplex1,&tmpComplex2,&ResultComplex);

//OutFloat(0,0,tmpComplex2.re,8,4);
//OutFloat(0,8,tmpComplex2.im,8,4);
//OutInt(56,8,i,4);
//Redraw();
//WaitReadKey();

  }
  *Phase=ResultComplex.im-PshHV;

  i=0;
  while (*Phase>360) {
        *Phase-=360;
        if (i>CalibrRepeat) {
           *Phase=0;
           break;
        }
        i++;
  }

  //f=ResultComplex.re/CalibrRepeat*10.0;
  //ChAmplDiff*10.0;
  (*Ampl)=f;
  if (f-(*Ampl)>=0.5) (*Ampl)++;
}*/


//#ifndef NoCross

float KAmplTgC(char TrSide, char TrPhase)
{
return 1.0;
}

float dPhaseTg(char TrSide, char TrPhase)
{
return 0.0;
}

signed char StartCrossMeasure(struct stInsulationParameters *Param,float Freq, float PhaseShift)
{
extern unsigned int IsCurrentGain;
extern unsigned int CurrentGain;

  signed char Result;
  u32 i,j,Page1,Page2,n,AmplifIndex2,IsXXInitial;
  float f;
  struct TComp tmpComplex1,tmpComplex2,ResultComplex1,ResultComplex2,ResultComplex3,tmp;

  Result=0;
  if (Setup.ZkSetupInfo.AveragingForZk!=Setup.GammaSetupInfo.AveragingForGamma)
     Setup.ZkSetupInfo.AveragingForZk=Setup.GammaSetupInfo.AveragingForGamma;

  if ((IsDate(Setup.InitialZkParameters.Year,Setup.InitialZkParameters.Month,Setup.InitialZkParameters.Day))&&
      (IsTime(Setup.InitialZkParameters.Hour,Setup.InitialZkParameters.Min,0))) IsXXInitial=1;
  else IsXXInitial=0;

  SetCurrentGain2;
  CurrentGain=2;

  //���������
  for (j=0;j<MaxCorrelationSide;j++) {
      if (((*Param).TrSideCount&(1<<j)==0)||((*Param).TrSideCount&(1<<(j+1))==0)) continue;

      //Select Gain
      SetGain(1,AmplifIndex[j][BushingCh]);
      switch (AmplifIndex[j+1][BushingCh]) {
          case 0:AmplifIndex2=0;break;
          case 1:AmplifIndex2=4;break;
          case 2:AmplifIndex2=1;break;
          case 3:AmplifIndex2=5;break;
      }

//      if  (GainValue[AmplifIndex[j+1][BushingCh]]>=GammaGainValue[1]*0.9)
//          AmplifIndex2=1;
//      else
//          AmplifIndex2=0;

      SetGain(4,AmplifIndex2);

      //Read  ������ ������� ����, ���� 2(LV) �������� �� 4 �����
      for (i=0;i<MaxBushingCountOnSide;i++) {
          ResultComplex1.re=0;
          ResultComplex1.im=0;
          ResultComplex2.re=0;
          ResultComplex2.im=0;
          ResultComplex3.re=0;
          ResultComplex3.im=0;
          tmpComplex1.im=0;

          //�������� �� �� �� � ��� ��� ��� � � �
          SetInputChannel(j+1,chAaCrossPhases+i);

          for (n=0;n<Setup.ZkSetupInfo.AveragingForZk;n++) {

#ifdef __emulator
              SetInputChannel(j+1,chAaCrossPhases+i);
#endif
              ReadData(0,0,0,0,chReadAllChannels,i+1);

#ifndef __arm
              //Calc Phase-Phase
              switch (i) {
                case 0: Page1=GammaPage0; break;
                case 1: Page1=GammaPage1; break;
                case 2: Page1=GammaPage2; break;
                default: break;
              }
              Page2=GammaPage3;
#endif
              tmpComplex1.re=0;
              ImpPhaseCalc((s16 *)(AddrSig[i]),
                           (s16 *)(AddrSig[3]),
                           UpLoadCount,
                           &Freq,
                           &tmpComplex2.re,&tmpComplex2.im,&tmpComplex1.re,Page1,Page2);

              tmp.re=ResultComplex1.re;
              tmp.im=ResultComplex1.im;
              vec_add(&tmpComplex1,&tmp,&ResultComplex1);

              tmp.re=ResultComplex2.re;
              tmp.im=ResultComplex2.im;
              vec_add(&tmpComplex2,&tmp,&ResultComplex2);

              if (Setup.ZkSetupInfo.CalcZkOnSide) {
                 //HV - Current shift
                 Page2=GammaPage0+CurrentOnCh[i];
                 Page1=GammaPage0+i;
                 tmpComplex1.re=0;

                 ImpPhaseCalc((s16 *)(AddrSig[i]),
                              (s16 *)(AddrSig[CurrentOnCh[i]]),
                              UpLoadCount,
                              &Freq,
                              &tmpComplex2.re,&tmpComplex2.im,&tmpComplex1.re,Page1,Page2);

                 tmp.re=ResultComplex3.re;
                 tmp.im=ResultComplex3.im;
                 vec_add(&tmpComplex2,&tmp,&ResultComplex3);
              }
          }

          SetPage(ParametersPage);
          ResultComplex1.re/=Setup.ZkSetupInfo.AveragingForZk;//��������
          ResultComplex1.re*=AmplCoeff[j][i];
          ResultComplex1.re=(ResultComplex1.re*ADCStep)/GainValue[AmplifIndex[j][BushingCh]]/1000.0;
          ResultComplex1.re=ResultComplex1.re*0.7071/(Setup.GammaSetupInfo.GammaSideSetup[j].InputImpedance[i]/100.0);
          ResultComplex1.re=ResultComplex1.re/(PiValue2*Freq*(*Param).TrSide[j].C[i]/10.0*1E-12);

          ResultComplex1.re/=1000.0;

          if ((IsXXInitial)||(RunXX))
             (*Param).SideToSideData[j].Amplitude1[i]=ResultComplex1.re;//��������


          ResultComplex2.re/=Setup.ZkSetupInfo.AveragingForZk;//��������
          ResultComplex2.re*=AmplCoeff[j+1][i];
          ResultComplex2.re=(ResultComplex2.re*ADCStep)/GammaGainValue[AmplifIndex2]/1000.0;
          ResultComplex2.re=ResultComplex2.re*0.7071/(Setup.GammaSetupInfo.GammaSideSetup[j+1].InputImpedance[i]/100.0);
          ResultComplex2.re=ResultComplex2.re/(PiValue2*Freq*(*Param).TrSide[j+1].C[i]/10.0*1E-12);

          ResultComplex2.re/=1000.0;

          if ((IsXXInitial)||(RunXX))
             (*Param).SideToSideData[j].Amplitude2[i]=ResultComplex2.re;//��������

          //�� ���������� �����������
          ResultComplex2.re*=((float)Setup.GammaSetupInfo.GammaSideSetup[j].RatedVoltage/(float)Setup.GammaSetupInfo.GammaSideSetup[j+1].RatedVoltage);

//          ResultComplex2.re*=(((float)Setup.GammaSetupInfo.GammaSideSetup[j].InputImpedance[i]/(float)Setup.GammaSetupInfo.GammaSideSetup[j+1].InputImpedance[i])*
//                                                     ((float)Setup.GammaSetupInfo.GammaSideSetup[j].RatedVoltage/(float)Setup.GammaSetupInfo.GammaSideSetup[j+1].RatedVoltage));

          //����� �������� ������� ��������
          f=ResultComplex2.im;
          f-=PhaseShift/*PhasesShift[HVSide][0]*/;

          switch (i) {
            case 0:PhaseRound(&f);break;
            case 1:
                   PhaseRound(&f);f+=PhasesShift[HVSide][2];
                   PhaseRound(&f);//f+=ADCPhasesShift[HVSide][1];
                   break;
            case 2:
                   PhaseRound(&f);f+=PhasesShift[HVSide][2];
                   PhaseRound(&f);//f-=ADCPhasesShift[HVSide][1];
                   break;
          }

          PhaseRound(&f);

          //�� ���������� �����������
          if ((f > 90)&&(f < 270)) {
                if (f > 180)
                  f -=180.0;
                else f +=180.0;
          }
          if ((RunXX)||(!IsXXInitial))
             (*Param).SideToSideData[j].PhaseShift[i]=f;
          else {
              f-=Setup.InitialZkParameters.SideToSideData[j].PhaseShift[i];
              PhaseRound(&f);
              (*Param).SideToSideData[j].PhaseShift[i]=f;
          }

          ResultComplex2.im=f;
          ResultComplex1.im=0;

          //�������� ���
          ResultComplex3.re/=Setup.ZkSetupInfo.AveragingForZk;
          ResultComplex3.re=ResultComplex3.re*ADCStep/1000.0/1.4142;
          if (IsCurrentGain) ResultComplex3.re/=CurrentGain;
          ResultComplex3.re=ResultComplex3.re*Setup.CalibrationCoeff.CurrentK[i]+Setup.CalibrationCoeff.CurrentB[i];


          SetPage(ParametersPage);
//          if ((IsXXInitial)||(RunXX))
             (*Param).SideToSideData[j].CurrentAmpl[i]= ResultComplex3.re;

          //���� ����
          ResultComplex3.im+=PhasesShift[HVSide][3];

//          ResultComplex3.im+=PhasesShift[HVSide][0];
          if (i) 
             ResultComplex3.im+=(PhasesShift[HVSide][1]-ADCPhasesShift[HVSide][1]);

          PhaseRound(&ResultComplex3.im);
//          if ((IsXXInitial)||(RunXX))
             (*Param).SideToSideData[j].CurrentPhase[i]=ResultComplex3.im;


           //Set I -90;              
           if (ResultComplex3.im>45) ResultComplex3.im-=90;

           //ReSet Phase on 0 I
           ResultComplex1.im=ResultComplex3.im;
           ResultComplex2.im=ResultComplex3.im-ResultComplex2.im;
           PhaseRound(&ResultComplex2.im);
           ResultComplex3.im=0;

           //Calc Real Amp Values and Zk
/*
           if ((*Param).TrSide[j].C[i]!=0)
              ResultComplex1.re=((*Param).TrSide[j].SourceAmplitude[i]/10000.0)  /(2*PiValue*Freq*(*Param).TrSide[j].C[i]/10.0*1E-12);
           else ResultComplex1.re=0;
           if ((*Param).TrSide[j].C[i+1]!=0)
              ResultComplex2.re=((*Param).TrSide[j+1].SourceAmplitude[i]/10000.0)/(2*PiValue*Freq*(*Param).TrSide[j+1].C[i]/10.0*1E-12);
           else ResultComplex2.re=0;

           //�� ���������� �����������
           ResultComplex2.re*=((float)Setup.GammaSetupInfo.GammaSideSetup[j].RatedVoltage/(float)Setup.GammaSetupInfo.GammaSideSetup[j+1].RatedVoltage);
*/


//           ResultComplex1.re/=1000.0;
//           ResultComplex2.re/=1000.0;

           //Calc Zk
           if (Setup.ZkSetupInfo.CalcZkOnSide) {
              vec_sub(&ResultComplex1,&ResultComplex2,&tmpComplex1);

              if ((fabs(ResultComplex3.re/Setup.GammaSetupInfo.GammaSideSetup[j].RatedCurrent)>0.3)&&
                  ((fabs((*Param).SideToSideData[j].PhaseShift[i])>0.1)&&(fabs((*Param).SideToSideData[j].PhaseShift[i])<359.9))) {

                 vec_div(&tmpComplex1,&ResultComplex3,&tmp);
                 if ((IsXXInitial)||(RunXX)) {
                    (*Param).SideToSideData[j].ZkAmpl[i]=tmp.re;
                    (*Param).SideToSideData[j].ZkPhase[i]=tmp.im;
                 }
              }
           }

      }
  }


/*
           if (XXMeasure) continue;

              SetPage(ParametersPage);
              //tmpComplex2=U2 �������� LVSide(HV2Side,LV2Side)
              //tmpComplex1=U1 �������� HVSide
              tmpComplex1.im=0.0;
              //tmpComplex2.im=(float)((*Param).SideToSideData[j].PhaseShift[i])/100;
              tmpComplex2.im=(*Param).SideToSideData[j].PhaseShift[i];

              //�������� �� ���� ������� � ��������� ������
              tmpComplex1.re=(*Param).SideToSideData[j].Amplitude1[i]*KAmplTgC(HVSide,i);
              tmpComplex2.re=(*Param).SideToSideData[j].Amplitude2[i]*KAmplTgC(j+1,i);
              tmpComplex1.im+=dPhaseTg(HVSide,i);
              tmpComplex2.im+=dPhaseTg(j+1,i);
              while (tmpComplex1.im < 0)
                   tmpComplex1.im+=360;
              while (tmpComplex1.im >= 360)
                tmpComplex1.im -=360;
              while (tmpComplex2.im < 0)
                   tmpComplex2.im+=360;
              while (tmpComplex2.im >= 360)
                tmpComplex2.im -=360;
              vec_sub(&tmpComplex1,&tmpComplex2,&tmpComplex22);

              //���� �� �������� � ��� �� 0
              if ((*Param).SideToSideData[j].CurrentAmpl[i] > 0.0)
                {
                tmpComplex22.re/=(*Param).SideToSideData[j].CurrentAmpl[i];
                Error&=~((unsigned long)1<<erLoCurrent);
                }
              else {
                Error|=((unsigned long)1<<erLoCurrent);
                Result=1;
                //goto ExitLabel;
                }
              //����� ������� ����
              tmpComplex22.im-=(*Param).SideToSideData[j].CurrentPhase[i];

              while (tmpComplex22.im < 0.0)
                   tmpComplex22.im+=360;
              while (tmpComplex22.im >= 360)
                   tmpComplex22.im -=360;
              //(*Param).SideToSideData[j].ZkPhase[i]=Round(ResultComplex[i].im*100.0);

              vec_to_cpx(&tmpComplex22);
              ResultComplex3[i].re+=tmpComplex22.re;
              ResultComplex3[i].im+=tmpComplex22.im;

              }
          ResultComplex[i].re/=Setup.ZkSetupInfo.AveragingForZk;
          ResultComplex[i].im/=Setup.ZkSetupInfo.AveragingForZk;
          cpx_to_vec(&ResultComplex[i]);
          ResultComplex2[i].re/=Setup.ZkSetupInfo.AveragingForZk;
          ResultComplex2[i].im/=Setup.ZkSetupInfo.AveragingForZk;
          cpx_to_vec(&ResultComplex2[i]);
          ResultComplex3[i].re/=Setup.ZkSetupInfo.AveragingForZk;
          ResultComplex3[i].im/=Setup.ZkSetupInfo.AveragingForZk;
          cpx_to_vec(&ResultComplex3[i]);
          (*Param).SideToSideData[j].ZkAmpl[i]=ResultComplex3[i].re;
          (*Param).SideToSideData[j].ZkPhase[i]=ResultComplex3[i].im;


          A1/=Setup.ZkSetupInfo.AveragingForZk;
          ABCResultComp[i].re/=Setup.ZkSetupInfo.AveragingForZk;
          ABCResultComp[i].im/=Setup.ZkSetupInfo.AveragingForZk;
          cpx_to_vec(&ABCResultComp[i]);

          //���� ����� ��������� � ���������
          tmpComplex2.im=(ResultComplex[i].im-(PhShift[3]-PhShift[i]));
          if (!XXMeasure)
            tmpComplex2.im-=Setup.InitialZkParameters.SideToSideData[j].PhaseShift[i];
          while (tmpComplex2.im < 0)
               tmpComplex2.im +=360;
          while (tmpComplex2.im >= 360)
               tmpComplex2.im -=360;
          //if (!XXMeasure)
            if ((tmpComplex2.im > 90)&&(tmpComplex2.im < 270))
              {
              if (tmpComplex2.im > 180)
                tmpComplex2.im -=180.0;
              else tmpComplex2.im +=180.0;
              }
          //(*Param).SideToSideData[j].PhaseShift[i]=Round(tmpComplex2.im*100.0);
          (*Param).SideToSideData[j].PhaseShift[i]=tmpComplex2.im;

          //���� ���� ������� ��������
          SetPage(ParametersPage);
          f=ResultComplex2[i].im-(PhShift[0]-PhShift[3]) + 90;
           //������� ���� ���� ������� �1
           f-=(float)((*Param).TrSide[Setup.ZkSetupInfo.CurrentSide].SourcePhase[Setup.ZkSetupInfo.CurrentType])/100;
           //������� ����� ���� ������� �1
           f+=ABCResultComp[i].im+(*Param).SideToSideData[j].PhaseShift[i];
            while (f < 0)
                 f+=360;
            while (f >= 360)
                 f -=360;
            if ((f > 90)&&(f < 270))
              if (f > 180)
                f -=180.0;
              else f +=180.0;
            f+=(*Param).SideToSideData[j].PhaseShift[i]; //������ ��� ������� ������� ��������
            while (f < 0)
                 f+=360;
            while (f >= 360)
                 f -=360;
            //(*Param).SideToSideData[j].CurrentPhase[i]=Round(f*100.0);
            (*Param).SideToSideData[j].CurrentPhase[i]=f;

            //�������� ���
            f=ResultComplex2[i].re;
            if (!XXMeasure) {
              if (Setup.ZkSetupInfo.CurrentSide != HVSide)
                if (Setup.InitialZkParameters.SideToSideData[j].Amplitude1[i] && Setup.InitialZkParameters.SideToSideData[j].Amplitude2[i]){
                  f*=(float)Setup.InitialZkParameters.SideToSideData[j].Amplitude1[i]
                    /Setup.InitialZkParameters.SideToSideData[j].Amplitude2[i];
                  }
                else
                  {
                    Error|=((unsigned long)1<<erXXMode);
                    Result=1;
                    goto ExitLabel;
                  }
              }
            (*Param).SideToSideData[j].CurrentAmpl[i]= f;

          //����������
          tmpComplex2.re=ResultComplex[i].re;

          if (!XXMeasure) {
            if (Setup.InitialZkParameters.SideToSideData[j].Amplitude1 && Setup.InitialZkParameters.SideToSideData[j].Amplitude2){
              tmpComplex2.re*=(float)(Setup.InitialZkParameters.SideToSideData[j].Amplitude1[i])
                             /Setup.InitialZkParameters.SideToSideData[j].Amplitude2[i];
              }
            else
              {
                Error|=((unsigned long)1<<erXXMode);
                Result=1;
                goto ExitLabel;
              }
            }
          SetPage(ParametersPage);
          (*Param).SideToSideData[j].Amplitude2[i]=tmpComplex2.re;//��������

          tmpComplex1.re=A1;
          (*Param).SideToSideData[j].Amplitude1[i]=tmpComplex1.re;//��������
*/
//ExitLabel:
//Select Gain
SetGain(4,AmplifIndex[0][GammaCh]);
SetInputChannel(0,chGamma);
return Result;
}

int RunXXTest(void)
{

//��������� ���������� ���������
extern signed char MeasurementResult;
extern int  BusyFlag;

int IsBusy=BusyFlag;
int i,j;

  LED2Yellow;
  ClearLCD();
  SetFont(Font8x8);
  OutString(4,4,MeasuringStr);
  Redraw();
  SetDeviceBusy();

  if (Setup.ZkSetupInfo.CalcZkOnSide)
     MeasurementResult=StartMeasure(svNoSave,0);
  else MeasurementResult=msStatusErrorZk;

  //OutInfo
  ClearLCD();
  SetFont(Font8x8);
  OutString(32,0,XXTestStr1);

  if ((MeasurementResult>=0)&&(fabs(Parameters->Current[HVSide]<20))) {
     Setup.InitialZkParameters.Day  =Parameters->Day;
     Setup.InitialZkParameters.Month=Parameters->Month;
     Setup.InitialZkParameters.Year =Parameters->Year;  //-2000
     Setup.InitialZkParameters.Hour =Parameters->Hour;
     Setup.InitialZkParameters.Min  =Parameters->Min;

     for (i=0;i<MaxCorrelationSide;i++) {
         for (j=0;j<MaxBushingCountOnSide;j++) {
             //������ ���������
             //��������� ������� ���� 1 (HV), V
             Setup.InitialZkParameters.SideToSideData[i].Amplitude1[j]=Parameters->SideToSideData[i].Amplitude1[j];
             //��������� ������� ���� 2 (LV), V
             Setup.InitialZkParameters.SideToSideData[i].Amplitude2[j]=Parameters->SideToSideData[i].Amplitude2[j];
             //����� ��� �������. ���� 1, Deg *100
             Setup.InitialZkParameters.SideToSideData[i].PhaseShift[j]=Parameters->SideToSideData[i].PhaseShift[j];
             //��������� ���� , A
             Setup.InitialZkParameters.SideToSideData[i].CurrentAmpl[j]=Parameters->SideToSideData[i].CurrentAmpl[j];
             //����� ��� �������. ���� 1, Deg *100
             Setup.InitialZkParameters.SideToSideData[i].CurrentPhase[j]=Parameters->SideToSideData[i].CurrentPhase[j];
             //��������� Zk, ��  (��� �������� - ��� �� Zk, � Uk)
             Setup.InitialZkParameters.SideToSideData[i].ZkAmpl[j]=Parameters->SideToSideData[i].ZkAmpl[j];
             //����� Zk �������. ����, Deg *100
             Setup.InitialZkParameters.SideToSideData[i].ZkPhase[j]=Parameters->SideToSideData[i].ZkPhase[j];
         }
         Setup.InitialZkParameters.SideToSideData[i].CurrentChannelShift=Parameters->SideToSideData[i].CurrentChannelShift;
     }
     TestSetup();
     OutString(16,8,XXTestStr2);
     Redraw();
     WaitReadKeyWithDelay(5/*���*/,KB_NO);
     if (!IsBusy) SetDeviceNoBusy();
     return 1;
  }
  OutString(8,8,XXTestStr3);
  Redraw();
  WaitReadKeyWithDelay(5/*���*/,KB_NO);
  if (!IsBusy) SetDeviceNoBusy();
  return 0;
}

//#endif
