#ifndef __arm
   #include "Protocol.h"
#endif
#include <Math.h>
#include <stdlib.h>			// acm, case sensitive?  Yes, eliminates linker error.
#include "CalcParam.h"
#include "Defs.h"
#include "RAM.h"
#include "LCD.h"
#include "Graph.h"
#include "KeyBoard.h"
#include "Utils.h"
#include "TypesDef.h"
#include "Link.h"
#ifdef __arm
   #include "ADCDevice.h"
#else
   #include "ADC.h"
#endif


void LineApr(float *X,float *Y,unsigned int ArrLen,float *K, float *B)
{
  float SX=0,SY=0,f1=0,f2=0,tmp;
  unsigned int i;

for (i=0;i<ArrLen;i++) {
    SX=SX+X[i];
    SY=SY+Y[i];
};
SX=SX/ArrLen;
SY=SY/ArrLen;
for (i=0;i<ArrLen;i++) {
   tmp=(X[i]-SX);
   f1=f1+(Y[i]*tmp);
   f2=f2+(tmp*tmp);
};
f1=f1/f2;
*K=f1;
*B=SY-f1*SX;
}

float GetAveragedValue(s16 *Arr, unsigned int ArrLen,int ValueNum, int AverageWidth)
{

return Arr[ValueNum];
/*
int i,StartP,EndP,Count=0;
float Sum=0;

if (ValueNum>=AverageWidth) StartP=ValueNum-AverageWidth; else StartP=0;
EndP=ValueNum+AverageWidth;
if (EndP>ArrLen-1) EndP=ArrLen-1;
for (i=StartP;i<=EndP;i++) {
    Sum=Sum+Arr[i];
    Count++;
}
return Sum/Count;
*/
}

#ifndef ImpCalc

#define MinMaxLimit  0.1
#define MinMaxLimit2 0.6
#ifdef gamma_m
#define SinCount 100
#else
#define SinCount 30
#endif
#define DefPoints 10

  int Maxs[SinCount];
  int Mins[SinCount];
  float Cross[SinCount*2];



void CalcSinParameters(s16 *Sn, unsigned int ArrLen, struct TSinParameters *SinParam,char AmplOnly,char TestMaxSin, float ClcFreq)
{
#ifndef OldCalc
if (Read3Channel) NewCalcSinParameters(Sn,ArrLen,SinParam,AmplOnly,ReadFreq3,ClcFreq);
else NewCalcSinParameters(Sn,ArrLen,SinParam,AmplOnly,ReadFreq,ClcFreq);
#else

  long i,FromP,ToP;
  int MaxValue,MinValue,j,j2,n;
  float tmpMaxValue,tmpMinValue;
  float MaxAmpl,MinAmpl;
  int MaxAmplCount,MinAmplCount;
  int Count;
  int MaxFlag,MinFlag;
  float Freq,Freq1,Freq2;
  char CrossZone,Down,CrossFind;
  unsigned int CrossCount;
//  int Maxs[SinCount];
//  int Mins[SinCount];
//  float Cross[SinCount*2];
  float X[DefPoints*2+1];
  float Y[DefPoints*2+1];
  float Phase1,Phase2;
  float NullLine;
  float K,B;
  float Value;
  char Points;
  int AvgPhase,AvgAmpl;
  char Ovfl;


//NewCalcSinParameters(Sn,ArrLen,SinParam,AmplOnly);
//return ;

Points=DefPoints;
(*SinParam).Phase=0;
(*SinParam).Freq=0;
(*SinParam).Ampl=0;

//Find Min Max
MaxValue=0;
MinValue=5000;
for (i=0;i<ArrLen;i++) {
    if (Sn[i]>MaxValue) MaxValue=Sn[i];
    if (Sn[i]<MinValue) MinValue=Sn[i];
}
if (MaxValue-MinValue<20) return;
NullLine=(MaxValue+MinValue)/2;
//Find MinAmpl MaxAmpl
MaxAmpl=0;
MinAmpl=0;
MaxAmplCount=0;
MinAmplCount=0;
MaxFlag=0;
MinFlag=0;
tmpMaxValue=0;
tmpMinValue=32000;
Maxs[0]=32000;
Mins[0]=32000;
AvgAmpl=3;
Ovfl=0;

FromP=AvgAmpl;
if (fabs(GetAveragedValue(Sn,ArrLen,AvgAmpl,AvgAmpl))<fabs(Sn[0])) {
   for (i=AvgAmpl;i<ArrLen-AvgAmpl;i++) {
       Value=GetAveragedValue(Sn,ArrLen,i,AvgAmpl);
       if (fabs(Value-NullLine)<(MaxValue-NullLine)*MinMaxLimit) {
          FromP=i+1;
          break;
       }
   }
}

for (i=FromP;i<ArrLen-AvgAmpl;i++) {
    Value=GetAveragedValue(Sn,ArrLen,i,AvgAmpl);
    if (MaxAmplCount<SinCount-1) {
      if ((Value-NullLine)>(MaxValue-NullLine)*MinMaxLimit) {
         if (MaxFlag==0) MaxFlag=1;
         else {
              if (Value>tmpMaxValue) {
                 tmpMaxValue=Value;
                 Maxs[MaxAmplCount]=i;
              }
         }
      } else {
        if ((MaxFlag==1)&&(tmpMaxValue>0)) {
           if (Maxs[MaxAmplCount]>1) {
              MaxAmpl=MaxAmpl+GetAveragedValue(Sn,ArrLen,Maxs[MaxAmplCount],0);
              MaxAmplCount++;
           }
        }
        if ((Value-NullLine)<(MaxValue-NullLine)*MinMaxLimit2) {
           MaxFlag=0;
           tmpMaxValue=0;
        }
      }
    } else Ovfl=1;

    if (MinAmplCount<SinCount-1) {
      if ((Value-NullLine)<(MinValue-NullLine)*MinMaxLimit) {
         if (MinFlag==0) {MinFlag=1;}
         else {
              if (Value<tmpMinValue) {
                 tmpMinValue=Value;
                 Mins[MinAmplCount]=i;
              }
         }
      } else {
        if ((MinFlag==1)&&(tmpMinValue<3200)) {
           if (Mins[MinAmplCount]>1) {
              MinAmpl=MinAmpl+GetAveragedValue(Sn,ArrLen,Mins[MinAmplCount],0);
              MinAmplCount++;
           }
        }
        if ((Value-NullLine)>(MinValue-NullLine)*MinMaxLimit2) {
           MinFlag=0;
           tmpMinValue=32000;
        }
      }
    } else Ovfl=1;
}
if (MaxAmplCount>0) MaxAmpl=MaxAmpl/MaxAmplCount; else MaxAmpl=0;
if (MinAmplCount>0) MinAmpl=MinAmpl/MinAmplCount; else MinAmpl=0;

NullLine=(MaxAmpl+MinAmpl)/2;
MaxAmpl=MaxAmpl-NullLine;
MinAmpl=MinAmpl-NullLine;

(*SinParam).Ampl=MaxAmpl;

if (!TestMaxSin) Ovfl=0;

if ((AmplOnly)||(Ovfl)) {
   (*SinParam).Freq=0;
   (*SinParam).Phase=0;
   return;
}
//Find
CrossZone=0;
CrossCount=0;
FromP=0;
if (MaxAmplCount>0)
   if (Maxs[0]<Mins[0]) FromP=Maxs[0];
if (MinAmplCount>0)
   if (Mins[0]<Maxs[0]) FromP=Mins[0];
ToP=ArrLen-1;
if ((MaxAmplCount>0)&&(MinAmplCount>0)) {
   if (Maxs[MaxAmplCount-1]>Mins[MinAmplCount-1]) ToP=Maxs[MaxAmplCount-1];
   else ToP=Mins[MinAmplCount-1];
}
CrossFind=0;
Down=0;
//AvgPhase=0;
AvgPhase=2;

if ((MaxAmpl<4)||(MinAmpl>-4))
   Points=1;
else
   if ((MaxAmpl<9)||(MinAmpl>-9))
      Points=2;
   else
      if ((MaxAmpl<18)||(MinAmpl>-18))
         Points=3;


for (i=FromP;i<=ToP;i++) {
    Value=GetAveragedValue(Sn,ArrLen,i,AvgPhase)-NullLine;

    if (CrossCount>=SinCount*2) break;
    if (Value>MaxAmpl*0.8)
       CrossZone=0;
    if (Value<MinAmpl*0.8)
       CrossZone=0;
    if (CrossZone==0) {
      if ((Value<MaxAmpl*0.55)&&(Value>0)) {
        CrossZone=1;
        Down=1;
        CrossFind=0;
      }
      if ((Value>MinAmpl*0.55)&&(Value<0)) {
        CrossZone=1;
        Down=0;
        CrossFind=0;
      }
    }

    if (CrossZone==1) {
       if (CrossFind==0) {
          if (Down==1) {
             if (Value<0)/*NullLine*/ {
                j2=i+Points-1;
                if ((i>=Points)&&(j2<=ToP)) {
                  j=i-Points-1;
                  if (j<0) j=0;
                  for (n=j;n<=j2;n++) {
                      X[n-j]=n;
                      Y[n-j]=GetAveragedValue(Sn,ArrLen,n,AvgPhase)-NullLine;
                  }
                  LineApr((float*)&X,(float*)&Y,j2-j+1,(float*)&K,(float*)&B);
                  if (K==0) Cross[CrossCount]=i;
                  else Cross[CrossCount]=(0-B)/K;
                  CrossCount++;
                }
                CrossFind=1;
             }
          } else {
             if (Value>0)/*NullLine*/ {
                j2=i+Points-1;
                if ((i>=Points)&&(j2<=ToP)) {
                  j=i-Points-1;
                  if (j<0) j=0;
                  for (n=j;n<=j2;n++) {
                      X[n-j]=n;
                      Y[n-j]=GetAveragedValue(Sn,ArrLen,n,AvgPhase)-NullLine;
                  }
                  LineApr((float*)&X,(float*)&Y,j2-j+1,(float*)&K,(float*)&B);
                  if (K==0) Cross[CrossCount]=i;
                  else Cross[CrossCount]=(0-B)/K;
                  CrossCount++;
                }
                CrossFind=1;
             }
          }
        }
    }
}

Freq1=0;
Count=0;
if (CrossCount>2) {
   i=0;
   while (i<CrossCount-1) {
       if ((i+2)<=CrossCount-1) {
          Freq1=Freq1+(Cross[i+2]-Cross[i]);
          Count++;
       }
       i=i+2;
   }
}
if (Count>0) Freq1=Freq1/Count;

Freq2=0;
Count=0;
if (CrossCount>3) {
   i=1;
   while (i<CrossCount-1) {
       if ((i+2)<=CrossCount-1) {
          Freq2=Freq2+(Cross[i+2]-Cross[i]);
          Count++;
       }
       i=i+2;
   }
}
if (Count>0) Freq2=Freq2/Count;

Freq=0;
if ((Freq1>0)&&(Freq2>0)) {
   Freq=(Freq1+Freq2)/2;
   (*SinParam).Freq=ArrLen/Freq;
} else {
   if (Freq1>0) {
      (*SinParam).Freq=ArrLen/Freq1;
      Freq=Freq1;
   } else
      if (Freq2>0) {
         (*SinParam).Freq=ArrLen/Freq2;
         Freq=Freq2;
      }
}

(*SinParam).Freq=((*SinParam).Freq/ArrLen)*ReadFreq;

Phase1=0;
Count=0;
if (CrossCount>0) {
   i=0;
   while (i<CrossCount-1) {
         Phase1=Phase1+((float)Cross[i]-Freq*(int)(i/2));
         Count++;
         i=i+2;
   }
}
if (Count>0) Phase1=Phase1/Count;

Phase2=0;
Count=0;
if (CrossCount>1) {
   i=1;
   while (i<CrossCount-1) {
         Phase2=Phase2+((float)Cross[i]-Freq*(int)((i-1)/2));
         Count++;
         i=i+2;
   }
}
if (Count>0) Phase2=Phase2/Count;

if (Mins[0]>Maxs[0]) {
   Phase1=Phase1-Freq/2;
   Phase2=Phase2-Freq;
} else {
   Phase2=Phase2-Freq/2;
}

if (Freq>0) (*SinParam).Phase=360.0-((Phase1+Phase2)/2.0/Freq*360.0);
else (*SinParam).Phase=0;
PhaseRound(&(*SinParam).Phase);
/*
if Freq>0 then begin
   MaxAmpl:=0;
   MinAmpl:=0;
   for j:=0 to MaxAmplCount-1 do begin
      i:=Maxs[j];
      MaxAmpl:=MaxAmpl+((Sn[i]-NullLine)*sin(2*pi*i/Freq+((*SinParam).Phase/360*2*pi)));
      MinAmpl:=MinAmpl+(sin(2*pi*i/Freq+((*SinParam).Phase/360*2*pi))*sin(2*pi*i/Freq+((*SinParam).Phase/360*2*pi)));
   end;
   for j:=0 to MinAmplCount-1 do begin
      i:=Mins[j];
      MaxAmpl:=MaxAmpl+((Sn[i]-NullLine)*sin(2*pi*i/Freq+((*SinParam).Phase/360*2*pi)));
      MinAmpl:=MinAmpl+(sin(2*pi*i/Freq+((*SinParam).Phase/360*2*pi))*sin(2*pi*i/Freq+((*SinParam).Phase/360*2*pi)));
   end;
   (*SinParam).MaxAmpl:=MaxAmpl/MinAmpl;
end;
*/

#endif
}


#endif

float CalcAmpl(s16 *a, int ToX)						//�acm,�*a == pointer array[ToX] to 16 bit sampled null line adjusted data.  Peak<<3V.
//#define quarter (unsigned int)(ReadFreq/60*0.45);
#define quarter 20;
{
//  unsigned int n,lock;
//  int atmp;
//  int Min,Max;
  int i;
//  long ZeroTmp;
//  int Zero;

   unsigned long Sum;

// double Sum;


/*
  ZeroTmp=0;
  for (i=0;i<ToX;i++) ZeroTmp=ZeroTmp+a[i];
  Zero=ZeroTmp/ToX;
*/
  //Zero=0;
/*
  Sum=0;                         //���� �������� ������� ����� �����, less accurate
  for (i=0;i<ToX;i++)            //�� �������� � ����	more resistant to noise
    { //a[i]=a[i]+1-rand()%11;
//    Min=a[i]-Zero;
//    Min=a[i];
//    if (Min>0) Sum=Sum+Min;
//    else Sum=Sum-Min;
    Sum+=abs(a[i]);
//    Sum=Sum+((float)a[i]-Zero)*((float)a[i]-Zero);
    }
  Sum=(double)Sum*AmplCalcMul/ToX*1.00020035715263;
  return Sum;
  //return sqrt(Sum*2/ToX);
*/

  Sum=0;                         //���� �������� ������� ����� �����,  this alg calculates less accurate
  for (i=0;i<ToX;i++)            //�� �������� � ����  but more noise immune
    {
    Sum+=abs(a[i]);					// acm, long overflow check: 4700*32000==150,400,000. << max long == 2,147,483,648.  ABS() equivalent to full wave rectification.
    }
  Sum=(float)Sum/ToX*AmplCalcMul;  	// 2.07 (float) from (double) acm, AmplCalcMul approximately Pi/2, why?  ABS() analogous to full wave rectification. 
  return Sum;						 	// acm, Sum==Vpeak=Vav*pi/2.  Verified, Sum used to store Source and PhaseAmplitudes.
  //return sqrt(Sum*2/ToX);

/*
  n=0; Sum=0; lock=0; //���� �������� ������� ������ � �������,    this alg calculates more acccurate
  Min=a[0]; Max=Min;        //�� ������ ��� ���������� ����			but less noise immune
  for (i=1;i<ToX;i++)
    {
//    a[i]=a[i]-(int)Zero;
    atmp=a[i];
    if (Min > atmp) Min=atmp;
    if (Max < atmp) Max=atmp;
    if (!lock)
      {
      if ((int)(atmp^a[i-1])<0)      //����� �������� ����� ����
        {
        lock=quarter;     //��������� ����� �� 0.45 �������
        if (n)
          if (atmp > a[i-1])
            Sum=Sum - Min;
          else
            Sum=Sum + Max;
        n++;
        Min=0; Max=0;
        }
      }
    else lock--;
    }
  return (float)Sum/(n-1);
*/

/*
  Sum=0;
  for (i=0;i<ToX;i++)
      Sum=Sum+(double)a[i]*a[i];
  return pow((double)Sum/ToX,0.5)*(double)1.4142135623730950488016887242097;
*/
}

//******* Changes below from 2.07 CalcFreq fix
//#define MaxAvg 24
//#define AmplPercent 0.1
//#define AvgAmpl 0.00

/*static int PrepareApr(s16 *a,s16 *x, s16 *y, int ToX, int n, float MaxAmpl,
#ifdef __arm
                                                                     int Up)
#else
                                                                     char Up)
#endif
{
  int i,i00,i1;
  int min;
  int max;

  max = MaxAmpl*AmplPercent;
//  max2 = (int)(MaxAmpl*AvgAmpl);
  //if (max<1) max=1;

  min = -max;
  if (Up) {
     //���� min
     i00 = n;
     while (a[i00]>min) {
       if (i00 > 0) i00--;
       else break;
     }
     //���� max
     i1 = n;
     while (a[i1]<max) {
       if (i1 < ToX) i1++;
       else break;
     }
  }else {
     //���� min
     i00 = n;
     while (a[i00]<max) {
       if (i00 > 0) i00--;
       else break;
     }
     //���� max
     i1 = n;
     while (a[i1]>min) {
       if (i1 < ToX) i1++;
       else break;
     }
  }
  //������������   overwriting
  if (i1-i00+1>MaxAvg) {
     i00+=(((i1-i00+1)-MaxAvg) / 2);
     i1-=(((i1-i00+1)-MaxAvg) / 2)-1;
  }
  if (i1==i00) {
     if (i00) i00--;
     if (i1<ToX) i1++;
  }

  for (i=i00;i<=i1;i++) {
    x[i - i00] = i ;
    y[i - i00] = a[i];
//    y[i - i0] =  GetAveragedValue(a,ToX,i,max2);
  }
//  i0_ := i0;
  return i1 - i00;
}*/


/*static float approximate(s16 *x,s16 *y,int n)
{
  float sx, sy, f1, f2, f;
  int i;
  s16 sumx,sumy;

  sumx = 0;
  sumy = 0;
  f1 = 0;
  f2 = 0;
  for (i=0;i<=n;i++) {
    sumx = sumx + x[i];
    sumy = sumy + y[i];
  }
  sx = (float)sumx / (n+1);
  sy = (float)sumy / (n+1);
  for (i=0;i<=n;i++) {
    f=(x[i] - sx);
    f1 = f1 + (f*y[i]);
    f2 = f2 + (f*f);
  }
  if (f2==0) f2= 1;
  f1= f1 / f2;
  if (f1==0) f1= 1;
  f1=(-(sy - f1*sx))/f1;
  return f1;

}*/

/*float CalcFreq(int *a, unsigned int ToX, float ReadFr, float *Phase, float *Ampl)
{
#define quarter (int)(ReadFreq/60*0.45);
  unsigned int i,n;
  float Min,Max;
  char lock;
  float Result,Sred,Sum;

  *Phase=0.0;
  *Ampl=CalcAmpl(a, ToX);
  if (*Ampl<100) {
     *Ampl=0.0;
     return 0.0;
  }

  Sred=0;
  for (i=0;i<ToX;i++)
    Sred=Sred+(float)a[i];
  Sred/=ToX;

  n=0;
  Min=-1;
  for (i=1;i<ToX;i++)
    if (((float)a[i-1]<Sred)&&((float)a[i]>=Sred))
      {
      n++;
      if (Min<0)
        Min=(float)i-((float)a[i]-Sred)/((float)a[i-1]-Sred);
      else
        Max=(float)i-((float)a[i]-Sred)/((float)a[i-1]-Sred);
      }

  Result=1.0/((Max-Min)/(n-1)/ReadFr);

return Result;
}*/

/*float CalcFreq(s16 *a, unsigned int ToX, float ReadFr, float *Phase, float *Ampl)
   #define MaxCrossCount 100
{
  unsigned int i;
  int  k, nUp,nDn,EndCross;
  float sumUp,sumDn, old_xUp, old_xDn, b;
  s16 old_a;
  s16 x[MaxAvg];
  s16 y[MaxAvg];
#ifdef __arm
  int
#else
  char
#endif
  startUp,startDn;
  float CrossUp[2],CrossDn[2];
  unsigned int CrossUpCount, CrossDnCount, Count;
  float Result;
  
  *Phase=0.0;
  *Ampl=CalcAmpl(a, ToX);
  if (*Ampl<300) {
     *Ampl=0.0;
     return 0.0;
  }

  b = 0;
  nUp = 0;
  nDn = 0;
  sumUp = 0;
  sumDn = 0;
  old_a = a[0];
  old_xUp = 0;
  old_xDn = 0;
  startUp = 0;
  startDn = 0;
  EndCross=0;
  CrossUpCount=0;
  CrossDnCount=0;
  for (i=1;i<ToX;i++) {
    if ((a[i] >=0)&&(old_a<0)) {                             //����� �������, ��������������  found crossing, do approx
      if (CrossUpCount<MaxCrossCount) {
         k = PrepareApr(a, x, y, ToX, i, *Ampl, 1);     //�������������� ������� ��� �������������  getting ready arrays for approx
         if (k>0) b=approximate(x, y, k);
         else b=i;
         CrossUp[0]=CrossUp[1];
         CrossUp[1]=b;
         CrossUpCount++;
         if (startUp) {
            sumUp = sumUp + b - old_xUp;
            nUp++;                                    //����� ���������  number of crossings
            old_xUp = b;
            EndCross=1;

         } else {
           startUp = 1;                               //�������� ������  subtracting the first
           old_xUp = b;
         }


      }
    }

    if ((a[i] <= 0) && (old_a > 0)) {        //����� �������, ��������������  found crossing do approx
       if (CrossDnCount<MaxCrossCount) {
          k = PrepareApr(a, x, y, ToX, i, *Ampl, 0);     //�������������� ������� ��� ������������� getting array ready for approx
          if (k>0) b = approximate(x, y, k);
          else b = i;
          CrossDn[0]=CrossDn[1];
          CrossDn[1]=b;
          CrossDnCount++;
          if (startDn) {
             sumDn = sumDn + b - old_xDn;
             nDn++;                                      //����� ���������
             old_xDn = b;
             EndCross=2;
          } else {
             startDn = 1;                               //�������� ������
             old_xDn = b;
          }
       }
    }
    old_a = a[i];

    if ((i&0xFF)==0) ScanMessages();

  }


  //�������� ���������  subtracting the last
  if (EndCross==1) {
      sumUp = sumUp-(b-CrossUp[0]);
//      CrossUpCount--;
      nUp--;
  } else {
     if (EndCross==2) {
        sumDn = sumDn- (b-CrossDn[0]);
//        CrossDnCount--;
        nDn--;
     }
  }
  Count=0;
  if ((!nUp)&&(!nDn)) Result =0;			// acm, (!nUp || !nDn) (demorgan equiv==!(nUp&&nDn)) to avoid all divide/0 simplifies logic some...
  else {
     if (nUp) {
        sumUp = 1/((sumUp / nUp)/ReadFr);
        Count++;
     } else sumUp = 0;
     if (nDn) {
        sumDn = 1/((sumDn / nDn)/ReadFr);
        Count++;
     } else sumDn = 0;
     Result = (sumUp+sumDn)/Count;
  }
return Result;
}*/
//**** new CalcFreq first used in 2.07 Algorithm by alexjr and implementation by alexsr
float CalcFreq(s16 *a, unsigned int ToX, float ReadFr, float *Phase, float *Ampl) 
#define MaxCrossCount 100
{
  unsigned int i;
  int  nUp, nDn, startUp, startDn, sumUp, sumDn,before,after;
  float negThreshold, posThreshold;
  float AmplPercent = 0.5;
  //s16 old_a;
  float Result = 0.0;
  
  *Phase=0.0;
  *Ampl=CalcAmpl(a, ToX);
  if (*Ampl<300) {
    *Ampl=0.0;
    return 0.0;
  }
  posThreshold = *Ampl * AmplPercent;  //amplitude hit first to start look for next crossing
  negThreshold = -posThreshold;
  
  nUp = 0;
  nDn = 0;
  sumUp = 0;
  sumDn = 0;
  //old_a = a[0];
  startUp = 0;
  startDn = 0;
  
  for (i=10;i<ToX-10;i++) 
  {
    before = 0;
    after = 0;
    for(u8 j=1;j<=10;j++)
    {
      before += a[i-j];
      after += a[i+j];
    }
    //if ((a[i] >=0)&&(old_a<0)) {         //found crossing
    if((after >= 0) && (before < 0))
    {
      if (nUp < MaxCrossCount) {
        
        if (startUp) {
          sumUp = i - startUp;          //overall interval involvint nUp crosings
          nUp++;                        //number of intervals (intersections - 1)
        } 
        else
          startUp = i;                  //First intersection positive
        
        if(i+10<ToX)
          i += 10;
        else
          i = ToX;
        
        while (a[i] < posThreshold && i < ToX) i++;  //2.07 finds 50% of rms amplitude in order to start looking for next crossing
      }
    }
    
    //if ((a[i] <= 0) && (old_a > 0)) {    //found crossing
    if((after <= 0) && (before > 0))
    {
      if (nDn < MaxCrossCount) {
        
        if (startDn) {
          sumDn = i - startDn;          //overall interval involvint nDn crosings
          nDn++;                        //number of intervals (intersections - 1)
        } 
        else
          startDn = i;                  //First intersection negative
        
        if(i+10<ToX)
          i += 10;
        else
          i = ToX;
        
        while (a[i] > negThreshold && i < ToX) i++;  //2.07 finds 50% of rms amplitude in order to start looking for next crossing
      }
    }
    //old_a = a[i];
    
    if ((i&0xFF)==0) ScanMessages();
    
  }
  
  if (nUp || nDn)
    Result = ReadFr* (float)(nUp + nDn)/ (float)(sumUp + sumDn);
  
  return Result;
}
//******* End changes to CalcFreq
/*
static float _Ampl(float x, float y)
{
//  return pow(x*x + y*y,0.5);
  return sqrt(x*x + y*y);
}

static float _Degr(float x,float y)
{
  float a,Result;

  a = _Ampl(x, y);
  if (a!=0) {
   Result = (acos(y/a))*180/PiValue;
   if (x<0) Result = -Result;
  } else Result = 0;
  while (Result < 0) Result = Result + 360;
  return Result;
}
*/
/*
void ImpPhaseCalc(int *a, int *x, int NPoints, float *Freq,float *Ampl, float *Faza, float *Ampl1,char Page0,char Page1)
{
  int i,ai,ai1,N;//,j,N;
//  float fSum,fSum2,fSum3;
//  float a_,b_,f_,s_;
  double fSum,fSum2,fSum3;
  float a_,b_;
  double f_,s_;
  char Page=GetPage();

    f_=ReadFreq;
    SetPage(Page0);

    if (*Freq<=0.5) {
      *Freq=CalcFreq(a,NPoints,f_,&a_,&b_);
    }

    N=Round(( (float)NPoints/(f_/(*Freq)) ));
    NPoints=Round(f_/(*Freq)*(N-1))+1;
//    NPoints=Round(f_/(*Freq)*(N));


    if (*Freq<=5) {
       *Freq=*Ampl=*Faza=0;
        return;
    }
    if (*Ampl1<0.5) {
       SetPage(Page0);
       *Ampl1=CalcAmpl(a,NPoints);
    if (*Ampl1<0.01) {*Ampl1=0;return;}
    }
    SetPage(Page1);
    *Ampl=CalcAmpl(x,NPoints);
    if (*Ampl<0.01) {*Ampl=0;return;}

  f_=(*Ampl1)/(*Ampl);
  fSum=fSum2=fSum3=0;
  s_=0;
  *Faza=0;
  for (i=0;i<NPoints;i++) {
    SetPage(Page0);
    ai=a[i];
//    a_=(float)a[i]/(*Ampl1);
    a_=(double)ai/(*Ampl1);
    if (i)
      ai1=a[i-1];
    SetPage(Page1);
    b_=(double)x[i]/(*Ampl);
    if (i)
      {
      if (ai > ai1)
        s_+=(double)ai-(double)f_*x[i];
      else s_+=(double)f_*x[i]-ai;
      }
    //fSum=fSum+((long)ai*ai);
    //fSum2=fSum2+((long)ai*x[i]);
    //fSum3=fSum3+((long)x[i]*x[i]);
    fSum=fSum+(a_*a_);
    fSum2=fSum2+(a_*b_);
    fSum3=fSum3+(b_*b_);

    if ((i&0xFF)==0) ScanMessages();

  }
  //fSum2*=f_;
  //fSum3*=f_*f_;
  //fSum=fSum2*2/(fSum+fSum3);
  fSum=fSum2/fSum;
  if (fSum < -1) fSum=-1;
  if (fSum > 1)  fSum=1;
*/
  /*57.295779513;//180/PiValue;*/
/*
  *Faza=acos(fSum);
  fSum=fSum2/fSum3;
  if (fSum < -1) fSum=-1;
  if (fSum > 1)  fSum=1;
*/
  /*57.295779513;//180/PiValue;*/
/*
  *Faza+=acos(fSum);
  *Faza=*Faza*57.295779513/2.0;
  if (s_ < 0) *Faza=360-*Faza;
  SetPage(Page);

  }
*/
    //��������� ��������   // "International" algorithm
	// acm, guess,
	// *a input array ref, e.g. chA
	// *x input array sig, e.g. Unn current
	// NPoints input const Uploadcount==4800
	// *Freq input ref frequency previously calculated via ReadSourcePhases()
	// *Ampl output, sig amplitude
	// *Faza output, ref/sig phase?
	// *Ampl1 input, generally 0.
	// Page0/Page1 unused.
void ImpPhaseCalc(s16 *a, s16 *x, int NPoints, float *Freq,float *Ampl, float *Faza, float *Ampl1,char Page0,char Page1)
{
 int N_,i,k;
 float a_,b_,f_,delta;
 long a1,a2;//,x1;
 int x1;

//    return; //DE 2-8-16 test skipping to locate reset
    f_=ReadFreq;

    if (*Freq<0.5) {
       SetPage(Page0);
//       *Freq=CalcFreq(a,NPoints-120,f_,&a_,&b_);
       *Freq=CalcFreq(a,NPoints,f_,&a_,&b_);		// acm, NPoints == UploadCount == 4800, array max 5120 elements
    }

//  OutInt(0,0,NPoints,4);
//  OutFloat(40,0,ReadFreq,7,2);
//  OutFloat(0,8,*Freq,7,2);
//  Redraw();


    if (*Freq<=5) {
       *Freq=*Ampl=*Faza=0;
        return;
    }

//    *Freq=50;

    N_=( (float)NPoints/(f_/(*Freq)) );	// acm, 4800.0/(6000.0/60.0)==48

    N_=f_/(*Freq)*(N_-1)+1;				// acm, 6000.0/60.0*(48-1)+1==4701
//    N_=f_/(*Freq)*(N_-1);
//    N_=Round(f_/(*Freq)*(N_-1));

//    ReCalcNullLines(N_,1);

    if (*Ampl1<0.5) {
       SetPage(Page0);
       *Ampl1=CalcAmpl(a,N_);
       if (*Ampl1<5.0) {
          *Freq=*Ampl=*Faza=0;
          return;
       }
    }

    a_=f_/(*Freq)/4;
    k=a_;
    if (a_-k>=0.5) k++;

    delta=((float)k/a_)*90.0-90.0;

    SetPage(Page0);
    a1=a[0];
    a2=a[k];
    SetPage(Page1);
    x1=x[0]/2;
    a_ = a1*x1;
    b_ = a2*x1;
    for (i=1;i<N_-1;i++) {
        SetPage(Page0);
        a1=a[i];
        a2=a[i+k];
        SetPage(Page1);
        x1=x[i];
        a_= a_+a1*x1;
        b_= b_+a2*x1;
        
        if ((i&0xFF)==0) ScanMessages();

    }
    SetPage(Page0);
    a1=a[N_-1];
    a2=a[N_-1+k];
    SetPage(Page1);
    x1=x[N_-1]/2;
    a_= (a_ + a1*x1)/N_;
    b_= (b_ + a2*x1)/N_;

    if (fabs(a_)>0.00001)
      f_ =atan(fabs(b_)/fabs(a_));
    else f_=PiValue/2;

//    f_=PiValue/2; //DE 2-8-16 force f_
    
    if((a_>0)&&(b_>0)) f_ = f_;
    else
      if((a_>0)&&(b_<0)) f_ = -f_;
      else
        if((a_<0)&&(b_>0)) f_ = PiValue - f_;
        else
          if((a_<0)&&(b_<0)) f_ = PiValue + f_;

     if (f_>0) *Faza=360.0-f_*180/PiValue-delta;
     else *Faza= (-(f_*180/PiValue))-delta;

    SetPage(Page1);
    *Ampl=CalcAmpl(x,N_);
    if (*Ampl<0.01) *Ampl=0;

//if ((*Freq >= 50.5)||(*Freq <= 49.5)){
  //OutFloat(0,0,(float)*Freq,8,4);
  //OutFloat(0,8,(float)N_,8,4);
  //OutFloat(0,0,(float)b_,13,6);
  //OutFloat(0,8,(float)a_,13,6);
  //Redraw();
  //WaitReadKey();
  //}

}


//--------------------------------------------------------------------
//��������������� ������� ��� GetGarmonSvertka
//  _Ampl - ������ ���������
//  _Degr - ������ ����
//--------------------------------------------------------------------
static float _GAmpl(float x, float y)
{
return sqrt(x*x+y*y);
}

static float _GDegr(float x, float y)
{
float a;
a=_GAmpl(x,y);
if (a!=0) {
   a=acos(y/a)*180.0/PiValue;
   if (x<0) a=-a;
} else return 0;
PhaseRound(&a);
return a;
}

/*
  Freq  - ������� ��������� � ��
  dX    - ��� �� ������� � ������������
*/
void GetGarmonSvertka(s16 *Channel,int FromX,int ToX,float Freq,float dX,float *A1,float *F1)
{
float Fsvertka,r1,Ys,Xs,cs,sn,cs1,sn1,tmp;
int i;

Fsvertka=dX*PiValue2*Freq;

Xs=0;
Ys=0;
*A1=0;
*F1=0;

cs1=cos(Fsvertka);
sn1=sin(Fsvertka);
cs=1;
sn=0;
for (i=FromX;i<ToX;i++)
    {
    r1=Channel[i];
    Xs+=r1*cs;
    Ys+=r1*sn;

    tmp=sn;
    sn=tmp*cs1+sn1*cs;
    cs=cs*cs1-tmp*sn1;
    }

*F1=360-_GDegr(Xs,Ys);
PhaseRound(F1);
*A1 = _GAmpl(Xs,Ys)*2.0/(float)(ToX-FromX+1);
}

