#ifndef LCD_h
#define LCD_h

#include "..\COMMON\LCD112x16.h"
#include "..\COMMON\Keyboard.h"

#define DisableLCD(Symbol) RedrawEnabled=0
#define EnableLCD() RedrawEnabled=1


extern unsigned char Video_Ram[];

#define Redraw() NeedRedraw()

void InitLCD(void);
void ClearLCD(void);
//void Redraw(void);
void RedrawInline(void);
#define ReDraw Redraw

#ifdef __emulator
   #define RedrawInline Redraw
#endif


void OutStringMask(unsigned int X,unsigned int Y,char const *String, char Mask);
void OutPicture(unsigned int X,unsigned int Y, char const *ptr);
void PutPixel( unsigned int x, unsigned int y);
void ClearRect(unsigned int X,unsigned int Y,unsigned int PicHeight,unsigned int PicWidth);

void SetLCDShim(char Out);
void SetLightOff(void);
void SetLightOn(void);


void TestLCD(void);

#endif

