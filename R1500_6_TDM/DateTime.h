#ifndef _datetime_h
#define _datetime_h

#define  HomeYear 0
#define  HomeYearValue 2000

#pragma pack(1)

struct StDateTime{
     char Sec,Min,Hour;
     unsigned short Year;
     char Month,Day;
};

struct StOutDateTime{
     char Hour,Min,Sec,DSec;
     char Day,Month,Year;
     char X1,Y1,X2,Y2,Font;
};
#pragma pack()


#define equStDateTime sizeof(struct StDateTime)

extern struct StDateTime DateTime;


#endif

