#ifndef __arm
#include "Protocol.h"
#else
#include "FRAM.h"
#include "EEPROM.h"
#endif
#include "Defs.h"
#include "LogFile.h"
#include "Setup.h"
#include "GammaMeasurement.h"
#include "Archive.h"
#include "RTC.h"
#include "DateTime.h"
#include "KeyBoard.h"
#include "tgDelta.h"
#include "Error.h"
#include "RAM.h"
#include "Link.h"

#ifndef __emulator
#ifdef __arm
__no_init struct stLogData LogData;
#else
__no_init __eeprom struct stLogData LogData[MaxLogData]   @ (0x1000-(szLogData*MaxLogData));
#endif
#else
struct stLogData LogData
#ifndef __arm
[MaxLogData]
#endif
;
#endif
char AllLog=0,LastLog=0
#ifdef __arm
,CurLog=0
#endif
;
unsigned int LastLogID=0;


#ifdef __arm
void LoadLog(char Num)
{
ReadFram(LogAddr+(szLogData*Num),(void *)&LogData,szLogData);
}

void SaveCurentLog(char Num)
{
WriteFram(LogAddr+(szLogData*Num),(void *)&LogData,szLogData);
}
#endif

void InitLog(void)
{ char i;
  unsigned int CurLogID=0;

  LastLogID=
  AllLog=
  LastLog=0;

  for (i=0;i<MaxLogData;i++) {
#ifdef __arm
      LoadLog(i);
      if (IsDate(LogData.Year,LogData.Month,LogData.Day)&&
          (LogData.Hour<24)&&
          (LogData.Min<60))
#else
      if (IsDate(LogData[i].Year,LogData[i].Month,LogData[i].Day)&&
          (LogData[i].Hour<24)&&
          (LogData[i].Min<60))
#endif
      {
          AllLog++;
#ifdef __arm
          CurLogID=LogData.LogID;
#else
          CurLogID=LogData[i].LogID;
#endif
          if (CurLogID>LastLogID) {
             LastLogID=CurLogID;
             LastLog=i+1;
          }
      } else break;
  }

}


char LogNumToIndex(char Num)
{ int Result;

if (Num<=AllLog) {
    Result=AllLog-Num;
    if (Result<LastLog)
       Result=LastLog-Result;
    else {
          Result=MaxLogData-(Result-LastLog);
    }
} else {
     Result=MaxLogData;
}
return Result-1;
}

void SaveLog(unsigned int LogCode)
{ char Side;
#ifndef __arm
char Page=GetPage();
#endif

  if (LastLog>=MaxLogData) LastLog=0;
#ifndef __arm
  SetPage(ParametersPage);
#endif

#ifdef __arm
  GET_TIME_INBUF();
  LoadLog(LastLog);
  LogData.Year=DateTime.Year;
  LogData.Month=DateTime.Month;
  LogData.Day=DateTime.Day;
  LogData.Hour=DateTime.Hour;
  LogData.Min=DateTime.Min;
  LogData.LogCode=LogCode;
  for (Side=0;Side<MaxTransformerSide;Side++) {
      //     Gamma - G (0 - 12.75% with step 0.05)
      LogData.Gamma[Side]=Parameters->TrSide[Side].Gamma;
      //Gamma Alarm level
      LogData.GammaRedThreshold[Side]=Setup.GammaSetupInfo.GammaSideSetup[Side].GammaRedThresholld;
      //Phase Angle
      LogData.GammaPhase[Side]=Parameters->TrSide[Side].GammaPhase;
      //Temperature - T (-70 - +185 0C with step 1 0C)
      LogData.Temperature[Side]=Parameters->Temperature[Side];
      //Trend - TR (0 - 51 %/Year with step 0.2%/Year) //acm, 4-27 change from .2 to 1
      LogData.Trend[Side]=Parameters->TrSide[Side].Trend;
      //Temp coeff. - TK (0 - 0.51%/0C with step 0.002)
      LogData.KT[Side]=Parameters->TrSide[Side].KT;
      //Temp coeff. Phase
      LogData.KTPhase[Side]=Parameters->TrSide[Side].KTPhase;
      LogData.DefectCode[Side]=Parameters->TrSide[Side].AlarmStatus;
   }
   if (LastLogID<65535) LastLogID++;
   LogData.LogID=LastLogID;
  SaveCurentLog(LastLog);
#else
  GET_TIME();
  LogData[LastLog].Year=DateTime.Year;
  LogData[LastLog].Month=DateTime.Month;
  LogData[LastLog].Day=DateTime.Day;
  LogData[LastLog].Hour=DateTime.Hour;
  LogData[LastLog].Min=DateTime.Min;
  LogData[LastLog].LogCode=LogCode;
  for (Side=0;Side<MaxTransformerSide;Side++) {
      //     Gamma - G (0 - 12.75% with step 0.05)
      LogData[LastLog].Gamma[Side]=Parameters->TrSide[Side].Gamma;
      //Gamma Alarm level
      LogData[LastLog].GammaRedThreshold[Side]=Setup.GammaSetupInfo.GammaSideSetup[Side].GammaRedThresholld;
      //Phase Angle
      LogData[LastLog].GammaPhase[Side]=Parameters->TrSide[Side].GammaPhase;
      //Temperature - T (-70 - +185 0C with step 1 0C)
      LogData[LastLog].Temperature[Side]=Parameters->Temperature[Side];
      //Trend - TR (0 - 51 %/Year with step 0.2%/Year)
      LogData[LastLog].Trend[Side]=Parameters->TrSide[Side].Trend;
      //Temp coeff. - TK (0 - 0.51%/0C with step 0.002)
      LogData[LastLog].KT[Side]=Parameters->TrSide[Side].KT;
      //Temp coeff. Phase
      LogData[LastLog].KTPhase[Side]=Parameters->TrSide[Side].KTPhase;
      LogData[LastLog].DefectCode[Side]=Parameters->TrSide[Side].AlarmStatus;
   }
   if (LastLogID<65535) LastLogID++;
   LogData[LastLog].LogID=LastLogID;
#endif

  LastLog++;
  if (AllLog<MaxLogData) AllLog++;
#ifndef __arm
  SetPage(Page);
#endif
  EventTrigger = EventTrigger | NewEventLog; //Flag new event log
}


void SaveLogIfNewError(unsigned long OldError, unsigned long NewError)
{int i;
OldError&=~(((unsigned long)1<<erStop)|((unsigned long)1<<erPause)/*|(1<<erUnitOff)*/);
NewError&=~(((unsigned long)1<<erStop)|((unsigned long)1<<erPause)/*|(1<<erUnitOff)*/);
if (OldError!=NewError) {
   //for (i=0;i<32;i++) if (OldError&&((unsigned long)1<<i)) NewError&=~((unsigned long)1<<i);   // acm, intent, mask out matching bits
   for (i=0;i<32;i++) if (OldError&((unsigned long)1<<i)) NewError&=~((unsigned long)1<<i);  // acm, v2.01, fix logging bug, explains why some msgs not getting posted
   if (!NewError) return;
   //SaveLog
   if (NewError&((unsigned long)1<<erFlashWriteFail)) {
      SaveLog(lcFlashWriteFail);
   }
   if (NewError&((unsigned long)1<<erFlashReadFail)) {
      SaveLog(lcFlashReadFail);
   }
   if (NewError&((unsigned long)1<<erPhaseShiftSet1)) {
      SaveLog(lcPhaseShiftSet1);
   }
   if (NewError&((unsigned long)1<<erPhaseShiftSet2)) {  // acm, 3-16-12, fix dumb russian mistake, Set2 not Set1 again
      SaveLog(lcPhaseShiftSet2);
   }
   // acm, v2.00, add additional isolation logs for transgrid problem
//   if (NewError&((unsigned long)1<<erPhaseShiftSetSpot1)) SaveLog(lcPhaseShiftSetSpot1);
//   if (NewError&((unsigned long)1<<erPhaseShiftSetSpot2)) SaveLog(lcPhaseShiftSetSpot2);
//   if (NewError&((unsigned long)1<<erPhaseShiftSetSpot3)) SaveLog(lcPhaseShiftSetSpot3);
//   if (NewError&((unsigned long)1<<erPhaseShiftSetSpot4)) SaveLog(lcPhaseShiftSetSpot4);

   if (NewError&((unsigned long)1<<erChannels)) {
      SaveLog(lcChannels);
   }
   if (NewError&((unsigned long)1<<erFreq)) {
      SaveLog(lcFreq);
   }
   if (NewError&((unsigned long)1<<erLoSignal)) {
      SaveLog(lcLowSignal);
   }
   if (NewError&(unsigned long)(1<<erHiSignal)) {
      SaveLog(lcHiSignal);
   }
   if (NewError&(unsigned long)(1<<erUnitOff)) {
      SaveLog(lcUnitOff);
   }
   if (NewError&(unsigned long)((unsigned long)1<<erSet1Off)) {
      SaveLog(lcSet1Off);
   }
   if (NewError&(unsigned long)((unsigned long)1<<erSet2Off)) {
      SaveLog(lcSet2Off);
   }
   
}
}

void SaveLogIfNewErrorAndSkipLast(unsigned long OldError, unsigned long NewError)
{int i;
 char LastLogCode;

 if (AllLog) {
     if ((LastLog==0)||(LastLog>=MaxLogData)) {
#ifdef __arm
        LoadLog(0);
        LastLogCode=LogData.LogCode;
#else
        LastLogCode=LogData[0].LogCode;
#endif
     } else {
#ifdef __arm
        LoadLog(LastLog-1);
        LastLogCode=LogData.LogCode;
#else
        LastLogCode=LogData[LastLog-1].LogCode;
#endif
     }
 } else
    LastLogCode=0;

OldError&=~(((unsigned long)1<<erStop)|((unsigned long)1<<erPause)/*|(1<<erUnitOff)*/);
NewError&=~(((unsigned long)1<<erStop)|((unsigned long)1<<erPause)/*|(1<<erUnitOff)*/);
if (LastLogCode != lcPowerOn)
{
  SaveLog(lcPowerOn);
}
if (OldError!=NewError) {
   for (i=0;i<16;i++) if (OldError&&((unsigned long)1<<i)) NewError&=~((unsigned long)1<<i);
   if (!NewError) return;
   //SaveLog
   if ((NewError&((unsigned long)1<<erFlashWriteFail))&&(LastLogCode!=lcFlashWriteFail)) {
      SaveLog(lcFlashWriteFail);
   }
   if ((NewError&((unsigned long)1<<erFlashReadFail))&&(LastLogCode!=lcFlashReadFail)) {
      SaveLog(lcFlashReadFail);
   }
   if ((NewError&((unsigned long)1<<erPhaseShiftSet1))&&(LastLogCode!=lcPhaseShiftSet1)) {
      SaveLog(lcPhaseShiftSet1);
   }
   if ((NewError&((unsigned long)1<<erPhaseShiftSet2))&&(LastLogCode!=erPhaseShiftSet2)) {
      SaveLog(lcPhaseShiftSet2);
   }
   if ((NewError&((unsigned long)1<<erChannels))&&(LastLogCode!=erChannels)) {
      SaveLog(lcChannels);
   }
   if ((NewError&((unsigned long)1<<erFreq))&&(LastLogCode!=lcFreq)) {
      SaveLog(lcFreq);
   }
   if ((NewError&((unsigned long)1<<erLoSignal))&&(LastLogCode!=lcLowSignal)) {
      SaveLog(lcLowSignal);
   }
   if ((NewError&(unsigned long)((unsigned long)1<<erHiSignal))&&(LastLogCode!=lcHiSignal)) {
      SaveLog(lcHiSignal);
   }
   if ((NewError&(unsigned long)((unsigned long)1<<erUnitOff))&&(LastLogCode!=lcUnitOff)) {
      SaveLog(lcUnitOff);
   }
   if ((NewError&(unsigned long)((unsigned long)1<<erSet1Off))&&(LastLogCode!=lcSet1Off)) {
      SaveLog(lcSet1Off);
   }
   if ((NewError&(unsigned long)((unsigned long)1<<erSet2Off))&&(LastLogCode!=lcSet2Off)) {
      SaveLog(lcSet2Off);
   }
   //DE 4-24-15 do not repeat Power turned on 
//   if ((NewError&(unsigned long)((unsigned long)1<<erUnitOff))&&(LastLogCode!=lcPowerOn)) {
   if (LastLogCode!=lcPowerOn) {
      SaveLog(lcPowerOn);
   }
}
}
