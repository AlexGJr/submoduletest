#include "Setup.h"
#include "EEPROM.h"
#include "ERROR.h"
#include "LCD.h"
#include "Graph.h"
#include "KeyBoard.h"
#include "DefKB.h"
#include "Utils.h"
#include "RTC.h"
#include "Balans.h"
#include "ScrlMngr.h"
#include "MenuMngr.h"
#include "StrConst.h"
#include "PicRes.h"
#include <string.h>
#include <math.h>
#include "SetChannels.h"
#include "RAM.h"
#include "CalcParam.h"
#include "GammaMeasurement.h"
#include <stdlib.h>
#include "Link.h"
#ifdef __arm
#include "FRAM.h"
#define FramSize 32768  // acm, 2-15-09, redefine here, defined in Fram.cpp
#include "defs.h"		// acm, 2-19
#include "ADCDevice.h"
#include "BoardAdd.h"
#else
#include "ADC.h"
#endif

#ifndef NoCross
#include "CrossMeasurement.h"
#endif
#include "Archive.h"
#include "Diagnosis.h"
#include "DateTime.h"

//#include "ModBusTCP\UART.h"
#include "ModBusTCP\ModbusServer.h"
#include "Uart.h"			// acm, resolve Usart_reset() for Understand
extern void Usart_reset(void);

#ifndef __emulator
#ifdef __arm
unsigned short SetupValid;
__no_init struct stSetup Setup;
#else
__no_init __eeprom unsigned int SetupValid @ 0x00;
__no_init __eeprom struct stSetup Setup @ SetupAddr;
#endif
#else
unsigned short SetupValid;
struct stSetup Setup;
#endif
unsigned char SaveCounter = 0; //dwe 12-22-15 added to count saves
//struct stSetup Setup;
extern void GetNextTime(_TDateTime CurTime);

extern void Balansing(void); // acm, resolve understand
extern void SetBalansingOnTime(void); //""

void SetSetupParam(void)  // acm, only called by LoadSetup(), or ScanMessages (often)
{
//����� ����������� (������=0)
#ifdef __arm
extern int
#else
extern unsigned int  SCAL;
extern char
#endif
ID_Slave, HiAlarmOn;
extern unsigned int HiAlarmOneCounter;
extern _TDateTime TimeNextMeas;      //����� ���������� ���������� ������

char TrSide,CurrentGammaStatus=stUnknown,Tmp;


  if (((!IsDate(Setup.SetupInfo.AutoBalans.Year,Setup.SetupInfo.AutoBalans.Month,Setup.SetupInfo.AutoBalans.Day))||
      (!IsTime(Setup.SetupInfo.AutoBalans.Hour,0,0)))&&(Setup.SetupInfo.AutoBalansActive))
  {
      Setup.SetupInfo.AutoBalansActive=
      Setup.SetupInfo.AutoBalans.Year=
      Setup.SetupInfo.AutoBalans.Month=
      Setup.SetupInfo.AutoBalans.Day=
      Setup.SetupInfo.AutoBalans.Hour=0;
      //TestSetup();
  }

  if (Setup.GammaSetupInfo.DaysToCalculateTrend<MinDaysForTrend) Setup.GammaSetupInfo.DaysToCalculateTrend=MinDaysForTrend;
  if (Setup.GammaSetupInfo.DaysToCalculateBASELINE<MinDaysForBaseLine) Setup.GammaSetupInfo.DaysToCalculateBASELINE=MinDaysForBaseLine;
  if (Setup.GammaSetupInfo.AveragingForGamma<1) Setup.GammaSetupInfo.AveragingForGamma=10;  // acm, v2.00 change from 1 to 10
  if (Setup.GammaSetupInfo.AveragingForGamma_X>100 || Setup.GammaSetupInfo.AveragingForGamma_X<30) Setup.GammaSetupInfo.AveragingForGamma_X=99;  // acm, v2.00, new, if out of range set to <V2.00 default weight, AG/Claude want avg mostly off
  if (Setup.GammaSetupInfo.MinDiagGamma<MinDaysForDiagnosis) Setup.GammaSetupInfo.MinDiagGamma=MinDaysForDiagnosis;
  if (Setup.GammaSetupInfo.DaysToCalculateTCoefficient<MinDaysToCalcTCoeff) Setup.GammaSetupInfo.DaysToCalculateTCoefficient=MinDaysToCalcTCoeff;
  if (Setup.SetupInfo.CalibTreshould < 1 || Setup.SetupInfo.CalibTreshould > 99) Setup.SetupInfo.CalibTreshould = 15;
TestSetup(); // acm, v2.00, any Setup change needs to be written back to FRAM, otherwise subsequent InitDAC() reloads setup..

  if (Setup.SetupInfo.Stopped) Error|=((unsigned long)1<<erStop);
  else Error&=~((unsigned long)1<<erStop);

  ID_Slave=Setup.SetupInfo.DeviceNumber;
#ifdef __arm
  switch (Setup.SetupInfo.BaudRate) {
    case 6:  AT91_BAUD_RATE=1000000;break;				// acm, legacy variable, AT91_BAUD_RATE is referenced in DMA_UART.c, which I just removed 2-10-12
    case 5:  AT91_BAUD_RATE=500000;break;
    case 4:  AT91_BAUD_RATE=230400;break;
    case 3:  AT91_BAUD_RATE=115200;break;
    case 2:  AT91_BAUD_RATE=57600;break;
    case 1:  AT91_BAUD_RATE=38400;break;
    default: AT91_BAUD_RATE=9600;
  }
  Usart_reset();
#else
  if (Setup.SetupInfo.BaudRate==2) {
     SCAL = Fck/8/57600-1;
  } else {
     if (Setup.SetupInfo.BaudRate==1) {
        SCAL = Fck/8/38400-1;
     } else {
        SCAL = Fck/8/9600-1;
     }
  }
  INIT_UART();
#endif

  if (MeasurementsInArchive) {
     FullStatus=stUnknown;
     for (TrSide=0;TrSide<MaxTransformerSide;TrSide++) {
         if  (!(Setup.GammaSetupInfo.ReadOnSide&(1<<TrSide))) continue;
#ifndef __arm
         SetPage(ParametersPage);
#endif
         Tmp=GetGammaStatus(Parameters->TrSide[TrSide].Gamma*0.01,TrSide,stUnknown,1);			
         GetFullStatus(Tmp,Parameters->TrSide[TrSide].Trend,Parameters->TrSide[TrSide].KT,&FullStatus,&Parameters->TrSide[TrSide].AlarmStatus,TrSide);
         if (Tmp>CurrentGammaStatus) CurrentGammaStatus=Tmp;							// acm, CurrentGammaStatus = larger of set 1 and set 2 gamma
     }
     GammaStatus=CurrentGammaStatus;
     OutGammaStatus(FullStatus,0);
//		OutGammaStatus(GammaStatus,0);		// acm, 2-10-12 ok, thought is I want to indicate alarm on either set 1 or set 2, FullStatus was last assigned set 2.
//											   upon closer inspection, original version ok.  &fullstatus from first set gets passed to second set calculation, i.e. 
//											   alarm on set 1 && no alarm on set 2 still drives red LED.
  }

  GetNextTime(DateTimeNowNoSec());
//  if ((!Setup.SetupInfo.Stopped)&&(TimeNextMeas)) {
//     SetRelayOnLine;
//  } else {
//     ClearRelayOnLine;
//  }
}

#ifdef __arm
void SaveSetup(void)
{
 TCRC CalcSetupCRC;  // acm, 2-13-09
 
 WriteFram(SetupAddr,(void *)&Setup, szFullSetup);
 CalcSetupCRC=CalcCRC((void*)&Setup,sizeof(Setup));  // acm, 2-13-09
 WriteFram(SetupCRCAddr,(void *)&CalcSetupCRC, sizeof(TCRC));  // acm, 2-13-09
 // ��� �� ���������, � ���� �� ���������� ���������� 	 as a check, and whether to discard the averaging (huh?  no check, just clears avg)
 ClearAvg();
 if(++SaveCounter>=10)  //dwe 12-22-15 SaveSetup() is executed 10 times on config save
 {
   SaveCounter=0;
   EventTrigger = EventTrigger | ConfigUpload; //Flag new Configuration
   Setup.SetupInfo.ConfigSaveCount = Setup.SetupInfo.ConfigSaveCount +1; //Increment on Config Save
 }
}

// acm, historical note, believe TestSetup came about cica ~1.4, noise hardening.
// SaveSetup() vs TestSetup analysis TBD, suspect SaveSetup() more efficient (FERAM write endurance 1E14)
// Unlike PDM, BHM only sets RefoSetSetup r MODBUS address and baud rate, so only two spots that trigger auto SaveSetup().  But, BHM runs TestSetup() for any MODBUS write, ensuring
// changes to Setup.  Should make two modules consistent!
void TestSetup(void)			// acm, this function updates FRAM with recently changed Setup values
{ unsigned int i,Flag=0;  // acm, 2-13-09
  char c;
  TCRC CalcSetupCRC;  // acm, 2-13-09
  
for (i=0;i<szFullSetup;i++) {
    ReadFram( SetupAddr+i,(void *)&c, sizeof(char));
    if (c!=((char*)(&Setup))[i])  // acm, 2-13-09                              
       WriteFram(SetupAddr+i,(void *)&((char*)(&Setup))[i], sizeof(char));
// acm, 2-13-09                                                    New check, verify if setup in RAM matches FRAM, if not, re-write FRAM (and with new CRC)
	   Flag=1;
      }   
  if (Flag) {
     CalcSetupCRC=CalcCRC((void*)&Setup,sizeof(Setup));
     WriteFram(SetupCRCAddr,(void *)&CalcSetupCRC, sizeof(TCRC));
// acm, 2-13-09
	 }
}

#endif

void LoadSetup(char SetParam, char SetTime)
{
  char i,j,Side;

#ifdef __arm
ReadFram(SetupFirstAddr,(void *)&SetupValid,sizeof(SetupValid));
#endif

if ((SetupValid&0xFF00)!=0xAA00) {		// acm, load default setup values if header byte invalid (enhance, check CRC too?)

   //Device Number
#ifdef TDM
   Setup.SetupInfo.DeviceNumber=10;
#else
   Setup.SetupInfo.DeviceNumber=1;
#endif
   //�������� ������ � ����������
   //0-9600
   //1-38400
#ifdef TDM
   Setup.SetupInfo.BaudRate=3;
#else
   Setup.SetupInfo.BaudRate=0;
#endif
   //����� ������ ���������� �� ����������
   Setup.SetupInfo.OutTime=3;
   //������ ���������� ����
   //Setup.SetupInfo.DisplayFlag=0x03FF;
   Setup.SetupInfo.DisplayFlag=0xFFFF;
   //����� ������� ���� ���������� ������
   Setup.SetupInfo.SaveDays=1;

   //���������� ����������
   Setup.SetupInfo.Stopped=0;
   //0-Relay Off
   //1-Relay On
   //2-by time
   Setup.SetupInfo.Relay=1;
   //Time of Relay test output ,sec
   Setup.SetupInfo.TimeOfRelayAlarm=10;
   Setup.SetupInfo.ModBusProtocol=0;  //RTU

   if (SetTime) {
   //��� ���������� ������� (0-����� �������� 1-�� �������)
//   Setup.SetupInfo.ScheduleType=1;  // acm 10-15-09 update default value to measure every hour not by sched.
	Setup.SetupInfo.ScheduleType=0;
	//���� � ������ ���������� ������� ����� ��������
   //���� � ������ ���������� �������
   for (i=0;i<MaxMeasurementsPerDay;i++) {
       Setup.SetupInfo.MeasurementsInfo[i].Hour=0;
       Setup.SetupInfo.MeasurementsInfo[i].Min=0;
   }

   Setup.SetupInfo.MeasurementsInfo[0].Hour=0;
   Setup.SetupInfo.MeasurementsInfo[0].Min=1;
   Setup.SetupInfo.MeasurementsInfo[1].Hour=3;
   Setup.SetupInfo.MeasurementsInfo[1].Min=0;
   Setup.SetupInfo.MeasurementsInfo[2].Hour=6;
   Setup.SetupInfo.MeasurementsInfo[2].Min=0;
   Setup.SetupInfo.MeasurementsInfo[3].Hour=9;
   Setup.SetupInfo.MeasurementsInfo[3].Min=0;
   Setup.SetupInfo.MeasurementsInfo[4].Hour=12;
   Setup.SetupInfo.MeasurementsInfo[4].Min=0;
   Setup.SetupInfo.MeasurementsInfo[5].Hour=15;
   Setup.SetupInfo.MeasurementsInfo[5].Min=0;
   Setup.SetupInfo.MeasurementsInfo[6].Hour=18;
   Setup.SetupInfo.MeasurementsInfo[6].Min=0;
   Setup.SetupInfo.MeasurementsInfo[7].Hour=21;
   Setup.SetupInfo.MeasurementsInfo[7].Min=0;

   //���� � ������ ���������� ������� ����� ��������
   // Setup.SetupInfo.dTime.Hour=6; // acm 10-15-09 update default value to measure every hour not by sched.
   Setup.SetupInfo.dTime.Hour=1;
   Setup.SetupInfo.dTime.Min=0;
   }
   
   Setup.SetupInfo.CalibTreshould=15; //2.10 redefined from synch source
   Setup.SetupInfo.InternalSyncFrequency=60.0; //ag 2-11-16 Initialize transformer system frequency default

   Setup.SetupInfo.LoadCurrentThresholdLevel=0; //DE 3-2-16 Initialize default
   Setup.SetupInfo.LoadCurrentThresholdSettings=0; //DE 3-2-16 Initialize default
   
#ifdef TDM
//   Setup.SetupInfo.ReadAnalogFromRegister=0;  // acm 10-15-09 update default value, checks IHM box "Data from SCADA"
	Setup.SetupInfo.ReadAnalogFromRegister=1;
#endif

   //Setup.SetupInfo.HeaterOnTemperature=0;
   Setup.SetupInfo.ResetCount=0; //2.06 renamed

   //0 bit is 1 - Read on HV side
   //1 bit is 1 - Read on LV side
   //2 bit is 1 - Read on HV2 side
   //3 bit is 1 - Read on LV2 side
   Setup.GammaSetupInfo.ReadOnSide=0x03;
   //Parameter Change (Allows New data storage, if G has changed since the last measurement on more then G*CH%)
   //Setup.GammaSetupInfo.MaxParameterChange=10;  // acm, v2.00 stole this for AveragingForGamma_X
   //Alarm Hysteresis (Releases an Alarm, if an alarming parameter drops below (PA - PA*AH%) of alarm threshold PA)
   Setup.GammaSetupInfo.AlarmHysteresis=15;
   //Allowed Bushing Current Unbalance %
   Setup.GammaSetupInfo.AllowedPhaseDispersion=20;
   //Days to Calculate Trend (Can not be less then 10 days)
   Setup.GammaSetupInfo.DaysToCalculateTrend=30;
   //Days to Calculate Temperature Coefficient after clearing memory
   Setup.GammaSetupInfo.DaysToCalculateTCoefficient=30;
   //Averaging for Gamma
   Setup.GammaSetupInfo.AveragingForGamma=10;
	// X==0, avg freeze, kinda dumb
	// X==.5, 2pt avg, avg changes more quickly
	// X==.33, old alg, pre-2.00 behavior
	// X==1, avg off, becomes last measured Unn
   Setup.GammaSetupInfo.AveragingForGamma_X=99; // acm, v2.00, average mostly off (pre 2.00 used .33, weighted average)

   //�����(���), ����� ������� ������������� �����
   Setup.GammaSetupInfo.ReReadOnAlarm=5;
   //Min Diagnosis Gamma
   Setup.GammaSetupInfo.MinDiagGamma=15;
   Setup.GammaSetupInfo.NEGtg=0;
   Setup.GammaSetupInfo.DaysToCalculateBASELINE=30;

   for (i=0;i<MaxTransformerSide;i++) {
       Setup.GammaSetupInfo.GammaSideSetup[i].Temperature0=90;
       //������ Gamma � % /0.05
 //      Setup.GammaSetupInfo.GammaSideSetup[i].GammaYellowThresholld=60;  // acm 10-15-09 update default value from 3% to 4%
 //      Setup.GammaSetupInfo.GammaSideSetup[i].GammaRedThresholld=100;
        Setup.GammaSetupInfo.GammaSideSetup[i].GammaYellowThresholld=80;  // acm 10-15-09 update default value from 5% to 6%
		Setup.GammaSetupInfo.GammaSideSetup[i].GammaRedThresholld=120;
 
	//On/Off Alarm
       //0 bit is 1 - Gamma to Alarm
       //1 bit is 1 - Temperature coefficient to Alarm ON
       //2 bit is 1 - Trend to Alarm ON
       Setup.GammaSetupInfo.GammaSideSetup[i].AlarmEnable=0x07;
       //Gamma Temperature Coefficient Alarm Threshold (Byte*0.002)
//       Setup.GammaSetupInfo.GammaSideSetup[i].TCoefficient=40;// acm 10-15-09 update default value to .10% from .08%
		Setup.GammaSetupInfo.GammaSideSetup[i].TCoefficient=50;
		//Gamma Trend Alarm Threshold (Byte*0.2	  Max- 5%/Yr)  //acm, 4-27 change from .2 to 1
//       Setup.GammaSetupInfo.GammaSideSetup[i].TrendAlarm=25;// acm 10-15-09 update default value to 50 from 5
		Setup.GammaSetupInfo.GammaSideSetup[i].TrendAlarm=250;
		Setup.GammaSetupInfo.GammaSideSetup[i].RatedVoltage=35;   // acm 10-15-09 appears to be overwritten below line 377, strategy unclear
//       Setup.GammaSetupInfo.GammaSideSetup[i].RatedCurrent=10; // acm 10-15-09 update default value to 5.0A from 10.0A
		Setup.GammaSetupInfo.GammaSideSetup[i].RatedCurrent=300; //DE 3-1-16 changed from 5 to 300

       Setup.GammaSetupInfo.GammaSideSetup[i].TgYellowThresholld=60; // acm 10-15-09 Tg, power factor warning, ok for now
       Setup.GammaSetupInfo.GammaSideSetup[i].TgRedThresholld=70;
       Setup.GammaSetupInfo.GammaSideSetup[i].TgVariationThresholld=20;
#ifdef __arm
       Setup.GammaSetupInfo.GammaSideSetup[i].ExternalSync=0;
#endif
       for (j=0;j<MaxBushingCountOnSide;j++) {
          Setup.GammaSetupInfo.GammaSideSetup[i].InputC[j]=100;
          Setup.GammaSetupInfo.GammaSideSetup[i].Tg0[j]=35;
          Setup.GammaSetupInfo.GammaSideSetup[i].C0[j]=5000;
          Setup.GammaSetupInfo.GammaSideSetup[i].InputCoeff[j]=1;
    //      Setup.GammaSetupInfo.GammaSideSetup[i].InputImpedance[j]=4000;// acm 10-15-09 update default value from 40.0 to 32.0 ohms.
			Setup.GammaSetupInfo.GammaSideSetup[i].InputImpedance[j]=3333;	// ag update to most current 50||100=33.33
			Setup.GammaSetupInfo.GammaSideSetup[i].ImpedanseValue[j]=127;
       }
   }

   for (i=0;i<InputChannelsCount;i++) {
       Setup.CalibrationCoeff.TemperatureK[i]=1.039;
       Setup.CalibrationCoeff.TemperatureB[i]=-0.253;
 //      Setup.CalibrationCoeff.CurrentK[i]=1000.0; // acm, 2-13-09
		Setup.CalibrationCoeff.CurrentK[i]=1.0; // acm 10-15-09 update default value from 1000 to 1 per AG
		Setup.CalibrationCoeff.CurrentB[i]=0.0;
 //      Setup.CalibrationCoeff.CurrentChannelOnPhase[i]=i+1;  // acm, 2-13-09
	Setup.CalibrationCoeff.CurrentChannelOnPhase[i]=0; // acm 10-15-09 update default value to "NOT CONNECTED" per AG
		Setup.CalibrationCoeff.HumidityOffset[i]=838.000;
       Setup.CalibrationCoeff.HumiditySlope[i]=31.575;
   }

   for (i=0;i<MaxTransformerSide;i++)
    for (j=0;j<MaxBushingCountOnSide;j++)
      Setup.CalibrationCoeff.VoltageK[i][j]=1.0;

#ifdef engl
/*
   f=0.31;
   for (i=0;i<TemperatureMaxChannels;i++)
       Setup.CalibrationCoeff.TemperatureK[i]=f;
   f=-130.0;
   for (i=0;i<TemperatureMaxChannels;i++)
       SetupInfo.TemperatureB[i]=f;
*/
#else
   f=0.281;
   for (i=0;i<TemperatureMaxChannels;i++)
       SetupInfo.TemperatureK[i]=f;
   f=-117.697;
   for (i=0;i<TemperatureMaxChannels;i++)
       SetupInfo.TemperatureB[i]=f;
#endif


   //�����(���), ����� ������� ������������� �����
   Setup.GammaSetupInfo.ReReadOnAlarm=5;

   //Min Diagnosis Gamma,%
//   Setup.GammaSetupInfo.MinDiagGamma=5;

   for (Side=0;Side<MaxTransformerSide;Side++) {

      //����������� ������ Off-Line �������� (-70 - +185 0C) with step 1 0C
      Setup.GammaSetupInfo.GammaSideSetup[Side].Temperature0=90;
      //C*100 ,�������
      Setup.GammaSetupInfo.GammaSideSetup[Side].InputC[0]=Setup.GammaSetupInfo.GammaSideSetup[Side].InputC[1]=Setup.GammaSetupInfo.GammaSideSetup[Side].InputC[2]=108;

      //������������ �������� �������. ����� ���� ���������� ��� ������� �� �������� ��������
#ifdef engl
      Setup.GammaSetupInfo.GammaSideSetup[Side].InputCoeff[0]=Setup.GammaSetupInfo.GammaSideSetup[Side].InputCoeff[1]=Setup.GammaSetupInfo.GammaSideSetup[Side].InputCoeff[2]=3.5;
#else
      Setup.GammaSetupInfo.GammaSideSetup[Side].InputCoeff[0]=Setup.GammaSetupInfo.GammaSideSetup[Side].InputCoeff[1]=Setup.GammaSetupInfo.GammaSideSetup[Side].InputCoeff[2]=3.3;
#endif

      //tg*100 ,%
      Setup.GammaSetupInfo.GammaSideSetup[Side].Tg0[0]=Setup.GammaSetupInfo.GammaSideSetup[Side].Tg0[1]=Setup.GammaSetupInfo.GammaSideSetup[Side].Tg0[2]=35;
      Setup.GammaSetupInfo.GammaSideSetup[Side].C0[0]=Setup.GammaSetupInfo.GammaSideSetup[Side].C0[1]=Setup.GammaSetupInfo.GammaSideSetup[Side].C0[2]=5000;

#ifdef engl
//      Setup.GammaSetupInfo.GammaSideSetup[Side].RatedVoltage=138.0;  // acm 10-15-09 update default value to 230kv from 138kv
		Setup.GammaSetupInfo.GammaSideSetup[Side].RatedVoltage=230.0;
#else
      Setup.GammaSetupInfo.RatedVoltage=220.0;
#endif

      //Input Impedance *10
//      Setup.GammaSetupInfo.GammaSideSetup[Side].InputImpedance[0]=Setup.GammaSetupInfo.GammaSideSetup[Side].InputImpedance[1]=Setup.GammaSetupInfo.GammaSideSetup[Side].InputImpedance[2]=4000; // acm 10-15-09 update default value from 40.0 to 32.0 ohms. 
		Setup.GammaSetupInfo.GammaSideSetup[Side].InputImpedance[0]=Setup.GammaSetupInfo.GammaSideSetup[Side].InputImpedance[1]=Setup.GammaSetupInfo.GammaSideSetup[Side].InputImpedance[2]=3333;	// ag update to most current 50||100=33.33
		
      //����������� ������ ����� �� ������������ ��������� ��������� � �������� (-70 - +185 0C) with step 1 0C
      //Min
      Setup.GammaSetupInfo.GammaSideSetup[Side].MinTemperature1=0;
      //Avg
      Setup.GammaSetupInfo.GammaSideSetup[Side].AvgTemperature1=0;
      //Max
      Setup.GammaSetupInfo.GammaSideSetup[Side].MaxTemperature1=0;

      //��������� ������� � �������� ��� ������� � ������������ (k*X+b).
      Setup.GammaSetupInfo.GammaSideSetup[Side].B[0]=Setup.GammaSetupInfo.GammaSideSetup[Side].B[1]=Setup.GammaSetupInfo.GammaSideSetup[Side].B[2]=0;
      Setup.GammaSetupInfo.GammaSideSetup[Side].K[0]=Setup.GammaSetupInfo.GammaSideSetup[Side].K[1]=Setup.GammaSetupInfo.GammaSideSetup[Side].K[2]=0;

      //������� ���������� �� ����� ��� ������� ������������� �������� �������.
      Setup.GammaSetupInfo.GammaSideSetup[Side].InputVoltage[0]=Setup.GammaSetupInfo.GammaSideSetup[Side].InputVoltage[1]=Setup.GammaSetupInfo.GammaSideSetup[Side].InputVoltage[2]=1;

       //Delta tg*100 ,%
      Setup.GammaSetupInfo.GammaSideSetup[Side].STABLEDeltaTg[0]=Setup.GammaSetupInfo.GammaSideSetup[Side].STABLEDeltaTg[1]=Setup.GammaSetupInfo.GammaSideSetup[Side].STABLEDeltaTg[2]=0;

       //Delta C*100 ,%
      Setup.GammaSetupInfo.GammaSideSetup[Side].STABLEDeltaC[0]=Setup.GammaSetupInfo.GammaSideSetup[Side].STABLEDeltaC[1]=Setup.GammaSetupInfo.GammaSideSetup[Side].STABLEDeltaC[2]=0;

      //tg*100 ,% ��� STABLE
      Setup.GammaSetupInfo.GammaSideSetup[Side].STABLETg[0]=Setup.GammaSetupInfo.GammaSideSetup[Side].STABLETg[1]=Setup.GammaSetupInfo.GammaSideSetup[Side].STABLETg[2]=35;

      //C*10 ,������
      Setup.GammaSetupInfo.GammaSideSetup[Side].STABLEC[0]=Setup.GammaSetupInfo.GammaSideSetup[Side].STABLEC[1]=Setup.GammaSetupInfo.GammaSideSetup[Side].STABLEC[2]=0;

      Setup.GammaSetupInfo.GammaSideSetup[Side].STABLETemperature=0;

      Setup.GammaSetupInfo.GammaSideSetup[Side].STABLESaved=0;

   }
   Setup.GammaSetupInfo.NEGtg=1;

   Setup.ZkSetupInfo.CalcZkOnSide = 0;
//   Setup.ZkSetupInfo.CurrentType = 0;
//   Setup.ZkSetupInfo.CurrentType = 0;
   Setup.ZkSetupInfo.AveragingForZk = 5;

   Setup.SetupInfo.AutoBalansActive=0;
   Setup.SetupInfo.WorkingDays=0;
   Setup.SetupInfo.AutoBalans.Year=0;
   Setup.SetupInfo.AutoBalans.Month=0;
   Setup.SetupInfo.AutoBalans.Day=0;
   Setup.SetupInfo.AutoBalans.Hour=0;
   Setup.SetupInfo.NoLoadTestActive=0;

   Setup.ZkSetupInfo.ZkDays=30;
   Setup.ZkSetupInfo.FaultDate=0;

   for (i=0;i<MaxCorrelationSide;i++) {
       for (j=0;j<MaxLoads;j++) {
           Setup.BaseLineZk.ZkP[i][j][BushingA]=0;
           Setup.BaseLineZk.ZkP[i][j][BushingB]=0;
           Setup.BaseLineZk.ZkP[i][j][BushingC]=0;
           Setup.BaseLineZk.ZkPCount[i][j]=0;
           Setup.BaseLineZk.ZkPOnDate[i][j]=0;
       }
   }

   SetupValid=0xAABB;  	// acm, new realization, reserved structure bytes are uninitialized above, as Setup is declared __no_init, meaning unitialized
						// at boot, presumably to reduce symbol table size.  If future FW unreserves byte(s), after FW update, there is a potential
						// nasty side effect.  On reboot following FW update, setup is loaded, all previously reserved bytes are initialized with
						// what ever data that was in RAM at time of last write Setup to Fram.  If the uninitialized data is checked for out of range
						// condition and is out of range, all is good.  If the value is within range, unexpecting user maynot realize the value isn't
						// set properly.  A work around is for user to validate all Setup values following FW update (something that has not been a
						// communicated requirement historically).  It is possible to create a mechanism that determines first boot from FW update.
						// When FW update OK determined, set a bit somewhere, then when main() boots, check bit, if set, can loadSetup, properly init
						// previously unitialized reserve byte(s).  Too much work for now, prefer redefine previously unused byte (v2.00 avg k).
    Setup.SetupInfo.SN = GetRand(1, 65535); //Assign serial number
    Setup.SetupInfo.ConfigSaveCount=0; //dwe, new feature to inc when config is saved
    Setup.SetupInfo.LoadCurrentThresholdLevel=300;  //2.05 
    Setup.SetupInfo.LoadCurrentThresholdSettings=32768; //2.05 new features and nothing enabled
    Setup.SetupInfo.Job1 = Setup.SetupInfo.Job2 = 0; //2.05

#ifdef __arm
   WriteFram(SetupFirstAddr,(void *)&SetupValid,sizeof(SetupValid));
   SaveSetup();
#endif
   
//   Error|=(1<<er_noinit_need2bal); //DE 5-1-15 removed // acm, v1.79, good idea to indicate BHM not capable of taking measurements until balance occurs
}
#ifdef __arm
  else if(SetupValid!=0xAABB){     //2.05
  ReadFram(SetupAddr,(void *)&Setup, szFullSetup);
   Setup.SetupInfo.SN = GetRand(1, 65535); //Assign serial number
   Setup.SetupInfo.ConfigSaveCount=0; //dwe, new feature to inc when config is saved
   Setup.SetupInfo.LoadCurrentThresholdLevel=300;  //2.05 
   Setup.SetupInfo.LoadCurrentThresholdSettings=32768; //2.05 new features and nothing enabled
   Setup.SetupInfo.Job1 = Setup.SetupInfo.Job2 = 0; //2.05
   Setup.SetupInfo.CalibTreshould=15; //2.10 
   Setup.SetupInfo.InternalSyncFrequency=60.0; //2.05 Initialize transformer system frequency default
   Setup.GammaSetupInfo.AveragingForGamma_X=99; //2.05 killing averaging
   Setup.SetupInfo.ResetCount=0; //2.06 clear reset count
   SaveSetup();
   SetupValid=0xAABB;
   WriteFram(SetupFirstAddr,(void *)&SetupValid,sizeof(SetupValid));
  }
  else ReadFram(SetupAddr,(void *)&Setup, szFullSetup);

#endif
if (SetParam) SetSetupParam();
}


char SelectSide(char *CurSide)
{ struct ScrollerMngr Scrl;
  KEY ch=KB_NO;

ClearLCD();
SetFont(Font8x8);
InitScroller(&Scrl,32,8,Font8x8);
AddScrollerString(&Scrl,SelSideStr3);
AddScrollerString(&Scrl,SelSideStr4);

OutString(8,0,SelSideStr1);
OutString(16,8,SelSideStr2);
OutPicture(22,7,RArr);
RedrawScrl(&Scrl);
while ((ch!=KB_ENTER)&&(ch!=KB_ESC)) {
  Redraw();
  ch=DoScrl(&Scrl);
//  OutInt(0,0,ch,3);
//  Redraw();
}
if (ch==KB_ENTER) {
  if (*CurSide!=GetCurScrl(&Scrl)-1)
   *CurSide=GetCurScrl(&Scrl)-1;
   return 1;
} else return 0;
}


#define  MySpace 10

static void int2mystr(int Value, char *aStr){
 char Shift=0;

  aStr[0]=0;
  aStr[1]=0;
  aStr[2]=0;
  aStr[3]=0;
  Value%=10000;

  if(Value>999) aStr[0]=Value/1000;             else {Shift=1;aStr[3]=MySpace;}
  if(Value>99)  aStr[1-Shift]=(Value%1000)/100; else {Shift=2;aStr[2]=MySpace;}
  if(Value>9)   aStr[2-Shift]=(Value%100)/10;   else {Shift=3;aStr[1]=MySpace;}
  if(Value>0)   aStr[3-Shift]=Value%10;         else {        aStr[0]=MySpace;}
}

static int mystr2int(char *aStr){
 int Result=0;

  if(aStr[0]==MySpace)return Result;else Result=aStr[0];
  if(aStr[1]==MySpace)return Result;else Result=Result*10+aStr[1];
  if(aStr[2]==MySpace)return Result;else Result=Result*10+aStr[2];
  if(aStr[3]==MySpace)return Result;else Result=Result*10+aStr[3];

  return Result;
}


static void OutMyString(int X, int Y, char * aStr, char Pos){
 char String[5]={' ',' ',' ',' ','\x00'};
 unsigned int i;

  if(aStr[0]!=MySpace) {
    String[0]=aStr[0]+48;
    if(aStr[1]!=MySpace) {
      String[1]=aStr[1]+48;
      if(aStr[2]!=MySpace) {
        String[2]=aStr[2]+48;
        if(aStr[3]!=MySpace) String[3]=aStr[3]+48;
      }
    }
  }

  for (i=0;i<(unsigned int)strlen(String);i++)
    if((i!=(unsigned int)Pos)&&(String[i]!=' '))String[i]='\x07';

  OutString1(X, Y, String);
}

static int MyInputInt(unsigned char X, unsigned char Y, int IntValue, char *CurPos, unsigned int *ExitKey)
{
#define DigitCount 4 //��� ��������� ����� ����� ����������
                     //��� ������� ����

   char EndInp;
   int ReturnValue;
   unsigned int ch;
   char aStr[DigitCount];

   int2mystr(IntValue, aStr);
   EndInp=0;
   while (1>0)
     {
       OutMyString(X,Y,aStr,(*CurPos));
       DrawLine(X+(*CurPos)*CurFontWidth,Y+CurFontHeight-1,X+((*CurPos)+1)*CurFontWidth-1,Y+CurFontHeight-1);
       Redraw();
       ch=WaitReadKeyWithDelay(NoKeyDelayInSec,KB_ESC);
       switch (ch) {
       case KB_ESC   : { *ExitKey=ch; EndInp=1; ReturnValue=IntValue; break;}
       case KB_ENTER : { *ExitKey=ch; EndInp=1; ReturnValue=mystr2int(aStr); break;}
       case KB_RIGHT : {if ((*CurPos)<DigitCount-1) (*CurPos)++; break;}
       case KB_LEFT  : {if ((*CurPos)>0) (*CurPos)--; break;}
       case KB_UP    : {if (aStr[*CurPos]==MySpace) aStr[*CurPos]=1; else
                        if (aStr[*CurPos]<9) aStr[*CurPos]++;
                        else aStr[*CurPos]=0;
                        for(ch=(*CurPos);ch>0;ch--)
                          if(aStr[ch-1]==MySpace)aStr[ch-1]=0;
                        break;}
       case KB_DOWN  : {if (aStr[*CurPos]>0) aStr[*CurPos]--;
                        else aStr[*CurPos]=9;
                        break;}
       }
       if (EndInp) break;
   }

   OutMyString(X, Y, aStr, DigitCount);
//   DrawLine(X+(*CurPos)*CurFontWidth,Y+CurFontHeight-1,X+((*CurPos)+1)*CurFontWidth-1,Y+CurFontHeight-1);
   Redraw();

   return ReturnValue;
}

static char PasswordOk(void){
#ifndef __debug
  unsigned int Psw=0,i;
  char Pos=0;
  unsigned int EscCode;
  KEY ch;

//Psw=0x152D;

PassAgain:
  ClearLCD();
//  DrawRect(0,0,111,15);
  SetFont(Font8x6);
  OutString(8,4,InpPwdStr);
//  OutStringCenterX(64,7,InpPwdCaption);
//  OutStringCenterX(64,59,ExitStr);
//  OutStringCenterX(64,25,EntPwd);
  DrawRect(62,2,90,13);
  Redraw();
  Psw=MyInputInt(64,4,Psw,&Pos,&EscCode);
  if(EscCode==KB_ESC)return 0;

  if (Psw==0x152D) {
#ifndef __emulator
     for (i=0;i<3000;i++) {
#else
     for (i=0;i<100;i++) {
#endif
//     for (i=0;i<100;i++) {
         Delay(5);
         ch=ReadKey();
         if (ch==KB_ESC) return 0;
         if (ch!=KB_NO) goto PassAgain;
         ScanMessages();
     }
  } else {
    if (WaitReadKeyWithDelay(NoKeyDelayInSec, KB_ESC)==KB_ESC) return 0;
    else goto PassAgain;
  }
  return 1;
#else
  return 1;
#endif
}

/*
char OnOffSide(char CurSide)
{ struct ScrollerMngr Scrl1,Scrl2;
  unsigned int ch=KB_NO;
  struct MenuMngr Menu;
//  unsigned int ScanMenuResult;

  extern flash char RArr[szRArr];

ClearLCD();
SetFont(Font8x8);
InitScroller(&Scrl1,88,0,Font8x8);
AddScrollerString(&Scrl1,OnOffSideStr3);
AddScrollerString(&Scrl1,OnOffSideStr4);
InitScroller(&Scrl2,88,8,Font8x8);
AddScrollerString(&Scrl2,OnOffSideStr3);
AddScrollerString(&Scrl2,OnOffSideStr4);

SetCurScrl(&Scrl1,(CurSide&1)+1);
SetCurScrl(&Scrl2,((CurSide&2)>>1)+1);

OutString(0,0,OnOffSideStr1);
OutString(0,8,OnOffSideStr2);

InitMenuMngr(&Menu,KB_UP,KB_DOWN,KB_ENTER);
AddCursor(&Menu, 80, 0, RArr, EmptyStr, ScrlMenuElement, &Scrl1, 0);
AddCursor(&Menu, 80, 8, RArr, EmptyStr, ScrlMenuElement, &Scrl2, 0);

while ((ch!=KB_ENTER)&&(ch!=KB_ESC)) {
  RedrawScrl(&Scrl1);
  RedrawScrl(&Scrl2);
  Redraw();
  ScanMenuNoWait(&Menu, &ch);
}
if (ch==KB_ENTER) {
   CurSide=0;
   if (GetCurScrl(&Scrl1)==2) CurSide|=(1<<0);
   if (GetCurScrl(&Scrl2)==2) CurSide|=(1<<1);
}
return CurSide;
}
*/
static char SetBaudRate(char Cur)
{ struct ScrollerMngr Scrl1;
  KEY ch=KB_NO;

ClearLCD();
SetFont(Font8x8);
InitScroller(&Scrl1,38,8,Font8x8);
AddScrollerString(&Scrl1,BaudRateStr3);
AddScrollerString(&Scrl1,BaudRateStr4);
AddScrollerString(&Scrl1,BaudRateStr5);
#ifdef __arm
AddScrollerString(&Scrl1,BaudRateStr6);
AddScrollerString(&Scrl1,BaudRateStr7);
AddScrollerString(&Scrl1,BaudRateStr8);
AddScrollerString(&Scrl1,BaudRateStr9);
#endif

SetCurScrl(&Scrl1,Cur+1);

OutString(24,0,BaudRateStr2);

OutPicture(30,8,RArr);
RedrawScrl(&Scrl1);
while ((ch!=KB_ENTER)&&(ch!=KB_ESC)) {
  Redraw();
  ch=DoScrl(&Scrl1);
}
if (ch==KB_ENTER) {
  if (Cur!=GetCurScrl(&Scrl1)-1)
   Cur=GetCurScrl(&Scrl1)-1;
}
return Cur;
}

static char InpRegType(void)
{
  KEY ch=KB_NO;
  char Save;
  struct ScrollerMngr Scrl1;

  if (Setup.SetupInfo.ScheduleType>1) Setup.SetupInfo.ScheduleType=0;
  Save=Setup.SetupInfo.ScheduleType;
  ClearLCD();
  SetFont(Font8x8);
  InitScroller(&Scrl1,19,8,Font8x8);
  AddScrollerString(&Scrl1,ReadTimeStr4);
  AddScrollerString(&Scrl1,ReadTimeStr5);

  SetCurScrl(&Scrl1,Save+1);

  OutString(24,0,ReadTimeStr3);
  OutPicture(10,8,RArr);

  RedrawScrl(&Scrl1);
  while ((ch!=KB_ENTER)&&(ch!=KB_ESC)) {
    Redraw();
    ch=DoScrl(&Scrl1);
  }
  if (ch==KB_ENTER) {
    if (Setup.SetupInfo.ScheduleType!=GetCurScrl(&Scrl1)-1)
     Setup.SetupInfo.ScheduleType=GetCurScrl(&Scrl1)-1;
     return 1;
  }
  return 0;
}

static void InpRegData(void)
{ char CurPos=0;
  KEY ch=KB_NO;
#ifdef __arm
  u32 h,m;
#else
  char h,m;
#endif

if (InpRegType()) {
   ClearLCD();
   SetFont(Font8x8);
   OutString(24,0,ReadTimeStr3);
   if (Setup.SetupInfo.ScheduleType) {
      //�� �������
      while ((ch!=KB_ENTER)&&(ch!=KB_ESC)) {
            OutInt(0,8,CurPos+1,2);
            DateTime.Hour=Setup.SetupInfo.MeasurementsInfo[CurPos].Hour;
            DateTime.Min=Setup.SetupInfo.MeasurementsInfo[CurPos].Min;
            OutTimeNoSec(40,8);

            Redraw();
            ch=WaitReadKeyWithDelay(NoKeyDelayInSec,KB_ESC);
            switch (ch) {
              case (char)KB_UP: {
                   if (CurPos<MaxMeasurementsPerDay-1) CurPos++;
#ifdef gamma_m
                   else CurPos=0;
#endif
                   break;
              }
              case (char)KB_DOWN: {
                   if (CurPos) CurPos--;
                   break;
              }
              case (char)KB_ENTER: {
                   h=Setup.SetupInfo.MeasurementsInfo[CurPos].Hour;
                   m=Setup.SetupInfo.MeasurementsInfo[CurPos].Min;
                   InputTimeNoSec(40,8,&h,&m);
                   Setup.SetupInfo.MeasurementsInfo[CurPos].Hour=h;
                   Setup.SetupInfo.MeasurementsInfo[CurPos].Min=m;
                   ch=KB_NO;
                   break;
              }
            }
      }
   } else {
      //����� ��������
//      if (SetupInfo.dTime.Min<10) SetupInfo.dTime.Min=10;
      OutString(22,8,ReadTimeStr6);
      h=Setup.SetupInfo.dTime.Hour;
      m=Setup.SetupInfo.dTime.Min;
      InputTimeNoSec(50,8,&h,&m);
      Setup.SetupInfo.dTime.Hour=h;
      Setup.SetupInfo.dTime.Min=m;
      if (Setup.SetupInfo.dTime.Hour>=12) {
         Setup.SetupInfo.dTime.Hour=12;
         Setup.SetupInfo.dTime.Min=0;
      }
   }
}
}


static char InpOnOffValue(char x, char y,char CurValue,char ROM* Str1,char ROM* Str2)
{
  KEY ch=KB_NO;
  char Save;
  struct ScrollerMngr Scrl1;

  InitScroller(&Scrl1,x,y,Font8x8);
  AddScrollerString(&Scrl1,Str1);
  AddScrollerString(&Scrl1,Str2);
  Save=CurValue;
  SetCurScrl(&Scrl1,CurValue+1);
  OutPicture(x-8,y,RArr);

  RedrawScrl(&Scrl1);
  while ((ch!=KB_ENTER)&&(ch!=KB_ESC)) {
    Redraw();
    ch=DoScrl(&Scrl1);
  }
  if (ch==KB_ENTER) {
     return GetCurScrl(&Scrl1)-1;
  }
  return Save;
}


static unsigned int InpFillPause(unsigned int CurValue)
{

  unsigned int OldValue;
  KEY ch=KB_NO;
  char CurCh=0;
  struct ScrollerMngr Scrl1;

  InitScroller(&Scrl1,88,8,Font8x8);
  AddScrollerString(&Scrl1,OnOffSideStr3);
  AddScrollerString(&Scrl1,OnOffSideStr4);

  OldValue=CurValue;
  while ((ch!=KB_ENTER)&&(ch!=KB_ESC)) {
     SetFont(Font8x6);
     switch (CurCh) {
       case 0:{OutString(16,8,FillPauseStr4);break;}
       case 1:{OutString(16,8,FillPauseStr5);break;}
       case 2:{OutString(16,8,FillPauseStr6);break;}
       case 3:{OutString(16,8,FillPauseStr8);break;}
       case 4:{OutString(16,8,FillPauseStr9);break;}
       case 5:{OutString(16,8,FillPauseStr10);break;}
       case 6:{OutString(16,8,FillPauseStr11);break;}
       case 7:{OutString(16,8,FillPauseStr7);break;}
       case 8:{OutString(16,8,FillPauseStr14);break;}
       case 9:{OutString(16,8,FillPauseStr15);break;}
#ifndef NoCross
       case 10:{OutString(16,8,FillPauseStr12);break;}
#endif
     }
     if (CurValue&(unsigned int)(1<<CurCh)) {
        SetCurScrl(&Scrl1,2);
     } else {
        SetCurScrl(&Scrl1,1);
     }
     RedrawScrl(&Scrl1);
     while ((ch!=KB_ENTER)&&(ch!=KB_ESC)&&(ch!=KB_UP)&&(ch!=KB_DOWN)) {
        Redraw();
        ch=DoScrl(&Scrl1);
     }
     if (GetCurScrl(&Scrl1)==2)
        CurValue|=(unsigned int)(1<<CurCh);
     else
        CurValue&=~(unsigned int)(1<<CurCh);

     if (ch==KB_UP) {
        if (CurCh<MaxShowValues) CurCh++; else CurCh=0;
        ch=KB_NO;
     }
     if (ch==KB_DOWN) {
        if (CurCh) CurCh--; else CurCh=MaxShowValues;
        ch=KB_NO;
     }

}
SetFont(Font8x8);
if (ch==KB_ENTER) {
 return CurValue;
} else  return OldValue;

}

static char SelectRelayMode(char Cur)
{ struct ScrollerMngr Scrl1;
  unsigned int ch=KB_NO;

ClearLCD();
SetFont(Font8x8);
InitScroller(&Scrl1,38,8,Font8x8);
AddScrollerString(&Scrl1,RelayStr3);
AddScrollerString(&Scrl1,RelayStr4);
AddScrollerString(&Scrl1,RelayStr5);

SetCurScrl(&Scrl1,Cur+1);

OutString(26,0,RelayStr2);

OutPicture(0,0,RelayIco);
OutPicture(30,8,RArr);
RedrawScrl(&Scrl1);
while ((ch!=KB_ENTER)&&(ch!=KB_ESC)) {
  Redraw();
  ch=DoScrl(&Scrl1);
}
if (ch==KB_ENTER) {
   Cur=GetCurScrl(&Scrl1)-1;
   if (Cur==2){//by time
     ClearLCD();
     OutString(4,0,RelayStr6);
     OutString(58,8,SecStr);
     Setup.SetupInfo.TimeOfRelayAlarm=InputInt(34,8,Setup.SetupInfo.TimeOfRelayAlarm,2);
     }
}
return Cur;
}

/*
static unsigned int InpAlarmEnable(unsigned int CurValue)
{ unsigned int OldValue;
  KEY ch=KB_NO;
  char CurCh=0;
  struct ScrollerMngr Scrl1;

  InitScroller(&Scrl1,88,8,Font8x8);
  AddScrollerString(&Scrl1,OnOffSideStr3);
  AddScrollerString(&Scrl1,OnOffSideStr4);

  OldValue=CurValue;
  while ((ch!=KB_ENTER)&&(ch!=KB_ESC)) {
     SetFont(Font8x6);
     switch (CurCh) {
       case 0:{OutString(8,8,AlarmEnableStr7);break;}
       case 1:{OutString(8,8,AlarmEnableStr8);break;}
       case 2:{OutString(8,8,AlarmEnableStr9);break;}
       case 3:{OutString(8,8,AlarmEnableStr10);break;}
       case 4:{OutString(8,8,AlarmEnableStr11);break;}
       case 5:{OutString(8,8,AlarmEnableStr12);break;}
       case 6:{OutString(8,8,AlarmEnableStr13);break;}
     }
     if (CurValue&(unsigned int)(1<<CurCh)) {
        SetCurScrl(&Scrl1,2);
     } else {
        SetCurScrl(&Scrl1,1);
     }
     RedrawScrl(&Scrl1);
     while ((ch!=KB_ENTER)&&(ch!=KB_ESC)&&(ch!=KB_UP)&&(ch!=KB_DOWN)) {
        Redraw();
        ch=DoScrl(&Scrl1);
     }
     if (GetCurScrl(&Scrl1)==2)
        CurValue|=(unsigned int)(1<<CurCh);
     else
        CurValue&=~(unsigned int)(1<<CurCh);

     if (ch==KB_UP) {
        if (CurCh<6) CurCh++; else CurCh=0;
        ch=KB_NO;
     }
     if (ch==KB_DOWN) {
        if (CurCh) CurCh--; else CurCh=6;
        ch=KB_NO;
     }

}
SetFont(Font8x8);
if (ch==KB_ENTER) {
 return CurValue;
} else  return OldValue;
}
*/
static void SetAlarmEnable(void)
{
  KEY ch=KB_NO;
  char i,CurPos=0;
  struct ScrollerMngr Scrl1,Scrl2,Scrl3;
  char AE[MaxTransformerSide];

  InitScroller(&Scrl1,88,8,Font8x8);
  AddScrollerString(&Scrl1,OnOffSideStr3);
  AddScrollerString(&Scrl1,OnOffSideStr4);

  InitScroller(&Scrl2,10,8,Font8x6);
//  AddScrollerString(&Scrl2,AlarmEnableStr7);
  AddScrollerString(&Scrl2,AlarmEnableStr8);
  AddScrollerString(&Scrl2,AlarmEnableStr9);
  AddScrollerString(&Scrl2,AlarmEnableStr10);
  AddScrollerString(&Scrl2,AlarmEnableStr11);
#ifndef NoCross
  AddScrollerString(&Scrl2,AlarmEnableStr12);
  AddScrollerString(&Scrl2,AlarmEnableStr13);
#endif

  InitScroller(&Scrl3,10,0,Font8x8);
  AddScrollerString(&Scrl3,AlarmEnableStr3);
  AddScrollerString(&Scrl3,AlarmEnableStr4);
//  AddScrollerString(&Scrl3,AlarmEnableStr5);
//  AddScrollerString(&Scrl3,AlarmEnableStr6);

  for (i=0;i<MaxTransformerSide;i++)
    AE[i] = Setup.GammaSetupInfo.GammaSideSetup[i].AlarmEnable;

  SetCurScrl(&Scrl2,1);
  SetCurScrl(&Scrl3,1);
  if (AE[GetCurScrl(&Scrl3)-1] & (unsigned int)(1<<(GetCurScrl(&Scrl2)))) {
     SetCurScrl(&Scrl1,2);
  } else {
     SetCurScrl(&Scrl1,1);
  }
     ClearLCD();
     RedrawScrl(&Scrl1);
     RedrawScrl(&Scrl2);
     RedrawScrl(&Scrl3);

  while ((ch!=KB_ENTER)&&(ch!=KB_ESC)) {

     if (CurPos==0) {
       while ((ch!=KB_ENTER)&&(ch!=KB_ESC)&&(ch!=KB_UP)&&(ch!=KB_DOWN)) {
          OutPicture(0,0,RArr);
          RedrawScrl(&Scrl1);
          RedrawScrl(&Scrl2);
          RedrawScrl(&Scrl3);
          Redraw();
          ch=DoScrl(&Scrl3);
          if (AE[GetCurScrl(&Scrl3)-1] & (unsigned int)(1<<(GetCurScrl(&Scrl2)))) {
             SetCurScrl(&Scrl1,2);
          } else {
             SetCurScrl(&Scrl1,1);
          }
       }
     }

     if (CurPos==1) {
       while ((ch!=KB_ENTER)&&(ch!=KB_ESC)&&(ch!=KB_UP)&&(ch!=KB_DOWN)) {
          OutPicture(0,8,RArr);
          RedrawScrl(&Scrl1);
          RedrawScrl(&Scrl2);
          RedrawScrl(&Scrl3);
          Redraw();
          ch=DoScrl(&Scrl2);
          if (AE[GetCurScrl(&Scrl3)-1] & (unsigned int)(1<<(GetCurScrl(&Scrl2)))) {
             SetCurScrl(&Scrl1,2);
          } else {
             SetCurScrl(&Scrl1,1);
          }
       }
     }

     if (CurPos==2) {
       while ((ch!=KB_ENTER)&&(ch!=KB_ESC)&&(ch!=KB_UP)&&(ch!=KB_DOWN)) {
          OutPicture(78,8,RArr);
          RedrawScrl(&Scrl1);
          RedrawScrl(&Scrl2);
          RedrawScrl(&Scrl3);
          Redraw();
          ch=DoScrl(&Scrl1);
       }
     }

     if (GetCurScrl(&Scrl1)==2)
        AE[GetCurScrl(&Scrl3)-1]|=(unsigned int)(1<<(GetCurScrl(&Scrl2)));
     else
        AE[GetCurScrl(&Scrl3)-1]&=~(unsigned int)(1<<(GetCurScrl(&Scrl2)));

     if (ch==KB_DOWN) {
        if (CurPos<2) CurPos++; else CurPos=0;
        ClearLCD();
        ch=KB_NO;
     }
     if (ch==KB_UP) {
        if (CurPos) CurPos--; else CurPos=2;
        ClearLCD();
        ch=KB_NO;
     }
}
SetFont(Font8x8);
if (ch==KB_ENTER)
  for (i=0;i<MaxTransformerSide;i++)
    if (Setup.GammaSetupInfo.GammaSideSetup[i].AlarmEnable != AE[i])
      Setup.GammaSetupInfo.GammaSideSetup[i].AlarmEnable = AE[i];
}

static void SetAlarmThresh(void)
{
  KEY ch=KB_NO;
  char i,CurPos=0;
  struct ScrollerMngr Scrl2,Scrl3;
  char AE[2][MaxTransformerSide];
  float f;

  InitScroller(&Scrl2,10,8,Font8x6);
  AddScrollerString(&Scrl2,AlarmThreshStr3);
  AddScrollerString(&Scrl2,AlarmThreshStr4);

  InitScroller(&Scrl3,10,0,Font8x8);
  AddScrollerString(&Scrl3,AlarmEnableStr3);
  AddScrollerString(&Scrl3,AlarmEnableStr4);
//  AddScrollerString(&Scrl3,AlarmEnableStr5);
//  AddScrollerString(&Scrl3,AlarmEnableStr6);

  for (i=0;i<MaxTransformerSide;i++){
    AE[0][i] = Setup.GammaSetupInfo.GammaSideSetup[i].GammaYellowThresholld;
    AE[1][i] = Setup.GammaSetupInfo.GammaSideSetup[i].GammaRedThresholld;
    }

  SetCurScrl(&Scrl2,1);
  SetCurScrl(&Scrl3,1);
  ClearLCD();
  RedrawScrl(&Scrl2);
  RedrawScrl(&Scrl3);
  SetFont(Font8x6);
  OutFloat(80,8,(float)AE[GetCurScrl(&Scrl2)-1][GetCurScrl(&Scrl3)-1]*0.05,5,2);
  Redraw();

  while ((ch!=KB_ENTER)&&(ch!=KB_ESC)) {

     if (CurPos==0) {
       while ((ch!=KB_ENTER)&&(ch!=KB_ESC)&&(ch!=KB_UP)&&(ch!=KB_DOWN)) {
          OutPicture(0,0,RArr);
          RedrawScrl(&Scrl2);
          RedrawScrl(&Scrl3);
          SetFont(Font8x6);
          OutFloat(80,8,(float)AE[GetCurScrl(&Scrl2)-1][GetCurScrl(&Scrl3)-1]*0.05,5,2);
          Redraw();
          ch=DoScrl(&Scrl3);
       }
     }

     if (CurPos==1) {
       while ((ch!=KB_ENTER)&&(ch!=KB_ESC)&&(ch!=KB_UP)&&(ch!=KB_DOWN)) {
          OutPicture(0,8,RArr);
          RedrawScrl(&Scrl2);
          RedrawScrl(&Scrl3);
          SetFont(Font8x6);
          OutFloat(80,8,(float)AE[GetCurScrl(&Scrl2)-1][GetCurScrl(&Scrl3)-1]*0.05,5,2);
          Redraw();
          ch=DoScrl(&Scrl2);
       }
       if (ch==KB_ENTER) {
         f=(float)AE[GetCurScrl(&Scrl2)-1][GetCurScrl(&Scrl3)-1]*0.05;
         SetFont(Font8x6);
         f=InputFloat(80,8,f,5,2);
         AE[GetCurScrl(&Scrl2)-1][GetCurScrl(&Scrl3)-1]=(char)(f*20);
         Redraw();
         ch=KB_NO;
       }
     }

     if (ch==KB_DOWN) {
        if (CurPos<1) CurPos++; else CurPos=0;
        ClearLCD();
        ch=KB_NO;
     }
     if (ch==KB_UP) {
        if (CurPos) CurPos--; else CurPos=1;
        ClearLCD();
        ch=KB_NO;
     }
  }
SetFont(Font8x8);
if (ch==KB_ENTER)
  for (i=0;i<MaxTransformerSide;i++){
    if (Setup.GammaSetupInfo.GammaSideSetup[i].GammaYellowThresholld != AE[0][i])
      Setup.GammaSetupInfo.GammaSideSetup[i].GammaYellowThresholld = AE[0][i];
    if (Setup.GammaSetupInfo.GammaSideSetup[i].GammaRedThresholld != AE[1][i])
      Setup.GammaSetupInfo.GammaSideSetup[i].GammaRedThresholld = AE[1][i];
    }
}

static void SetTgAlarmThresh(void)
{
  KEY ch=KB_NO;
  char i,CurPos=0;
  struct ScrollerMngr Scrl2,Scrl3;
  int AE[3][MaxTransformerSide];
  float f;

  InitScroller(&Scrl2,10,8,Font8x6);
  AddScrollerString(&Scrl2,AlarmThreshStr6);
  AddScrollerString(&Scrl2,AlarmThreshStr7);
  AddScrollerString(&Scrl2,AlarmThreshStr8);

  InitScroller(&Scrl3,10,0,Font8x8);
  AddScrollerString(&Scrl3,AlarmEnableStr3);
  AddScrollerString(&Scrl3,AlarmEnableStr4);
//  AddScrollerString(&Scrl3,AlarmEnableStr5);
//  AddScrollerString(&Scrl3,AlarmEnableStr6);

  for (i=0;i<MaxTransformerSide;i++){
    AE[0][i] = Setup.GammaSetupInfo.GammaSideSetup[i].TgYellowThresholld;
    AE[1][i] = Setup.GammaSetupInfo.GammaSideSetup[i].TgRedThresholld;
    AE[2][i] = Setup.GammaSetupInfo.GammaSideSetup[i].TgVariationThresholld;
    }

  SetCurScrl(&Scrl2,1);
  SetCurScrl(&Scrl3,1);
  ClearLCD();
  RedrawScrl(&Scrl2);
  RedrawScrl(&Scrl3);
  SetFont(Font8x6);
  OutFloat(80,8,(float)AE[GetCurScrl(&Scrl2)-1][GetCurScrl(&Scrl3)-1]*0.01001,5,2);
  Redraw();

  while ((ch!=KB_ENTER)&&(ch!=KB_ESC)) {

     if (CurPos==0) {
       while ((ch!=KB_ENTER)&&(ch!=KB_ESC)&&(ch!=KB_UP)&&(ch!=KB_DOWN)) {
          OutPicture(0,0,RArr);
          RedrawScrl(&Scrl2);
          RedrawScrl(&Scrl3);
          SetFont(Font8x6);
          OutFloat(80,8,(float)AE[GetCurScrl(&Scrl2)-1][GetCurScrl(&Scrl3)-1]*0.01001,5,2);
          Redraw();
          ch=DoScrl(&Scrl3);
       }
     }

     if (CurPos==1) {
       while ((ch!=KB_ENTER)&&(ch!=KB_ESC)&&(ch!=KB_UP)&&(ch!=KB_DOWN)) {
          OutPicture(0,8,RArr);
          RedrawScrl(&Scrl2);
          RedrawScrl(&Scrl3);
          SetFont(Font8x6);
          OutFloat(80,8,(float)AE[GetCurScrl(&Scrl2)-1][GetCurScrl(&Scrl3)-1]*0.01001,5,2);
          Redraw();
          ch=DoScrl(&Scrl2);
       }
       if (ch==KB_ENTER) {
         f=(float)AE[GetCurScrl(&Scrl2)-1][GetCurScrl(&Scrl3)-1]*0.01001;
         SetFont(Font8x6);
         f=InputFloat(80,8,f,5,2);
         AE[GetCurScrl(&Scrl2)-1][GetCurScrl(&Scrl3)-1]=(char)(f*100.0001);
         Redraw();
         ch=KB_NO;
       }
     }

     if (ch==KB_DOWN) {
        if (CurPos<1) CurPos++; else CurPos=0;
        ClearLCD();
        ch=KB_NO;
     }
     if (ch==KB_UP) {
        if (CurPos) CurPos--; else CurPos=1;
        ClearLCD();
        ch=KB_NO;
     }
  }
SetFont(Font8x8);
if (ch==KB_ENTER)
  for (i=0;i<MaxTransformerSide;i++){
    if (Setup.GammaSetupInfo.GammaSideSetup[i].TgYellowThresholld != AE[0][i])
      Setup.GammaSetupInfo.GammaSideSetup[i].TgYellowThresholld = AE[0][i];
    if (Setup.GammaSetupInfo.GammaSideSetup[i].TgRedThresholld != AE[1][i])
      Setup.GammaSetupInfo.GammaSideSetup[i].TgRedThresholld = AE[1][i];
    if (Setup.GammaSetupInfo.GammaSideSetup[i].TgVariationThresholld != AE[2][i])
      Setup.GammaSetupInfo.GammaSideSetup[i].TgVariationThresholld = AE[2][i];
    }
}

static void SetTKAlarm(void)
{
  KEY ch=KB_NO;
  char i,CurPos=0;
  struct ScrollerMngr Scrl2,Scrl3;
  char AE[MaxTransformerSide];
  float f;

  InitScroller(&Scrl2,10,8,Font8x6);
  AddScrollerString(&Scrl2,TKAlarmStr3);

  InitScroller(&Scrl3,10,0,Font8x8);
  AddScrollerString(&Scrl3,AlarmEnableStr3);
  AddScrollerString(&Scrl3,AlarmEnableStr4);
//  AddScrollerString(&Scrl3,AlarmEnableStr5);
//  AddScrollerString(&Scrl3,AlarmEnableStr6);

  for (i=0;i<MaxTransformerSide;i++){
    AE[i] = Setup.GammaSetupInfo.GammaSideSetup[i].TCoefficient;
    }

  SetCurScrl(&Scrl2,1);
  SetCurScrl(&Scrl3,1);
  ClearLCD();
  RedrawScrl(&Scrl2);
  RedrawScrl(&Scrl3);
  SetFont(Font8x6);
  OutFloat(80,8,(float)AE[GetCurScrl(&Scrl3)-1]*0.002,5,3);
  Redraw();

  while ((ch!=KB_ENTER)&&(ch!=KB_ESC)) {

     if (CurPos==0) {
       while ((ch!=KB_ENTER)&&(ch!=KB_ESC)&&(ch!=KB_UP)&&(ch!=KB_DOWN)) {
          OutPicture(0,0,RArr);
          RedrawScrl(&Scrl2);
          RedrawScrl(&Scrl3);
          SetFont(Font8x6);
          OutFloat(80,8,(float)AE[GetCurScrl(&Scrl3)-1]*0.002,5,3);
          Redraw();
          ch=DoScrl(&Scrl3);
       }
     }

     if (CurPos==1) {
       while ((ch!=KB_ENTER)&&(ch!=KB_ESC)&&(ch!=KB_UP)&&(ch!=KB_DOWN)) {
          OutPicture(0,8,RArr);
          RedrawScrl(&Scrl2);
          RedrawScrl(&Scrl3);
          SetFont(Font8x6);
          OutFloat(80,8,(float)AE[GetCurScrl(&Scrl3)-1]*0.002,5,3);
          Redraw();
          ch=DoScrl(&Scrl2);
       }
       if (ch==KB_ENTER) {
         f=(float)AE[GetCurScrl(&Scrl3)-1]*0.002;
         SetFont(Font8x6);
         f=InputFloat(80,8,f,5,3);
         AE[GetCurScrl(&Scrl3)-1]=(char)(f*500);
         Redraw();
         ch=KB_NO;
       }
     }

     if (ch==KB_DOWN) {
        if (CurPos<1) CurPos++; else CurPos=0;
        ClearLCD();
        ch=KB_NO;
     }
     if (ch==KB_UP) {
        if (CurPos) CurPos--; else CurPos=1;
        ClearLCD();
        ch=KB_NO;
     }
  }
SetFont(Font8x8);
if (ch==KB_ENTER)
  for (i=0;i<MaxTransformerSide;i++){
    if (Setup.GammaSetupInfo.GammaSideSetup[i].TCoefficient != AE[i])
      Setup.GammaSetupInfo.GammaSideSetup[i].TCoefficient = AE[i];
    }
}

static void SetTrendAlarm(void)
{
  KEY ch=KB_NO;
  char i,CurPos=0;
  struct ScrollerMngr Scrl2,Scrl3;
  char AE[MaxTransformerSide];
  float f;

  InitScroller(&Scrl2,10,8,Font8x6);
  AddScrollerString(&Scrl2,TrendAlarmStr3);

  InitScroller(&Scrl3,10,0,Font8x8);
  AddScrollerString(&Scrl3,AlarmEnableStr3);
  AddScrollerString(&Scrl3,AlarmEnableStr4);
//  AddScrollerString(&Scrl3,AlarmEnableStr5);
//  AddScrollerString(&Scrl3,AlarmEnableStr6);

  for (i=0;i<MaxTransformerSide;i++){
    AE[i] = Setup.GammaSetupInfo.GammaSideSetup[i].TrendAlarm;
    }

  SetCurScrl(&Scrl2,1);
  SetCurScrl(&Scrl3,1);
  ClearLCD();
  RedrawScrl(&Scrl2);
  RedrawScrl(&Scrl3);
  SetFont(Font8x6);
  OutFloat(80,8,1*(float)AE[GetCurScrl(&Scrl3)-1],4,1); //acm, 4-27 change from .2 to 1
  Redraw();

  while ((ch!=KB_ENTER)&&(ch!=KB_ESC)) {

     if (CurPos==0) {
       while ((ch!=KB_ENTER)&&(ch!=KB_ESC)&&(ch!=KB_UP)&&(ch!=KB_DOWN)) {
          OutPicture(0,0,RArr);
          RedrawScrl(&Scrl2);
          RedrawScrl(&Scrl3);
          SetFont(Font8x6);
          OutFloat(80,8,(float)AE[GetCurScrl(&Scrl3)-1]*1,4,1); //acm, 4-27 change from .2 to 1
          Redraw();
          ch=DoScrl(&Scrl3);
       }
     }

     if (CurPos==1) {
       while ((ch!=KB_ENTER)&&(ch!=KB_ESC)&&(ch!=KB_UP)&&(ch!=KB_DOWN)) {
          OutPicture(0,8,RArr);
          RedrawScrl(&Scrl2);
          RedrawScrl(&Scrl3);
          SetFont(Font8x6);
          OutFloat(80,8,(float)AE[GetCurScrl(&Scrl3)-1]*1,4,1); //acm, 4-27 change from .2 to 1
          Redraw();
          ch=DoScrl(&Scrl2);
       }
       if (ch==KB_ENTER) {
         f=(float)AE[GetCurScrl(&Scrl3)-1]*1;  //acm, 4-27 change from .2 to 1
         SetFont(Font8x6);
         f=InputFloat(80,8,f,4,1);
         AE[GetCurScrl(&Scrl3)-1]=(char)(f*5);
         Redraw();
         ch=KB_NO;
       }
     }

     if (ch==KB_DOWN) {
        if (CurPos<1) CurPos++; else CurPos=0;
        ClearLCD();
        ch=KB_NO;
     }
     if (ch==KB_UP) {
        ClearLCD();
        if (CurPos) CurPos--; else CurPos=1;
        ch=KB_NO;
     }
  }
SetFont(Font8x8);
if (ch==KB_ENTER)
  for (i=0;i<MaxTransformerSide;i++){
    if (Setup.GammaSetupInfo.GammaSideSetup[i].TrendAlarm != AE[i])
      Setup.GammaSetupInfo.GammaSideSetup[i].TrendAlarm = AE[i];
    }
}

static void SetRVoltage(void)
{
  KEY ch=KB_NO;
  char i,CurPos=0;
  struct ScrollerMngr Scrl2,Scrl3;
  float AE[MaxTransformerSide];
  float f;

  InitScroller(&Scrl2,10,8,Font8x6);
  AddScrollerString(&Scrl2,RVoltageStr3);

  InitScroller(&Scrl3,10,0,Font8x8);
  AddScrollerString(&Scrl3,AlarmEnableStr3);
  AddScrollerString(&Scrl3,AlarmEnableStr4);
//  AddScrollerString(&Scrl3,AlarmEnableStr5);
//  AddScrollerString(&Scrl3,AlarmEnableStr6);

  for (i=0;i<MaxTransformerSide;i++){
    AE[i] = Setup.GammaSetupInfo.GammaSideSetup[i].RatedVoltage;
    }

  SetCurScrl(&Scrl2,1);
  SetCurScrl(&Scrl3,1);
  ClearLCD();
  RedrawScrl(&Scrl2);
  RedrawScrl(&Scrl3);
  SetFont(Font8x6);
  OutFloat(74,8,(float)AE[GetCurScrl(&Scrl3)-1],6,0);
  Redraw();

  while ((ch!=KB_ENTER)&&(ch!=KB_ESC)) {

     if (CurPos==0) {
       while ((ch!=KB_ENTER)&&(ch!=KB_ESC)&&(ch!=KB_UP)&&(ch!=KB_DOWN)) {
          OutPicture(0,0,RArr);
          RedrawScrl(&Scrl2);
          RedrawScrl(&Scrl3);
          SetFont(Font8x6);
          OutFloat(68,8,(float)AE[GetCurScrl(&Scrl3)-1],7,2);
          Redraw();
          ch=DoScrl(&Scrl3);
       }
     }

     if (CurPos==1) {
       while ((ch!=KB_ENTER)&&(ch!=KB_ESC)&&(ch!=KB_UP)&&(ch!=KB_DOWN)) {
          OutPicture(0,8,RArr);
          RedrawScrl(&Scrl2);
          RedrawScrl(&Scrl3);
          SetFont(Font8x6);
          OutFloat(68,8,(float)AE[GetCurScrl(&Scrl3)-1],7,2);
          Redraw();
          ch=DoScrl(&Scrl2);
       }
       if (ch==KB_ENTER) {
         f=(float)AE[GetCurScrl(&Scrl3)-1];
         SetFont(Font8x6);
         f=InputFloat(68,8,f,7,2);
         AE[GetCurScrl(&Scrl3)-1]=f;
         Redraw();
         ch=KB_NO;
       }
     }

     if (ch==KB_DOWN) {
        if (CurPos<1) CurPos++; else CurPos=0;
        ClearLCD();
        ch=KB_NO;
     }
     if (ch==KB_UP) {
        ClearLCD();
        if (CurPos) CurPos--; else CurPos=1;
        ch=KB_NO;
     }
  }
SetFont(Font8x8);
if (ch==KB_ENTER)
  for (i=0;i<MaxTransformerSide;i++){
    if (Setup.GammaSetupInfo.GammaSideSetup[i].RatedVoltage != AE[i])
      Setup.GammaSetupInfo.GammaSideSetup[i].RatedVoltage = AE[i];
    }
}

static void SetRCurrent(void)
{
  KEY ch=KB_NO;
  char i,CurPos=0;
  struct ScrollerMngr Scrl2,Scrl3;
  float AE[MaxTransformerSide];
  float f;

  InitScroller(&Scrl2,10,8,Font8x6);
  AddScrollerString(&Scrl2,RCurrentStr3);

  InitScroller(&Scrl3,10,0,Font8x8);
  AddScrollerString(&Scrl3,AlarmEnableStr3);
  AddScrollerString(&Scrl3,AlarmEnableStr4);
//  AddScrollerString(&Scrl3,AlarmEnableStr5);
//  AddScrollerString(&Scrl3,AlarmEnableStr6);

  for (i=0;i<MaxTransformerSide;i++){
    AE[i] = Setup.GammaSetupInfo.GammaSideSetup[i].RatedCurrent;
    }

  SetCurScrl(&Scrl2,1);
  SetCurScrl(&Scrl3,1);
  ClearLCD();
  RedrawScrl(&Scrl2);
  RedrawScrl(&Scrl3);
  SetFont(Font8x6);
  OutFloat(68,8,AE[GetCurScrl(&Scrl3)-1],7,2);
  Redraw();

  while ((ch!=KB_ENTER)&&(ch!=KB_ESC)) {

     if (CurPos==0) {
       while ((ch!=KB_ENTER)&&(ch!=KB_ESC)&&(ch!=KB_UP)&&(ch!=KB_DOWN)) {
          OutPicture(0,0,RArr);
          RedrawScrl(&Scrl2);
          RedrawScrl(&Scrl3);
          SetFont(Font8x6);
          OutFloat(68,8,AE[GetCurScrl(&Scrl3)-1],7,2);
          Redraw();
          ch=DoScrl(&Scrl3);
       }
     }

     if (CurPos==1) {
       while ((ch!=KB_ENTER)&&(ch!=KB_ESC)&&(ch!=KB_UP)&&(ch!=KB_DOWN)) {
          OutPicture(0,8,RArr);
          RedrawScrl(&Scrl2);
          RedrawScrl(&Scrl3);
          SetFont(Font8x6);
          OutFloat(68,8,AE[GetCurScrl(&Scrl3)-1],7,2);
          Redraw();
          ch=DoScrl(&Scrl2);
       }
       if (ch==KB_ENTER) {
         f=AE[GetCurScrl(&Scrl3)-1];
         SetFont(Font8x6);
         f=InputFloat(68,8,f,7,2);
         AE[GetCurScrl(&Scrl3)-1]=f;
         Redraw();
         ch=KB_NO;
       }
     }

     if (ch==KB_DOWN) {
        if (CurPos<1) CurPos++; else CurPos=0;
        ClearLCD();
        ch=KB_NO;
     }
     if (ch==KB_UP) {
        ClearLCD();
        if (CurPos) CurPos--; else CurPos=1;
        ch=KB_NO;
     }
  }
SetFont(Font8x8);
if (ch==KB_ENTER)
  for (i=0;i<MaxTransformerSide;i++){
    if (Setup.GammaSetupInfo.GammaSideSetup[i].RatedCurrent != AE[i])
      Setup.GammaSetupInfo.GammaSideSetup[i].RatedCurrent = AE[i];
    }
}

static void SetTg0(void)
{
  KEY ch=KB_NO;
  char i,CurPos=0;
  struct ScrollerMngr Scrl2,Scrl3;
  int AE[MaxBushingCountOnSide][MaxTransformerSide];
  float f;

  InitScroller(&Scrl2,10,8,Font8x6);
  AddScrollerString(&Scrl2,Tg0Str3);
  AddScrollerString(&Scrl2,Tg0Str4);
  AddScrollerString(&Scrl2,Tg0Str5);

  InitScroller(&Scrl3,10,0,Font8x8);
  AddScrollerString(&Scrl3,AlarmEnableStr3);
  AddScrollerString(&Scrl3,AlarmEnableStr4);
//  AddScrollerString(&Scrl3,AlarmEnableStr5);
//  AddScrollerString(&Scrl3,AlarmEnableStr6);

  for (i=0;i<MaxTransformerSide;i++){
    AE[0][i] = Setup.GammaSetupInfo.GammaSideSetup[i].Tg0[0];
    AE[1][i] = Setup.GammaSetupInfo.GammaSideSetup[i].Tg0[1];
    AE[2][i] = Setup.GammaSetupInfo.GammaSideSetup[i].Tg0[2];
    }

  SetCurScrl(&Scrl2,1);
  SetCurScrl(&Scrl3,1);
  ClearLCD();
  RedrawScrl(&Scrl2);
  RedrawScrl(&Scrl3);
  SetFont(Font8x6);
  OutFloat(80,8,(float)AE[GetCurScrl(&Scrl2)-1][GetCurScrl(&Scrl3)-1]*0.01,5,2);
  Redraw();

  while ((ch!=KB_ENTER)&&(ch!=KB_ESC)) {

     if (CurPos==0) {
       while ((ch!=KB_ENTER)&&(ch!=KB_ESC)&&(ch!=KB_UP)&&(ch!=KB_DOWN)) {
          OutPicture(0,0,RArr);
          RedrawScrl(&Scrl2);
          RedrawScrl(&Scrl3);
          SetFont(Font8x6);
          OutFloat(80,8,(float)AE[GetCurScrl(&Scrl2)-1][GetCurScrl(&Scrl3)-1]*0.01,5,2);
          Redraw();
          ch=DoScrl(&Scrl3);
       }
     }

     if (CurPos==1) {
       while ((ch!=KB_ENTER)&&(ch!=KB_ESC)&&(ch!=KB_UP)&&(ch!=KB_DOWN)) {
          OutPicture(0,8,RArr);
          RedrawScrl(&Scrl2);
          RedrawScrl(&Scrl3);
          SetFont(Font8x6);
          OutFloat(80,8,(float)AE[GetCurScrl(&Scrl2)-1][GetCurScrl(&Scrl3)-1]*0.01,5,2);
          Redraw();
          ch=DoScrl(&Scrl2);
       }
       if (ch==KB_ENTER) {
         f=(float)AE[GetCurScrl(&Scrl2)-1][GetCurScrl(&Scrl3)-1]*0.01;
         SetFont(Font8x6);
         f=InputFloat(80,8,f,5,2);
         AE[GetCurScrl(&Scrl2)-1][GetCurScrl(&Scrl3)-1]=(int)(f*100);
         Redraw();
         ch=KB_NO;
       }
     }

     if (ch==KB_DOWN) {
        if (CurPos<1) CurPos++; else CurPos=0;
        ClearLCD();
        ch=KB_NO;
     }
     if (ch==KB_UP) {
        ClearLCD();
        if (CurPos) CurPos--; else CurPos=1;
        ch=KB_NO;
     }
  }
SetFont(Font8x8);
if (ch==KB_ENTER)
  for (i=0;i<MaxTransformerSide;i++){
    if (Setup.GammaSetupInfo.GammaSideSetup[i].Tg0[0]!=AE[0][i])
      Setup.GammaSetupInfo.GammaSideSetup[i].Tg0[0]=AE[0][i];
    if (Setup.GammaSetupInfo.GammaSideSetup[i].Tg0[1]!=AE[1][i])
      Setup.GammaSetupInfo.GammaSideSetup[i].Tg0[1]=AE[1][i];
    if (Setup.GammaSetupInfo.GammaSideSetup[i].Tg0[2]!=AE[2][i])
      Setup.GammaSetupInfo.GammaSideSetup[i].Tg0[2]=AE[2][i];
    }
}

static void SetC0(void)
{
  KEY ch=KB_NO;
  char i,CurPos=0;
  struct ScrollerMngr Scrl2,Scrl3;
  u16 AE[MaxBushingCountOnSide][MaxTransformerSide];
  float f;

  InitScroller(&Scrl2,10,8,Font8x6);
  AddScrollerString(&Scrl2,C0Str3);
  AddScrollerString(&Scrl2,C0Str4);
  AddScrollerString(&Scrl2,C0Str5);

  InitScroller(&Scrl3,10,0,Font8x8);
  AddScrollerString(&Scrl3,AlarmEnableStr3);
  AddScrollerString(&Scrl3,AlarmEnableStr4);
//  AddScrollerString(&Scrl3,AlarmEnableStr5);
//  AddScrollerString(&Scrl3,AlarmEnableStr6);

  for (i=0;i<MaxTransformerSide;i++){
    AE[0][i] = Setup.GammaSetupInfo.GammaSideSetup[i].C0[0];
    AE[1][i] = Setup.GammaSetupInfo.GammaSideSetup[i].C0[1];
    AE[2][i] = Setup.GammaSetupInfo.GammaSideSetup[i].C0[2];
    }

  SetCurScrl(&Scrl2,1);
  SetCurScrl(&Scrl3,1);
  ClearLCD();
  RedrawScrl(&Scrl2);
  RedrawScrl(&Scrl3);
  SetFont(Font8x6);
  OutFloat(80,8,(float)AE[GetCurScrl(&Scrl2)-1][GetCurScrl(&Scrl3)-1]*0.1,5,1);
  Redraw();

  while ((ch!=KB_ENTER)&&(ch!=KB_ESC)) {

     if (CurPos==0) {
       while ((ch!=KB_ENTER)&&(ch!=KB_ESC)&&(ch!=KB_UP)&&(ch!=KB_DOWN)) {
          OutPicture(0,0,RArr);
          RedrawScrl(&Scrl2);
          RedrawScrl(&Scrl3);
          SetFont(Font8x6);
          OutFloat(80,8,(float)AE[GetCurScrl(&Scrl2)-1][GetCurScrl(&Scrl3)-1]*0.1,5,1);
          Redraw();
          ch=DoScrl(&Scrl3);
       }
     }

     if (CurPos==1) {
       while ((ch!=KB_ENTER)&&(ch!=KB_ESC)&&(ch!=KB_UP)&&(ch!=KB_DOWN)) {
          OutPicture(0,8,RArr);
          RedrawScrl(&Scrl2);
          RedrawScrl(&Scrl3);
          SetFont(Font8x6);
          OutFloat(80,8,(float)AE[GetCurScrl(&Scrl2)-1][GetCurScrl(&Scrl3)-1]*0.1,5,1);
          Redraw();
          ch=DoScrl(&Scrl2);
       }
       if (ch==KB_ENTER) {
         f=(float)AE[GetCurScrl(&Scrl2)-1][GetCurScrl(&Scrl3)-1]*0.1;
         SetFont(Font8x6);
         f=InputFloat(80,8,f,5,1);
         AE[GetCurScrl(&Scrl2)-1][GetCurScrl(&Scrl3)-1]=(unsigned int)(fabs(f)*10);
         Redraw();
         ch=KB_NO;
       }
     }

     if (ch==KB_DOWN) {
        if (CurPos<1) CurPos++; else CurPos=0;
        ClearLCD();
        ch=KB_NO;
     }
     if (ch==KB_UP) {
        ClearLCD();
        if (CurPos) CurPos--; else CurPos=1;
        ch=KB_NO;
     }
  }
SetFont(Font8x8);
if (ch==KB_ENTER)
  for (i=0;i<MaxTransformerSide;i++){
    if (Setup.GammaSetupInfo.GammaSideSetup[i].C0[0]!=AE[0][i])
      Setup.GammaSetupInfo.GammaSideSetup[i].C0[0]=AE[0][i];
    if (Setup.GammaSetupInfo.GammaSideSetup[i].C0[1]!=AE[1][i])
      Setup.GammaSetupInfo.GammaSideSetup[i].C0[1]=AE[1][i];
    if (Setup.GammaSetupInfo.GammaSideSetup[i].C0[2]!=AE[2][i])
      Setup.GammaSetupInfo.GammaSideSetup[i].C0[2]=AE[2][i];
    }
}

static void SetTgTemp(void)
{
  KEY ch=KB_NO;
  char i,CurPos=0;
  struct ScrollerMngr Scrl2,Scrl3;
  char AE[MaxTransformerSide];
  float f;

  InitScroller(&Scrl2,10,8,Font8x6);
  AddScrollerString(&Scrl2,TgTempStr3);

  InitScroller(&Scrl3,10,0,Font8x8);
  AddScrollerString(&Scrl3,AlarmEnableStr3);
  AddScrollerString(&Scrl3,AlarmEnableStr4);
//  AddScrollerString(&Scrl3,AlarmEnableStr5);
//  AddScrollerString(&Scrl3,AlarmEnableStr6);

  for (i=0;i<MaxTransformerSide;i++){
    AE[i] = Setup.GammaSetupInfo.GammaSideSetup[i].Temperature0;
    }

  SetCurScrl(&Scrl2,1);
  SetCurScrl(&Scrl3,1);
  ClearLCD();
  RedrawScrl(&Scrl2);
  RedrawScrl(&Scrl3);
  SetFont(Font8x6);
  OutFloat(80,8,(float)AE[GetCurScrl(&Scrl3)-1]-70,4,0);
  Redraw();

  while ((ch!=KB_ENTER)&&(ch!=KB_ESC)) {

     if (CurPos==0) {
       while ((ch!=KB_ENTER)&&(ch!=KB_ESC)&&(ch!=KB_UP)&&(ch!=KB_DOWN)) {
          OutPicture(0,0,RArr);
          RedrawScrl(&Scrl2);
          RedrawScrl(&Scrl3);
          SetFont(Font8x6);
          OutFloat(80,8,(float)AE[GetCurScrl(&Scrl3)-1]-70,4,0);
          Redraw();
          ch=DoScrl(&Scrl3);
       }
     }

     if (CurPos==1) {
       while ((ch!=KB_ENTER)&&(ch!=KB_ESC)&&(ch!=KB_UP)&&(ch!=KB_DOWN)) {
          OutPicture(0,8,RArr);
          RedrawScrl(&Scrl2);
          RedrawScrl(&Scrl3);
          SetFont(Font8x6);
          OutFloat(80,8,(float)AE[GetCurScrl(&Scrl3)-1]-70,4,0);
          Redraw();
          ch=DoScrl(&Scrl2);
       }
       if (ch==KB_ENTER) {
         f=(float)AE[GetCurScrl(&Scrl3)-1]-70;
         SetFont(Font8x6);
         f=InputFloat(80,8,f,4,0);
         AE[GetCurScrl(&Scrl3)-1]=(char)(f+70);
         Redraw();
         ch=KB_NO;
       }
     }

     if (ch==KB_DOWN) {
        if (CurPos<1) CurPos++; else CurPos=0;
        ClearLCD();
        ch=KB_NO;
     }
     if (ch==KB_UP) {
        ClearLCD();
        if (CurPos) CurPos--; else CurPos=1;
        ch=KB_NO;
     }
  }
SetFont(Font8x8);
if (ch==KB_ENTER)
  for (i=0;i<MaxTransformerSide;i++){
    if (Setup.GammaSetupInfo.GammaSideSetup[i].Temperature0 != AE[i])
      Setup.GammaSetupInfo.GammaSideSetup[i].Temperature0 = AE[i];
    }
}

static void SetInputC(void)
{
  KEY ch=KB_NO;
  char i,CurPos=0;
  struct ScrollerMngr Scrl2,Scrl3;
  u16 AE[MaxBushingCountOnSide][MaxTransformerSide];
  float f;

  InitScroller(&Scrl2,10,8,Font8x6);
  AddScrollerString(&Scrl2,InputCStr3);
  AddScrollerString(&Scrl2,InputCStr4);
  AddScrollerString(&Scrl2,InputCStr5);

  InitScroller(&Scrl3,10,0,Font8x8);
  AddScrollerString(&Scrl3,AlarmEnableStr3);
  AddScrollerString(&Scrl3,AlarmEnableStr4);
//  AddScrollerString(&Scrl3,AlarmEnableStr5);
//  AddScrollerString(&Scrl3,AlarmEnableStr6);

  for (i=0;i<MaxTransformerSide;i++){
    AE[0][i] = Setup.GammaSetupInfo.GammaSideSetup[i].InputC[0];
    AE[1][i] = Setup.GammaSetupInfo.GammaSideSetup[i].InputC[1];
    AE[2][i] = Setup.GammaSetupInfo.GammaSideSetup[i].InputC[2];
    }

  SetCurScrl(&Scrl2,1);
  SetCurScrl(&Scrl3,1);
  ClearLCD();
  RedrawScrl(&Scrl2);
  RedrawScrl(&Scrl3);
  SetFont(Font8x6);
  OutFloat(72,8,(float)AE[GetCurScrl(&Scrl2)-1][GetCurScrl(&Scrl3)-1]*0.01,6,2);
  Redraw();

  while ((ch!=KB_ENTER)&&(ch!=KB_ESC)) {

     if (CurPos==0) {
       while ((ch!=KB_ENTER)&&(ch!=KB_ESC)&&(ch!=KB_UP)&&(ch!=KB_DOWN)) {
          OutPicture(0,0,RArr);
          RedrawScrl(&Scrl2);
          RedrawScrl(&Scrl3);
          SetFont(Font8x6);
          OutFloat(72,8,(float)AE[GetCurScrl(&Scrl2)-1][GetCurScrl(&Scrl3)-1]*0.01,6,2);
          Redraw();
          ch=DoScrl(&Scrl3);
       }
     }

     if (CurPos==1) {
       while ((ch!=KB_ENTER)&&(ch!=KB_ESC)&&(ch!=KB_UP)&&(ch!=KB_DOWN)) {
          OutPicture(0,8,RArr);
          RedrawScrl(&Scrl2);
          RedrawScrl(&Scrl3);
          SetFont(Font8x6);
          OutFloat(72,8,(float)AE[GetCurScrl(&Scrl2)-1][GetCurScrl(&Scrl3)-1]*0.01,6,2);
          Redraw();
          ch=DoScrl(&Scrl2);
       }
       if (ch==KB_ENTER) {
         f=(float)AE[GetCurScrl(&Scrl2)-1][GetCurScrl(&Scrl3)-1]*0.01;
         SetFont(Font8x6);
         f=InputFloat(72,8,f,6,2);
         AE[GetCurScrl(&Scrl2)-1][GetCurScrl(&Scrl3)-1]=(unsigned int)(fabs(f)*100.00001);
         Redraw();
         ch=KB_NO;
       }
     }

     if (ch==KB_DOWN) {
        if (CurPos<1) CurPos++; else CurPos=0;
        ClearLCD();
        ch=KB_NO;
     }
     if (ch==KB_UP) {
        ClearLCD();
        if (CurPos) CurPos--; else CurPos=1;
        ch=KB_NO;
     }
  }
SetFont(Font8x8);
if (ch==KB_ENTER)
  for (i=0;i<MaxTransformerSide;i++){
    if (Setup.GammaSetupInfo.GammaSideSetup[i].InputC[0]!=AE[0][i])
      Setup.GammaSetupInfo.GammaSideSetup[i].InputC[0]=AE[0][i];
    if (Setup.GammaSetupInfo.GammaSideSetup[i].InputC[1]!=AE[1][i])
      Setup.GammaSetupInfo.GammaSideSetup[i].InputC[1]=AE[1][i];
    if (Setup.GammaSetupInfo.GammaSideSetup[i].InputC[2]!=AE[2][i])
      Setup.GammaSetupInfo.GammaSideSetup[i].InputC[2]=AE[2][i];
    }
}

static void SetImpedance(void)
{
  KEY ch=KB_NO;
  char i,CurPos=0;
  struct ScrollerMngr Scrl2,Scrl3;
  u16 AE[MaxBushingCountOnSide][MaxTransformerSide];
  float f;

  InitScroller(&Scrl2,10,8,Font8x6);
  AddScrollerString(&Scrl2,ImpedanceStr3);
  AddScrollerString(&Scrl2,ImpedanceStr4);
  AddScrollerString(&Scrl2,ImpedanceStr5);

  InitScroller(&Scrl3,10,0,Font8x8);
  AddScrollerString(&Scrl3,AlarmEnableStr3);
  AddScrollerString(&Scrl3,AlarmEnableStr4);
//  AddScrollerString(&Scrl3,AlarmEnableStr5);
//  AddScrollerString(&Scrl3,AlarmEnableStr6);

  for (i=0;i<MaxTransformerSide;i++){
    AE[0][i] = Setup.GammaSetupInfo.GammaSideSetup[i].InputImpedance[0];
    AE[1][i] = Setup.GammaSetupInfo.GammaSideSetup[i].InputImpedance[1];
    AE[2][i] = Setup.GammaSetupInfo.GammaSideSetup[i].InputImpedance[2];
    }

  SetCurScrl(&Scrl2,1);
  SetCurScrl(&Scrl3,1);
  ClearLCD();
  RedrawScrl(&Scrl2);
  RedrawScrl(&Scrl3);
  SetFont(Font8x6);
  OutFloat(72,8,(float)AE[GetCurScrl(&Scrl2)-1][GetCurScrl(&Scrl3)-1]*0.01,6,2);
  Redraw();

  while ((ch!=KB_ENTER)&&(ch!=KB_ESC)) {

     if (CurPos==0) {
       while ((ch!=KB_ENTER)&&(ch!=KB_ESC)&&(ch!=KB_UP)&&(ch!=KB_DOWN)) {
          OutPicture(0,0,RArr);
          RedrawScrl(&Scrl2);
          RedrawScrl(&Scrl3);
          SetFont(Font8x6);
          OutFloat(72,8,(float)AE[GetCurScrl(&Scrl2)-1][GetCurScrl(&Scrl3)-1]*0.01,6,2);
          Redraw();
          ch=DoScrl(&Scrl3);
       }
     }

     if (CurPos==1) {
       while ((ch!=KB_ENTER)&&(ch!=KB_ESC)&&(ch!=KB_UP)&&(ch!=KB_DOWN)) {
          OutPicture(0,8,RArr);
          RedrawScrl(&Scrl2);
          RedrawScrl(&Scrl3);
          SetFont(Font8x6);
          OutFloat(72,8,(float)AE[GetCurScrl(&Scrl2)-1][GetCurScrl(&Scrl3)-1]*0.01,6,2);
          Redraw();
          ch=DoScrl(&Scrl2);
       }
       if (ch==KB_ENTER) {
         f=(float)AE[GetCurScrl(&Scrl2)-1][GetCurScrl(&Scrl3)-1]*0.01;
         SetFont(Font8x6);
         f=InputFloat(72,8,f,6,2);
         AE[GetCurScrl(&Scrl2)-1][GetCurScrl(&Scrl3)-1]=(unsigned int)(fabs(f)*100);
         Redraw();
         ch=KB_NO;
       }
     }

     if (ch==KB_DOWN) {
        if (CurPos<1) CurPos++; else CurPos=0;
        ClearLCD();
        ch=KB_NO;
     }
     if (ch==KB_UP) {
        ClearLCD();
        if (CurPos) CurPos--; else CurPos=1;
        ch=KB_NO;
     }
  }
SetFont(Font8x8);
if (ch==KB_ENTER)
  for (i=0;i<MaxTransformerSide;i++){
    if (Setup.GammaSetupInfo.GammaSideSetup[i].InputImpedance[0]!=AE[0][i])
      Setup.GammaSetupInfo.GammaSideSetup[i].InputImpedance[0]=AE[0][i];
    if (Setup.GammaSetupInfo.GammaSideSetup[i].InputImpedance[1]!=AE[1][i])
      Setup.GammaSetupInfo.GammaSideSetup[i].InputImpedance[1]=AE[1][i];
    if (Setup.GammaSetupInfo.GammaSideSetup[i].InputImpedance[2]!=AE[2][i])
      Setup.GammaSetupInfo.GammaSideSetup[i].InputImpedance[2]=AE[2][i];
    }
}

static void SetCalcZk(void)
{
  KEY ch=KB_NO;
  char CurPos=0;
  struct ScrollerMngr Scrl1,Scrl3;
  char AE;

  InitScroller(&Scrl1,88,8,Font8x8);
  AddScrollerString(&Scrl1,OnOffSideStr3);
  AddScrollerString(&Scrl1,OnOffSideStr4);

  InitScroller(&Scrl3,10,0,Font8x8);
  AddScrollerString(&Scrl3,AlarmEnableStr14);
//  AddScrollerString(&Scrl3,AlarmEnableStr5);
//  AddScrollerString(&Scrl3,AlarmEnableStr6);

  AE = Setup.ZkSetupInfo.CalcZkOnSide;

  SetCurScrl(&Scrl3,1);
  if (AE & (unsigned int)(1<<(GetCurScrl(&Scrl3)-1))) {
     SetCurScrl(&Scrl1,2);
  } else {
     SetCurScrl(&Scrl1,1);
  }
     ClearLCD();
     RedrawScrl(&Scrl1);
     RedrawScrl(&Scrl3);

  while ((ch!=KB_ENTER)&&(ch!=KB_ESC)) {

     if (CurPos==0) {
       while ((ch!=KB_ENTER)&&(ch!=KB_ESC)&&(ch!=KB_UP)&&(ch!=KB_DOWN)) {
          OutPicture(0,0,RArr);
          RedrawScrl(&Scrl1);
          RedrawScrl(&Scrl3);
          SetFont(Font8x6);
          OutString(10,8,CalcZkStr3);
          Redraw();
          ch=DoScrl(&Scrl3);
          if (AE & (unsigned int)(1<<(GetCurScrl(&Scrl3)-1))) {
             SetCurScrl(&Scrl1,2);
          } else {
             SetCurScrl(&Scrl1,1);
          }
       }
     }

     if (CurPos==1) {
       while ((ch!=KB_ENTER)&&(ch!=KB_ESC)&&(ch!=KB_UP)&&(ch!=KB_DOWN)) {
          OutPicture(0,8,RArr);
          RedrawScrl(&Scrl1);
          RedrawScrl(&Scrl3);
          SetFont(Font8x6);
          OutString(10,8,CalcZkStr3);
          Redraw();
          ch=DoScrl(&Scrl1);
       }
     }

     if (GetCurScrl(&Scrl1)==2)
        AE|=(unsigned int)(1<<(GetCurScrl(&Scrl3)-1));
     else
        AE&=~(unsigned int)(1<<(GetCurScrl(&Scrl3)-1));

     if (ch==KB_DOWN) {
        if (CurPos<1) CurPos++; else CurPos=0;
        ClearLCD();
        ch=KB_NO;
     }
     if (ch==KB_UP) {
        if (CurPos) CurPos--; else CurPos=1;
        ClearLCD();
        ch=KB_NO;
     }
}
SetFont(Font8x8);
if (ch==KB_ENTER)
  if (Setup.ZkSetupInfo.CalcZkOnSide != AE)
    Setup.ZkSetupInfo.CalcZkOnSide = AE;
}

static void SetReadSides(void)
{
  KEY ch=KB_NO;
  char CurPos=0;
  struct ScrollerMngr Scrl1,Scrl3;
  char AE;

  InitScroller(&Scrl1,88,8,Font8x8);
  AddScrollerString(&Scrl1,OnOffSideStr3);
  AddScrollerString(&Scrl1,OnOffSideStr4);

  InitScroller(&Scrl3,10,0,Font8x8);
  AddScrollerString(&Scrl3,AlarmEnableStr3);
  AddScrollerString(&Scrl3,AlarmEnableStr4);
//  AddScrollerString(&Scrl3,AlarmEnableStr5);
//  AddScrollerString(&Scrl3,AlarmEnableStr6);

  AE = Setup.GammaSetupInfo.ReadOnSide;

  SetCurScrl(&Scrl3,1);
  if (AE & (unsigned int)(1<<(GetCurScrl(&Scrl3)-1))) {
     SetCurScrl(&Scrl1,2);
  } else {
     SetCurScrl(&Scrl1,1);
  }
     ClearLCD();
     RedrawScrl(&Scrl1);
     RedrawScrl(&Scrl3);

  while ((ch!=KB_ENTER)&&(ch!=KB_ESC)) {

     if (CurPos==0) {
       while ((ch!=KB_ENTER)&&(ch!=KB_ESC)&&(ch!=KB_UP)&&(ch!=KB_DOWN)) {
          OutPicture(0,0,RArr);
          RedrawScrl(&Scrl1);
          RedrawScrl(&Scrl3);
          SetFont(Font8x6);
          OutString(10,8,ReadTimeStr1);
          Redraw();
          ch=DoScrl(&Scrl3);
          if (AE & (unsigned int)(1<<(GetCurScrl(&Scrl3)-1))) {
             SetCurScrl(&Scrl1,2);
          } else {
             SetCurScrl(&Scrl1,1);
          }
       }
     }

     if (CurPos==1) {
       while ((ch!=KB_ENTER)&&(ch!=KB_ESC)&&(ch!=KB_UP)&&(ch!=KB_DOWN)) {
          OutPicture(0,8,RArr);
          RedrawScrl(&Scrl1);
          RedrawScrl(&Scrl3);
          SetFont(Font8x6);
          OutString(10,8,ReadTimeStr1);
          Redraw();
          ch=DoScrl(&Scrl1);
       }
     }

     if (GetCurScrl(&Scrl1)==2)
        AE|=(unsigned int)(1<<(GetCurScrl(&Scrl3)-1));
     else
        AE&=~(unsigned int)(1<<(GetCurScrl(&Scrl3)-1));

     if (ch==KB_DOWN) {
        if (CurPos<1) CurPos++; else CurPos=0;
        ClearLCD();
        ch=KB_NO;
     }
     if (ch==KB_UP) {
        if (CurPos) CurPos--; else CurPos=1;
        ClearLCD();
        ch=KB_NO;
     }
}
SetFont(Font8x8);
if (ch==KB_ENTER)
  if (Setup.GammaSetupInfo.ReadOnSide != AE)
    Setup.GammaSetupInfo.ReadOnSide = AE;
}

static void SetCurrentTypeSide(void)
{ struct ScrollerMngr Scrl1;//,Scrl2;
  unsigned int ch=KB_NO;
//  char Cur=1;

ClearLCD();

//InitScroller(&Scrl2,8,8,Font8x6);
//AddScrollerString(&Scrl2,AlarmEnableStr3);
//AddScrollerString(&Scrl2,AlarmEnableStr4);
//AddScrollerString(&Scrl2,AlarmEnableStr5);
//AddScrollerString(&Scrl2,AlarmEnableStr6);

//SetCurScrl(&Scrl2,Setup.GammaSetupInfo.CurrentChannel+1);

InitScroller(&Scrl1,90,8,Font8x6);
AddScrollerString(&Scrl1,CurrentTypeStr03);
AddScrollerString(&Scrl1,CurrentTypeStr3);
AddScrollerString(&Scrl1,CurrentTypeStr4);
AddScrollerString(&Scrl1,CurrentTypeStr5);
AddScrollerString(&Scrl1,CurrentTypeStr11);

SetCurScrl(&Scrl1,Setup.GammaSetupInfo.LoadChannel+1);

SetFont(Font8x6);
while ((ch!=KB_ENTER)&&(ch!=KB_ESC)) {
   SetFont(Font8x6);
   OutString(4,0,CurrentTypeStr6);
   OutString(4,8,CurrentTypeStr7);
   OutPicture(80,8,RArr);
   RedrawScrl(&Scrl1);
   Redraw();
   ch=DoScrl(&Scrl1);
}
if (ch==KB_ENTER) {
/*
     if (Setup.ZkSetupInfo.CurrentType!=GetCurScrl(&Scrl1)-1)
       Setup.ZkSetupInfo.CurrentType=GetCurScrl(&Scrl1)-1;
     if (Setup.ZkSetupInfo.CurrentSide!=GetCurScrl(&Scrl2)-1)
       Setup.ZkSetupInfo.CurrentSide=GetCurScrl(&Scrl2)-1;
*/
     Setup.GammaSetupInfo.LoadChannel=GetCurScrl(&Scrl1)-1;
     ch=KB_NO;
}
SetFont(Font8x8);
}

/*
static void SetCalibrV(void)
{
  KEY ch=KB_NO;
  char i,CurPos=0,j,aAmplifIndex;
  struct ScrollerMngr Scrl2,Scrl3;
  float AE[MaxTransformerSide][MaxBushingCountOnSide];
  float f,f2;

  InitScroller(&Scrl2,10,8,Font8x6);
  AddScrollerString(&Scrl2,CurrentTypeStr3);
  AddScrollerString(&Scrl2,CurrentTypeStr4);
  AddScrollerString(&Scrl2,CurrentTypeStr5);

  InitScroller(&Scrl3,10,0,Font8x6);
  AddScrollerString(&Scrl3,AlarmEnableStr3);
  AddScrollerString(&Scrl3,AlarmEnableStr4);
//  AddScrollerString(&Scrl3,AlarmEnableStr5);
//  AddScrollerString(&Scrl3,AlarmEnableStr6);

  for (i=0;i<MaxTransformerSide;i++)
    for (j=0;j<MaxBushingCountOnSide;j++)
      AE[i][j] = Setup.CalibrationCoeff.VoltageK[i][j];

  SetCurScrl(&Scrl2,1);
  SetCurScrl(&Scrl3,1);
  ClearLCD();
  SetFont(Font8x6);

  SetInputChannel(GetCurScrl(&Scrl3)-1,chAaCrossPhases+GetCurScrl(&Scrl2)-1);
  ReadData(1,1,1,1,chReadAllChannels,0);
  SetPage(GammaPage3); aAmplifIndex=AmplifIndex[GetCurScrl(&Scrl3)-1][GammaCh];
  if (GetCurScrl(&Scrl3)==1)
    {
    switch (GetCurScrl(&Scrl2)) {
      case 1: SetPage(GammaPage0); break;
      case 2: SetPage(GammaPage1); break;
      case 3: SetPage(GammaPage2); break;
      }
    aAmplifIndex=AmplifIndex[GetCurScrl(&Scrl3)-1][BushingCh];
    f = CalcAmpl((s16*)(AddrSig[GetCurScrl(&Scrl2)-1]), UpLoadCount);
    }
  else
    f = CalcAmpl((s16*)(AddrSig[3]), UpLoadCount);
  f2 = f*AE[GetCurScrl(&Scrl3)-1][GetCurScrl(&Scrl2)-1]*(float)ADCStep/GainValue[aAmplifIndex];
  SetFont(Font8x6);
  OutInt(82,0,(int)(Round(f)),5);

  while ((ch!=KB_ENTER)&&(ch!=KB_ESC)) {
      OutString(60,0,CalibrVStr3);
      OutString(54,8,CalibrVStr4);

     if (CurPos==0) {
       while ((ch!=KB_ENTER)&&(ch!=KB_ESC)&&(ch!=KB_UP)&&(ch!=KB_DOWN)) {

          SetFont(Font8x6);
          OutInt(82,0,(int)(Round(f)),5);
          OutPicture(0,0,RArr);
          RedrawScrl(&Scrl2);
          RedrawScrl(&Scrl3);
          OutFloat(76,8,f2,6,0);
          Redraw();
          ch=DoScrl(&Scrl3);
       }
     }

     if (CurPos==1) {
       while ((ch!=KB_ENTER)&&(ch!=KB_ESC)&&(ch!=KB_UP)&&(ch!=KB_DOWN)) {

          SetFont(Font8x6);
          OutInt(82,0,(int)(Round(f)),5);
          OutPicture(0,8,RArr);
          RedrawScrl(&Scrl2);
          RedrawScrl(&Scrl3);
          SetFont(Font8x6);
          OutFloat(76,8,f2,6,0);
          Redraw();
          ch=DoScrl(&Scrl2);
       }
       if (ch==KB_ENTER) {
          SetInputChannel(GetCurScrl(&Scrl3)-1,chAaCrossPhases+GetCurScrl(&Scrl2)-1);
          ReadData(1,1,1,1,chReadAllChannels,0);
          SetPage(GammaPage3); aAmplifIndex=AmplifIndex[GetCurScrl(&Scrl3)-1][GammaCh];
          if (GetCurScrl(&Scrl3)==1)
            {
            switch (GetCurScrl(&Scrl2)) {
              case 1: SetPage(GammaPage0); break;
              case 2: SetPage(GammaPage1); break;
              case 3: SetPage(GammaPage2); break;
              }
              aAmplifIndex=AmplifIndex[GetCurScrl(&Scrl3)-1][BushingCh];
            f = CalcAmpl((s16*)(AddrSig[GetCurScrl(&Scrl2)-1][0]), UpLoadCount);
            }
          else
            f = CalcAmpl((s16*)(AddrSig[3]), UpLoadCount);

          SetFont(Font8x6);
          OutInt(82,0,(int)(Round(f)),5);

         f2=f*AE[GetCurScrl(&Scrl3)-1][GetCurScrl(&Scrl2)-1]*(float)ADCStep/GainValue[aAmplifIndex];
         SetFont(Font8x6);
         f2=InputFloat(76,8,f2,6,0);
         AE[GetCurScrl(&Scrl3)-1][GetCurScrl(&Scrl2)-1]=f2*GainValue[aAmplifIndex]/(float)ADCStep/f;
         Redraw();
         ch=KB_NO;
       }
     }

     if (ch==KB_DOWN) {
        if (CurPos<1) CurPos++; else CurPos=0;
        ClearLCD();
        ch=KB_NO;
     }
     if (ch==KB_UP) {
        ClearLCD();
        if (CurPos) CurPos--; else CurPos=1;
        ch=KB_NO;
     }
  }
SetFont(Font8x8);
if (ch==KB_ENTER)
  for (i=0;i<MaxTransformerSide;i++)
    for (j=0;j<MaxBushingCountOnSide;j++)
      if (Setup.CalibrationCoeff.VoltageK[i][j] != AE[i][j])
        Setup.CalibrationCoeff.VoltageK[i][j] = AE[i][j];
SetInputChannel(0,chGamma);
}
*/



/*static void CalibrTemp(void)
{ char CurChannel=0,MaxChamnnel;
  KEY key;
  float v[4][2];
  float k,b;
  struct ScrollerMngr Scrl1;

  SetFont(Font8x8);
  if (BoardType()&(1<<7)) MaxChamnnel=4; else MaxChamnnel=3;

  InitScroller(&Scrl1,88,8,Font8x8);
  AddScrollerString(&Scrl1,CalibrTStr4);
  AddScrollerString(&Scrl1,CalibrTStr5);
  AddScrollerString(&Scrl1,CalibrTStr6);
  if (MaxChamnnel==4) AddScrollerString(&Scrl1,CalibrTStr7);

  SetCurScrl(&Scrl1,1);

ReReadCh:

  ClearLCD();
  SetFont(Font8x8);
  OutString(12,0,StrTEMP);
  OutString(20,8,CalibrTStr3);
  OutPicture(80,8,RArr);
  RedrawScrl(&Scrl1);
  Redraw();
  while ((key!=KB_ENTER)&&(key!=KB_ESC)) {
      key=DoScrl(&Scrl1);
      Redraw();
  }

  if (key==KB_ENTER) {
     CurChannel=GetCurScrl(&Scrl1)-1;
     OutString(0,8,StrTEMP2);
     while(1){
        v[CurChannel][0]=ReadTemperatureADCValue(CurChannel);
        OutFloat(54,8,v[CurChannel][0],5,2);
//        OutFloat(54,v[CurChannel][0],5,2);
        Redraw();
        key=ReadKey();
        if((key==KB_ESC)||(key==KB_ENTER))break;
     }
     if (key==KB_ENTER){
        OutString(0,8,StrTEMP3);
        while(1){
            v[CurChannel][1]=ReadTemperatureADCValue(CurChannel);
            OutFloat(54,8,v[CurChannel][1],5,2);
            Redraw();
            key=ReadKey();
            if((key==KB_ESC)||(key==KB_ENTER))break;
        }
        if (key==KB_ENTER){
           k=100.0/(v[CurChannel][1]-v[CurChannel][0]);
           b=0.0-(k*v[CurChannel][0]);
           b+=(100.0-(k*v[CurChannel][1]));
           b/=2;
           Setup.CalibrationCoeff.TemperatureK[CurChannel]=k;
           Setup.CalibrationCoeff.TemperatureB[CurChannel]=b;
        } else { key=KB_NO; goto ReReadCh;}
     } else { key=KB_NO; goto ReReadCh;}
     key=KB_NO;
     goto ReReadCh;
  }
}*/


static void CalibrI(void)
{ char CurChannel=0;
  KEY key;
  struct ScrollerMngr Scrl1;

  SetFont(Font8x8);

  InitScroller(&Scrl1,88,8,Font8x8);
  AddScrollerString(&Scrl1,CalibrTStr4);
  AddScrollerString(&Scrl1,CalibrTStr5);
  AddScrollerString(&Scrl1,CalibrTStr6);

  SetCurScrl(&Scrl1,1);

ReReadCh:

  ClearLCD();
  SetFont(Font8x8);
  OutString(12,0,CalibrIStr2);
  OutString(20,8,CalibrTStr3);
  OutPicture(80,8,RArr);
  RedrawScrl(&Scrl1);
  Redraw();
  while ((key!=KB_ENTER)&&(key!=KB_ESC)) {
      key=DoScrl(&Scrl1);
      Redraw();
  }

  if (key==KB_ENTER) {
     CurChannel=GetCurScrl(&Scrl1)-1;
     ClearLCD();
     OutString(12,0,CalibrIStr2);
     OutString(0,8,CalibrIStr3);
     Setup.CalibrationCoeff.CurrentK[CurChannel]=InputFloat(50,8,Setup.CalibrationCoeff.CurrentK[CurChannel],7,2);
     ClearLCD();
     OutString(12,0,CalibrIStr2);
     OutString(0,8,CalibrIStr4);
     Setup.CalibrationCoeff.CurrentB[CurChannel]=InputFloat(56,8,Setup.CalibrationCoeff.CurrentB[CurChannel],6,3);
  } else return;
  key=KB_NO;
  goto ReReadCh;

}

/*static void DoCalibrHumidity(void)
{ char CurChannel;
  KEY key;
  struct ScrollerMngr Scrl1;

  SetFont(Font8x8);

  InitScroller(&Scrl1,8,8,Font8x8);
  AddScrollerString(&Scrl1,CalibrHStr3);
  AddScrollerString(&Scrl1,CalibrHStr4);

  SetCurScrl(&Scrl1,1);

ReReadH:

  ClearLCD();
  SetFont(Font8x8);
  OutString(12,0,CalibrHStr2);
  OutPicture(0,8,RArr);
  RedrawScrl(&Scrl1);
  Redraw();
  while ((key!=KB_ENTER)&&(key!=KB_ESC)) {
      key=DoScrl(&Scrl1);
      Redraw();
  }

  if (key==KB_ENTER) {
     CurChannel=GetCurScrl(&Scrl1)-1;
     if (CurChannel==0) {
        Setup.CalibrationCoeff.HumidityOffset[0]=InputFloat(60,8,Setup.CalibrationCoeff.HumidityOffset[0],6,3);
     }
     if (CurChannel==1) {
        Setup.CalibrationCoeff.HumiditySlope[0]=InputFloat(60,8,Setup.CalibrationCoeff.HumiditySlope[0],6,3);
     }
     key=KB_NO;
     goto ReReadH;
   }
}*/

/*static void TemperatureCfg(void)
{
  KEY ch=KB_NO;
  char i,CurPos=0;
  struct ScrollerMngr Scrl2,Scrl3;
  char TonSet[MaxTransformerSide];

  InitScroller(&Scrl2,10,8,Font8x6);
  AddScrollerString(&Scrl2,TOnSetStr3);
  AddScrollerString(&Scrl2,TOnSetStr4);
  AddScrollerString(&Scrl2,TOnSetStr5);
  AddScrollerString(&Scrl2,TOnSetStr6);
  if (BoardType()&(1<<7)) AddScrollerString(&Scrl2,TOnSetStr7);

  InitScroller(&Scrl3,10,0,Font8x6);
  AddScrollerString(&Scrl3,AlarmEnableStr3);
  AddScrollerString(&Scrl3,AlarmEnableStr4);

  for (i=0;i<MaxTransformerSide;i++){
    TonSet[i] = Setup.GammaSetupInfo.GammaSideSetup[i].TemperatureConfig;
    if (TonSet[i]>3) {
       TonSet[i]=0;
       Setup.GammaSetupInfo.GammaSideSetup[i].TemperatureConfig=TonSet[i];
    }
  }

  SetCurScrl(&Scrl2,TonSet[i]+1);
  SetCurScrl(&Scrl3,1);
  ClearLCD();
  RedrawScrl(&Scrl2);
  RedrawScrl(&Scrl3);
  Redraw();

  while ((ch!=KB_ENTER)&&(ch!=KB_ESC)) {

     if (CurPos==0) {
       while ((ch!=KB_ENTER)&&(ch!=KB_ESC)&&(ch!=KB_UP)&&(ch!=KB_DOWN)) {
          SetCurScrl(&Scrl2,TonSet[GetCurScrl(&Scrl3)-1]+1);
          SetFont(Font8x6); OutString(46,0,TOnSetStr1);
          OutPicture(0,0,RArr);
          RedrawScrl(&Scrl2);
          RedrawScrl(&Scrl3);
          Redraw();
          ch=DoScrl(&Scrl3);
       }
     }

     if (CurPos==1) {
       while ((ch!=KB_ENTER)&&(ch!=KB_ESC)&&(ch!=KB_UP)&&(ch!=KB_DOWN)) {
          TonSet[GetCurScrl(&Scrl3)-1]=GetCurScrl(&Scrl2)-1;
          SetFont(Font8x6); OutString(46,0,TOnSetStr1);
          OutPicture(0,8,RArr);
          RedrawScrl(&Scrl2);
          RedrawScrl(&Scrl3);
          Redraw();
          ch=DoScrl(&Scrl2);
       }
     }

     if (ch==KB_DOWN) {
        if (CurPos<1) CurPos++; else CurPos=0;
        ClearLCD();
        SetFont(Font8x6); OutString(46,0,TOnSetStr1);
        ch=KB_NO;
     }
     if (ch==KB_UP) {
        ClearLCD();
        SetFont(Font8x6); OutString(46,0,TOnSetStr1);
        if (CurPos) CurPos--; else CurPos=1;
        ch=KB_NO;
     }
  }
SetFont(Font8x8);
if (ch==KB_ENTER)
  TonSet[GetCurScrl(&Scrl3)-1]=GetCurScrl(&Scrl2)-1;
  for (i=0;i<MaxTransformerSide;i++){
      if (TonSet[i]>3) TonSet[i]=0;
      if (Setup.GammaSetupInfo.GammaSideSetup[i].TemperatureConfig!=TonSet[i])
          Setup.GammaSetupInfo.GammaSideSetup[i].TemperatureConfig=TonSet[i];
    }
}*/


#define MaxCurPos  40

#define crDate            0
#define crTime            1
#define crDevNumber       2
#define crBaudRate        3
#define crProtocol        4
#define crSide            5
#define crBalansing       6
#define crABalansing      7
#define crRegister        8
#define crStopped         9
#define crFillPause       10
#define crShowTime        11
#define crAverages        12
#define crRelay           13
#define crReReadAlarm     14
#define crAlarmHysteresis 15
#define crDaysToCalcTrend 16
#define crDaysToCalcTK    17
#define crPhaseDispersion 18
#define crEnableAlarm     19
#define crAlarmThresh     20
#define crTKAlarm         21
#define crTrendAlarm      22
#define crTgThresh        23
#define crRVoltage        24
#define crRCurrent        25
#define crTg0             26
#define crC0              27
#define crTgTemp          28
#define crInputC          29
#define crImpedance       30
#define crCalibrI         31
#define crCurrentType     32
#define crCalcZk          33
#define crBASELINEDays    34
#define crBASELINE        35
#define crClear           36
#define ClearAll          37
#define crRunXXTest       38
#define crSingle          39


//#define crtgDelta         25
//#define crBushC           26
//#define crInpDiv          29
//#define crCalibr4_20      11
//#define crTempTest        32

char RunSetupMenu(void)
{
  KEY ch=KB_NO;
  char CurPos;
  char First=1;
  char EditDateTime=0;
#ifdef __arm
  u32 Y,D,M;
#endif
  _TDateTime CurTime;

       CurPos=0;
       while (1) {
          if ((CurPos==crTime)||(CurPos==crDate)) {
#ifdef __arm
             GET_TIME_INBUF();
#else
             GET_TIME();
#endif
          }
          if (!First) {
                ch=WaitReadKeyWithDelay(NoKeyDelayInSec,KB_ESC);
          }
          if (ch==KB_ESC) {
             SetSetupParam();
             ClearLCD();
             return EditDateTime;
          }
          if ((ch==KB_RIGHT)||(ch==KB_UP)) {
             CurPos++;
             if (CurPos>=MaxCurPos) CurPos=0;
             First=1;
          }
          if ((ch==KB_LEFT)||(ch==KB_DOWN)) {
             if (CurPos) CurPos--;
             else CurPos=MaxCurPos-1;
             First=1;
          }
          if (First) {
             ClearLCD();
             SetFont(Font8x8);
          }
          switch (CurPos){
            case crDate: {
               if (ch==KB_ENTER) {
#ifdef __arm
                  Y=DateTime.Year;
                  D=DateTime.Day;
                  M=DateTime.Month;
                  InputDate(32,8,&D,&M,&Y);
                  DateTime.Year=Y;
                  DateTime.Month=M;
                  DateTime.Day=D;
                  SET_TIME_FROMBUF();
#else
                  InputDate(32,8,&DateTime.Day,&DateTime.Month,&DateTime.Year);
                  SET_TIME();
#endif
                  EditDateTime=1;
               }
                  ClearLCD();
                  OutPicture(0,0,DateIco);
                  OutString(48,0,DateStr);
                  OutDate(32,8);
                  Redraw();
                  First=0;
               break;
            }
            case crTime: {
               if (ch==KB_ENTER) {
#ifdef __arm
                  Y=DateTime.Hour;
                  M=DateTime.Min;
                  D=DateTime.Sec;
                  InputTime(32,8,&Y,&M,&D);
                  DateTime.Hour=Y;
                  DateTime.Min=M;
                  DateTime.Sec=D;
                  SET_TIME_FROMBUF();
#else
                  InputDate(32,8,&DateTime.Day,&DateTime.Month,&DateTime.Year);
                  SET_TIME();
#endif
                  EditDateTime=1;
               }
                  OutPicture(0,0,ClockIco);
                  OutString(48,0,TimeStr);
                  OutTime(32,8);
                  Redraw();
                  First=0;
               break;
            }
            case crDevNumber: {
               if (First) {
                  OutPicture(1,0,DeviceNumIco);
                  OutString(40,0,DeviceNumberStr1);
                  OutString(40,8,DeviceNumberStr2);
                  Redraw();
                  First=0;
               }
               if (ch==KB_ENTER) {
                  ClearLCD();
                  OutPicture(1,0,DeviceNumIco);
                  OutString(40,0,DeviceNumberStr1);
                  OutString(44,8,DeviceNumberStr3);
                  Setup.SetupInfo.DeviceNumber=InputInt(60,8,Setup.SetupInfo.DeviceNumber,3);
                  First=1;
               }
               break;
            }

            case crBaudRate: {
               if (First) {
                  OutPicture(1,0,CompIco);
                  OutString(32,0,BaudRateStr1);
                  OutString(32,8,BaudRateStr2);
                  Redraw();
                  First=0;
               }
               if (ch==KB_ENTER) {
                  Setup.SetupInfo.BaudRate=SetBaudRate(Setup.SetupInfo.BaudRate);
                  First=1;
               }
               break;
            }
            case crProtocol : {
               if (First) {
                  OutPicture(1,0,CompIco);
                  OutString(40,0,ProtocolStr1);
                  OutString(40,8,ProtocolStr2);
                  Redraw();
                  First=0;
               }
               if (ch==KB_ENTER) {
                  ClearLCD();
                  OutPicture(1,0,CompIco);
                  OutString(40,0,ProtocolStr1);
                  Setup.SetupInfo.ModBusProtocol=InpOnOffValue(64,8,Setup.SetupInfo.ModBusProtocol,ProtocolStr3,ProtocolStr4);
                  Redraw();
                  First=1;
               }
               break;
            }

            case crSide: {
               if (First) {
                  OutPicture(1,3,ReadLR);
                  OutString(32,0,ReadFromSideStr1);
                  OutString(32,8,ReadFromSideStr2);
                  Redraw();
                  First=0;
               }
               if (ch==KB_ENTER) {
                  ClearLCD();
                  SetReadSides();
                  Redraw();
                  First=1;
               }
               break;
            }
            case crBalansing: {
               if (First) {
                  OutPicture(0,0,BalIco);
                  OutString(30,4,StBalansStr);
                  Redraw();
                  First=0;
               }
               if (ch==KB_ENTER) {
                  Balansing();
                  First=1;
               }
               break;
            }
            case crABalansing: {
               if (First) {
                  OutPicture(0,0,ABalIco);
                  OutString(30,0,StBalansStr);
                  OutString(38,8,StTimeBalansStr);
                  Redraw();
                  First=0;
               }
               if (ch==KB_ENTER) {
                  SetBalansingOnTime();
                  First=1;
               }
               break;
            }
            case crRegister: {
               if (First) {
                  OutPicture(2,0,ScheduleIco);
                  OutString(36,0,ReadTimeStr1);
                  OutString(32,8,ReadTimeStr2);
                  Redraw();
                  First=0;
               }
               if (ch==KB_ENTER) { InpRegData();   EditDateTime=1; First=1;}
               break;
            }
            case crStopped: {
               if (First) {
                  OutPicture(0,0,StoppedIco);
                  OutString(48,0,StoppedStr1);
                  OutString(32,8,StoppedStr2);
                  Redraw();
                  First=0;
               }
               if (ch==KB_ENTER) {
                  ClearLCD();
                  OutPicture(0,0,StoppedIco);
                  OutString(32,0,StoppedStr2);
                  Setup.SetupInfo.Stopped=InpOnOffValue(44,8,Setup.SetupInfo.Stopped,StoppedStr4,StoppedStr3);
                  First=1;
               }
               break;
            }
            case crFillPause: {
               if (First) {
                  OutPicture(0,0,FillPauseIco);
                  OutString(48,0,FillPauseStr1);
                  OutString(44,8,FillPauseStr2);
                  Redraw();
                  First=0;
               }
               if (ch==KB_ENTER) {
                  ClearLCD();
                  OutString(32,0,FillPauseStr3);
                  Setup.SetupInfo.DisplayFlag=InpFillPause(Setup.SetupInfo.DisplayFlag);
                  Redraw();
                  First=1;
               }
               break;
            }
            case crShowTime: {
               if (First) {
                  OutPicture(0,0,ShowTimeIco);
                  OutString(32,4,ShowTimeStr1);
                  Redraw();
                  First=0;
               }
               if (ch==KB_ENTER) {
                  ClearLCD();
                  OutPicture(0,0,ShowTimeIco);
                  OutString(32,0,ShowTimeStr1);
                  OutString(64,8,SecStr);
                  Setup.SetupInfo.OutTime=InputInt(40,8,Setup.SetupInfo.OutTime,2);
                  First=1;
               }
               break;
            }
            case crAverages: {
               if (First) {
                  OutPicture(0,0,AvgIco);
                  OutString(40,0,AverageNumStr1);
                  OutString(44,8,AverageNumStr2);
                  Redraw();
                  First=0;
               }
               if (ch==KB_ENTER) {
                  ClearLCD();
                  OutPicture(0,0,AvgIco);
                  OutString(40,0,AverageNumStr1);
                  Setup.GammaSetupInfo.AveragingForGamma=InputInt(56,8,Setup.GammaSetupInfo.AveragingForGamma,3);
                  First=1;
               }
               break;
            }
            case crRelay: {
               if (First) {
                  OutPicture(0,0,RelayIco);
                  OutString(26,0,RelayStr1);
                  OutString(26,8,RelayStr2);
                  Redraw();
                  First=0;
               }
               if (ch==KB_ENTER) {
                  Setup.SetupInfo.Relay=SelectRelayMode(Setup.SetupInfo.Relay);
                  First=1;
               }
               break;
            }
            case crReReadAlarm: {
               if (First) {
                  OutPicture(0,0,ReReadIco);
                  OutString(26,0,ReReadAlarmStr1);
                  OutString(26,8,ReReadAlarmStr2);
                  Redraw();
                  First=0;
               }
               if (ch==KB_ENTER) {
                  ClearLCD();
                  OutPicture(0,0,ReReadIco);
                  OutString(26,0,ReReadAlarmStr1);
                  OutString(66,8,MinStr);
                  Setup.GammaSetupInfo.ReReadOnAlarm=InputInt(34,8,Setup.GammaSetupInfo.ReReadOnAlarm,3);
                  First=1;
               }
               break;
            }
            case crAlarmHysteresis: {
               if (First) {
                  OutPicture(0,0,HysterIco);
                  OutString(26,0,AlarmHysteresisStr1);
                  OutString(26,8,AlarmHysteresisStr2);
                  Redraw();
                  First=0;
               }
               if (ch==KB_ENTER) {
                  ClearLCD();
                  //OutPicture(0,0,);
                  OutString(26,0,AlarmHysteresisStr2);
                  OutString(66,8,PercentStr);
                  Setup.GammaSetupInfo.AlarmHysteresis=InputInt(42,8,Setup.GammaSetupInfo.AlarmHysteresis,2);
                  First=1;
               }
               break;
            }
            case crDaysToCalcTrend: {
               if (First) {
                  OutPicture(0,0,TrendDaysIco);
                  OutString(26,0,DaysTrendStr1);
                  OutString(26,8,DaysTrendStr2);
                  Redraw();
                  First=0;
               }
               if (ch==KB_ENTER) {
                  ClearLCD();
                  OutPicture(0,0,TrendDaysIco);
                  OutString(26,0,DaysTrendStr2);
                  OutString(66,8,DaysStr);
                  do {
                  if (Setup.GammaSetupInfo.DaysToCalculateTrend < 10)
                    Setup.GammaSetupInfo.DaysToCalculateTrend = 10;
                  Setup.GammaSetupInfo.DaysToCalculateTrend=InputInt(34,8,Setup.GammaSetupInfo.DaysToCalculateTrend,3);
                  }while (Setup.GammaSetupInfo.DaysToCalculateTrend < 10);
                  First=1;
               }
               break;
            }
            case crDaysToCalcTK: {
               if (First) {
                  OutPicture(0,0,TrendDaysIco);
                  OutString(26,0,DaysTKStr1);
                  OutString(26,8,DaysTKStr2);
                  Redraw();
                  First=0;
               }
               if (ch==KB_ENTER) {
                  ClearLCD();
                  OutPicture(0,0,TrendDaysIco);
                  OutString(26,0,DaysTKStr2);
                  OutString(66,8,DaysStr);
                  do {
                  if (Setup.GammaSetupInfo.DaysToCalculateTCoefficient < 10)
                    Setup.GammaSetupInfo.DaysToCalculateTCoefficient = 10;
                  Setup.GammaSetupInfo.DaysToCalculateTCoefficient=InputInt(34,8,Setup.GammaSetupInfo.DaysToCalculateTCoefficient,3);
                  }while (Setup.GammaSetupInfo.DaysToCalculateTCoefficient < 10);
                  First=1;
               }
               break;
            }
            case crPhaseDispersion: {
               if (First) {
                  OutPicture(0,0,PhaseDispIco);
                  OutString(26,0,PhaseDispStr1);
                  OutString(26,8,PhaseDispStr2);
                  Redraw();
                  First=0;
               }
               if (ch==KB_ENTER) {
                  ClearLCD();
                  OutPicture(0,0,PhaseDispIco);
                  OutString(26,0,PhaseDispStr2);
                  OutString(66,8,DegrStr);
                  Setup.GammaSetupInfo.AllowedPhaseDispersion=InputInt(42,8,Setup.GammaSetupInfo.AllowedPhaseDispersion,2);
                  First=1;
               }
               break;
            }
            case crEnableAlarm: {
               if (First) {
                  OutPicture(0,0,AlarmEnIco);
                  OutString(26,0,AlarmEnableStr1);
                  OutString(26,8,AlarmEnableStr2);
                  Redraw();
                  First=0;
               }
               if (ch==KB_ENTER) {
                  ClearLCD();
                  SetAlarmEnable();
                  Redraw();
                  First=1;
               }
               break;
            }
            case crAlarmThresh: {
               if (First) {
                  OutPicture(0,0,ThreshldsIco);
                  OutString(24,0,AlarmThreshStr1);
                  OutString(24,8,AlarmThreshStr2);
                  Redraw();
                  First=0;
               }
               if (ch==KB_ENTER) {
                  ClearLCD();
                  SetAlarmThresh();
                  Redraw();
                  First=1;
               }
               break;
            }
            case crTKAlarm: {
               if (First) {
                  OutPicture(0,0,ThreshldsIco);
                  OutString(24,0,TKAlarmStr1);
                  OutString(24,8,TKAlarmStr2);
                  Redraw();
                  First=0;
               }
               if (ch==KB_ENTER) {
                  ClearLCD();
                  SetTKAlarm();
                  Redraw();
                  First=1;
               }
               break;
            }
            case crTrendAlarm: {
               if (First) {
                  OutPicture(0,0,ThreshldsIco);
                  OutString(28,0,TrendAlarmStr1);
                  OutString(28,8,TrendAlarmStr2);
                  Redraw();
                  First=0;
               }
               if (ch==KB_ENTER) {
                  ClearLCD();
                  SetTrendAlarm();
                  Redraw();
                  First=1;
               }
               break;
            }
            case crTgThresh: {
               if (First) {
                  OutPicture(0,0,ThreshldsIco);
                  OutString(24,0,AlarmThreshStr5);
                  OutString(24,8,AlarmThreshStr2);
                  Redraw();
                  First=0;
               }
               if (ch==KB_ENTER) {
                  ClearLCD();
                  SetTgAlarmThresh();
                  Redraw();
                  First=1;
               }
               break;
            }
            case crRVoltage: {
               if (First) {
                  OutPicture(0,0,RatedVoltIco);
                  OutString(28,0,RVoltageStr1);
                  OutString(28,8,RVoltageStr2);
                  Redraw();
                  First=0;
               }
               if (ch==KB_ENTER) {
                  ClearLCD();
                  SetRVoltage();
                  Redraw();
                  First=1;
               }
               break;
            }
            case crRCurrent: {
               if (First) {
                  OutPicture(0,0,RatedVoltIco);
                  OutString(28,0,RCurrentStr1);
                  OutString(28,8,RCurrentStr2);
                  Redraw();
                  First=0;
               }
               if (ch==KB_ENTER) {
                  ClearLCD();
                  SetRCurrent();
                  Redraw();
                  First=1;
               }
               break;
            }
            case crTg0: {
               if (First) {
                  OutPicture(0,0,BushTgIco);
                  OutString(24,0,Tg0Str1);
                  OutString(24,8,Tg0Str2);
                  Redraw();
                  First=0;
               }
               if (ch==KB_ENTER) {
                  ClearLCD();
                  SetTg0();
                  Redraw();
                  First=1;
               }
               break;
            }
            case crC0: {
               if (First) {
                  OutPicture(0,0,BushCIco);
                  OutString(24,0,C0Str1);
                  OutString(24,8,C0Str2);
                  Redraw();
                  First=0;
               }
               if (ch==KB_ENTER) {
                  ClearLCD();
                  SetC0();
                  Redraw();
                  First=1;
               }
               break;
            }
            case crTgTemp: {
               if (First) {
                  OutPicture(0,0,BushTgTempIco);
                  OutString(20,0,TgTempStr1);
                  OutString(20,8,TgTempStr2);
                  Redraw();
                  First=0;
               }
               if (ch==KB_ENTER) {
                  ClearLCD();
                  SetTgTemp();
                  Redraw();
                  First=1;
               }
               break;
            }
            case crInputC: {
               if (First) {
                  OutPicture(0,0,DBCIco);
                  OutString(28,0,InputCStr1);
                  OutString(28,8,InputCStr2);
                  Redraw();
                  First=0;
               }
               if (ch==KB_ENTER) {
                  ClearLCD();
                  SetInputC();
                  Redraw();
                  First=1;
               }
               break;
            }
            case crImpedance: {
               if (First) {
                  OutPicture(0,0,InpImpIco);
                  OutString(28,0,ImpedanceStr1);
                  OutString(28,8,ImpedanceStr2);
                  Redraw();
                  First=0;
               }
               if (ch==KB_ENTER) {
                  ClearLCD();
                  SetImpedance();
                  Redraw();
                  First=1;
               }
               break;
            }
#ifndef NoCross
            case crCalcZk: {
               if (First) {
                  OutPicture(0,0,ZkIco);
                  OutString(26,0,CalcZkStr1);
                  OutString(26,8,CalcZkStr2);
                  Redraw();
                  First=0;
               }
               if (ch==KB_ENTER) {
                  ClearLCD();
                  SetCalcZk();
                  Redraw();
                  First=1;
               }
               break;
            }
#endif
            case crCurrentType: {
               if (First) {
                  OutPicture(0,0,CurrentIco);
                  OutString(30,0,CurrentTypeStr1);
                  OutString(44,8,CurrentTypeStr2);
                  Redraw();
                  First=0;
               }
               if (ch==KB_ENTER) {
                  ClearLCD();
                  SetCurrentTypeSide();
                  Redraw();
                  First=1;
               }
               break;
            }

            case crCalibrI: {
               if (First) {
                  OutPicture(0,0,CurrentIco);
                  OutString(24,0,CalibrIStr1);
                  OutString(26,8,CalibrIStr2);
                  Redraw();
                  First=0;
               }
               if (ch==KB_ENTER) {
                  ClearLCD();
                  CalibrI();
                  First=1;
               }
               break;
            }
            case crBASELINEDays: {
               if (First) {
                  OutPicture(0,0,BaseLineDaysIco);
                  OutString(32,0,BaseLineStr2);
                  OutString(32,8,BaseLineStr5);
                  Redraw();
                  First=0;
               }
               if (ch==KB_ENTER) {
                  ClearLCD();
                  OutString(32,0,BaseLineStr2);
                  OutString(32,8,BaseLineStr5);
                  Setup.GammaSetupInfo.DaysToCalculateBASELINE=InputInt(88,8,Setup.GammaSetupInfo.DaysToCalculateBASELINE,3);
                  if (Setup.GammaSetupInfo.DaysToCalculateBASELINE<MinDaysForBaseLine)
                      Setup.GammaSetupInfo.DaysToCalculateBASELINE=MinDaysForBaseLine;
                  First=1;
               }
               ch=KB_NO;
               break;
            }
            case crBASELINE: {
               if (First) {
                  OutPicture(0,0,BaseLineIco);
                  OutString(36,0,BaseLineStr1);
                  OutString(32,8,BaseLineStr2);
                  Redraw();
                  First=0;
               }
               if (ch==KB_ENTER) {
#ifdef __arm
                  GET_TIME_INBUF();
#else
                  GET_TIME();
#endif
                  CurTime = DoPackDateTime(EncodeDate(DateTime.Year%100,DateTime.Month,DateTime.Day),
                                           EncodeTime(DateTime.Hour,DateTime.Min,  0));
                  if (RunBaseLine(CurTime,CurDiagRegime)) {
                     //Ok
                     ClearLCD();
                     OutString(32,0,BaseLineStr2);
                     OutString(32,8,BaseLineStr3);
                  } else {
                     //Error
                     ClearLCD();
                     OutString(32,0,BaseLineStr2);
                     OutString(32,8,BaseLineStr4);
                  }
                  Redraw();
                  WaitReadKeyWithDelay(NoKeyDelayInSec,KB_ESC);
                  First=1;
               }
               ch=KB_NO;
               break;
            }
            case crClear: {
               if (First) {
                  OutPicture(0,0,ClearIco);
                  OutString(36,0,DelArchStr1);
                  OutString(32,8,DelArchStr2);
                  Redraw();
                  First=0;
                  }
               if (ch==KB_ENTER) {
                  WorkWithArchive(0);
                  First=1;
               }
               ch=KB_NO;
               break;
            }
            case ClearAll: {
               if (First) {
                  OutPicture(0,0,ClearAllIco);
                  OutString(28,0,DelArchStr3);
                  OutString(24,8,DelArchStr4);
                  Redraw();
                  First=0;
                  }
               if (ch==KB_ENTER) {
                  ClearLCD();
                  SetFont(Font8x6);
                  OutString(42, 0, DelArchiveStr1);
                  OutString(9, 8, DelArchiveStr2);
                  Redraw();
                  ReadKey();
                  ch=WaitReadKeyWithDelay(BREAKDELAY, KB_TIMEOUT);
                  if (ch==KB_ENTER) {
                     ClearLCD();
                     SetFont(Font8x6);
                     OutString(30, 4, DelArchiveStr4);
                     Redraw();

                     ClearAllData();

                     ClearLCD();
                     SetFont(Font8x6);
                     OutString(36, 0, DelArchiveStr5);
                     OutString(36, 8, DelArchiveStr6);
                     Redraw();
                     WaitReadKeyWithDelay(BREAKDELAY, KB_TIMEOUT);
                  }
                  First=1;
               }
               ch=KB_NO;
               break;
            }

            case crRunXXTest: {
               if (First) {
                  OutPicture(0,0,BalIco);
                  OutString(30,0,XXTestStr4);
                  OutString(30,8,XXTestStr5);
                  Redraw();
                  First=0;
               }
               if (ch==KB_ENTER) {
                  RunXXTest();
                  First=1;
               }

               ch=KB_NO;
               break;
            }

            case crSingle: {
               if (First) {
                  OutPicture(0,0,SingleIco);
                  OutString(40,0,SingleStr1);
                  OutString(25,8,SingleStr2);
                  Redraw();
                  First=0;
                  }
               if (ch==KB_ENTER) {
                  DoSingle();
                  First=1;
               }
               ch=KB_NO;
               break;
            }

          default: break;
          }
          ch=KB_NO;


       }
}

char RunSetup(void)
{
    if (PasswordOk()) {
#ifdef __arm
      RunSetupMenu();
      SaveSetup();
      return 1;
#else
      return RunSetupMenu();
#endif
    }
    else return 0;
}

// acm, 2-18-09 comment out to simplify code, not change functionality
// acm, 2-13-09 Boltov fix
//TCRC ReadSetupCRC(void)
//{  TCRC FRAMSetupCRC;
//   ReadFram(SetupCRCAddr,(void *)&FRAMSetupCRC, sizeof(TCRC));
//   return FRAMSetupCRC;
//}  

int TestSetupCRC(void)                                         // acm, 2-18-09, convert routine into function, return if mismatch, used to toss bad measurment check in GammaMeasurement.cpp
{	TCRC FRAMSetupCRC, CalcSetupCRC;
//int i,j, badpot;												// acm, 2-19
//char readextdacvalue;
// FRAMSetupCRC=ReadSetupCRC();                                 // acm, 2-18-09 comment out to simplify code, not change functionality

ReadFram(SetupCRCAddr,(void *)&FRAMSetupCRC, sizeof(TCRC));  // acm, 2-16-09, read CRC into variable FAMSetupCRC
CalcSetupCRC=CalcCRC((void*)&Setup,sizeof(Setup));

// acm, 3-9, correction, POT can be read back, datasheet wrong.  Readback algorithm as stated by Analog FAE: "You can use SDO with SPI mode to read back
// the SPI data, in this case the SPI data is wrttten twice, the second time the data will appear at SDO. SDO can be connected to MISO pin of the SPI uC."
// acm, 2-19-09 abandon, turns out POT cannot be read back in SPI mode (but can in I2C mode, odd).
// acm, 2-19, diagnostic code, compare all 6 POT values to stored in RAM, if bad set flag and output LED/mesg 
// LED1OFF; LED1Green; badpot=0;  // clear RED LED if previously set
// for (i=0;i<MaxTransformerSide;i++)
//     for (j=0;j<MaxBushingCountOnSide;j++)
//		{
//			readextdacvalue=GetExtDac(i,j);
//			if (Setup.GammaSetupInfo.GammaSideSetup[i].ImpedanseValue[j]!=readextdacvalue)
//			{
//				badpot=1;
//				LED1Red; 
//				InitLCD(); // if this isn't done, unplug display will clear LCDConnected bit for good...
//				OutString(0,0,"POT ERROR#"); OutInt(56,0,i*j,1);
//				OutString(0,12,"Value="); OutString(56,12,readextdacvalue));
//				Redraw();
//				break;
//			}
//		}
//if (FRAMSetupCRC!=CalcSetupCRC || badpot) 

	if (FRAMSetupCRC!=CalcSetupCRC) 
	{
// 			LED1Red; 
			InitLCD(); // if this isn't done, unplug display will clear LCDConnected bit for good...
			OutString(0,0,"FRAM/RAM CRC mismatch");
			Redraw();
		
		Delay(5000); 
		LoadSetup(1,1);                                 // acm, reload setup and invoke SetSetupParam.  Implicit assumption is FRAM is more robust than RAM from noise event
//    CalcSetupCRC=CalcCRC((void*)&Setup,sizeof(Setup));        // acm, redundant (double), as LoadSetup calls SaveSetup, which recalcs/rewrites CRC to FRAM
//    WriteFram(SetupCRCAddr,(void *)&CalcSetupCRC, sizeof(TCRC));
//	InitDAC();						// acm, 2-19-09, rewrite DAC and POT values, changed mind as redundant if you trace code...
	return (0); // return 0 means mismatch occured
	}
	else
	return (1);
 
}

int GetRand(int min, int max)
{
  //static int Init = 0;
  unsigned int rc;
  unsigned int time;
  //2.05 ag
  //if(GET_TIME(&year,&month,&day,&hour,&min,&sec)){
  GET_TIME_INBUF();
  //EncodeTime(DateTime.Hour,DateTime.Min,0));
  if (DateTime.Min>0 || DateTime.Hour>0 || DateTime.Day>0){
    time = DateTime.Day + 10*DateTime.Hour + 100*DateTime.Min + 1000*DateTime.Sec;
  }
  else {
    Delay(2000);
    time = GetTic32();
  }
 
  //end ag

//  ClearLCD();
//  OutString(0,0,"time");
//  OutInt(0,8,time,5);
//  Redraw();
//  Delay(2000);
    
/*
   * Formula:  
   *    rand() % N   <- To get a number between 0 - N-1
   *    Then add the result to min, giving you 
   *    a random number between min - max.
   */  
  srand(time);
  rc = (rand() % (max - min + 1) + min);
//  ClearLCD();
//  OutString(0,0,"SN");
//  OutInt(0,8,Setup.SetupInfo.SN,5);
//  Redraw();
//  Delay(2000);
  return (rc);
}
