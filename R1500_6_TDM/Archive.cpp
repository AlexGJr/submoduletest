#ifndef __arm
#include "Protocol.h"
#include "ADC.h"
#else
#include "FRAM.h"
#include "DACDevice.h"
#endif
#include "Defs.h"
#include "MeasurementDefs.h"
#include "GammaMeasurement.h"
#include "Setup.h"
#include "Archive.h"
#include "DateTime.h"
#include "rtc.h"
#include "tgDelta.h"
#include "Diagnosis.h"
#include "Error.h"
#include "DefKB.h"
#include "FLASH.h"
#include "RAM.h"
#include "SysUtil.h"
#include <stdlib.h>
#include <string.h>
#include "DrvFlash.h"
#include "LCD.h"
#include "Graph.h"
#include "StrConst.h"
#include "KeyBoard.h"
#include "cListBox.h"
#include "MenuMngr.h"
#include "PicRes.h"
#include <stdio.h>
#include "CrossMeasurement.h"
#include "Utils.h"
#include "RTC.h"
#include "LogFile.h"
#include "DAC.h"
#include "EEPROM.h"
#include "Link.h"


//__no_init __eeprom struct stInsulationParameters ArchiveData[MaxData]   @ (0x02+szSetupInfo+szInitialParameters+szTgParameters+0x02);
unsigned int MeasurementsInArchive=0,LastMeasurement=1;

#define MeasurementSize 64
unsigned char MeasurementInSector=0;
unsigned int MaxData=0;

char IsInitialParam=0;

char FlashEnable=0;

extern
#ifdef __arm
unsigned int
#else
char
#endif
DiagResult;


extern float AvgGammaAmpl[MaxTransformerSide];
extern float AvgGammaPhase[MaxTransformerSide];
extern char  Avgs[MaxTransformerSide];

extern void TestSetup(); // acm, do this so Understand resolves

void ClearAvg(void)
{
  int TrSide;
  for (TrSide=0;TrSide<MaxTransformerSide;TrSide++) {
      AvgGammaAmpl[TrSide]=0;
      AvgGammaPhase[TrSide]=0;
      Avgs[TrSide]=0;
  }    
}

void LoadLastMeasurement(void)
{ _TDateTime DiagDate;
  char TrSide,CurrentGammaStatus=stUnknown,Tmp
#ifndef __arm
  ,OldPage=GetPage()
#endif
  ;

  FullStatus=stUnknown;
  SetPage(ParametersPage);
  if (MeasurementsInArchive) {
     LoadData(Parameters,0,ParametersPage);
     for (TrSide=0;TrSide<MaxTransformerSide;TrSide++) {
         if  (!(Setup.GammaSetupInfo.ReadOnSide&(1<<TrSide))) continue;
         Tmp=GetGammaStatus(Parameters->TrSide[TrSide].Gamma*0.01,TrSide,stUnknown,1);
         GetFullStatus(Tmp,Parameters->TrSide[TrSide].Trend,Parameters->TrSide[TrSide].KT,&FullStatus,&Parameters->TrSide[TrSide].AlarmStatus,TrSide);
         if (Tmp>CurrentGammaStatus) CurrentGammaStatus=Tmp;

         Trend[TrSide]=Parameters->TrSide[TrSide].Trend;
         KT[TrSide]=Parameters->TrSide[TrSide].KT;
         KTPhase[TrSide]=Parameters->TrSide[TrSide].KT;
     }
     GammaStatus=CurrentGammaStatus;
//     DiagResult=DoDiagnosis(&Parameters,&Diagnosis,NORMALDiagRegime);
     SetPage(ParametersPage);
     DiagDate = DoPackDateTime(EncodeDate(Parameters->Year,Parameters->Month,Parameters->Day),
                               EncodeTime(Parameters->Hour,Parameters->Min,  0));
     DiagResult=DoSumDiag(DiagDate,Diagnosis,/*NORMALDiagRegime*/STABLEDiagRegime,CurDiagRegime,MeasurementsInArchive,CurDiagType,ParametersPage,0);
  } else {
     memset(Parameters, 0,szInsulationParameters);
  }
  SetPage(OldPage);
  OutGammaStatus(FullStatus,0);
  ClearAvg();
}

void InitInitialParameters(void)
{ char i;
  //Initial Parameters
  if ((IsDate(Setup.InitialParameters.Year,Setup.InitialParameters.Month,Setup.InitialParameters.Day))&&
      (Setup.InitialParameters.Hour<24)&&
      (Setup.InitialParameters.Min<60))
  {
     //��������� ������� ����, mV
     for (i=0;i<MaxBushingCountOnSide;i++) {
        if (Setup.InitialParameters.TrSideParam[HVSide].SignalPhase[i]>(unsigned int)36000) Setup.InitialParameters.TrSideParam[HVSide].SignalPhase[i]=0;
        if (Setup.InitialParameters.TrSideParam[HVSide].SourcePhase[i]>(unsigned int)36000) Setup.InitialParameters.TrSideParam[HVSide].SourcePhase[i]=0;
     }
     //Initial Param Saved
     IsInitialParam=1;
  } else ClearInitParam(AllSides);
}


char DelArchive(void)
{
// unsigned UNI_INT key;

/*
  ClearLCD();
  SetFont(Font8x6);
  OutString(42, 0, DelArchiveStr1);
  OutString(9, 8, DelArchiveStr2);
  Redraw();

  key=WaitReadKeyWithDelay(BREAKDELAY, KB_TIMEOUT);

  if (key==KB_ENTER) {
*/
  ClearLCD();
  SetFont(Font8x6);
  OutString(30, 4, DelArchiveStr4);
  Redraw();

  ClearFlash();
  if (!OpenFlash()) {
     Error|=((unsigned long)1<<erFlashReadFail);
     Error|=((unsigned long)1<<erFlashWriteFail);
  } else {
     SetPage(MeasListPage);
     CreateList((u16 *)MeasListTempAddr,&MeasurementsInArchive,stForward/*����� � ������*/);
     SaveFileIndexes(MeasListTempAddr);
     LoadLastMeasurement();
  }
  ClearAvg();
  ClearLCD();
  SetFont(Font8x6);
  OutString(36, 0, DelArchiveStr5);
  OutString(36, 8, DelArchiveStr6);
  Redraw();
  WaitReadKeyWithDelay(BREAKDELAY, KB_TIMEOUT);
  
//  }

  return 1;
}

char *GetZamerString(s16 Ind){

char Str[20];
char Param[20];
TFile f;
#ifndef __arm
char Page=GetPage();
#endif
s16 i;

  OutStr[0]=0;
  sprintf(Str,"%03d",MeasurementsInArchive-Ind);
  strcat(Str, " ");
  strcpy(OutStr, Str);
  SetPage(MeasListPage);
#ifdef __arm
  i=LoadFileIndex(Ind);
#else
  i=((uint*)MeasList)[Ind];
#endif

  if (FileOpen(&f,i,fmRead)) {
     if (FileRead(&f,Param,0,10)==10) {
        sprintf(Str,"%02d.%02d.%02d %02d:%02d",
                ((struct stInsulationParameters *)&Param)->Day,
                ((struct stInsulationParameters *)&Param)->Month,
                ((struct stInsulationParameters *)&Param)->Year%100,
                ((struct stInsulationParameters *)&Param)->Hour,
                ((struct stInsulationParameters *)&Param)->Min);

        strcat(OutStr, Str);

     }
  }
  SetPage(Page);
  return OutStr;
}

char WorkWithArchive(char View){
extern signed char FillFlag;

unsigned int j;
KEY key=KB_NO;
char res,i=1
#ifndef __arm
,Page=GetPage()
#endif
;
_TDateTime DiagDate;
struct stDiagnosis Diag;
#ifdef __emulator
   struct stInsulationParameters TParam;
   struct stInsulationParameters *Param=&TParam;
#else
#ifdef __arm
   struct stInsulationParameters TParam;
   struct stInsulationParameters *Param=&TParam;
#else
   struct stInsulationParameters *Param =(struct stInsulationParameters *) (0xFFFF-szInsulationParameters);
#endif
#endif

  SetFont(Font6x5);
  ClearLCD();
  OutString(18,4,ArchLoading);
  Redraw();
  if(MeasurementsInArchive==0){
    ClearLCD();
    SetFont(Font8x6);
    OutString(12,4,NoArchiveError);
    Redraw();
    WaitReadKey();
    return 0;
  }


SetPage(LBPage);
InitCheckListBox(LB, 7, 2, 105, 2, Font6x5, 0);
SetDynamicCLB(LB, MeasurementsInArchive, &GetZamerString, 0);

StartDraw:
  ClearLCD();
//  SetFont(Font6x5);
//  OutString(24, 0, ArchViewCaption);
  DrawRect(1,0,110,15);
  SetPage(LBPage);
while(1){
  if(i){
    ReDrawCheckListBox(LB);
    Redraw();
    i=0;
  }

  key = WaitReadKeyWithDelay(BREAKDELAY, KB_TIMEOUT);

  if (key==KB_ESC) {res=0;goto EndProc;}
  if (key==KB_TIMEOUT) {res=KB_TIMEOUT;goto EndProc;}
  if (key==KB_ENTER){
    if (View) {
       //View

       SetPage(LBPage);
       j=LB->CurPosition;
       SetPage(TempPage);
       LoadArchDate(Param,j,TempPage);

       DiagDate=DateToDateTime(EncodeDate(Param->Year,
                                          Param->Month,
                                          Param->Day))+
                TimeToDateTime(EncodeTime(Param->Hour,
                                          Param->Min,
                                            0));
       DoSumDiag(DiagDate,&Diag,STABLEDiagRegime,CurDiagRegime,MeasurementsInArchive-j,CurDiagType,TempPage,0);
       LoadData(Param,j,TempPage);

       FillFlag=-1;
       while(1){
          ClearLCD();
          SetFont(Font8x6);
          ShowParameters(Param,&Diag,0xFC,key,TempPage,TempPage,shNoShowErrors);
          Redraw();
          key = WaitReadKeyWithDelay(BREAKDELAY, KB_TIMEOUT);
          if (key==KB_ESC) break;
       }

       key=KB_NO;
       i=1;
       goto StartDraw;

    } else if((*LB).Count>0){


      key=YesNoMessage(DelMeasConfirm,Font8x6);
      if (key==KB_ENTER){
        SetPage(MeasListPage);

        FileDelete(LoadFileIndex((*LB).CurPosition));

        if ((*LB).CurPosition<MeasurementsInArchive-1) {//�� ������� �� ��������� � ������
           LoadFileIndexes(MeasListTempAddr);
           memmove(((u16*)MeasListTempAddr)+(*LB).CurPosition, ((u16*)MeasListTempAddr)+(*LB).CurPosition+1, (MeasurementsInArchive-(*LB).CurPosition-1)*sizeof(u16));
           SaveFileIndexes(MeasListTempAddr);
        }
        MeasurementsInArchive--;
        (*LB).Count--;
        if((*LB).CurPosition>=(*LB).Count)
          if((*LB).Count>0)(*LB).CurPosition=(*LB).Count-1;else (*LB).CurPosition=0;
        if((*LB).TopLine>=(*LB).Count)
          if((*LB).Count>0)(*LB).TopLine=(*LB).Count-1;else (*LB).TopLine=0;
        i=1;
        if (MeasurementsInArchive==0) {
           FullStatus=stUnknown;
           GammaStatus=stUnknown;
           OutGammaStatus(FullStatus,0);
           goto EndProc;
        }
        ClearAvg();
      } else
      if (key==KB_TIMEOUT) {res=KB_TIMEOUT;goto EndProc;}
      goto StartDraw;
    }
  }
  if ((key==KB_MEM)&&(!View)&&((*LB).Count>0)){
    key=YesNoMessage(DelArchiveConfirm,Font8x6);
    if(key==KB_ENTER){
      DelArchive();
      SetPage(LBPage);
      (*LB).Count=0;
      (*LB).CurPosition=0;
      FullStatus=stUnknown;
      GammaStatus=stUnknown;
      OutGammaStatus(FullStatus,0);
      i=1;
      goto EndProc;
    } else if (key==KB_TIMEOUT) {res=KB_TIMEOUT; goto EndProc;}
    goto StartDraw;
  }

  SetPage(LBPage);
  HandleCheckListBox(LB, key, &i);
}

EndProc:
  SetPage(Page);
  ReadKey();
  return res;
}


unsigned int MeasurementNumToIndex(unsigned int Num)
{
if (Num<=MeasurementsInArchive) return (MeasurementsInArchive-Num);
return 0;
}

#ifdef __arm
void SaveFileIndexes(void * MeasListAddr)
{
  WriteFram(FileListAddr,MeasListAddr,sizeof(u16)*MeasurementsInArchive);
}

u16 LoadFileIndex(u16 LoadIndex)
{u16 data;
  ReadFram(FileListAddr+(LoadIndex*sizeof(u16)),(void *)&data,sizeof(u16));
 return data;
}

void LoadFileIndexes(void * MeasListAddr)
{
  ReadFram(FileListAddr,MeasListAddr,sizeof(u16)*MeasurementsInArchive);
}

#endif

char LoadData(struct stInsulationParameters *Param,unsigned int Num,char aPage)
{
#ifndef __arm
  char Page=GetPage();
#endif
  TFile f;
  u16 Index;
  //For regigter protocol;
  extern
#ifdef __arm
  unsigned int
#else
  char
#endif
  MeasLoaded;

  SetPage(MeasListPage);
#ifdef __arm
  Index=LoadFileIndex(Num);
#else
  Index=((u16*)MeasList)[Num];
#endif
  if (FileOpen(&f,Index,fmRead)) {
     MeasLoaded=0;
     SetPage(aPage);
     if ((unsigned long)FileRead(&f,(char*)Param,0,szInsulationParameters)==szInsulationParameters) {SetPage(Page); return 1;}
  }
  SetPage(Page);
  return 0;
}


unsigned int LoadArchGamma(unsigned int Num)
{

return 0;
}


void ClaerAllSource(void)
{ unsigned int Size;

  Size=(ReadCount<<1);
#ifdef __arm
  memset(BaseAddrSig1[0], 0, Size);
  memset(BaseAddrSig2[0], 0, Size);
  memset(BaseAddrSig3[0], 0, Size);
  memset(BaseAddrSig4[0], 0, Size);
#else
  SetPage0;
  memset((void *)BaseAddrSig1[0], 0, Size);
  memset((void *)BaseAddrSig1[1], 0, Size);
  SetPage1;
  memset((void *)BaseAddrSig2[0], 0, Size);
  memset((void *)BaseAddrSig3[1], 0, Size);
  SetPage2;
  memset((void *)BaseAddrSig3[0], 0, Size);
  memset((void *)BaseAddrSig3[1], 0, Size);
  SetPage3;
  memset((void *)BaseAddrSig4[0], 0, Size);
  memset((void *)BaseAddrSig4[1], 0, Size);
  SetPage(GammaPage4);
  memset((void *)BaseAddrSig4[0], 0, Size);
  memset((void *)BaseAddrSig4[1], 0, Size);
  SetPage(GammaPage7);
  memset((void *)BaseAddrSig4[0], 0, Size);
  memset((void *)BaseAddrSig4[1], 0, Size);
#endif
}

void ClearArchive(void)
{ unsigned int i;
#ifndef __arm
 char Page=GetPage();
#endif
  extern unsigned int OutRelayFrom;
  extern unsigned int CurMeasurement;
  extern unsigned int SaveNum;
  //��������� ���������� ���������
  extern signed char MeasurementResult;

//  unsigned int i,j;


  ClearFlash();
  if (!OpenFlash()) {
     Error|=((unsigned long)1<<erFlashReadFail);
     Error|=((unsigned long)1<<erFlashWriteFail);
  } else {
#ifdef __arm
     CreateList((u16 *)MeasListTempAddr,&MeasurementsInArchive,stForward/*����� � ������*/);
     SaveFileIndexes(MeasListTempAddr);
#else
     SetPage(MeasListPage);
     CreateList((uint*)MeasList,(uint *)&MeasurementsInArchive,stForward);
#endif
  }
  OutRelayFrom=0;
  CurMeasurement=0;
  //�����  ���������� ������������ ������
  SaveNum=1;
  //������� ��������� Gamma
  GammaStatus=stUnknown;
  //������� ��������� �� ���� ����������
  FullStatus=stUnknown;
  MeasurementResult=0;
  DiagResult=0;

  for (i=0;i<szInsulationParameters;i++)
       *((char*)Parameters+i)=0;

  for (i=0;i<szDiagnosis;i++)
       *((char*)Diagnosis+i)=0;

  ClearAvg();

  OutGammaStatus(GammaStatus=stUnknown,0);
  SetPage(Page);

}


void LoadArchDate(struct stInsulationParameters *Buf,unsigned int Num,char aPage)
{
TFile f;
#ifndef __arm
char Page=GetPage();
#endif
u16 i;

  SetPage(MeasListPage);
#ifdef __arm
  i=LoadFileIndex(Num);
#else
  i=((uint*)MeasList)[Num];
#endif
  if (FileOpen(&f,i,fmRead)) {
     SetPage(aPage);
     if (FileRead(&f,(char*)Buf,0,10)==10) {
     }
  }
  SetPage(Page);
}

void ClearInitialTgDelta(char Side)
{ char i,j,MaxSide,FromSide;

if (Side==AllSides) {
   MaxSide=MaxTransformerSide;
   FromSide=0;
} else {
   MaxSide=Side;
   FromSide=Side;
}

for (i=FromSide;i<MaxSide;i++) {
    Setup.GammaSetupInfo.GammaSideSetup[i].STABLEDate=0;
    Setup.GammaSetupInfo.GammaSideSetup[i].HeatDate=0;
    Setup.GammaSetupInfo.GammaSideSetup[i].STABLESaved=0;
    Setup.GammaSetupInfo.GammaSideSetup[i].MinTemperature1=0;
    Setup.GammaSetupInfo.GammaSideSetup[i].AvgTemperature1=0;
    Setup.GammaSetupInfo.GammaSideSetup[i].MaxTemperature1=0;

    for (j=0;j<MaxBushingCountOnSide;j++) {
         Setup.GammaSetupInfo.GammaSideSetup[i].STABLETg[j]=Setup.GammaSetupInfo.GammaSideSetup[i].Tg0[j];
         Setup.GammaSetupInfo.GammaSideSetup[i].STABLEC[j]=Setup.GammaSetupInfo.GammaSideSetup[i].C0[j];
    }
}
}

void ClearInitialZk(void)
{ char i,j;

Setup.InitialZkParameters.Year=0;
Setup.InitialZkParameters.Day=0;
Setup.InitialZkParameters.Month=0;
Setup.InitialZkParameters.Hour=0;
Setup.InitialZkParameters.Min=0;

for (i=0;i<MaxCorrelationSide;i++) {
    for (j=0;j<MaxBushingCountOnSide;j++) {
        //������ ���������
        //��������� ������� ���� 1 (HV), V
        Setup.InitialZkParameters.SideToSideData[i].Amplitude1[j]=0;
        //��������� ������� ���� 2 (LV), V
        Setup.InitialZkParameters.SideToSideData[i].Amplitude2[j]=0;
        //����� ��� �������. ���� 1, Deg *100
        Setup.InitialZkParameters.SideToSideData[i].PhaseShift[j]=0;
        //��������� ���� , A
        Setup.InitialZkParameters.SideToSideData[i].CurrentAmpl[j]=0;
        //����� ��� �������. ���� 1, Deg *100
        Setup.InitialZkParameters.SideToSideData[i].CurrentPhase[j]=0;
        //��������� Zk, ��  (��� �������� - ��� �� Zk, � Uk)
        Setup.InitialZkParameters.SideToSideData[i].ZkAmpl[j]=0;
        //����� Zk �������. ����, Deg *100
        Setup.InitialZkParameters.SideToSideData[i].ZkPhase[j]=0;
     }
     Setup.InitialZkParameters.SideToSideData[i].CurrentChannelShift=0;
}
Setup.InitialZkParameters.Year=0;
Setup.InitialZkParameters.Day=0;
Setup.InitialZkParameters.Month=0;
Setup.InitialZkParameters.Hour=0;
Setup.InitialZkParameters.Min=0;

for (i=0;i<MaxCorrelationSide;i++) {
    for (j=0;j<MaxLoads;j++) {
        Setup.BaseLineZk.ZkP[i][j][BushingA]=0;
        Setup.BaseLineZk.ZkP[i][j][BushingB]=0;
        Setup.BaseLineZk.ZkP[i][j][BushingC]=0;
        Setup.BaseLineZk.ZkPCount[i][j]=0;
        Setup.BaseLineZk.ZkPOnDate[i][j]=0;
    }
}
TestSetup();
}

void ClearAllData(void)
{unsigned int i;
#ifndef __arm
 char Page=GetPage();
#endif

SetDeviceBusy();
LED2Yellow;

SetPage(ParametersPage);
for (i=0;i<MaxTransformerSide;i++) {
     Parameters->TrSide[i].Trend=
     Parameters->TrSide[i].KTPhase=
     Parameters->TrSide[i].KT=
     Trend[i]=
     KTPhase[i]=
     KT[i]=0;
     Setup.GammaSetupInfo.GammaSideSetup[i].STABLEDate=0;
     Setup.GammaSetupInfo.GammaSideSetup[i].STABLETemperature=0;
     Setup.GammaSetupInfo.GammaSideSetup[i].STABLESaved=0;
}
SetPage(Page);
ClearInitParam(AllSides);
ClearInitialTgDelta(AllSides);
ClearInitialZk();
ClearArchive();
for (i=0;i<MaxBushingCountOnSide;i++) {
    SetDACDevice(HVSide,127,i);
    SetDACDevice(LVSide,127,i);
    Setup.GammaSetupInfo.GammaSideSetup[HVSide].ImpedanseValue[i]=127;
    Setup.GammaSetupInfo.GammaSideSetup[LVSide].ImpedanseValue[i]=127;
}
SaveLog(lcClearAll);
#ifdef __arm
TestSetup();
#endif
SetDeviceNoBusy();
}

void ClearAllDataOnSide(char Side)
{unsigned int i;
#ifndef __arm
 char Page=GetPage();
#endif

SetPage(ParametersPage);
Parameters->TrSide[Side].Trend=
Parameters->TrSide[Side].KTPhase=
Parameters->TrSide[Side].KT=
Trend[Side]=
KTPhase[Side]=
KT[Side]=0;
Setup.GammaSetupInfo.GammaSideSetup[Side].STABLEDate=0;
Setup.GammaSetupInfo.GammaSideSetup[Side].STABLETemperature=0;
Setup.GammaSetupInfo.GammaSideSetup[Side].STABLESaved=0;

SetPage(Page);
ClearInitParam(Side);
ClearInitialTgDelta(Side);
ClearArchive();
for (i=0;i<MaxBushingCountOnSide;i++) {
    SetDACDevice(Side,127,i);
    Setup.GammaSetupInfo.GammaSideSetup[Side].ImpedanseValue[i]=127;
}
SaveLog(lcClearAll);
#ifdef __arm
TestSetup();
#endif
}

/*  acm, move this into DoIt.h, it lives there on PD and Main, unsure why got moved here...
void SetPause(void)
{
extern unsigned int PausedFrom;
#ifdef __arm
GET_TIME_INBUF();
#else
GET_TIME();
#endif
PausedFrom=(unsigned int)DateTime.Hour*(unsigned int)60+(unsigned int)DateTime.Min;
if (!PausedFrom) PausedFrom=1;
Error|=((unsigned long)1<<erPause);
}
*/

unsigned int GetMOnDate(unsigned long Date,unsigned long Time)
{

  unsigned int i;
  int MNumber=0;
  unsigned long CurDate,CurTime;
#ifdef __arm
  struct stInsulationParameters *ArchiveData = TempParam;
#else
  struct stInsulationParameters *ArchiveData =(struct stInsulationParameters *) (0xFFFF-szInsulationParameters);
#endif

 for (i=0;i<MeasurementsInArchive;i++) {
//ReadFromFLASh
    SetPage(TempPage);
    LoadData(ArchiveData,MeasurementNumToIndex(MeasurementsInArchive-i),TempPage);

    CurDate=(unsigned long)(2000+(unsigned long)(*ArchiveData).Year%100)*10000+(unsigned long)(*ArchiveData).Month*100+(unsigned long)(*ArchiveData).Day;
    CurTime=(unsigned long)(*ArchiveData).Hour*10000+(unsigned long)(*ArchiveData).Min*100;

    if ( (CurDate<Date)||
         ((CurDate==Date)&&(CurTime<=Time))
       )
    {
        break;
    }
    MNumber++;
}
if (MeasurementsInArchive-MNumber+1>MeasurementsInArchive) return 0;
else return MeasurementsInArchive-MNumber+1;
}
