#ifndef __arm
#include "Protocol.h"
#include "ADC.h"
#include "DAC.h"
#else
#include "ADCDevice.h"
#include "DACDevice.h"
#endif
#include "Defs.h"
#include "Keyboard.h"
#include "DefKB.h"
#include "LCD.h"
#include "GammaMeasurement.h"
#include "CalcParam.h"
#include "Error.h"
#include "DateTime.h"
#include "tgDelta.h"
#include "SetChannels.h"
#include "RAM.h"
#include "Graph.h"
#include "StrConst.h"
#include "DrawGraph.h"
#include "Setup.h"
#include "Utils.h"
#include "Archive.h"
#include "Complex.h"
#include "LogFile.h"
#include <math.h>
#include "Link.h"

//Flag indicating chnage in some hw settings
extern volatile char ChangeDACValues;

extern void TestSetup(); // acm, do this so Understand resolves
extern void SaveSetup(); // acm, do this so Understand resolves
extern char SelectSide(char *CurSide); // acm, do this so Understand resolves

extern char swap_phaseBC[]; 	  // acm, v1.79 FW, set in beginning of Main()
extern char setx_state;			// acm, swap_state defined Main(), drivenin SetInputChannel(), 0==don't swap, 1==set1 selected, 2=set 2 selected
extern char stop_yellow_flashing;
extern float CalcAmpl(s16 *a, int ToX); //acm, resolves Understand

#ifndef OldR1500_6

char RunAutoBalans(char Side, float KoeffP,struct StOneGraph *Graph, float AvrgAmpl)
{ float Ampl1,Ampl2,Phase2,Freq,f;
unsigned int i;
int j;
char MinDACValue[MaxBushingCountOnSide];
float MinAmpl=10000;
unsigned int StartPoint,PointsCount;
unsigned int Count=0;
long l;
float Step,K,AmplOld=0;
float AmplA,AmplB,AmplC,ABPhaseShift,ACPhaseShift,GammaAmpl;
#ifdef __arm
unsigned int
#else
char
#endif
ShiftPhase,Up,
EmptyStep=0,
First=1;

if ((Setup.GammaSetupInfo.ReadOnSide&(1<<Side))==0) return 0;

Up=0;
for (i=0;i<MaxBushingCountOnSide;i++) {
  if ((DACValue[Side][i]<50)||(DACValue[Side][i]>200)) { Up=1; break;}
}


if (Up) {
  //ReSet DAC
  for (i=0;i<MaxBushingCountOnSide;i++) {
    SetDACDevice(Side,127,i);	// acm, ext DAC to board 2 comparator REF
  }
}

if (fabs(KoeffP)<0.001) {
  ClearLCD();
  SetFont(Font8x8);
  OutString(4,4,CalibrStr);
  Redraw();
  
  AddrSig[0]=(s16*)BaseAddrSig1[Side];
  AddrSig[1]=(s16*)BaseAddrSig2[Side];
  AddrSig[2]=(s16*)BaseAddrSig3[Side];
  AddrSig[3]=(s16*)BaseAddrSig4[Side];
  
  SetDACDevice(Side,DACValue[Side][BushingA],BushingA);
  SetDACDevice(Side,DACValue[Side][BushingB],BushingB);
  SetDACDevice(Side,DACValue[Side][BushingC],BushingC);
  
  SetInputChannel(Side,chPhaseA);
  Freq=0;
  KoeffP=0;
  Ampl1=0;
  
  if (!ReadSourcePhases(&AmplA,&AmplB,&AmplC,&ABPhaseShift,&ACPhaseShift, Side, &Freq, 1)) {
    SetInputChannel(Side,chGamma);
    return 0;
  } else {
    if (!RunCalibration(&KoeffP,&Freq,&Ampl1,0,Side)) {
      SetInputChannel(Side,chGamma);
      return 0;
    } else {
      AvrgAmpl=0;
      //Phase shift pahse A relatively phaseA
      SetInputChannel(Side,chPhaseA);
      ReadAmlPhase(&i,&Ampl1,KoeffP,Freq,Side);
      AvrgAmpl+=((float)i/10.0);
      //Phase shift pahse B relatively phaseA
      SetInputChannel(Side,chPhaseB);
      ReadAmlPhase(&i,&Ampl1,KoeffP,Freq,Side);
      AvrgAmpl+=((float)i/10.0);
      //Phase shift pahse C relatively phaseA
      SetInputChannel(Side,chPhaseC);
      ReadAmlPhase(&i,&Ampl1,KoeffP,Freq,Side);
      AvrgAmpl+=((float)i/10.0);
      AvrgAmpl/=3.0;
    }
  }
}

SetInputChannel(Side,chGamma);
for (i=0;i<MaxBushingCountOnSide;i++)
MinDACValue[i]=DACValue[Side][i];

do {
#ifndef __arm
  NOKBOffOnADCRead=1;
#endif
  ReadData(0,1,chReadAllChannels,Side,SetNullLine,0);
#ifndef __arm
  NOKBOffOnADCRead=0;
#endif
  AmplifIndex[Side][GammaCh]=AmplifIndex[Side][BushingOnGammaCh];
  
  Freq=0;
  Ampl1=0;
  ImpPhaseCalc((s16 *)(AddrSig[0]),
               (s16 *)(AddrSig[3]),
               BalansingUpLoadCount,&Freq,&Ampl2,&Phase2,&Ampl1,GammaPage0,GammaPage3);
  Ampl1=(Ampl1*(float)ADCStep/GainValue[AmplifIndex[Side][BushingCh]]);
  Ampl2=(Ampl2*(float)ADCStep/GammaGainValue[AmplifIndex[Side][GammaCh]]);
  GammaAmpl=Ampl2;
  
  if (Ampl2<MinAmpl) {
    MinAmpl=Ampl2;
    for (i=0;i<MaxBushingCountOnSide;i++)
      MinDACValue[i]=DACValue[Side][i];
    EmptyStep=0;
  } else {
    if ((AmplOld>0)&&(AmplOld<Ampl2)) EmptyStep++;
  }
  
  if (GammaAmpl/AvrgAmpl*100.0<0.04) break;
  if (EmptyStep>3) break;
  
  Phase2=Phase2-KoeffP;
  PhaseRound(&Phase2);
  
  AmplOld=Ampl2;
  
  //Analysing Max Phase
  if ((Phase2>=330)||(Phase2<30)) {
    //Down A
    ShiftPhase=BushingA;
    Up=0;
  } else
    if ((Phase2>=90)&&(Phase2<150)) {
      //Down B
      ShiftPhase=BushingB;
      Up=0;
    } else
      if ((Phase2>=210)&&(Phase2<270)) {
        //Down C
        ShiftPhase=BushingC;
        Up=0;
      } else
        if ((Phase2>=150)&&(Phase2<210)) {
          //Up A
          ShiftPhase=BushingA;
          Up=1;
        } else
          if ((Phase2>=270)&&(Phase2<330)) {
            //Up B
            ShiftPhase=BushingB;
            Up=1;
          } else {
            //Up C
            ShiftPhase=BushingC;
            Up=1;
          }
  
  
  //Calc Step
  if (((GainValue[AmplifIndex[Side][BushingCh]]>3)&&(Ampl2<1))||((GainValue[AmplifIndex[Side][BushingCh]]<3)&&(Ampl2<1.5))) {
    K=1;
  } else {
    K=DACValue[Side][ShiftPhase];
    
    f=AvrgAmpl/(1.0-0.2*(K/255.0));
    f=(f-(AvrgAmpl+Ampl2))/(0.2*f)*255.0;
    K=(fabs(K-f)/GainValue[AmplifIndex[Side][BushingCh]]);
  }
  
  if (K<2) Step=1;
  else Step=K;
  
  //Draw Current Graph
  if (Graph==0) goto ReSetDAC;
  if (Count>999) break;
  
  SetFont(Font6x5);
  ClearLCD();
  f=ReadFreq/Freq;
  PointsCount=f;
  if (f-PointsCount>=0.5) PointsCount++;
  
  StartPoint=0;
  
  SetPage0;
  for (i=2;i<BalansingUpLoadCount;i++) {
    if ((AddrSig[0][i-1]<0)&&(AddrSig[0][i]>=0)) {
      StartPoint=i;
      break;
    }
  }
  
  InitGraph(Graph,GammaPage3,(s16*)&AddrSig[3][StartPoint],PointsCount);
  DrawGraph(Graph,16,0,111,15);
  
  if (AvrgAmpl>0) GammaAmpl=GammaAmpl/AvrgAmpl*100.0;
  else GammaAmpl=0;
  if (GammaAmpl>10) {
    l=GammaAmpl;
    if (GammaAmpl-l>=0.5) l++;
    OutInt(0,0,l,3);
  } else {
    if (GammaAmpl>1)
      OutFloat(0,0,GammaAmpl,3,1);
    else
      OutFloat(0,0,GammaAmpl,3,2);
  }
  
  i=Phase2;
  if (Phase2-(float)i>0.5) i++;
  OutInt(0,6,i,3);
  
  OutInt(17,0,Count,3);
  
  OutInt(59,0,DACValue[Side][0],3);
  OutInt(78,0,DACValue[Side][1],3);
  OutInt(97,0,DACValue[Side][2],3);
  
  Redraw();
  
ReSetDAC:
  Count++;
  if (First) Step/=2.0;
  i=Step;
  if (Step-i>=0.5) i++;
  
  if (First) {
    //      Step=Step/0.866;
    Step=Step/1.1;
    for (j=0;j<MaxBushingCountOnSide;j++) {
      if (j!=ShiftPhase) {
        if (Up){
          if(DACValue[Side][j]>Step)
            DACValue[Side][j]-=Step;
          else
            DACValue[Side][j]=0;
        } else {
          if(DACValue[Side][j]+Step<255)
            DACValue[Side][j]+=Step;
          else
            DACValue[Side][j]=255;
        }
#ifndef __emulator
        SetDACDevice(Side,DACValue[Side][j],j);
#endif
      }
    }
    First=0;
  }
  
  if (Up) {
    if(DACValue[Side][ShiftPhase]+i<255)
      DACValue[Side][ShiftPhase]+=i;
    else
      DACValue[Side][ShiftPhase]=255;
  } else {
    if(DACValue[Side][ShiftPhase]>i)
      DACValue[Side][ShiftPhase]-=i;
    else
      DACValue[Side][ShiftPhase]=0;
  }
#ifndef __emulator
  SetDACDevice(Side,DACValue[Side][ShiftPhase],ShiftPhase);
#endif
  
} while (1);

for (i=0;i<MaxBushingCountOnSide;i++) {
  DACValue[Side][i]=MinDACValue[i];
  Setup.GammaSetupInfo.GammaSideSetup[Side].ImpedanseValue[i]=MinDACValue[i];
  SetDACDevice(Side,DACValue[Side][i],i);
}

return 1;
}

void SaveInitial(void)
{//signed char CurRegime;
#ifdef __arm
  unsigned int
#else
    char
#endif
      i;
  //unsigned long tmpError;
  //unsigned long erMask; //2.06 mask for errors that are not important for intial data
  
  IsInitialParam=0;
  ClearLCD();
  SetFont(Font8x8);
  OutString(4,4,MeasuringStr);
  Redraw();
  
#ifdef __arm
  SaveSetup();		// acm, some Setup values in RAM previously modified during balance, saving to FRAM avoids CRC mismatch error in StartMeasure()
#endif
  
  StartMeasure(svSaveAllways,0);  // 2.05 StartMeasure(svNoSave,0);												// acm, test measurement, dont save data value
  
  if (!(Error&0x1800FFFF)) {	//2.05 ag hardcoded mask, otherwise optimizes out error masking. Errors 0-15 and 27,28 Load threshould
    SetPage(ParametersPage);
    //Date of measurement
    Setup.InitialParameters.Day=Parameters->Day;
    Setup.InitialParameters.Month=Parameters->Month;
    Setup.InitialParameters.Year=Parameters->Year;  //-2000
    //Time of measurement
    Setup.InitialParameters.Hour=Parameters->Hour;
    Setup.InitialParameters.Min=Parameters->Min;
    
    for (i=0;i<MaxTransformerSide;i++) {
      
      //Parameters f each set og bushings
      //Phase shift between measuring channels (degrees)*100
      Setup.InitialParameters.TrSideParam[i].ChPhaseShift=Parameters->TrSide[i].ChPhaseShift;
      
      
      //Phase signal amplitude, mV
      Setup.InitialParameters.TrSideParam[i].SourceAmplitude[BushingA]=Parameters->TrSide[i].SourceAmplitude[BushingA];
      Setup.InitialParameters.TrSideParam[i].SourceAmplitude[BushingB]=Parameters->TrSide[i].SourceAmplitude[BushingB];
      Setup.InitialParameters.TrSideParam[i].SourceAmplitude[BushingC]=Parameters->TrSide[i].SourceAmplitude[BushingC];
      
      Setup.InitialParameters.TrSideParam[i].PhaseAmplitude[BushingA]=Parameters->TrSide[i].PhaseAmplitude[BushingA];
      Setup.InitialParameters.TrSideParam[i].PhaseAmplitude[BushingB]=Parameters->TrSide[i].PhaseAmplitude[BushingB];
      Setup.InitialParameters.TrSideParam[i].PhaseAmplitude[BushingC]=Parameters->TrSide[i].PhaseAmplitude[BushingC];
      
      //Temperature~(-70 - +185 0C) with step 1 0C
      Setup.InitialParameters.TrSideParam[i].Temperature=Parameters->TrSide[i].Temperature;
      
      //Gamma, %*0.01
      Setup.InitialParameters.TrSideParam[i].GammaAmplitude=Parameters->TrSide[i].Gamma;
      if (Parameters->TrSide[i].Gamma>50)  // acm, v1.79, change from 100, i.e %.5 is considered bad, 1% really bad
      {
        //Error|=(unsigned long)(1<<warnFlashYelHiUnnBal); 	// acm, if Unn/imbalance is >1% blink Yellow fast to warn user (normally <=1%)
        Setup.InitialParameters.TrSideParam[i].DefectCode|=warnFlashYelHiUnnBal;  	// acm, v1.79 start using this.  Bit 0, phaseB/C swap, bit 1=hi balance encountered, one char/side.  Same values stored in data record, confusing?
      }
      
      if(Setup.InitialParameters.TrSideParam[i].DefectCode)
        stop_yellow_flashing = 0;  // acm, let the yellow flashing begin, alert hi balance OR phaseswap, for short time until next measurement
      
      //Imbalance phase ,degrees, Byte*0.01
      Setup.InitialParameters.TrSideParam[i].GammaPhase=Parameters->TrSide[i].GammaPhase;
      
      //Initial Gamma Temperature Coefficient, *0.002
      Setup.InitialParameters.TrSideParam[i].KT=0;
      
      Setup.InitialParameters.TrSideParam[i].KTPhase=0;
      
      //KT Saved in inishial =0; not saved any other value;
      Setup.InitialParameters.TrSideParam[i].KTSaved=0;
      
      Setup.InitialParameters.TrSideParam[i].SourcePhase[BushingA]=Parameters->TrSide[i].SourcePhase[BushingA];
      Setup.InitialParameters.TrSideParam[i].SourcePhase[BushingB]=Parameters->TrSide[i].SourcePhase[BushingB];
      Setup.InitialParameters.TrSideParam[i].SourcePhase[BushingC]=Parameters->TrSide[i].SourcePhase[BushingC];
      
      Setup.InitialParameters.TrSideParam[i].SignalPhase[BushingA]=Parameters->TrSide[i].SignalPhase[BushingA];
      Setup.InitialParameters.TrSideParam[i].SignalPhase[BushingB]=Parameters->TrSide[i].SignalPhase[BushingB];
      Setup.InitialParameters.TrSideParam[i].SignalPhase[BushingC]=Parameters->TrSide[i].SignalPhase[BushingC];
      
      Setup.GammaSetupInfo.GammaSideSetup[i].ImpedanseValue[BushingA]=DACValue[i][BushingA];
      Setup.GammaSetupInfo.GammaSideSetup[i].ImpedanseValue[BushingB]=DACValue[i][BushingB];
      Setup.GammaSetupInfo.GammaSideSetup[i].ImpedanseValue[BushingC]=DACValue[i][BushingC];
      
      Setup.InitialParameters.TrSideParam[i].Current=Parameters->Current[i];
      if (Parameters->Current[i]-Setup.InitialParameters.TrSideParam[i].Current>=0.5)
        Setup.InitialParameters.TrSideParam[i].Current++;
    }
    
#ifdef __arm
    SaveSetup();
#endif
    IsInitialParam=1;
  }
}

// acm, v1.79 add new function to test if phaseBC need swapping.
// Decided to do prior to balancing rather than at boot time, means code needs to be executed in either balansing() or FullAutoBalansing()
void AutoPhaseRotate(unsigned int ch)
{
  // unused return values...
  float AmplA, AmplB, AmplC;
  float ABPhaseShift, ACPhaseShift, Freq=0;
  
  swap_phaseBC[ch]=0;  // init, disable logic that switches phase B&C, test normal case, if error, enable switching logic
  
  if (!ReadSourcePhases(&AmplA,&AmplB,&AmplC,&ABPhaseShift,&ACPhaseShift, ch, &Freq, 0)) // following code block to move from main() to balansing()
  {
    if(Error&((unsigned long)1<<(erPhaseShiftSet1+ch))) // acm, phase rotation error?
    {
      Error&=~((unsigned long)1<<(erPhaseShiftSet1+ch));
      // triple redundancy
      swap_phaseBC[ch]=								// indicate to ADCDevice IRQ routine and SetChannels phase B & C swap necessary
        Parameters->TrSide[ch].DefectCode=					// store DefectCode in NOR Flash
          Setup.InitialParameters.TrSideParam[ch].DefectCode=warnsetphaserot;	// store in Setup structure, phase B & C swapped (clear out previous warnFlashYelHiUnnBal)
      stop_yellow_flashing = 0;  						// enable yellow flashing, indicate phase swap
      // TestSetup();								// write Setup structure change to FRAM->unnecessary, if balance, initial data saved at end
      //Error|=(unsigned long)(1<<(warnset1phaserot+ch));	                // reset error, prior to reinvoking ReadSourcePhases(), but indicate to astute user there is a phase rotation problem (extended error fast blinks green, modify?)
    }
  } // repeat ReadSourcePhases, should work this time if phases B/C were swapped incorrectly  
}

u32 FullAutoBalansing(void)
{
#ifdef __arm
  unsigned int
#else
    char
#endif
      i;
  float KoeffP,AvrgAmpl;
  struct StOneGraph Graph;
  char Result=0,Cnt=0;
  
  SetDeviceBusy();	// acm, v2 PAUSE unit, disable MODBUS cmds.  This may eliminate false observation that balance failed.
  
  Error&=~((unsigned long)1<<erPhaseShiftSet1);
  Error&=~((unsigned long)1<<erPhaseShiftSet2);
  Error&=~((unsigned long)1<<erChannels);
  Error&=~((unsigned long)1<<erUnitOff);
  Error&=~((unsigned long)1<<erPhaseAOff);
  Error&=~((unsigned long)1<<erPhaseBOff);
  Error&=~((unsigned long)1<<erPhaseCOff);
  Error&=~((unsigned long)1<<erFreq);
  Error&=~((unsigned long)1<<erLoSignal);
  Error&=~((unsigned long)1<<erHiSignal);
  Error&=~((unsigned long)1<<(erSet1Off));
  Error&=~((unsigned long)1<<(erSet2Off));
  Error&=~((unsigned long)1<<(er_noinit_need2bal));  //acm, v1.79
  Error&=~((unsigned long)1<<erSide1Threshold);	        // dwe, clear Side12 Threshold test failed 
  Error&=~((unsigned long)1<<erSide2Threshold);	        // dwe, clear Side2 Threshold test failed
  Error&=~((unsigned long)1<<erSide1Critical);
  Error&=~((unsigned long)1<<erSide2Critical);
  Error&=~((unsigned long)1<<erLoCurrent);
  Error&=~((unsigned long)1<<erwarnFlashYelHiUnnBal);  // acm, reset error condition at next measurement (after balancing), thought is user has been informed, and >1% imbalance is acceptable
  Error&=~((unsigned long)1<<erRainTimer);	        // dwe, erRainTimer //DE 4-4-16 changed 
  Error&=~((unsigned long)1<<erFlashWriteFail);	        // dwe, clear erFlashWriteFail 
  Error&=~((unsigned long)1<<erFlashReadFail);	        // dwe, clear erFlashReadFail 
  Error&=~((unsigned long)1<<erHVTestFail);	//2.05 ag 12-16-16 this has never been cleared
  Error&=~((unsigned long)1<<erLVTestFail);	//2.05 ag 12-16-16 this has never been cleared
  
  ChangeDACValues=0;
  
  for (i=0;i<MaxTransformerSide;i++) {
    if (Setup.GammaSetupInfo.ReadOnSide&(1<<i)) {
      ClearLCD();
      Cnt++;
      KoeffP=0;
      AvrgAmpl=0;
      AutoPhaseRotate(i); // acm, v1.79, swap phase BC if necessary
      if (RunAutoBalans(i,KoeffP,&Graph,AvrgAmpl)) Result++;
    }
  }
  
  if (ChangeDACValues) ChangeDACValues;
  if ((Result==Cnt)&&(Result>0)) {
    SaveInitial();
    EventTrigger = EventTrigger | NewlyBalanced; //Flag new balance
    SetDeviceNoBusy();		// acm, v2 now react to MODBUS cmds
    return 1;
  } else 
  {
    //	  Error|=(1<<er_noinit_need2bal); //DE 5-1-15 removed // acm, v1.79, good idea to indicate BHM not capable of taking measurements until balance occurs
    // acm, 2-3-12 need to save Setup changes?
    SetDeviceNoBusy();	// acm, v2 now react to MODBUS cmds
    return 0;
  }
}

#endif


void Balansing(void)  // acm, this is called via external display menu.  Normal balance algorithm is FullAutoBalansing()->RunAutoBalans, here is more of manual/diagnostic balancing function
{
  
#ifdef __arm
  KEY ch=KB_NO;
  unsigned int
#else
    char ch,
#endif
CurRegime=5,RegimeChanged=1;
struct TSinParameters SinParam1,SinParam2;
float f;
float /*KoeffA,*/KoeffP;
unsigned int i;
long l;
unsigned long CurError;
char Side=0;
struct StOneGraph Graph;
unsigned int StartPoint,PointsCount;
char Balansed=0;
float Ampl1,AmplA,AmplB,AmplC,ABPhaseShift,ACPhaseShift;
float AvrgAmpl;

#ifndef OldR1500_6
#ifdef __arm
unsigned int
#else
char
#endif
ChangePhase=0;
#endif

AutoPhaseRotate(0); // acm, v1.79, swap phase BC if necessary, set0
AutoPhaseRotate(1); // 										   set1

while (1) {
  RegimeChanged=1;
  if (!SelectSide(&Side))					//acm, I think keep on balancing specified side until ESC key
  {
    if (Balansed) goto DoSaveInitial;	// acm, if user balanced both sides, then save initial data, otherwise bail
    else 
    {
      //		   Error|=(1<<er_noinit_need2bal); //DE 5-1-15 removed // acm, v1.79, good idea to indicate BHM not capable of taking measurements until balance occurs
      return ;
    }
  }
  
  
  ClearLCD();
  SetFont(Font8x8);
  OutString(4,4,CalibrStr);
  Redraw();
  
  AddrSig[0]=(s16*)BaseAddrSig1[Side];
  AddrSig[1]=(s16*)BaseAddrSig2[Side];
  AddrSig[2]=(s16*)BaseAddrSig3[Side];
  AddrSig[3]=(s16*)BaseAddrSig4[Side];
  
  SetDACDevice(Side,DACValue[Side][BushingA],BushingA);
  SetDACDevice(Side,DACValue[Side][BushingB],BushingB);
  SetDACDevice(Side,DACValue[Side][BushingC],BushingC);
  
  ChangeDACValues=0;
  
  SetInputChannel(Side,chPhaseA);
  
  Error&=~((unsigned long)1<<erPhaseShiftSet1);
  Error&=~((unsigned long)1<<erPhaseShiftSet2);
  Error&=~((unsigned long)1<<erChannels);
  Error&=~((unsigned long)1<<erUnitOff);
  Error&=~((unsigned long)1<<erPhaseAOff);
  Error&=~((unsigned long)1<<erPhaseBOff);
  Error&=~((unsigned long)1<<erPhaseCOff);
  Error&=~((unsigned long)1<<erFreq);
  Error&=~((unsigned long)1<<erLoSignal);
  Error&=~((unsigned long)1<<erHiSignal);
  Error&=~((unsigned long)1<<(erSet1Off));
  Error&=~((unsigned long)1<<(erSet2Off));
  Error&=~((unsigned long)1<<(er_noinit_need2bal));  //acm, v1.79
  Error&=~((unsigned long)1<<erSide1Threshold);	        // dwe, clear Side12 Threshold test failed 
  Error&=~((unsigned long)1<<erSide2Threshold);	        // dwe, clear Side2 Threshold test failed
  Error&=~((unsigned long)1<<erSide1Critical);
  Error&=~((unsigned long)1<<erSide2Critical);
  Error&=~((unsigned long)1<<erLoCurrent);
  Error&=~((unsigned long)1<<erwarnFlashYelHiUnnBal);  // acm, reset error condition at next measurement (after balancing), thought is user has been informed, and >1% imbalance is acceptable
  Error&=~((unsigned long)1<<erRainTimer);	        // dwe, erRainTimer //DE 4-4-16 changed 
  Error&=~((unsigned long)1<<erFlashWriteFail);	        // dwe, clear erFlashWriteFail 
  Error&=~((unsigned long)1<<erFlashReadFail);	        // dwe, clear erFlashReadFail 
  Error&=~((unsigned long)1<<erHVTestFail);	//2.05 ag 12-16-16 this has never been cleared
  Error&=~((unsigned long)1<<erLVTestFail);	//2.05 ag 12-16-16 this has never been cleared
  
  f=0;
  KoeffP=0;
  SinParam1.Ampl=0;
  
  if (ReadSourcePhases(&AmplA,&AmplB,&AmplC,&ABPhaseShift,&ACPhaseShift, Side, &f, 1)) {
    if (RunCalibration(&KoeffP,&f,&SinParam1.Ampl,0,Side)) {
      AvrgAmpl=0;
      //Phase shift phase A relative to phase A and amplitude
      SetInputChannel(Side,chPhaseA);
      ReadAmlPhase(&i,&Ampl1,KoeffP,f,Side);
      AvrgAmpl+=((float)i/10.0);
      //Phase shift phase B relative to phase A
      SetInputChannel(Side,chPhaseB);
      ReadAmlPhase(&i,&Ampl1,KoeffP,f,Side);
      AvrgAmpl+=((float)i/10.0);
      //Phase shift phase C relative to phase A
      SetInputChannel(Side,chPhaseC);
      ReadAmlPhase(&i,&Ampl1,KoeffP,f,Side);
      AvrgAmpl+=((float)i/10.0);
      AvrgAmpl/=3.0;
    }
  }
  
  CurError=Error;
  CurError&=~(((unsigned long)1<<erStop)|((unsigned long)1<<erPause));
  if (CurError) {
    SetInputChannel(Side,chGamma);
    ShowError(Setup.SetupInfo.OutTime,CurError,1);
    if (ChangeDACValues) SaveLog(lcBalance);
    return;
  }
  
  ClaerAllSource();
  
  ReadKey();
  ch=KB_NO;
  Balansed=1;
  while ((ch!=KB_ESC)&&(ch!=KB_ENTER)) {
    if (RegimeChanged) {
      ClearLCD();
      //Unn
      if (CurRegime==0) {
        SetInputChannel(Side,chGamma);
        SetFont(Font8x8);
        OutString(10,0,UnnStr);
        OutString(86,0,AmplEdPrcStr);
        OutString(82,8,DegStr);
        OutString(6,8,PhaseStr);
      }
      //A
      if (CurRegime==1) {
        SetInputChannel(Side,chPhaseA);
        SetFont(Font8x8);
        OutString(16,0,PhaseAStr);
      }
      //B
      if (CurRegime==2) {
        SetInputChannel(Side,chPhaseB);
        SetFont(Font8x8);
        OutString(16,0,PhaseBStr);
      }
      //C
      if (CurRegime==3) {
        SetInputChannel(Side,chPhaseC);
        SetFont(Font8x8);
        OutString(16,0,PhaseCStr);
      }
      if ((CurRegime>0)&&(CurRegime<4)) {
        SetFont(Font8x8);
        OutString(24,8,AmplGainStr);
        OutString(80,0,AmplEdStr);
      }
      //Unn Long
      if (CurRegime==4) {
        SetInputChannel(Side,chGamma);
        SetFont(Font8x8);
        OutString(10,0,UnnStr);
        OutString(86,0,AmplEdPrcStr);
      }
      //Unn Sin
      if (CurRegime==5) {
        SetFont(Font6x5);
        InitGraph(&Graph,GammaPage3,(s16*)AddrSig[3],2);
        DrawGraph(&Graph,16,0,111,15);
        SetInputChannel(Side,chGamma);
      }
      
      
      Redraw();
      RegimeChanged=0;
    }
#ifndef __arm
    NOKBOffOnADCRead=1;
#endif
    MinReadData=1;
    ReadData(0,1,chReadAllChannels,Side,SetNullLine,0);
    MinReadData=0;
#ifndef __arm
    NOKBOffOnADCRead=0;
#endif
    
    AmplifIndex[Side][GammaCh]=AmplifIndex[Side][BushingOnGammaCh];
    //Unn
    if (CurRegime==0) {
      
      SinParam1.Ampl=0;
      SinParam2.Freq=0;
      ImpPhaseCalc((s16 *)(AddrSig[0]),
                   (s16 *)(AddrSig[3]),
                   BalansingUpLoadCount,
                   &SinParam2.Freq,
                   &SinParam2.Ampl,&SinParam2.Phase,&SinParam1.Ampl,GammaPage0,GammaPage3);
      SinParam1.Phase=0;
      SinParam2.Ampl=(SinParam2.Ampl*(float)ADCStep/GammaGainValue[AmplifIndex[Side][GammaCh]]);
      
      if (AvrgAmpl>0) SinParam2.Ampl=SinParam2.Ampl/AvrgAmpl*100.0;
      else SinParam2.Ampl=0;
      
      if (SinParam2.Ampl>10) {
        l=SinParam2.Ampl;
        if (SinParam2.Ampl-l>=0.5) l++;
        OutInt(44,0,l,5);
      } else {
        if (SinParam2.Ampl>1)
          OutFloat(44,0,SinParam2.Ampl,5,2);
        else
          OutFloat(44,0,SinParam2.Ampl,5,3);
      }
      
      if (SinParam2.Freq) {
        f=SinParam2.Phase-SinParam1.Phase-KoeffP;
        PhaseRound(&f);
        
        l=f;
        if (f-l>=0.5) l++;
        if (l>360) l-=360;
        OutInt(56,8,l,3);
      } else {
        OutInt(56,8,0,3);
      }
    }
    //A B C
    if ((CurRegime)&&(CurRegime<4)) {
      i=GainValue[AmplifIndex[Side][BushingCh]];
      if (GainValue[AmplifIndex[Side][BushingCh]]-i>0.5) i++;
      OutInt(64,8,i,2);
      SetPage3;
      SinParam2.Ampl=CalcAmpl((s16 *)(AddrSig[3]),BalansingUpLoadCount);
      SinParam2.Ampl=(SinParam2.Ampl*(float)ADCStep/GammaGainValue[AmplifIndex[Side][GammaCh]])/*ChAmplitudeDifference*/;
      
      l=SinParam2.Ampl;
      if (SinParam2.Ampl-l>=0.5) l++;
      OutInt(48,0,l,4);
      
    }
    //Unn  Long
    if (CurRegime==4) {
      SetPage3;
      SinParam2.Ampl=CalcAmpl((s16 *)(AddrSig[3]),BalansingUpLoadCount);
      SinParam2.Ampl=(SinParam2.Ampl*(float)ADCStep/GammaGainValue[AmplifIndex[Side][GammaCh]]);
      
      if (AvrgAmpl>0) SinParam2.Ampl=SinParam2.Ampl/AvrgAmpl*100.0;
      else SinParam2.Ampl=0;
      
      if (SinParam2.Ampl>10) {
        l=SinParam2.Ampl;
        if (SinParam2.Ampl-l>=0.5) l++;
        OutInt(44,0,l,5);
      } else {
        if (SinParam2.Ampl>1)
          OutFloat(44,0,SinParam2.Ampl,5,2);
        else
          OutFloat(44,0,SinParam2.Ampl,5,3);
      }
      
    }
    //Unn Sin
    if (CurRegime==5) {
      ClearLCD();
      SinParam1.Ampl=0;
      SinParam2.Freq=0;
      
      ImpPhaseCalc((s16 *)(AddrSig[0]),
                   (s16 *)(AddrSig[3]),
                   BalansingUpLoadCount,
                   &SinParam2.Freq,
                   &SinParam2.Ampl,&SinParam2.Phase,&SinParam1.Ampl,GammaPage0,GammaPage3);
      
      if (SinParam2.Freq>1)
        f=ReadFreq/SinParam2.Freq;
      else
        f=0;
      
      PointsCount=f;
      if (f-PointsCount>=0.5) PointsCount++;
      
      StartPoint=0;
      SetPage0;
      for (i=2;i<BalansingUpLoadCount-1;i++) {
        if ((AddrSig[0][i-1]<0)&&(AddrSig[0][i+1]>0)) {
          StartPoint=i;
          break;
        }
      }
      
      InitGraph(&Graph,GammaPage3,(s16*)&AddrSig[3][StartPoint],PointsCount);
      
      DrawGraph(&Graph,16,0,111,15);
      
      SinParam2.Ampl=(SinParam2.Ampl*(float)ADCStep/GammaGainValue[AmplifIndex[Side][GammaCh]]);
      
      if (AvrgAmpl>0) SinParam2.Ampl=SinParam2.Ampl/AvrgAmpl*100.0;
      else SinParam2.Ampl=0;
      
      if (SinParam2.Ampl>10) {
        l=SinParam2.Ampl;
        if (SinParam2.Ampl-l>=0.5) l++;
        OutInt(0,0,l,3);
      } else {
        if (SinParam2.Ampl>1)
          OutFloat(0,0,SinParam2.Ampl,3,1);
        else
          OutFloat(0,0,SinParam2.Ampl,3,2);
      }
      
      f=SinParam2.Phase-KoeffP;
      PhaseRound(&f);
      i=f;
      if (f-(float)i>=0.5) i++;
      OutInt(0,6,i,3);
      
#ifndef OldR1500_6
      switch (ChangePhase) {
      case BushingA:OutString(18,0,ShortPhaseA);break;
      case BushingB:OutString(18,0,ShortPhaseB);break;
      case BushingC:OutString(18,0,ShortPhaseC);break;
      }
#endif
      OutInt(59,0,DACValue[Side][0],3);
      OutInt(78,0,DACValue[Side][1],3);
      OutInt(97,0,DACValue[Side][2],3);
    }
    
#ifdef __emulator
    RegimeChanged=1;
#endif
    
    Redraw();
    ch=ReadKey();
#ifndef OldR1500_6
    if (ch==KB_MOD) {
      if (ChangePhase<MaxBushingCountOnSide-1) ChangePhase++;
      else ChangePhase=0;
    }
    if (ch==KB_MEM) {
      RunAutoBalans(Side,KoeffP,&Graph,AvrgAmpl);
      RegimeChanged=1;
    }
    
    if (ch==KB_UP) {
      if (DACValue[Side][ChangePhase]<255) DACValue[Side][ChangePhase]++;
      SetDACDevice(Side,DACValue[Side][ChangePhase],ChangePhase);
    }
    if (ch==KB_DOWN) {
      if (DACValue[Side][ChangePhase]) DACValue[Side][ChangePhase]--;
      SetDACDevice(Side,DACValue[Side][ChangePhase],ChangePhase);
    }
#endif
    if (ch==KB_RIGHT) {
      if (CurRegime<5) CurRegime++; else CurRegime=0;
      RegimeChanged=1;
    }
    if (ch==KB_LEFT) {
      if (CurRegime) CurRegime--; else CurRegime=5;
      RegimeChanged=1;
    }
  }
  
  
  SetInputChannel(HVSide,chGamma);
}

DoSaveInitial:
  
  if (ChangeDACValues) SaveLog(lcBalance);
  
  ClearLCD();
  //Clear Data
  
  RegimeChanged=0;
  CurRegime=0;
  ch=YesNoMessage(SaveInitStr,Font8x6);
  
  if (ch==KB_ENTER) {
    SaveInitial();
    
    ClearLCD();
    SetFont(Font8x6);
    OutString(0,0,AlarmEnableStr3);
    OutString(0,8,AlarmEnableStr4);
    OutString(35,0,UnnStr);
    OutString(35,8,UnnStr);
    OutString(105,0,PercentStr);
    OutString(105,8,PercentStr);
    SetFont(Font8x8);
    OutFloat(60,0,(float)Setup.InitialParameters.TrSideParam[0].GammaAmplitude*0.01,5,2);
    OutFloat(60,8,(float)Setup.InitialParameters.TrSideParam[1].GammaAmplitude*0.01,5,2);
    Redraw();
    ReadKey();
    WaitReadKeyWithDelay(NoKeyDelayInSec,KB_ESC);
  }
}

void RunAutoBalansOnSide(char Side)	// acm, apparently unused
{
  float KoeffP,AvrAmpl;
  struct StOneGraph Graph;
  
  ChangeDACValues=0;
  
  if (Setup.GammaSetupInfo.ReadOnSide&(1<<Side)) {
    ClearLCD();
    KoeffP=0;
    AvrAmpl=0;
    RunAutoBalans(Side,KoeffP,&Graph,AvrAmpl);
    SaveInitial();
  }
}

void SetBalansingOnTime(void)
{
  u32 Y,D,M,OnDay;
  KEY ch=KB_NO;
  
  ClearLCD();
  SetFont(Font8x8);
  
  OutString(16,0,StTimeBalansStr1);
  if (Setup.SetupInfo.AutoBalans.Year!=0)
    Y=Setup.SetupInfo.AutoBalans.Year%100;
  else
    Y=DateTime.Year%100;
  if ((Setup.SetupInfo.AutoBalans.Month>0)&&(Setup.SetupInfo.AutoBalans.Month<13))
    M=Setup.SetupInfo.AutoBalans.Month;
  else
    M=DateTime.Month;
  if ((Setup.SetupInfo.AutoBalans.Day>0)&&(Setup.SetupInfo.AutoBalans.Month<32))
    D=Setup.SetupInfo.AutoBalans.Day;
  else
    D=DateTime.Day;
  InputDate(28,8,&D,&M,&Y);
  Setup.SetupInfo.AutoBalans.Year=Y%100;
  Setup.SetupInfo.AutoBalans.Month=M;
  Setup.SetupInfo.AutoBalans.Day=D;
  
  
  ClearLCD();
  OutString(16,0,StTimeBalansStr2);
  OutString(56,8,StTimeBalansStr3);
  Setup.SetupInfo.AutoBalans.Hour=InputInt(32,8,Setup.SetupInfo.AutoBalans.Hour,2);
  
  if (Setup.SetupInfo.AutoBalans.Hour>23) Setup.SetupInfo.AutoBalans.Hour=23;
  
  ClearLCD();
  OutString(28,0,StTimeBalansStr4);
  OutString(50,8,StTimeBalansStr5);
  OnDay=InputInt(32,8,Setup.SetupInfo.WorkingDays,1);
  
  ch=YesNoMessage(StTimeBalansStr7,Font8x6);
  if (ch==KB_ENTER) {
    Setup.SetupInfo.NoLoadTestActive=1;
  } else
    
    ch=KB_NO;
  ch=YesNoMessage(StTimeBalansStr6,Font8x6);
  if (ch==KB_ENTER) {
    Setup.SetupInfo.AutoBalansActive=OnDay+1;
    Setup.SetupInfo.WorkingDays=OnDay;
  } else {
    Setup.SetupInfo.AutoBalansActive=0;
    Setup.SetupInfo.WorkingDays=OnDay;
  }
  TestSetup();
  
}
