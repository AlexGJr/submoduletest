#ifndef _drvflash_h
#define _drvflash_h

#include "TypesDef.h"
#include "flash.h"
#include "defs.h"

/*

  �������� !!! ��� ��������� ������� ������ ����������� ������ ���� ������������

*/

#pragma pack(1)

//��������� ��� ����������
#define stForward 0 //����� � ������
#define stBack    1 //������ � ������

//��������� ��� ������ ����� ������
#define id_expanded_main    0x00
#define id_expanded_program 0x01
#define id_expanded_hide    0x02

//��������� ��� ����������� �������������� FAT
#define id_block_free       0xFF
#define id_block_first      0xFE
#define id_block_middle     0xFC
#define id_block_end        0xF8
#define id_block_one        0xF0
#define id_block_del        0x00

//��������� ��� ������� ������/������
#define fmRead   0x0001 //��������� ������
#define fmWrite  0x0002 //��������� ������
#define fmResize 0x0004 //���������� ��������� ������� �����

//��������� ��� ����������� ����
#define equFatTypeData       0   //������ ������
#define equFatTypeConfig     1   //������������
#define equFatTypeStat       2   //����������
#define equFatTypeImpulse    3   //������
#define equFatTypeAgregat    4   //�������
#define equFatTypeRoute      5   //�������
#define equFatTypeBreak      6   //�����������
#define equFatTypePower      7   //��������
#define equFatTypeRoot       8   //�������� �������
#define equFatTypeDir        9   //����������
#define equFatTypeObj        10  //������
#define equFatTypeDsp        11  //���� ���
#define equFatTypeRazgon     12  //������-�����

#define equFatTypeAll        14  //����������� ���� ����� ������
#define equFatTypeFilter     15  //������ ��������
#define equFatTypePribor     16  //
#define equFatTypePrecession 17  //��������� ����
#define equFatTypeImage      18  //���� ��������

#define equFatTypeCircled    20
#define equFatTypePermanent  21
#define equFatTypeDKV        22

#define equFatTypeNone       0xFF


typedef struct stIndexClaster
{
char       id;
_TDateTime date;
char       bad;
u16       attr;
u32      seek;
u32      prev;
} TIndexClaster;

typedef struct stFile
{
char  fm;
u32 id;
u16  count;
} TFile;

typedef struct stFatExtraData
{
u32 f_end_id;
_TDateTime f_end_date;

u32 f_first_id;
_TDateTime f_first_date;
} TFatExtraData;

#ifndef __emulator
  #define f_extra_data ((TFatExtraData *)FatExtraBuf)
#else
  extern TFatExtraData *f_extra_data;
#endif

//�������������
    char InitDrvFlash(void);
    char OpenFlash(void);

//�������� ������
    char  ClearFlash(void);
    u32 FreeBlockFlash(void);
    u32 FreeFlash(void);
    u32 SizeFlash(void);
    int   FileDelete(u32 id);

//������ ������ ������
    int  FileCreate(TFile *f,u16 Attr, u32 Size,_TDateTime *Date);
    int  FileClose(TFile *f);
    int  FileOpen(TFile *f,u16 id, unsigned int Mode);
    long FileRead(TFile *f, char *buf, u32 offset,u32 count);
    long FileWrite(TFile *f, char *buf, u32 offset,u32 count);

//�������
    int  FlashRebildFreeBlock(void);
    void CreateList(unsigned short *buf,u32 *count,char direction);

#pragma pack()

#endif

