#ifndef BoardAdd_h
#define BoardAdd_h

#include "debug.h"

#ifndef __emulator

#include "Board.h"

#define 	AT91C_AIC_SRCTYPE_EXT_LOW_LEVEL        ((unsigned int) 0x0 <<  5) // (AIC) External Sources Code Label Low-Level Sensitive
#define 	AT91C_AIC_SRCTYPE_EXT_NEGATIVE_EDGE    ((unsigned int) 0x1 <<  5) // (AIC) External Sources Code Label Negative Edge triggered
#define 	AT91C_AIC_SRCTYPE_INT_LEVEL_SENSITIVE AT91C_AIC_SRCTYPE_INT_HIGH_LEVEL


// �������� � ��
// ��������� ��� ����������� �� ��������
// ��������� !!! ���������� ���������� unsigned int i
#define DelayNs(nS) for (i=0;i<((nS)/15+1);i++) __asm( "nop" );


// �������� � ���
// Flash ~1.1 ���
// RAM ~0.6 ���
void DelayMks(unsigned int mkS);

#define AT91C_PASS ((unsigned int)(0xA5<<24))

void WaitReadButton(void);

void FlashLED(unsigned int LED);


// ������ ����������� ����������
// 0 - ������; 7 - ����� �������
#define KEYBOARD_PRIORITY       1 // 32 �� = ����������, ����������
#define USB_PRIORITY            2 // USB
#define ADC_PRIORITY            5 // ������� ������������
#define TIMER_CAPTURE_LEVEL     6 // ������� ������� Timer0-A
#define USART_INTERRUPT_LEVEL	3 //USART

/*
TIMER0,1-�������
TIMER2-ADC Freq
PWM0-��� ������
PWM1,PWM2,PWM3-��������
PIT-Keyboard

� ������ SYS ������ ���������� - �� ����������� -> ���������, ��� ������ � PIT=Keyboard
*/

#endif

// Flash ~1.1 ��
// RAM ~0.6 ��
void Delay(unsigned int mS);
void DelayMks(unsigned int mkS);

// ��� ������� ����� � RAM
extern void AT91F_enable_interrupt(void);
extern void AT91F_disable_interrupt(void);

#endif

