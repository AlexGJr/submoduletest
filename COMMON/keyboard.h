#ifndef _keyboard_h
#define _keyboard_h

#include "DefKb.h"

// ���� ���������� ����� ��� ����������� � ������� �� �������
// ���������� 1, ���� ������� ���-�� �������; ����� 0
typedef char (*IDLEFUNC)(void);

// ���������� � ��������� ����� ��� ���������� �������� ������
// ��� ����� ������ ���������� ����, ��� IDLEFUNC
typedef void (*SLICE)(void);

// ���������� ���� ������ ������� KB_ON
typedef void (*POWER)(void);

void InitKeyboard(void);

// ��� ���������� ����������
// ������, ���� ���������� IRQ
void StartKeyboard(void);
// ���� ���������� ����������
void StopKeyboard(void);

//**********************************************
// ���������� ��� ������� � ������� �����
//----------------------------------------------
//  Result - ��� �������
//  		 KB_NO - ���
//**********************************************
KEY ReadKey(void);

//**********************************************
// ���� ������� ���������� ��� ������� � ������� �����
//----------------------------------------------
//  Result - ��� �������
//
//**********************************************
KEY WaitReadKey(void);
KEY WaitReadKeyWithDelay(float DelaySec,KEY KeyAtTime);

// � ������� ����� - ����� �������
KEY ReadKey2(void);
KEY WaitReadKey2(void);



// ���� �� ���� ��������� ����� �������, ������� IdleFunc
// ���������� KB_IDLE, ���� ������� ���-�� �������
KEY ReadKeyIdle(IDLEFUNC IdleFunc);

// 1 - ������ ���-��
char KeyPressed(void);

// �������� ��� �������
void ClearKeyboard(void);


void PutKey(KEY ch); // �������� ������ � �������


extern SLICE Slice;
extern POWER Power;
extern KEY LastKey; // ��������� ������� �������

void AutoRepeatEnable(void);
void AutoRepeatDisable(void);

//void PowerOffEnable(void);
//void PowerOffDisable(void);

//void BeepEnable(void);
//void BeepDisable(void);

// Val - ������
void SetLightOffVal(int Val);
// 0 - ���� � ���; 1 - ��� �� ����� �������
void SetLightOnMode(int Val);
// Val - ������
void SetPowerOffVal(int Val);


 // ������ ������� 32 ��� � ���. ������ ����� ���������������
unsigned int GetTic32(void);

// �������, ��� ���� ������������, �� �� ����� ���������
void NeedRedraw(void);
// ����������� ��� �� �����������
int RedrawProcessed(void);


#ifdef __emulator
extern char TryClose;
#define BreakCode if (TryClose) break;
#else
#define BreakCode
#endif


#ifndef __emulator
extern volatile char KeysPressed[];
extern volatile unsigned short KeysPressedTik[];
extern volatile char KeysByUp[];

// �������� �������� �� �������
// ���� (IsShift[i]=1), �� ���� KeysByUp[i]=1;
extern volatile char IsShift[];
#endif

void TestKeyboard(void);

#endif
