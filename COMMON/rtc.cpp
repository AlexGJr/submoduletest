// DS1339 ����� TWI
#include "rtc.h"
#include "BoardAdd.h"
#include <string.h>

#include "Error.h"

#include "SysUtil.h"

#ifndef __emulator

static void ReInitRTC(void)
{
AT91F_PIO_MultiDriverDisable(AT91C_BASE_PIOA, AT91C_PA3_TWD | AT91C_PA4_TWCK);
AT91F_PIO_CfgOutput(AT91C_BASE_PIOA,AT91C_PA3_TWD | AT91C_PA4_TWCK);

AT91F_PIO_SetOutput(AT91C_BASE_PIOA,AT91C_PA3_TWD | AT91C_PA4_TWCK);
DelayMks(10);
AT91F_PIO_ClearOutput(AT91C_BASE_PIOA,AT91C_PA4_TWCK);
DelayMks(10);
AT91F_PIO_SetOutput(AT91C_BASE_PIOA,AT91C_PA4_TWCK);

AT91F_PIO_Disable(AT91C_BASE_PIOA,AT91C_PA3_TWD | AT91C_PA4_TWCK);
AT91F_PIO_A_RegisterSelection(AT91C_BASE_PIOA,AT91C_PA3_TWD | AT91C_PA4_TWCK);
AT91F_PIO_MultiDriverEnable(AT91C_BASE_PIOA,AT91C_PA3_TWD | AT91C_PA4_TWCK);
}


void InitRTC(void)
{
/*
// Configure TWI PIOs
AT91F_TWI_CfgPIO();
AT91F_PIO_CfgOpendrain(AT91C_BASE_PIOA, (unsigned int) AT91C_PA3_TWD);
// Configure PMC by enabling TWI clock
AT91F_TWI_CfgPMC();
// Configure TWI in master mode
AT91F_TWI_Configure ( AT91C_BASE_TWI);
// Set TWI Clock Waveform Generator Register
AT91C_BASE_TWI->TWI_CWGR  = 128 | (64 << 8); // 358.2 ���
*/

ReInitRTC();

// Configure TWI PIOs
AT91F_TWI_CfgPIO();

AT91F_PIO_CfgOpendrain(AT91C_BASE_PIOA, (unsigned int) AT91C_PA3_TWD);
// Configure PMC by enabling TWI clock
AT91F_TWI_CfgPMC();
// Configure TWI in master mode
AT91F_TWI_Configure ( AT91C_BASE_TWI);
// Set TWI Clock Waveform Generator Register
//AT91C_BASE_TWI->TWI_CWGR  = 64 | (64 << 8);
//AT91C_BASE_TWI->TWI_CWGR  = 120 | (120 << 8) | (2<<16); // 100.0 ���
//AT91C_BASE_TWI->TWI_CWGR  = 29 | (29 << 8) | (2<<16);  //acm, 11-5-10, change to within spec, was 49khz, to 200khz, but saw 6 errors over 2 days running, freeze count too small?
AT91C_BASE_TWI->TWI_CWGR  = 15 | (15 << 8) | (2<<16); //375khz
//AT91C_BASE_TWI->TWI_CWGR  = 59 | (59 << 8) | (2<<16); // 100khz

//AT91C_BASE_TWI->TWI_CR = AT91C_TWI_MSEN;         // enable Mastermode

}


void WriteTWI(int Addr, char* Data, int Len)
{
	unsigned int status, i;

AT91F_disable_interrupt();

    // Set the TWI Master Mode Register
	AT91C_BASE_TWI->TWI_MMR = ( (0x68<<16) | AT91C_TWI_IADRSZ_1_BYTE ) & ~AT91C_TWI_MREAD;	
	
	// Set TWI Internal Address Register
	AT91C_BASE_TWI->TWI_IADR = Addr;

	status = AT91C_BASE_TWI->TWI_SR;
		
	// Send first byte
	AT91C_BASE_TWI->TWI_THR = *(Data++);
	
	AT91C_BASE_TWI->TWI_CR = AT91C_TWI_START | AT91C_TWI_MSEN;  // acm, 5-3-13, just noticed in datasheet flow diagram, START is automatic (for write, not read)!  Original datasheet this not case, should rewrite & test someday.
		
	while (Len-- >1){
		i=0x1FFF;
		// Wait THR Holding register to be empty
		while (!(AT91C_BASE_TWI->TWI_SR & AT91C_TWI_TXRDY)){
			if ((--i)==0){
				Error|=(unsigned long)(1<<erClock_rd_TWI);
				break;	//2.06 just to exit infinite loop
			} 	
        }
	
		AT91C_BASE_TWI->TWI_THR = *(Data++);
	}

	AT91C_BASE_TWI->TWI_CR = AT91C_TWI_STOP | AT91C_TWI_MSEN;		

	status = AT91C_BASE_TWI->TWI_SR;
    status = status;
	i=0x1FFF;
	// Wait transfer is finished
    while (!(AT91C_BASE_TWI->TWI_SR & AT91C_TWI_TXCOMP)) {
		if ((--i)==0){
			Error|=(unsigned long)(1<<erClock_rd_TWI);
			break;	//2.06 just to exit infinite loop
		}
	}
	
AT91F_enable_interrupt();
}


int ReadTWI(int Addr, char* Data, int Len)
{
	unsigned int status_ok=0, status, ovfl=0, i;

	//AT91F_disable_interrupt();  // disable IRQ guarantees atomic, but increases system IRQ latency.
	// Note, DS1339 loads time into buffer at beginning of I2C xfer, so multi-byte xfer required to avoid clock roll-over issues 
    //InitRTC();  // acm, this just needs to be done once during boot.  Fair bit of over head.
	
	AT91C_BASE_TWI->TWI_CR = AT91C_TWI_MSEN | ((unsigned int) 0x1 <<  5);  // acm, per datasheet disable slave mode bit too
	
	// Set the TWI Master Mode Register
	AT91C_BASE_TWI->TWI_MMR = (0x68<<16) | AT91C_TWI_IADRSZ_1_BYTE | AT91C_TWI_MREAD;	// acm, 1 byte internal address, RTC intrinsic address == 0x68
	
	// Set TWI Internal Address Register
	AT91C_BASE_TWI->TWI_IADR = Addr;
	
	// Start transfer
	AT91C_BASE_TWI->TWI_CR = AT91C_TWI_START ;
		
	while (Len-- >1)
	{
		// Wait RHR Holding register is full
        i=0x1FFF;
		status = AT91C_BASE_TWI->TWI_SR; ovfl|=(status & AT91C_TWI_OVRE);
        while (!(status & AT91C_TWI_RXRDY)) {
			status = AT91C_BASE_TWI->TWI_SR; ovfl|=(status & AT91C_TWI_OVRE);
            if ((--i)==0) {
              status_ok=1;
              break;
            }
        }

		// Read byte
		*(Data++) = AT91C_BASE_TWI->TWI_RHR;
	}
	
	AT91C_BASE_TWI->TWI_CR = AT91C_TWI_STOP;

	i=0x1FFF;
	status = AT91C_BASE_TWI->TWI_SR; ovfl|=(status & AT91C_TWI_OVRE);
	while (!(status & AT91C_TWI_RXRDY)) {
		status = AT91C_BASE_TWI->TWI_SR; ovfl|=(status & AT91C_TWI_OVRE);
		if ((--i)==0) {
			status_ok=2;
			break;
		}
	}
	
	// Read byte
	*(Data++) = AT91C_BASE_TWI->TWI_RHR;
		
	// Wait transfer is finished
	i=0x1FFF;
	status = AT91C_BASE_TWI->TWI_SR; ovfl|=(status & AT91C_TWI_OVRE);
	while (!(status & AT91C_TWI_TXCOMP)) {
		status = AT91C_BASE_TWI->TWI_SR; ovfl|=(status & AT91C_TWI_OVRE);
		if ((--i)==0) {
			status_ok=3;
			break;
		}
	}
	
	if (ovfl) status_ok=4;
	
return status_ok;
}

//*******************************************************
char SET_TIME(u32 year,u32 month,u32 day,u32 hour,u32 min,u32 sec)
{
char ch[7];
if (Error&(unsigned long)(1<<erClock_rd_TWI)){ //2.06 ag init if previous operation errored out
	InitRTC();
	Error&=~(unsigned long)(1<<erClock_rd_TWI);
}
//  -------- Sec ---------
ch[0x00] = (((sec/10)<<4)&0xF0) | ((sec%10)&0x0F);
//  -------- Min ---------
ch[0x01] = (((min/10)<<4)&0xF0) | ((min%10)&0x0F);
//  -------- Hour ---------
ch[0x02] = (((hour/10)<<4)&0x30) | ((hour%10)&0x0F);
//  -------- Day ---------
ch[0x04] = (((day/10)<<4)&0xF0) | ((day%10)&0x0F);
//  -------- Month ---------
ch[0x05] = (((month/10)<<4)&0x10) | ((month%10)&0x0F);
//  -------- Year ---------
ch[0x06] = (((year/10)<<4)&0xF0) | ((year%10)&0x0F);

//if (ch[0x06] == 0x00) ch[0x06] = 0x10; // 12-8 ACM debug code just to confirm whether FW is source of RTC year getting zeroed out, if this happens, writing to 0x10 will avoid the generation of a clock error

WriteTWI(0x00,ch,7);	//2.06 ag may set erClock_rd_TWI

return 1;
}

char ReadTWI_busy=0;	// acm, semaphore

//*******************************************************
#pragma optimize=s 2
char GET_TIME(u32 *year,u32 *month,u32 *day,u32 *hour,u32 *min,u32 *sec)  // acm, (now) only called by GET_TIME_INBUF
{
char ch[7],ch1[7];
//_TDateTime OldCurTime, CurTime; //2.06 ag commented // acm, v1.79 code review change from int
int timeout; //, timeout2;  // acm, timeouts used to set clock error bits

ReadTWI_busy=1;	// acm, v1.79, guarentee autonomous read, start.  Upon further review, no IRQ handler calls GET_TIME(), with only 1 TWI device, pending xfer only gets delayed short interval, so non-issue.

//  acm, don't see good rational for re-reading until any byte changes, asking for trouble.  Sync to second change not required.
//  we have 1 second to waste here, so put code that ensures good RTC read here.
//2.06 ag returned to original code added error check and additional InitRTC() on error
if (Error&(unsigned long)(1<<erClock_rd_TWI)){
	InitRTC();
	Error&=~(unsigned long)(1<<erClock_rd_TWI);
}

timeout=0xFF;

ReadTWI(0x00,ch,7); 	//2.06 ag using Allan's error handling
do {
  memmove(ch1,ch,7);
  ReadTWI(0x00,ch,7);
  
  if (timeout--==0) {
	  Error|=(unsigned long)(1<<erClock_rd_TWI);
	  
	  break;}	//2.06 adding to original code breaking the loop
	  
} while ((ch[0]!=ch1[0])||(ch[1]!=ch1[1])||(ch[2]!=ch1[2])||(ch[3]!=ch1[3])||(ch[4]!=ch1[4])||(ch[5]!=ch1[5])||(ch[6]!=ch1[6]));

if (Error&(unsigned long)(1<<erClock_rd_TWI)) {
	ReadTWI_busy=0; return 0;}
else {
	*sec=((ch[0x00]>>4)*10)+(ch[0x00]&0x0F);
	*min=((ch[0x01]>>4)*10)+(ch[0x01]&0x0F);
	*hour=(((ch[0x02]&0x30)>>4)*10)+(ch[0x02]&0x0F);
	*day=((ch[0x04]>>4)*10)+(ch[0x04]&0x0F);
	*month=(((ch[0x05]&0x10)>>4)*10)+(ch[0x05]&0x0F);
	*year=((ch[0x06]>>4)*10)+(ch[0x06]&0x0F);
	ReadTWI_busy=0; 
	return 1;
	}
}

#else

#include <vcl\vcl.h>
#include "rtc.h"


char GET_TIME(u32 *year,u32 *month,u32 *day,u32 *hour,u32 *min,u32 *sec)
{

  TDateTime DT;
  unsigned short Year,Month,Day,Hour,Min,Sec,MSec;

  DT=TDateTime();
  DT=TDateTime::CurrentDateTime();
  DT.DecodeDate(&Year,&Month,&Day);
  DT.DecodeTime(&Hour,&Min,&Sec,&MSec);
  *year  = Year;
  *month = Month;
  *day   = Day;
  *hour  = Hour;
  *min   = Min;
  *sec   = Sec;

return 1;
}


char SET_TIME(u32 year,u32 month,u32 day,u32 hour,u32 min,u32 sec)
{
return 1;
}


void InitRTC(void)
{
}

#endif


void TestRTC(void)
{
u32 year,month,day,hour,min,sec;

InitRTC();
year=5;
month=6;
day=28;
hour=10;
min=33;
sec=0;
SET_TIME(year,month,day,hour,min,sec);

while (1) {
  GET_TIME(&year,&month,&day,&hour,&min,&sec);
  Delay(3000);
}
}

//*******************************************************
char RTC_STATUS()
{
/* DS1339 data sheet:
Bit 7: Oscillator Stop Flag (OSF). A logic 1 in this bit indicates that the oscillator either is stopped or was stopped
for some period of time and may be used to judge the validity of the clock and date data. This bit is edge triggered
and is set to logic 1 when the oscillator stops. The following are examples of conditions that can cause the OSF bit
to be set:
1) The first time power is applied.
2) The voltage on both VCC and VBACKUP are insufficient to support oscillation.
3) The EOSC bit is turned off.
4) External influences on the crystal (e.g., noise, leakage, etc.).
This bit remains at logic 1 until written to logic 0. This bit can only be written to a logic 0.
*/	
	char status[2]={0,0};  // acm, v2.00, fix uninitialized pointer situation
	
	if(ReadTWI(0x0f,status,2)==0)  // acm, v2.00, change LEN from 1 to 2.  ReadTWI() written such that min returned bytes ==2, i.e LEN >1 to work properly.  To read one byte, must simultaneously assert START/STOP.  
		return status[0];  // acm, return valid status (status[1] == trickle charg register?)

	return 0;  // acm, ReadTWI() returned >0, some sort of communication state error.  Resolution, return OSC bit status ok (mask nusiance error).
}

void clr_RTC_STATUS()
{
char status[2]={0,0};  // acm, v2.00, fix uninitialized pointer situation

	WriteTWI(0x0f,status,1);  // acm, in this case, LEN ==1 apparently works even though code intended to support >1 byte writes.  But logic doesn't match datasheet, START/STOP is now automatic, we don't need to drive it too.
	
	return ;
}

