#include	"BoardAdd.h"
#include	"LCD.h"


void ResetWatchDog(void) @ "USBCODE"
{
#ifndef __emulator
AT91C_BASE_WDTC->WDTC_WDCR = AT91C_WDTC_WDRSTT | AT91C_PASS;
#endif
}

// 16 �
void EnableWatchDog(void)
{
#ifndef __emulator
AT91C_BASE_WDTC->WDTC_WDMR = AT91C_WDTC_WDV | AT91C_WDTC_WDRSTEN | AT91C_WDTC_WDRPROC | AT91C_WDTC_WDD | AT91C_WDTC_WDDBGHLT; // acm, v2.01->  // acm, v2.00 Main adds | AT91C_WDTC_WDDBGHLT;, MAC file doesn't disable WDT, best of both worlds.
#endif
}


void DisableWatchDog(void) @ "USBCODE"
{
#ifndef __emulator
AT91C_BASE_WDTC->WDTC_WDMR = AT91C_WDTC_WDDIS;
#endif
}


#ifndef __emulator
__ramfunc
#endif
void Restart(void)
{
#ifndef __emulator
AT91C_BASE_RSTC->RSTC_RCR |= AT91C_RSTC_PROCRST | AT91C_RSTC_PERRST | AT91C_PASS;
//__asm("sub r0, r0,r0;");
__asm("movs r0, #0;");  // acm, change to work in Thumb mode
__asm("bx r0;");
#endif
}


void TestWatchDog(void)
{
PrintDebug("AAA");
Redraw();
  EnableWatchDog();
  while (1) {
    DelayMks(1000000);
    PrintDebug("1");
    Redraw();
    ResetWatchDog();
  }
}

