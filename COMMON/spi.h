#ifndef SPI_h
#define SPI_h

#include "BoardAdd.h"

#ifndef SAM7SE
#define SPI_CS0 AT91C_PA11_NPCS0
#define SPI_CS1 AT91C_PA9_NPCS1
#define SPI_CS2 AT91C_PA10_NPCS2
#define SPI_CS3 AT91C_PA22_NPCS3
#else
#define SPI_CS0 0
#define SPI_CS1 0
#define SPI_CS2 0
#define SPI_CS3 0
#endif

#define SPI_CSx(CS3,CS2,CS1,CS0) (CS0 | CS1 | CS2 | CS3)

#define SPI_CS_ALTERA   SPI_CSx(SPI_CS3,SPI_CS2,SPI_CS1,SPI_CS0) // 0000  Altera
#define SPI_CS_DAC1     SPI_CSx(SPI_CS3,SPI_CS2,SPI_CS1,0)       // 0001  DAC1
#define SPI_CS_DAC2     SPI_CSx(SPI_CS3,SPI_CS2,0,SPI_CS0)       // 0010  DAC2
#define SPI_CS_FRAM     SPI_CSx(0,0,SPI_CS1,SPI_CS0)             // 1100  Fram
#define SPI_CS_CRT      SPI_CSx(0,SPI_CS2,0,0)                   // 1011  CrossTalk
#define SPI_CS_CRT_R    SPI_CSx(0,SPI_CS2,0,SPI_CS0)             // 1010  ���������� CrossTalk

#if defined(__TTMon)
#define SPI_CS_DAC3     SPI_CSx(SPI_CS3,SPI_CS2,0,0)             // 0011  DAC3
#define SPI_CS_R1       SPI_CSx(SPI_CS3,0,SPI_CS1,0)             // 0101
#define SPI_CS_R2       SPI_CSx(SPI_CS3,0,0,0)                   // 0111
#define SPI_CS_TEMP     SPI_CSx(0,SPI_CS2,SPI_CS1,SPI_CS0)       // 1000  Temperature
#define SPI_CS_PHAZE    SPI_CSx(0,SPI_CS2,SPI_CS1,0)             // 1001  Phaze
#define SPI_CS_FLASH    SPI_CSx(0,0,SPI_CS1,0)                   // 1101  FLASH
#endif

#if defined(__PDMon)
#define SPI_CS_MOSI     SPI_CSx(SPI_CS3,0,SPI_CS1,SPI_CS0)       // 0100 MOSI ���
#endif

#if defined(__R1500_6)
#define SPI_CS_LDAC     SPI_CSx(SPI_CS3,SPI_CS2,0,0)             // 0011  LDAC
#define SPI_CS_MOSI     SPI_CSx(SPI_CS3,0,SPI_CS1,SPI_CS0)       // 0100  MOSI ���
#define SPI_CS_FLASH    SPI_CSx(0,0,SPI_CS1,0)                   // 1101  FLASH
#define SPI_CS_MAINADC  SPI_CSx(0,SPI_CS2,SPI_CS1,0)             // 1001  Ext ADC
#define SPI_CS_TEMP     SPI_CSx(0,SPI_CS2,SPI_CS1,SPI_CS0)       // 1000  ADC Temp
#define SPI_CS_R1       SPI_CSx(SPI_CS3,0,SPI_CS1,0)             // 0101  R1
#define SPI_CS_R2       SPI_CSx(SPI_CS3,0,0,SPI_CS0)             // 0110  R2
#define SPI_CS_R3       SPI_CSx(SPI_CS3,0,0,0)                   // 0111  R3
#define SPI_CS_CAN      SPI_CSx(0,0,0,0)                         // 1111  CAN
#endif

#if defined(__R1500)
#define SPI_CS_LDAC     SPI_CSx(SPI_CS3,SPI_CS2,0,0)             // 0011  LDAC
#define SPI_CS_FLASH    SPI_CSx(0,0,SPI_CS1,0)                   // 1101  FLASH
#define SPI_CS_MAINADC  SPI_CSx(0,SPI_CS2,SPI_CS1,0)             // 1001  Ext ADC
#define SPI_CS_TEMP     SPI_CSx(0,SPI_CS2,SPI_CS1,SPI_CS0)       // 1000  ADC Temp
#define SPI_CS_R1       SPI_CSx(SPI_CS3,0,SPI_CS1,0)             // 0101  R1
#define SPI_CS_R2       SPI_CSx(SPI_CS3,0,0,SPI_CS0)             // 0110  R2
#endif

#if defined(__TIM3)
#define SPI_CS_MOSI     SPI_CSx(SPI_CS3,0,SPI_CS1,SPI_CS0)       // 0100  MOSI ���
#endif

#if defined(TDM_V2)
#define SPI_CS_TEMP     SPI_CSx(0,SPI_CS2,SPI_CS1,SPI_CS0)       // 1000  ADC Temp
#endif

//((48000/kHz)<<8)

#define SPIFREQ_24000    0x200
#define SPIFREQ_16000    0x300
#define SPIFREQ_8000     0x600
#define SPIFREQ_4000     0xC00
#define SPIFREQ_2400    0x1400
#define SPIFREQ_2000    0x1800
#define SPIFREQ_1000    0x3000

extern volatile unsigned int SPIBusy;

void SPI_SetCS(unsigned int CS);
void SPI_ClearCS(void);
void SPI_Prepare(unsigned int kHz, unsigned int Mode);
// ������-������
char SPI_WriteRead(char Value);
// ������ ������. ����� ���������� ������� ������� SPI_Read()
void SPI_Write(char Value);
// ������ ������ �� ��������
char SPI_Read(void);


void TestSPI(void);

#endif
