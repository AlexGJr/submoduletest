// ---------------------------------------------------------
//   ATMEL Microcontroller Software Support  -  ROUSSET  -
// ---------------------------------------------------------
// The software is delivered "AS IS" without warranty or
// condition of any  kind, either express, implied or
// statutory. This includes without limitation any warranty
// or condition with respect to merchantability or fitness
// for any particular purpose, or against the infringements of
// intellectual property rights of others.
// ---------------------------------------------------------
//  File: at91SAM7S256_NoRemap.xlc
//
//  1.1 24/Feb/05 JPP    : Creation for 4.11A
//  1.x 13/Apr/05        : HEAP
//
//  $Revision: 1.1 $
//
// ---------------------------------------------------------

//*************************************************************************
// XLINK command file template for EWARM/ICCARM
//
// Usage:  xlink  -f lnkarm  <your_object_file(s)>
//                -s <program start label>  <C/C++ runtime library>
//
// $Revision: 1.1 $
//*************************************************************************

//************************************************
// Inform the linker about the CPU family used.
// AT91SAM7S256 Memory mapping
// No remap
//  ROMSTART
//  Start address 0x0000 0000
//  Size 256 Kbo  0x0004 0000
//  RAMSTART
//  Start address 0x0020 0000
//  Size  64 Kbo  0x0001 0000
// Remap done
//  RAMSTART
//  Start address 0x0000 0000
//  Size  64 Kbo  0x0001 0000
//  ROMSTART
//  Start address 0x0010 0000
//  Size 256 Kbo  0x0004 0000

//************************************************
-carm

//*************************************************************************
// Internal Ram segments mapped AFTER REMAP 64 K.
//*************************************************************************
// Use these addresses for the .
// Use these addresses for the .
-Z(CONST)INTRAMSTART_REMAP=00200000
-Z(CONST)INTRAMEND_REMAP=0020FFFF

//*************************************************************************
// Read-only segments mapped to Flash 256 K.
//*************************************************************************
-DROMSTART=00002000
-DROMEND=0003FFFF
//*************************************************************************
// Read/write segments mapped to 64 K RAM.
//*************************************************************************
-DRAMSTART=00200000
-DRAMEND=0020FFFF

//************************************************
// Address range for reset and exception
// vectors (INTVEC).
// The vector area is 32 bytes,
// an additional 32 bytes is allocated for the
// constant table used by ldr PC in cstartup.s79.
//************************************************
-Z(CODE)INTVEC=00-3F

//************************************************
// Startup code and exception routines (ICODE).
//************************************************
// 5-1-13 acm, steal 0x10 more from seg ICODE to make more room for CODE_ID, reasonable because we dont do software IRQs SWITAB
//-Z(CODE)ICODE,DIFUNCT,IDATA=0-0000038F
//-Z(CODE)SWITAB=0-0000038F
-Z(CODE)ICODE,DIFUNCT,IDATA=0-0000037F
-Z(CODE)SWITAB=0-0000037F

// 3-16-09, figured out system architecture/strategy to program FW:  USB code resides in first 0x2000 region, pcloader starts with 2nd block, leaving USB code unchanged.  This is required because code executing FW update cannot change.  To remain compatible with units in field, must maintain this strategy, i.e. cannot grow USB code segment.

//-Z(CODE)USBCODE=00000390-00001FFB
//-Z(CONST)USBCONST,INITTAB,CODE_ID=00000390-00001FFB
-Z(CODE)USBCODE=00000380-00001FFB
-Z(CONST)USBCONST,INITTAB,CODE_ID=00000380-00001FFB

-Z(DATA)USBDATA=00200000-0020004FF

//************************************************
// Code segments may be placed anywhere.
//************************************************
-Z(CODE)CODE=ROMSTART-ROMEND

//************************************************
// __ramfunc code copied to and executed from RAM.
//************************************************
-Z(DATA)CODE_I=RAMSTART-RAMEND
-QCODE_I=CODE_ID

//************************************************
// Various constants and initializers.
//************************************************
-Z(CONST)DATA_ID,DATA_C=ROMSTART-ROMEND
-Z(CONST)CHECKSUM=ROMSTART-ROMEND

//************************************************
// Data segments.
//************************************************
-Z(DATA)DATA_I,DATA_Z,DATA_N=RAMSTART-RAMEND

//************************************************
// ICCARM produces code for __ramfunc functions in
// CODE_I segments. The -Q XLINK command line
// option redirects XLINK to emit the code in the
// debug information associated with the CODE_I
// segment, where the code will execute.
//************************************************

//*************************************************************************
// Stack and heap segments.
//*************************************************************************

// acm, 3-4 increase size to fix USB FW upload hang, auto variable array requies 8k, 0x2000

-D_CSTACK_SIZE=(2000+100*4)
-D_IRQ_STACK_SIZE=(3*8*4)
-D_HEAP_SIZE=(450*2)

-Z(DATA)CSTACK+_CSTACK_SIZE=RAMSTART-RAMEND
-Z(DATA)IRQ_STACK+_IRQ_STACK_SIZE=RAMSTART-RAMEND
-Z(DATA)HEAP+_HEAP_SIZE=RAMSTART-RAMEND

//*************************************************************************
// ELF/DWARF support.
//
// Uncomment the line "-Felf" below to generate ELF/DWARF output.
// Available format specifiers are:
//
//   "-yn": Suppress DWARF debug output
//   "-yp": Multiple ELF program sections
//   "-yas": Format suitable for debuggers from ARM Ltd (also sets -p flag)
//
// "-Felf" and the format specifiers can also be supplied directly as
// command line options, or selected from the Xlink Output tab in the
// IAR Embedded Workbench.
//*************************************************************************

// -Felf
