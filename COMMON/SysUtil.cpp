
#include "DateTime.h"
#include "rtc.h"
#include "SysUtil.h"
#include <stdio.h>
#include "Defs.h"
#include "Error.h"
#include "trstr.h" // acm, need for fw_build_date
#include "Setup.h"
#include "LCD.h"

/*
#include "Graph.h"
#include "KeyBoard.h"
#include "LCD.h"
#include "Defs.h"
*/
extern struct StDateTime DateTime;
extern char fresh_RTC; // acm, defined in main(), set starts 30 min window when RTC update allowed
extern int stale_RTC; // acm, defined in main(), used to implement 30 min window
extern _TDateTime EndMeasurementTime;
extern unsigned int MeasurementsInArchive;

extern char ReadTWI_busy;	// acm, v1.79 defined semaphore

/* The MonthDays array can be used to quickly find the number of
  days in a month:  MonthDays[IsLeapYear(Y), M]      */

ROM char MonthDays[2][12]=
    {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31,
     31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

void GET_TIME_INBUF(void)
{
u32 year;
u32 month;
u32 day;
u32 hour;
u32 min;
u32 sec;

if(!ReadTWI_busy) // acm, 3-21-12 v1.79.  Code isn't re-entrant, if ReadTWI in progress (precaution, unlikely, unless I missed something), don't read again
	if(GET_TIME(&year,&month,&day,&hour,&min,&sec)) // acm, 2-20-12, don't clobber DateTime if GET_TIME returns error
	{
		DateTime.Year=year;
		DateTime.Month=month;
		DateTime.Day=day;
		DateTime.Hour=hour;
		DateTime.Min=min;
		DateTime.Sec=sec;
	}
}

_TDateTime new_time;  // acm, u32.  Using int causes odd problem?


char SET_TIME_FROMBUF(void)
{
u32 year=DateTime.Year;
u32 month=DateTime.Month;
u32 day=DateTime.Day;
u32 hour=DateTime.Hour;
u32 min=DateTime.Min;
u32 sec=DateTime.Sec;

new_time = DoPackDateTime(EncodeDate(DateTime.Year%100+2000,DateTime.Month,DateTime.Day),
                          EncodeTime(DateTime.Hour,DateTime.Min,  DateTime.Sec));

// acm, always ignore time set to before fw build date
// acm, ignore RTC change request ONCE if measurements in archive and new time in past
//      idea is to filter out a little possibility of false RTC change that may result in loss of stored measurements
if (new_time<DoPackDateTime(EncodeDate(fw_build_date[5]%100+2000,fw_build_date[4],fw_build_date[3]),EncodeTime(fw_build_date[2],fw_build_date[1],  0)))
		{			
			DateTimeNowNoSec();			// acm, refresh DateTime.xxx structure
			return (0);				// acm, sendanswer() will say error back to user
		}
	
if ((new_time<EndMeasurementTime) && !fresh_RTC && MeasurementsInArchive) 
{
	Error|=(unsigned long)(1<<erClock);	// 2-17-11 set error to flash LED, caution USER
	LED1Yellow; //acm, 2-27-12, add more pizzaz?
	DateTimeNowNoSec();			// acm, refresh DateTime.xxx structure
	stale_RTC=GetTic32();		// acm, start timer now, use sftw timer incase RTC gets set to past...
	fresh_RTC=1;				// acm, set flag, 30 min window allowing user to set time to past
	return (0);				// acm, sendanswer() will say error back to user
}

//LoadLastMeasurement(); // acm, refresh EndMeasurementTime(?)-> redundant
		
SET_TIME(year,month,day,hour,min,sec);
Error&=~(unsigned long)(1<<erClock);				// 11-16-08 cast as long, ensure erClock bit cleared
//Error&=~(unsigned long)(1<<erClock_rd_TWI);	//2.06 ad comment out. Only set or cleared in rtc.cpp
//Error&=~(unsigned long)(1<<erClock2); 		// ag 2-24-16 comment this allowing erClock2 to exist and modifying in main() 1196 not to blink LED
LED1YOFF;	// acm, 2-28-12, change from allledoff, to avoid clearing red alarm...
clr_RTC_STATUS(); // acm, clear OSF bit, v2.00

return (1); 										// acm, set time finished ok
}

//----- DivMod -----
void DivMod(u32 Dividend, u32 Divisor, u32 *Result, u32 *Remainder)
{
*Result    = Dividend/Divisor;
*Remainder = Dividend%Divisor;
}

//----- IsLeapYear -----
char IsLeapYear(unsigned int Year)
{
return ((Year % 4 == 0) && ((Year % 100 != 0) || (Year % 400 == 0)));
}

//----- IsDate -----
char IsDate(unsigned int Year, unsigned int Month, unsigned int Day)
{
char ROM *DayTable;
DayTable = (char ROM *)&MonthDays[IsLeapYear(Year)];
if ((Year >= 1) && (Year <= 9999) && (Month >= 1) && (Month <= 12) &&
    (Day >= 1) && (Day <= DayTable[Month-1])) return 1;
else return 0;
}

//----- IsTime -----
char IsTime(unsigned int Hour,unsigned int Min,unsigned int Sec)
{
if ( (Hour<24) && (Min < 60) && (Sec < 60) ) {return 1;}
return 0;
}
//---------------------------------------------
_TDate EncodeDate(unsigned int Year, unsigned int Month, unsigned int Day)
{
char j;
long i;
_TDate Result=0;
char ROM *DayTable;

Year=Year%100+2000;

DayTable = (char ROM *)&MonthDays[IsLeapYear(Year)];

if (IsDate(Year,Month,Day)==1)
  {
  for (j=1;j<=Month-1;j++) {Day+=DayTable[j-1];}
  i = Year - 1;

  Result = (long)i*365 + (long)i/4 - (long)i/100 + (long)i/400 + Day - _TDateDelta;
  }
return Result;
}

void DecodeDate(_TDate Date,unsigned int *Year,unsigned int *Month,unsigned int *Day)
{
u32
  D1 = 365,
  D4 = D1 * 4 + 1,
  D100 = D4 * 25 - 1,
  D400 = D100 * 4 + 1;
u32
  Y, M, D, I;
u32  T;
char ROM *DayTable;

  T = Date+_TDateDelta;

  if (T == 0)
    {
    *Year = 0;
    *Month = 0;
    *Day = 0;
    }
  else
    {
    T--;
    Y = 1;
    while (T >= D400)
      {
      T-=D400;
      Y+=400;
      }
    DivMod(T, D100, &I, &D);
    if (I == 4)
      {
      I--;
      D+=D100;
      }
    Y += (I * 100);
    DivMod(D, D4, &I, &D);
    Y += (I * 4);
    DivMod(D, D1, &I, &D);
    if (I == 4)
      {
      I--;
      D += D1;
      }
    Y+=I;
    DayTable = (char ROM *)&MonthDays[IsLeapYear(Y)];
    M = 1;
    while (1>0)
      {
      I = DayTable[M-1];
      if (D < I) break;
      D -= I;
      M++;
      }
    if (Year!=0) *Year = Y;
    if (Month!=0) *Month = M;
    if (Day!=0) *Day = D + 1;
    }
}

_TTime EncodeTime(unsigned int Hour, unsigned int Min, unsigned int Sec)
{
_TTime result=0;

if (IsTime(Hour,Min,Sec)==1)
  {
  result = ((_TTime)Hour * 60*60) + ((_TTime)Min * 60) + (_TTime)Sec;
  }
return  result;
}

void DecodeTime(_TTime Time,unsigned int *Hour,unsigned int *Min,unsigned int *Sec)
{
u32 _hour,_min,_sec;
DivMod(Time, 60, &_min, &_sec);
DivMod(_min, 60, &_hour, &_min);
if (Hour!=0) *Hour=_hour;
if (Min!=0) *Min=_min;
if (Sec!=0) *Sec=_sec;
}


_TDateTime DoPackDateTime(_TDate Date, _TTime Time)
{
return ((Date*_TSecPerDay)+Time);
}

void DoUnPackDateTime(_TDateTime DateTime,_TDate *Date, _TTime *Time)
{
DivMod(DateTime,_TSecPerDay,(u32 *)Date,(u32 *)Time);
}



void DateTimeNow(_TDateTime *Date)
{
#ifdef R1700Mega1
#ifdef __arm9
  struct StDateTime DateTime;
  GET_TIME(&DateTime);
#else
  GET_TIME();
#endif
(*Date)=DoPackDateTime( EncodeDate(2000+(DateTime.Year%100),DateTime.Month,DateTime.Day),
                        EncodeTime(DateTime.Hour,DateTime.Min,DateTime.Sec));
#endif

// 2-17-12 found bug, this equate is wrong, supposed to be __arm.  Side-effect minimal?, as DateTimeNow() only called by FileCreate.  Theory:  saved times a bit off
//#ifdef __arm7
#ifdef __arm

  GET_TIME_INBUF();
  (*Date)=DoPackDateTime( EncodeDate(2000+(DateTime.Year%100),DateTime.Month,DateTime.Day),
                          EncodeTime(DateTime.Hour,DateTime.Min,DateTime.Sec));
#endif
}

_TDateTime DateTimeNowNoSec(void)
{
 _TDateTime Result;

GET_TIME_INBUF();
Result=DoPackDateTime( EncodeDate((_TDate)2000+(DateTime.Year%100),DateTime.Month,DateTime.Day),
                       EncodeTime(DateTime.Hour,DateTime.Min,0));

return Result;
}

struct StDateTime UnpackDateTime(_TDateTime DateTime)
{
struct StDateTime t;
_TDate Date;
_TTime Time;
unsigned int Hour,Min,Sec, Year,Month,Day;
DoUnPackDateTime(DateTime,&Date,&Time);
DecodeTime(Time,&Hour,&Min,&Sec);
DecodeDate(Date,&Year,&Month,&Day);
t.Sec   = Sec;
t.Min   = Min;
t.Hour  = Hour;
t.Year  = Year;
t.Month = Month;
t.Day   = Day;
return t;
}

void AddDateTime(_TDateTime* DT, int Sec)
{
_TDate Date;
_TTime Time;

DoUnPackDateTime((*DT),&Date,&Time);
Time += Sec;
if (Time>_TSecPerDay)
  {
  Date += (Time/_TSecPerDay);
  Time  = (Time%_TSecPerDay);
  }
(*DT)=DoPackDateTime(Date,Time);
}

char DateTimeDiapason(_TDateTime dbeg, _TDateTime dend, _TDateTime dcur)
{
return (dbeg<=dcur) ? ((dend>=dcur) ? 1 : 0) : 0;
}

float Between(float DT0, float DTN, float DT)
{
if (DT0==DTN) return 1.0;
if (DT>=DTN) return 1.0;
if (DT<=DT0) return 0.0;
return (DT-DT0)/(DTN-DT0);
}

float DateTimeBetween(_TDateTime DT0, _TDateTime DTN, _TDateTime DT)
{
if (DT0==DTN) return 1.0;
if (DT>=DTN) return 1.0;
if (DT<=DT0) return 0.0;
return (float)(DT-DT0)/(float)(DTN-DT0);
}

void DateTimeToStr(_TDateTime DT, char *Str)
{
unsigned int _hour,_min,_sec, Year,Month,Day;
_TDate Date;
_TTime Time;

DoUnPackDateTime(DT,&Date,&Time);
DecodeDate(Date,&Year,&Month,&Day);
DecodeTime(Time,&_hour,&_min,&_sec);
sprintf(Str,"%02d.%02d.%04d %02d:%02d:%02d",Day,Month,Year,_hour,_min,_sec);
}


void TimeToStr(_TDateTime DT, char *Str)
{
unsigned int _hour,_min,_sec;
_TDate Date;
_TTime Time;
DoUnPackDateTime(DT,&Date,&Time);
DecodeTime(Time,&_hour,&_min,&_sec);
sprintf(Str,"%02d:%02d:%02d",_hour,_min,_sec);
}


#ifndef __emulator
TCRC CalcCRC(void *Buf,u32 Len) @ "USBCODE"
#else
TCRC CalcCRC(void *Buf,u32 Len)
#endif
{
register unsigned int i;
register TCRC CRC,c,Dop;

CRC=0xAAAA;
for(i=0;i<Len;i++) {
    Dop=((CRC & 0x8000)!=0)?1:0;
	CRC=((CRC & 0x7FFF)<<1) | Dop;
	c=(TCRC)((char*)Buf)[i];
	CRC^=c;
}
return CRC;
}


#ifndef __emulator
int CheckCRC(void *Buf,u16 Len,TCRC *aCRC) @ "USBCODE"
#else
int CheckCRC(void *Buf,u16 Len,TCRC *aCRC)
#endif
{
register TCRC CRC, fl;

CRC=*aCRC;
*aCRC=0;
fl=(CRC==CalcCRC(Buf,Len));
*aCRC=CRC;
return fl;
}


void SetCRC(void *Buf,unsigned short Len,TCRC *aCRC)
{
*aCRC=0;
*aCRC=CalcCRC(Buf,Len);
}

//᫮���? TDateTime
_TDateTime AddTime(_TDateTime DateTime1,_TTime Time, char sync)
{
_TTime AddTime1;
_TDate AddDate1;
_TDateTime DateTime;

DoUnPackDateTime(DateTime1, &AddDate1, &AddTime1);

AddTime1+=Time;
//if (AddTime1>=SecInDay)
if (AddTime1>=_TSecPerDay)
  {
   AddDate1+=(AddTime1/_TSecPerDay);
   if (sync) AddTime1=0;		//ag 4-16-16 This syncs interval measurements to midnight at midnight
   else AddTime1=(AddTime1%_TSecPerDay);
  }

DateTime=DoPackDateTime(AddDate1, AddTime1);

return DateTime;
}

void AddTimeToDateTime(_TDateTime* DT, _TTime AddT)
{
_TDate Date;
_TTime Time;
_TDate Date2;
_TTime Time2;

DoUnPackDateTime((*DT),&Date,&Time);
DoUnPackDateTime(AddT,&Date2,&Time2);
Time+=Time2;
if (Time>_TSecPerDay)
  {
  Date += (Time/_TSecPerDay);
  Time  = (Time%_TSecPerDay);
  }
(*DT)=DoPackDateTime(Date,Time);
}

_TDate DateTimeToDate(_TDateTime DateTime)
{ _TDateTime Date,Time;
DivMod(DateTime,_TSecPerDay,&Date,&Time);
return Date;
}

_TTime DateTimeToTime(_TDateTime DateTime)
{ _TDateTime Date,Time;
DivMod(DateTime,_TSecPerDay,&Date,&Time);
return Time;
}

_TDateTime TimeToDateTime(_TTime Time)
{
  return Time%_TSecPerDay;
}

_TDateTime DateToDateTime(_TDate Date)
{
 return DoPackDateTime(Date,0);
}
