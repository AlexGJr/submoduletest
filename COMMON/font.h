#ifndef _font_h
#define _font_h

#include "protocol.h"
#include "sysutil.h"
#include "typesdef.h"

#define     Font6x5              1
#define     Font8x6              2
#define     Font8x8              3
#define     Font8x16             4
#define     Font12x16            5
#define     Font16x16            6

extern char CurFontWidth; //������ �������� �����
extern char CurFontHeight;  //������  �������� �����
extern char CurFontPtrNumer;

void OutCharAND(u16 X,u16 Y,char S,u16 Mask);
void SetFont(char FontType);

char CurFontPtr(u16 i1,u16 i2);
u16 strlenwidth(char *Str);



#endif
