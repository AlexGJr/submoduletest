#include "BoardAdd.h"
#include "FreqMeasure.h"
#include "Timer.h"
#include "IRQ.h"
#include "DAC.h"
#include "SPI.h"
#include "LCD.h"
#include "Graph.h"
#include "keyboard.h"
#include "debug.h"
#include <stdio.h>




/*
AT91PS_TC AT91C_BASE_TCx[3]={AT91C_BASE_TC0,AT91C_BASE_TC1,AT91C_BASE_TC2};

// Timer=0..2, 5MHz-1Hz
// � LineOut ��������� !!!
void PrepareWaveTimer(int Timer, int LineOut, float *Freq)
{
unsigned int clk;
unsigned int f;

// First, enable the clock of the TIMER
if (Timer==0) AT91F_PMC_EnablePeriphClock ( AT91C_BASE_PMC, 1<< AT91C_ID_TC0 ) ;
else if (Timer==1) AT91F_PMC_EnablePeriphClock ( AT91C_BASE_PMC, 1<< AT91C_ID_TC1 ) ;
else if (Timer==2) AT91F_PMC_EnablePeriphClock ( AT91C_BASE_PMC, 1<< AT91C_ID_TC2 ) ;

// Disable the clock and the interrupts
AT91C_BASE_TCx[Timer]->TC_CCR = AT91C_TC_CLKDIS ;
AT91C_BASE_TCx[Timer]->TC_IDR = 0xFFFFFFFF ;

// Clear status bit
clk = AT91C_BASE_TCx[Timer]->TC_SR;


if (*Freq>1000.0) {
	if (*Freq>300000.0) {
		f=1+((unsigned int)(MCK/2.0/(*Freq)/2.0))&0xFFFF;
                if (f<1) f=1;
		*Freq=(float)MCK/2.0/(((float)f-1)*2.0);
                clk=TC_CLKS_MCK2;
	} else {
		f=1+((unsigned int)(MCK/32.0/(*Freq)/2.0))&0xFFFF;
                if (f<1) f=1;
		*Freq=(float)MCK/32.0/(((float)f-1)*2.0);
                clk=TC_CLKS_MCK32;
	}
} else {
	// 1000 - 1 ��
	f=1+((unsigned int)(MCK/1024.0/(*Freq)/2.0))&0xFFFF;
        if (f<1) f=1;
	*Freq=(float)MCK/1024.0/(((float)f-1)*2.0);
        clk=TC_CLKS_MCK1024;
}


AT91C_BASE_TCx[Timer]->TC_RA = f;
AT91C_BASE_TCx[Timer]->TC_RC = f*2;

// Set the Mode of the Timer Counter
AT91C_BASE_TCx[Timer]->TC_CMR = clk | AT91C_TC_WAVE | AT91C_TC_WAVESEL_UP_AUTO | AT91C_TC_ACPA_SET | AT91C_TC_ACPC_CLEAR;

}


void StartWaveTimer(int Timer)
{
// Enable and Trigger the clock
AT91C_BASE_TCx[Timer]->TC_CCR = AT91C_TC_SWTRG | AT91C_TC_CLKEN;
}


void StopWaveTimer(int Timer)
{
// Disable the clock
AT91C_BASE_TCx[Timer]->TC_CCR = AT91C_TC_CLKDIS;

}


void SetWaveFreq(float aFreq) // �������, ��   7 500 000 - 28.6
{
float Freq;

Freq=aFreq;
PrepareWaveTimer(0,1,&Freq);
StartWaveTimer(0);
WaitReadButton();
StopWaveTimer(0);
}
*/

volatile unsigned int count_timer_interrupt;
volatile unsigned int TimerA,TimerB;

#pragma diag_suppress=Pa082
void Timer0_IRQ_Handler(void)
{
volatile unsigned int dummy;
// Acknowledge interrupt status
dummy = AT91C_BASE_TC0->TC_SR;

count_timer_interrupt++;
if (count_timer_interrupt<50) {
  TimerA+=AT91C_BASE_TC0->TC_RA;
  TimerB+=AT91C_BASE_TC0->TC_RB;
} else
  count_timer_interrupt=TimerA=TimerB=0;

// Disable clock after measure
//AT91C_BASE_TC0->TC_CCR = AT91C_TC_CLKDIS ;
}


void PrepareCaptureTimer()
//void InitTimerCapture(AT91PS_TC	TimerBase,unsigned int TimerID,AT91PS_PIO PIO_Base, unsigned int PIN_TIOA )
{
volatile unsigned int dummy;

//* First, enable the clock of the TIMER
AT91F_PMC_EnablePeriphClock ( AT91C_BASE_PMC, 1<< AT91C_ID_TC0 ) ;

// Configure PIO controllers to periph mode
AT91F_PIO_CfgPeriph( AT91C_BASE_PIOA, 0, AT91C_PA0_TIOA0 | AT91C_PA1_TIOB0);

// Disable the clock and the interrupts
AT91C_BASE_TC0->TC_CCR = AT91C_TC_CLKDIS ;
AT91C_BASE_TC0->TC_IDR = 0xFFFFFFFF ;
dummy = AT91C_BASE_TC0->TC_SR ;

// Must be set CMR Before write in RB or RA
// Set the Mode of the Timer Counter
//AT91C_TC_LDBSTOP |
AT91C_BASE_TC0->TC_CMR = TC_CLKS_MCK32 | AT91C_TC_ETRGEDG_RISING | AT91C_TC_ABETRG | AT91C_TC_LDRA_FALLING | AT91C_TC_LDRB_RISING;

// open Timer interrupt
AT91F_AIC_ConfigureIt ( AT91C_BASE_AIC, AT91C_ID_TC0, TIMER_CAPTURE_LEVEL, AT91C_AIC_SRCTYPE_INT_EDGE_TRIGGERED, Timer0_IRQ_Handler);
AT91C_BASE_TC0->TC_IER = AT91C_TC_LDRBS;  //  IRQ enable CPC
	
}






void TestFreqMeasure(void)
{
char s[10];

PrepareCaptureTimer();

count_timer_interrupt=0;
TimerA=0;
TimerB=0;
AT91F_AIC_EnableIt (AT91C_BASE_AIC, AT91C_ID_TC0); // Enable Interrupt
StartWaveTimer(0);

while (1) {

//OutInt(0,0,AT91C_BASE_TC0->TC_RA,5);
//PrintDebugHex(AT91C_BASE_TC0->TC_RA);
if (count_timer_interrupt) {
  OutInt( 0,0,TimerA/count_timer_interrupt,5);
  OutInt( 0,8,TimerB/count_timer_interrupt,5);
  sprintf(s,"%7.4f",(float)MCK/32.0/(float)(TimerB/count_timer_interrupt));
  OutString( 32,8,s);
}
OutInt(32,0,count_timer_interrupt,5);
NeedRedraw();
//WaitReadKey();
}
}

