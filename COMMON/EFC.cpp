#include "BoardAdd.h"
#include "EFC.h"
#include "Defs.h"
#include "DefKB.h"
#include "LCD.h"
#include "Graph.h"
#include "KeyBoard.h"
#include "WatchDog.h"

#ifdef SAM7SE
#define MC_FSR MC0_FSR
#define MC_FMR MC0_FMR
#define MC_FCR MC0_FCR
#endif

__ramfunc void EFC_Init(void)
{
while (!(AT91C_BASE_MC->MC_FSR & AT91C_MC_FRDY)) ;
AT91C_BASE_MC->MC_FMR = ((AT91C_MC_FMCN)&(72 <<16)) | AT91C_MC_FWS_1FWS ;  // acm, change to same FMCN value used in Cstartup.cpp
while (!(AT91C_BASE_MC->MC_FSR & AT91C_MC_FRDY)) ;
}


// adr ���������� � 0x00100000
// pbuf �.�. �������� EFC_PAGE_SIZE
__ramfunc u32 EFC_WritePage(u32 adr, u32 *pbuf)
{
unsigned int *pflash;
unsigned int page;
unsigned int region;
unsigned int i, Retry;

Retry=8;

Again:
  
if ((--Retry)==0)
  return 0;

pflash = (unsigned int *)adr;
page = (adr & 0x3FFFF)/EFC_PAGE_SIZE;
region = (page/EFC_PagesInTheLockRegion);

EFC_Init();

if (AT91C_BASE_MC->MC_FSR & (region << 16)) {
  // lock set, clear it
  AT91C_BASE_MC->MC_FMR = ((AT91C_MC_FMCN)&(48 <<16)) | AT91C_MC_FWS_1FWS ;  // acm, 2-27 same change as inCstartup for reliability
  AT91C_BASE_MC->MC_FCR = (0x5A << 24) | (region << 8 ) |AT91C_MC_FCMD_UNLOCK;
}
while (!(AT91C_BASE_MC->MC_FSR & AT91C_MC_FRDY)) ;

for (i = 0; i < EFC_PAGE_SIZE_UINT; i++)
  *(pflash + i ) = *(pbuf + i);

AT91C_BASE_MC->MC_FCR = (0x5A << 24) | (page << 8 ) |AT91C_MC_FCMD_START_PROG;
while (!(AT91C_BASE_MC->MC_FSR & AT91C_MC_FRDY)) ;


// Verify
EFC_Init();
for (i = 0; i < EFC_PAGE_SIZE_UINT; i++)
  if ((*(pflash + i )) != (*(pbuf + i))) goto Again;

return 1;
}


__ramfunc char EFCWrite(u32 adr, u8 *pbuf, u32 Len, u32 NeedReset)
{
u32 l;
l=0;
while (l<Len) {
   if(!EFC_WritePage(adr, (u32*)&(pbuf[l]))) return 0;
   l+=EFC_PAGE_SIZE;
   adr+=EFC_PAGE_SIZE;
}
if (NeedReset) {
    Restart();
}
return 1;
}



// adr ���������� � 0x00100000
// pbuf �.�. �������� EFC_PAGE_SIZE
__ramfunc u32 EFC_ReadPage(u32 adr, u32 *pbuf)
{
unsigned int *pflash = (unsigned int *)adr;
unsigned int i;

EFC_Init();

for (i = 0; i < EFC_PAGE_SIZE_UINT; i++)
  *(pbuf + i) = *(pflash + i );

return 1;
}




void TestEFC(void)
{

unsigned int buf1[8192/4];
int i;

#define TestAddr 0x0000
unsigned int *pbuf3=(unsigned int *)TestAddr;

//AT91C_BASE_MC->MC_FCR = (0x5A << 24) | AT91C_MC_FCMD_ERASE_ALL;
//while (!(AT91C_BASE_MC->MC_FSR & AT91C_MC_FRDY)) ;

for(i=0;i<8192/4;i++)
   buf1[i]=i+9;
AT91F_disable_interrupt();
EFCWrite(0x100000 | TestAddr,(u8*)buf1,8192,0);
AT91F_enable_interrupt();

for( i=0;i<8192/4;i++)
    if (buf1[i]!=pbuf3[i])
        buf1[i]=0;

}
