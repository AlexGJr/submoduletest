#ifndef _SysUtils_h
#define _SysUtils_h

//#include "Defs.h"
#include "TypesDef.h"
#include "rtc.h"
//#include "ModBusTCP\ModBus.h"

//#define SecInDay ((long)3600*24/2)

char IsLeapYear(unsigned int Year);
char IsDate(unsigned int Year, unsigned int Month, unsigned int Day);
char IsTime(unsigned int Hour,unsigned int Min,unsigned int Sec);

//������� ��� ��������/���������� ���� � ������� � ��������� �� 2-� ������
_TDate EncodeDate(unsigned int Year, unsigned int Month, unsigned int Day);
_TTime EncodeTime(unsigned int Hour, unsigned int Min, unsigned int Sec);
void   DecodeDate(_TDate Date,unsigned int *Year,unsigned int *Month,unsigned int *Day);
void   DecodeTime(_TTime Time,unsigned int *Hour,unsigned int *Min,unsigned int *Sec);

_TDateTime DoPackDateTime(_TDate Date, _TTime Time);
void DoUnPackDateTime(_TDateTime DateTime,_TDateTime *Date, _TDateTime *Time);


void DateTimeNow(_TDateTime *Date);
_TDateTime DateTimeNowNoSec(void);

//struct StDateTime UnpackDateTime(_TDateTime DateTime);

void GET_TIME_INBUF(void);
char SET_TIME_FROMBUF(void);  // acm, 11-28 change from void to char


// Days between 1/1/0001 and 01/01/2000
#define  _TDateDelta (_TDate)730119
// ������ � ������
//#define _TSecPerDay  ((_TTime)24 * 60 * 60)
#define _TSecPerDay  ((_TTime)86400)


void AddDateTime(_TDateTime* DT, int Sec);
char DateTimeDiapason(_TDateTime dbeg, _TDateTime dend, _TDateTime dcur);


float Between(float DT0, float DTN, float DT);

// ��������� DT ����� DT0 � DT1
float DateTimeBetween(_TDateTime DT0, _TDateTime DTN, _TDateTime DT);
void DateTimeToStr(_TDateTime DT, char *Str);
void TimeToStr(_TDateTime DT, char *Str);

_TDateTime AddTime(_TDateTime DateTime1,_TTime Time,char sync);

TCRC CalcCRC(void *Buf,u32 Len);
int CheckCRC(void *Buf,u16 Len,TCRC *aCRC);
void SetCRC(void *Buf,u16 Len,TCRC *aCRC);
void AddTimeToDateTime(_TDateTime* DT, _TTime AddT);
_TDate DateTimeToDate(_TDateTime DateTime);
_TTime DateTimeToTime(_TDateTime DateTime);
_TDateTime TimeToDateTime(_TTime Time);
_TDateTime DateToDateTime(_TDate Date);

//void memcpypacked(void *dest, void *src, int len);
//void memsetpacked(void *buf,char b,int len);

#endif


