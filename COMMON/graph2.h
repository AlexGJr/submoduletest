#ifndef Graph2_h
#define Graph2_h

#include "protocol.h"
#include "scrlmngr.h"
#include "defkb.h"

#define scrWidth  ScreenRealWidth
#define scrHalf   (unsigned int)ScreenWidth/2
#define scrHeight ScreenHeight

char fStrLen(char ROM *s);
void fStrCpy(char *s1, char ROM *s2);
void fStrCat(char *s1, char ROM *s2);
void OutProgressBar(unsigned int X,unsigned int Y,unsigned int W,unsigned int H,char Percent);
void InternalMessage(char *Mes);
void Message(char ROM *Cap, char ROM *Mes, char ROM *Stat, char Rect);
unsigned int MessageBox3(char ROM *Cap, char ROM *Mes, char ROM *Stat, char Font);
unsigned int SmallMessageBox(char ROM *Caption, char ROM * Text1, char ROM * Text2,char ROM * Text3, char Btn1,char Btn2);

void OutTimeAnalog(unsigned int x, unsigned int y);

int MyInputInt(unsigned int X, unsigned int Y, int IntValue, char *CurPos, unsigned int *ExitKey);
void OutNoiseShift(unsigned int X, unsigned int Y, signed char aValue);
char InputNoiseShift(unsigned int X,unsigned int Y,signed char Value, unsigned int *ExitKey);

void GetStartAndLen(char ROM *Str, char *Start, char *End);
void InpOnOffValue(unsigned int x, unsigned int y,char *CurValue,char ROM* Str1,char ROM* Str2, unsigned int *ExitKey);
void InputScrl(struct ScrollerMngr *Scrl, KEY *ExitKey);

void Battery(unsigned int x,unsigned int y, float Val);
void DrawButton(int x,int y, char ROM *Text);
void DrawLongButton(int x, int y, char ROM *Text);
void DrawFrame(int x1,int y1,int x2,int y2);
char InputName(char ROM* Title, char MaxLength, char* String);
char* TruncStr(char *Str,unsigned int Width);
char* trim(char*s);
void OutMyString(int X, int Y, char * aStr, char Pos){

#endif
