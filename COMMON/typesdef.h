#ifndef _TypesDef_h
#define _TypesDef_h

typedef signed char s8;
typedef unsigned char u8;

typedef signed short s16;
typedef unsigned short u16;

typedef signed int s32;
typedef unsigned int u32;

typedef u32 _TDateTime;
typedef u32 _TDate;
typedef u32 _TTime;

typedef u16 TCRC;

#endif
